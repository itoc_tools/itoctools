<?php

namespace Database\Seeders;

use App\TrackEvent;
use Illuminate\Database\Seeder;

class TrackEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrackEvent::factory()->count(50)->create();
    }
}
