<?php

namespace Database\Seeders;

use App\SystemModule;
use Illuminate\Database\Seeder;

class SystemModuleSeeder extends Seeder 
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $module = SystemModule::create([
            'name' => 'Eventos'
        ]);
    }
}