<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('attributes')->insert([
            [
                'label' => 'id_event',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'application',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'time_created',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'time_received',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'assigned_user',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'assigned_group',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'category',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'description',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'state',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'priority',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'severity',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'sub_category',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'title',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'type',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'general'
            ],
            [
                'label' => 'cliente',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'custom_attribute'
            ],
            [
                'label' => 'Hostname',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'custom_attribute'
            ],
            [
                'label' => 'cResponsable',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'custom_attribute'
            ],
            [
                'label' => 'Solicitud',
                'type' => 'string',
                'operators' => json_encode(["equal", "not_equal", "contains", "not_contains"]),
                'category' => 'custom_attribute'
            ]
        ]);
    }
}
