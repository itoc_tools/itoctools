<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Eloquent ORM
        $user = User::create([
            'name' => 'Administrator',
            'email' => 'admin@triara.com',
            'password' => bcrypt('triara')
        ]);
        $user->assign('Administrator');

        $user2 = User::create([
            'name' => 'Roberto Grajeda',
            'email' => 'roberto.grajeda@triara.com',
            'password' => bcrypt('Triara123')
        ]);
        $user2->assign('Administrator');

        $user3 = User::create([
            'name' => 'Juan Betancourt',
            'email' => 'juan.betancourt@triara.com',
            'password' => bcrypt('ju4n.b3t4nc0ur')
        ]);
        $user3->assign('Administrator');

        $user4 = User::create([
            'name' => 'Rubi Mendez',
            'email' => 'rubi.mendez@triara.com',
            'password' => bcrypt('Rub1.m3nd3z')
        ]);
        $user4->assign('Administrator');

        $user5 = User::create([
            'name' => 'Miguel Perez',
            'email' => 'miguel.perez@triara.com',
            'password' => bcrypt('m1gu3l.pe3r3z')
        ]);
        $user5->assign('Administrator');

        $user6 = User::create([
            'name' => 'David Gomez',
            'email' => 'david.gomez@triara.com',
            'password' => bcrypt('d4v1d.g0m3z')
        ]);
        $user6->assign('Administrator');

        $user7 = User::create([
            'name' => 'Alberto Garcia',
            'email' => 'alberto.garcia@triara.com',
            'password' => bcrypt('triara')
        ]);
        $user7->assign('Administrator');

        $user8 = User::create([
            'name' => 'Rodrigo Mendoza',
            'email' => 'rodrigo.mendoza@triara.com',
            'password' => bcrypt('triara')
        ]);
        $user8->assign('Administrator');

        //factory(User::class,5)->create();
    }
}
