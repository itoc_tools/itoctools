<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder 
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call(RoleSeed::class);
        $this->call(UserSeeder::class);
        $this->call(DeviceTreeSeeder::class);
        $this->call(TreeSeeder::class);
        $this->call(AttributeSeeder::class);
        // $this->call(TrackEventSeeder::class);
        $this->call(SystemModuleSeeder::class);
    }
}
