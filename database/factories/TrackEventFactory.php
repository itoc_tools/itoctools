<?php

namespace Database\Factories;

use App\TrackEvent;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class TrackEventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TrackEvent::class;


    /**
     * Genera un ID de evento random, con el fin de poblar la tabla Track Event 
     */
    public function generateId()
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';

        $textId = sprintf(
            '%s-%s-%s-%s-%s',
            substr(str_shuffle($permitted_chars), 0, 8),
            substr(str_shuffle($permitted_chars), 0, 4),
            substr(str_shuffle($permitted_chars), 0, 4),
            substr(str_shuffle($permitted_chars), 0, 4),
            substr(str_shuffle($permitted_chars), 0, 12)
        );

        return $textId;
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->generateId(),
            'event_id' => $this->generateId(),
            'monitoring_tool_id' => rand(50, 59),
            'monitoring_tool_closed' => rand(0, 1),
            'closed' => rand(0, 1),
            'monitoring_tool_closed_at' => Carbon::now()->toDateTimeString(),
            'closed_at' => Carbon::now()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ];
    }
}
