<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('id_event');
            $table->longText('annotation_list')->nullable();
            $table->string('application')->nullable();
            $table->text('assigned_group')->nullable();
            $table->text('assigned_user')->nullable();
            $table->text('category')->nullable();
            $table->text('ci_resolution_info')->nullable();
            $table->text('control_transferred')->nullable();
            $table->json('custom_attribute_list')->nullable();
            $table->text('description')->nullable();
            $table->text('drilldown_url')->nullable();
            $table->text('duplicate_count')->nullable();
            $table->text('history_line_list_ref')->nullable();
            $table->text('instruction_available')->nullable();
            $table->text('log_only')->nullable();
            $table->text('originating_server')->nullable();
            $table->text('priority')->nullable();
            $table->text('received_as_notify')->nullable();
            $table->text('received_on_ci_downtime')->nullable();
            $table->text('relationships_included')->nullable();
            $table->text('self')->nullable();
            $table->text('sequence_number')->nullable();
            $table->text('severity')->nullable();
            $table->text('skip_duplicate_suppression')->nullable();
            $table->text('state')->nullable();
            $table->text('sub_category')->nullable();
            $table->longText('symptom_list')->nullable();
            $table->text('time_changed')->nullable();
            $table->text('time_changed_label')->nullable();
            $table->text('time_created')->nullable();
            $table->text('time_created_label')->nullable();
            $table->text('time_received')->nullable();
            $table->text('time_received_label')->nullable();
            $table->text('title')->nullable();
            $table->text('type')->nullable();
            $table->text('version')->nullable();
            $table->string('user_name')->nullable();
            $table->string('notified')->nullable();
            $table->string('notifiedPlainTemplate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
