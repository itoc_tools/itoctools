<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDatatimeToEventsFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events_filters', function (Blueprint $table) {
            $table->datetime('time_created_dt')->nullable();
            $table->datetime('time_received_dt')->nullable();
            $table->datetime('time_changed_dt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events_filters', function (Blueprint $table) {
            $table->dropColumn('time_created_dt');
            $table->dropColumn('time_received_dt');
            $table->dropColumn('time_changed_dt');
        });
    }
}
