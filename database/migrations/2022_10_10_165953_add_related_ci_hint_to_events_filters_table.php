<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelatedCiHintToEventsFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events_filters', function (Blueprint $table) {
            $table->string('RELATED_CI_HINT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events_filters', function (Blueprint $table) {
            $table->dropColumn('RELATED_CI_HINT')->nullable();
        });
    }
}
