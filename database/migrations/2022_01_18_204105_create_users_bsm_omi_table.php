<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersBsmOmiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_bsm', function (Blueprint $table) {
            $table->id();
            $table->integer('entity_id')->unsigned()->nullable()->index();
            $table->string('us_bsm')->nullable();
            $table->string('pass_bsm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_bms');
    }
}
