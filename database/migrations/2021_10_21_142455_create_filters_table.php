<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filters', function (Blueprint $table) {
            $table->id();
            $table->longText('name');
            $table->longText('description');
            $table->longText('sql');
            $table->longText('template');
            $table->longText('cuenta');
            $table->longText('tag')->nullable();
            $table->longText('to');
            $table->longText('cc')->nullable();
            $table->longText('subject');
            $table->longText('check_template');
            $table->boolean('state')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filters');
    }
}
