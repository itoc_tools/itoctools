<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntegrationsTable extends Migration 
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('integrations', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->string('description');
            $table->string('method', 10);
            $table->string('endpoint');
            $table->string('auth_type', 30);
            $table->text('auth_params');
            $table->text('headers');
            $table->string('body_type', 10);
            $table->string('body_params_type', 25)->nullable();
            $table->text('body_params');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('integrations');
    }
}