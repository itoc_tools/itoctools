<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('track_events', function (Blueprint $table) {
            $table->string('id',50)->nullable()->index();
            $table->string('event_id',50)->nullable()->index();
            $table->integer('monitoring_tool_id')->unsigned()->nullable()->index();
            $table->boolean('monitoring_tool_closed')->default(false);
            $table->boolean('closed')->default(false);
            $table->timestamp('monitoring_tool_closed_at')->nullable();
            $table->timestamp('closed_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('track_events');
    }
}
