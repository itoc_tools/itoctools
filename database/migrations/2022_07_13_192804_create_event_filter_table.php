<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventFilterTable extends Migration 
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('event_filter', function (Blueprint $table) {
            $table->string('id_event');
            $table->integer('filter_id')->unsigned();
            $table->string('notified')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('event_filter');
    }
}
