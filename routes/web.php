<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use App\Filter;
use App\Http\Controllers\reports\DynamicReportController;
use App\Http\Controllers\reports\ReportController;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Barryvdh\Debugbar\Facades\Debugbar;

Auth::routes();

// --- habilidades, roles, usuarios
Route::get('users-list', 'Admin\UserController@getAll');
Route::get('roles-list', 'Admin\RoleController@getAll');
Route::get('abilities-list', 'Admin\AbilityController@getAll');
//------------------------------------
Route::get('documents-list', 'DocumentController@getAll');
Route::get('scripts-list', 'ScriptController@getAll');
Route::get('devices-list', 'DeviceController@getAll');
Route::post('devices-save', 'DeviceController@save');
Route::post('devices-connect', 'DeviceController@connect');
Route::get('devices-tree-list', 'DeviceTreeController@getTree');

// --- Se crea un prefijo para eliminar el /admin/ de todas las url ---//
Route::middleware(['auth'])->prefix('admin')->group(function () {
    Route::resource('/users', 'Admin\UserController');
    Route::resource('/roles', 'Admin\RoleController');
    Route::resource('/abilities', 'Admin\AbilityController');
});

// --- Se agrupan para aplicar el middleware de auth ---//
Route::middleware(['auth'])->group(function () {
    Route::get('tools', 'ToolController@index')->name('tools');
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/user/profile', 'ProfileController@breadcrumb')->name('profile');
    Route::resource('documents', 'DocumentController');
    Route::resource('devices', 'DeviceController');
    Route::resource('scripts', 'ScriptController');
    /*
     * Trees
     */
    Route::resource('devices-tree', 'DeviceTreeController');
    // -- Events
    Route::resource('/events', 'Events\EventsController');
    Route::POST('/manageCustomAttributes{id}/{method}', 'Events\EventsController@manageCustomAttributes')->name('events.manageCustomAttributes');
    Route::POST('/createAnnotation{id}/{method}', 'Events\EventsController@createAnnotation')->name('events.createAnnotation');
    Route::POST('/masiveCustomAttributes', 'Events\EventsController@masiveCustomAttributes')->name('events.masiveCustomAttributes');
    Route::POST('/masiveAnnotation', 'Events\EventsController@masiveAnnotation')->name('events.masiveAnnotation');
    Route::POST('/massiveAssign', 'Events\EventsController@massiveAssign')->name('events.massiveAssign');
    Route::POST('/massiveState', 'Events\EventsController@massiveState')->name('events.massiveState');

    Route::post('events-create-correlation', 'Events\EventsController@createCorrelation')->name('events.create-correlation');

    // -- Filters
    Route::get('/reloadEventsAjax', 'Events\EventsController@reloadEventsAjax')->name('reloadEventsAjax');
    Route::get('/telmexSlim', 'Events\EventsControllerFilters@telmexSlim')->name('telmexSlim');
    Route::get('/telmexSlimReload', 'Events\EventsControllerFilters@telmexSlimReload')->name('telmexSlimReload');
    Route::get('/openException', 'Events\EventsControllerFilters@openException')->name('openException');
    Route::get('/openExceptionReload', 'Events\EventsControllerFilters@openExceptionReload')->name('openExceptionReload');
    Route::get('/alertsWithOutIMSDInProgress', 'Events\EventsControllerFilters@alertsWithOutIMSDInProgress')->name('alertsWithOutIMSDInProgress');
    Route::get('/alertsWithOutIMSDInProgressReload', 'Events\EventsControllerFilters@alertsWithOutIMSDInProgressReload')->name('alertsWithOutIMSDInProgressReload');
    Route::get('/infraHardware', 'Events\EventsControllerFilters@infraHardware')->name('infraHardware');
    Route::get('/infraHardwaresReload', 'Events\EventsControllerFilters@infraHardwaresReload')->name('infraHardwaresReload');
    Route::get('/infraRed', 'Events\EventsControllerFilters@infraRed')->name('infraRed');
    Route::get('/infraRedReload', 'Events\EventsControllerFilters@infraRedReload')->name('infraRedReload');
    Route::get('/infraSeguridad', 'Events\EventsControllerFilters@infraSeguridad')->name('infraSeguridad');
    Route::get('/infraSeguridadReload', 'Events\EventsControllerFilters@infraSeguridadReload')->name('infraSeguridadReload');
    Route::get('/zenossQro', 'Events\EventsControllerFilters@zenossQro')->name('zenossQro');
    Route::get('/zenossQroReload', 'Events\EventsControllerFilters@zenossQroReload')->name('zenossQroReload');
    Route::get('/zenossMtyItoc', 'Events\EventsControllerFilters@zenossMtyItoc')->name('zenossMtyItoc');
    Route::get('/zenossMtyItocReload', 'Events\EventsControllerFilters@zenossMtyItocReload')->name('zenossMtyItocReload');
    Route::get('/zenossSRE', 'Events\EventsControllerFilters@zenossSRE')->name('zenossSRE');
    Route::get('/zenossSREReload', 'Events\EventsControllerFilters@zenossSREReload')->name('zenossSREReload');
    Route::get('/zenossGBMV', 'Events\EventsControllerFilters@zenossGBMV')->name('zenossGBMV');
    Route::get('/zenossGBMVReload', 'Events\EventsControllerFilters@zenossGBMVReload')->name('zenossGBMVReload');
    Route::get('/alertsCriticasChat', 'Events\EventsControllerFilters@alertsCriticasChat')->name('alertsCriticasChat');
    Route::get('/alertsCriticasChatReload', 'Events\EventsControllerFilters@alertsCriticasChatReload')->name('alertsCriticasChatReload');
    Route::get('/exchangeCriticas', 'Events\EventsControllerFilters@exchangeCriticas')->name('exchangeCriticas');
    Route::get('/exchangeCriticasReload', 'Events\EventsControllerFilters@exchangeCriticasReload')->name('exchangeCriticasReload');
    Route::get('/graphicsReload', 'Events\EventsControllerFilters@graphicsReload')->name('graphicsReload');
    Route::get('/ingHerramientas', 'Events\EventsControllerFilters@ingHerramientas')->name('ingHerramientas');
    Route::get('/ingHerramientasReload', 'Events\EventsControllerFilters@ingHerramientasReload')->name('ingHerramientasReload');
    Route::get('/ITOC_VCloud', 'Events\EventsControllerFilters@ITOC_VCloud')->name('ITOC_VCloud');
    Route::get('/ITOC_VCloudReload', 'Events\EventsControllerFilters@ITOC_VCloudReload')->name('ITOC_VCloudReload');
    Route::get('/eventPlataformasClientes', 'Events\EventsControllerFilters@eventPlataformasClientes')->name('eventPlataformasClientes');
    Route::get('/eventPlataformasClientesReload', 'Events\EventsControllerFilters@eventPlataformasClientesReload')->name('eventPlataformasClientesReload');
    Route::get('/eventPingReinicios', 'Events\EventsControllerFilters@eventPingReinicios')->name('eventPingReinicios');
    Route::get('/eventPingReiniciosReload', 'Events\EventsControllerFilters@eventPingReiniciosReload')->name('eventPingReiniciosReload');
    Route::get('/eventExcepcionSinAtender', 'Events\EventsControllerFilters@eventExcepcionSinAtender')->name('eventExcepcionSinAtender');
    Route::get('/eventExcepcionSinAtenderReload', 'Events\EventsControllerFilters@eventExcepcionSinAtenderReload')->name('eventExcepcionSinAtenderReload');
    Route::get('/eventSitescope', 'Events\EventsControllerFilters@eventSitescope')->name('eventSitescope');
    Route::get('/eventSitescopeReload', 'Events\EventsControllerFilters@eventSitescopeReload')->name('eventSitescopeReload');
    Route::get('/eventNagiosURLsZabbix', 'Events\EventsControllerFilters@eventNagiosURLsZabbix')->name('eventNagiosURLsZabbix');
    Route::get('/eventNagiosURLsZabbixReload', 'Events\EventsControllerFilters@eventNagiosURLsZabbixReload')->name('eventNagiosURLsZabbixReload');
    Route::get('/eventAWSNewRelic', 'Events\EventsControllerFilters@eventAWSNewRelic')->name('eventAWSNewRelic');
    Route::get('/eventAWSNewRelicReload', 'Events\EventsControllerFilters@eventAWSNewRelicReload')->name('eventAWSNewRelicReload');
    Route::get('/eventEventosdeRFC', 'Events\EventsControllerFilters@eventEventosdeRFC')->name('eventEventosdeRFC');
    Route::get('/eventEventosdeRFCReload', 'Events\EventsControllerFilters@eventEventosdeRFCReload')->name('eventEventosdeRFCReload');
    Route::get('/eventZenossCloudVIP', 'Events\EventsControllerFilters@eventZenossCloudVIP')->name('eventZenossCloudVIP');
    Route::get('/eventZenossCloudVIPReload', 'Events\EventsControllerFilters@eventZenossCloudVIPReload')->name('eventZenossCloudVIPReload');
    Route::get('/eventEventosResueltos', 'Events\EventsControllerFilters@eventEventosResueltos')->name('eventEventosResueltos');
    Route::get('/eventEventosResueltosReload', 'Events\EventsControllerFilters@eventEventosResueltosReload')->name('eventEventosResueltosReload');
    Route::get('/eventClientesSAP', 'Events\EventsControllerFilters@eventClientesSAP')->name('eventClientesSAP');
    Route::get('/eventClientesSAPReload', 'Events\EventsControllerFilters@eventClientesSAPReload')->name('eventClientesSAPReload');
    Route::get('/eventAlertasAvaya', 'Events\EventsControllerFilters@eventAlertasAvaya')->name('eventAlertasAvaya');
    Route::get('/eventAlertasAvayaReload', 'Events\EventsControllerFilters@eventAlertasAvayaReload')->name('eventAlertasAvayaReload');
    Route::get('/eventIntegracionesITOCTOOLs', 'Events\EventsControllerFilters@eventIntegracionesITOCTOOLs')->name('eventIntegracionesITOCTOOLs');
    Route::get('/eventIntegracionesITOCTOOLsReload', 'Events\EventsControllerFilters@eventIntegracionesITOCTOOLsReload')->name('eventIntegracionesITOCTOOLsReload');
    Route::get('/eventAlertasNA', 'Events\EventsControllerFilters@eventAlertasNA')->name('eventAlertasNA');
    Route::get('/eventAlertasNAReload', 'Events\EventsControllerFilters@eventAlertasNAReload')->name('eventAlertasNAReload');
    Route::get('/eventEventosDummyPrueba', 'Events\EventsControllerFilters@eventEventosDummyPrueba')->name('eventEventosDummyPrueba');
    Route::get('/eventEventosDummyPruebaReload', 'Events\EventsControllerFilters@eventEventosDummyPruebaReload')->name('eventEventosDummyPruebaReload');
    Route::get('/eventPoleos', 'Events\EventsControllerFilters@eventPoleos')->name('eventPoleos');
    Route::get('/eventPoleosReload', 'Events\EventsControllerFilters@eventPoleosReload')->name('eventPoleosReload');

    /* Custom filters */
    Route::get('/filters', [\App\Http\Controllers\FilterController::class, 'index'])->name('filter.index');
    Route::get('/filters/create', [\App\Http\Controllers\FilterController::class, 'create'])->name('filter.create');
    Route::post('/filters', [\App\Http\Controllers\FilterController::class, 'store'])->name('filter.store');
    Route::get('/filters/{id}', [\App\Http\Controllers\FilterController::class, 'show']);
    Route::get('/filters/{id}/edit', [\App\Http\Controllers\FilterController::class, 'edit'])->name('filter.edit');
    Route::put('/filters/{id}', [\App\Http\Controllers\FilterController::class, 'update'])->name('filter.update');
    Route::get('/filters/{id}', [\App\Http\Controllers\FilterController::class, 'destroy'])->name('filter.destroy');
    Route::get('/replicate/{id}', [\App\Http\Controllers\FilterController::class, 'replicateFilter'])->name('filter.replicate');
    Route::put('/filter-switch', [\App\Http\Controllers\FilterController::class, 'filterSwitch'])->name('filter.switch');

    /* custom attributes */
    Route::get('/attribute', [\App\Http\Controllers\AttributeController::class, 'attribute'])->name('attribute.all');
    Route::get('/attributes', [\App\Http\Controllers\AttributeController::class, 'index'])->name('attribute.index');
    Route::get('attribute/create', [\App\Http\Controllers\AttributeController::class, 'create'])->name('attribute.create');
    Route::post('attribute', [\App\Http\Controllers\AttributeController::class, 'store'])->name('attribute.store');
    Route::get('attribute/{id}', [\App\Http\Controllers\AttributeController::class, 'show'])->name('attribute.show');
    Route::get('attribute/{id}/edit', [\App\Http\Controllers\AttributeController::class, 'edit'])->name('attribute.edit');
    Route::put('attribute/{id}', [\App\Http\Controllers\AttributeController::class, 'update'])->name('attribute.update');
    Route::delete('attribute/{id}', [\App\Http\Controllers\AttributeController::class, 'destroy'])->name('attribute.destroy');


    /* Contactos */
    Route::get('/contacts', [\App\Http\Controllers\ContactController::class, 'index'])->name('contact.index');
    Route::get('/contacts/create', [\App\Http\Controllers\ContactController::class, 'create'])->name('contact.create');
    Route::post('/contacts', [\App\Http\Controllers\ContactController::class, 'store'])->name('contact.create');
    Route::get('/contact/{id}', [\App\Http\Controllers\ContactController::class, 'show'])->name('contact.show');
    Route::get('/contact/{id}/edit', [\App\Http\Controllers\ContactController::class, 'edit'])->name('contact.edit');
    Route::put('/contacts', [\App\Http\Controllers\ContactController::class, 'update'])->name('contact.update');
    Route::get('/contacts/{id}', [\App\Http\Controllers\ContactController::class, 'destroy'])->name('contact.destroy');

    /* Grupos de correo */
    Route::get('/groups', [\App\Http\Controllers\GroupController::class, 'index'])->name('group.index');
    Route::get('/groups/create', [\App\Http\Controllers\GroupController::class, 'create'])->name('group.create');
    Route::post('/groups', [\App\Http\Controllers\GroupController::class, 'store'])->name('group.create');
    Route::get('/groups/{id}', [\App\Http\Controllers\GroupController::class, 'show'])->name('group.show');
    Route::get('/groups/{id}/edit', [\App\Http\Controllers\GroupController::class, 'edit'])->name('group.edit');
    Route::put('/groups', [\App\Http\Controllers\GroupController::class, 'update'])->name('group.update');
    Route::get('/groups/{id}', [\App\Http\Controllers\GroupController::class, 'destroy'])->name('group.destroy');

    /* Integraciones */
    Route::resource('integrations', 'IntegrationController');
    Route::post('/integrations/{integration}/check-connection', 'IntegrationController@connect')->name('integrations.connect');

    /* User Preferences */
    Route::post('/preferences/save-events-table-state', 'UserPreferenceController@saveEventsTableState')->name('preferences.saveEventsTableState');
    Route::get('/preferences/get-events-table-preferences', 'UserPreferenceController@getEventsTablePreferences')->name('preferences.getEventsTablePreferences');

    /* AWS */
    Route::get('/aws', [\App\Http\Controllers\AwsController::class, 'index'])->name('aws.index');
    Route::get('/aws/accounts/{accountId}', [\App\Http\Controllers\AwsController::class, 'showAccount'])->name('aws.showAccounts');
    Route::get('/aws/services/{accountId}/{clientId}', [\App\Http\Controllers\AwsController::class, 'showServices'])->name('aws.showServices');
    Route::get('/aws/instances/{type}/{accountId}/{clientId}', [\App\Http\Controllers\AwsController::class, 'showInstances'])->name('aws.showInstances');

    /* API Request */
    Route::get('integration/create', [\App\Http\Controllers\IntegrationController::class, 'create']);
    Route::get('integration/{id}', [\App\Http\Controllers\IntegrationController::class, 'show']);

    // Log Activity Users
    Route::get('logActivity', function () {
        $logs = \App\Helpers\LogActivity::logActivityLists();
        return view('admin.log.index')->with([
            'title' => 'Registro de actividades',
            'breadcrumb' => [
                [
                    'title' => 'Registro de actividades'
                ]
            ],
            'logs' => $logs
        ]);
    });
});

// --- Cambiar el lenguaje ---//
Route::get('lang/{locale}', 'LangController@index');
Route::post('document-image-upload', 'DocumentController@imageUpload')->name('image.upload');

// Modificacion de la imagen de perfil del usuario
Route::post('/modificar-img-perfil', 'ImageProfileController@test');

// Reports section
Route::get('/show_reports', [ReportController::class, 'homeReports']);

// New Relic Request WebHook
Route::post('webhook', [WebhookController::class, 'index']);

// Exposed Web Services
Route::get('/service/{id}', [\App\Http\Controllers\ExposedServices\ExternalController::class, 'index'])->name('service');
Route::post('/addcustomattribute', 'ExposedServices\ExternalController@addcustomattribute');
Route::post('/addannotation', 'ExposedServices\ExternalController@addannotation');

// Testing DB connectio directly to SQL Server 
Route::get('/testsql', 'TestSqlServerConnController@test');

Route::get('test-application', 'TesterController');

// forza a que todas las urls en la vista se establezcan con https
//URL::forceScheme('https');
