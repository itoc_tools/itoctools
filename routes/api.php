<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::get('aws', function() {
	return 'Hola';
});

Route::get('aws/{param1}', function($param1, Request $request) {
	return App\Http\Controllers\AwsController::rawRequest(['aws', $param1], 'GET');
});

Route::get('aws/{param1}/{param2}', function($param1, $param2) {
	return App\Http\Controllers\AwsController::rawRequest(['aws', $param1, $param2], 'GET');
});

Route::get('aws/{param1}/{param2}/{param3}', function($param1, $param2, $param3) {
	return App\Http\Controllers\AwsController::rawRequest(['aws', $param1, $param2, $param3], 'GET');
});

Route::post('aws/{param1}', function($param1, Request $request) {
	return App\Http\Controllers\AwsController::rawRequest(['aws', $param1], $request->getContent(), 'POST');
});

Route::post('aws/{param1}/{param2}', function($param1, $param2, Request $request) {
	return App\Http\Controllers\AwsController::rawRequest(['aws', $param1, $param2], $request->getContent(), 'POST');
});

Route::post('aws/{param1}/{param2}/{param3}', function($param1, $param2, $param3) {
	return App\Http\Controllers\AwsController::rawRequest(['aws', $param1, $param2, $param3], $request->getContent(), 'POST');
});

Route::put('aws/{param1}', function($param1, Request $request) {
	return App\Http\Controllers\AwsController::rawRequest(['aws', $param1], $request->getContent(), 'PUT');
});

Route::put('aws/{param1}/{param2}', function($param1, $param2, Request $request) {
	return App\Http\Controllers\AwsController::rawRequest(['aws', $param1, $param2], $request->getContent(), 'PUT');
});

Route::put('aws/{param1}/{param2}/{param3}', function($param1, $param2, $param3) {
	return App\Http\Controllers\AwsController::rawRequest(['aws', $param1, $param2, $param3], $request->getContent(), 'PUT');
});