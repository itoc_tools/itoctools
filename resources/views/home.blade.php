@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <img src="{{ URL::asset('theme/assets/images/logo-icon.png') }}" width="25%">
                <img src="{{ URL::asset('theme/assets/images/logo-text.png') }}" width="75%">
                <div></div>
                <div>
                    <i class="icon-ghost"></i>
                    itoc [Desarrollo Web]
                    <i class="icon-ghost"></i>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection