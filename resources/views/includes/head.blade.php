<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config('app.name', 'ITOC') }}</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Automatización ITOC">

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('theme/assets/images/logo-icon.png') }}">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/DataTables/datatables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/DataTables/Buttons-2.2.3/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/DataTables/Select-1.4.0/select.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/DataTables/ColReorder-1.5.6/colReorder.dataTables.min.css') }}">

    <!-- Summer Note -->
    <link rel="stylesheet" href="{{ asset('theme/assets/node_modules/summernote/dist/summernote-bs4.css') }}">

    <!-- Toast -->
    <link rel="stylesheet" href="{{ asset('theme/assets/node_modules/toast-master/css/jquery.toast.css') }}">

    <!-- Nestable -->
    <link rel="stylesheet" href="{{ asset('theme/assets/node_modules/nestable/nestable.css') }}">

    <!-- Select2 custom -->
    <link rel="stylesheet" href="{{ asset('theme/assets/node_modules/select2/dist/css/select2.min.css') }}">

    <!-- Ribbons -->
    <link rel="stylesheet" href="{{ asset('theme/ecommerce/dist/css/pages/ribbon-page.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('theme/ecommerce/dist/css/style.min.css') }}">

    <!-- xeditable css -->
    <link rel="stylesheet" href="{{ asset('theme/assets/node_modules/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css') }}" />

    <!-- Switch -->
    <link rel="stylesheet" href="{{ asset('theme/assets/node_modules/bootstrap4-toggle/css/bootstrap4-toggle.min.css') }}">

    <!-- multi-select css -->
    <link rel="stylesheet" href="{{ asset('theme/assets/node_modules/multiselect/css/multi-select.css') }}">

    <!-- Wizard css -->
    <link rel="stylesheet" href="{{ asset('theme/assets/node_modules/wizard/steps.css') }}">

    <!-- Tags Input -->
    <link rel="stylesheet" href="{{ asset('theme/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">

    <!-- jQuery querybuilder -->
    @for ($i = 1; $i <= 1000; $i++)
        @if (Request::is('filters/'.$i.'/edit'))
            <link rel="stylesheet" href="{{ asset('theme/assets/node_modules/jquery-query-builder/dist/css/query-builder.default.min.css') }}">

            <link rel="stylesheet" href="{{ asset('theme/ecommerce/dist/js/bootstrap/3.3.7/css/bootstrap.min.css') }}">
            <link rel="stylesheet" href="{{ asset('theme/ecommerce/dist/css/pages/select2.min.css') }}">
            <link rel="stylesheet" href="{{ asset('theme/ecommerce/dist/css/pages/multi-select.css') }}">
            <link rel="stylesheet" href="{{ asset('theme/ecommerce/dist/css/pages/bootstrap-select.min.css') }}">
        @endif
    @endfor

    @if (Request::is('filters/create'))
        <link rel="stylesheet" href="{{ asset('theme/assets/node_modules/jquery-query-builder/dist/css/query-builder.default.min.css') }}">

        <link rel="stylesheet" href="{{ asset('theme/ecommerce/dist/js/bootstrap/3.3.7/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('theme/ecommerce/dist/css/pages/select2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('theme/ecommerce/dist/css/pages/multi-select.css') }}">
        <link rel="stylesheet" href="{{ asset('theme/ecommerce/dist/css/pages/bootstrap-select.min.css') }}">
    @endif

    <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/profile-photo-croppie.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- <link href="http://itoc-tools.triara.mexico/itoc/resources/libraries//jstree/themes/default/style.min.css" type="text/css" rel="stylesheet"> -->
    <link href="{{ asset('theme/jstree/style.min.css') }}" type="text/css" rel="stylesheet">

    <!-- ContextMenu 2.x -->
    <link rel="stylesheet" type="text/css" href="https://rawgithub.com/swisnl/jQuery-contextMenu/master/dist/jquery.contextMenu.css">

    <!-- Font Awesome 6 -->
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/font-awesome-6/css/font-awesome-6.min.css') }}">

    <!-- waitMe -->
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/waitMe/waitMe.min.css') }}">


    <style type="text/css">
        div.dt-buttons {
            float: right;
        }

        blockquote.blockquote {
            color: #666;
            background: #f9f9f9;
            font-size: 1.1em;
            border-left: 5px solid #ccc;
            margin: 1.5em 15px;
            padding: 0.5em 15px;
            quotes: "\201C""\201D""\2018""\2019";
        }

        blockquote p {
            display: inline;
        }

        .panel-fullscreen {
            display: block;
            z-index: 9999;
            position: fixed !important;
            width: 100%;
            height: 100%;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            overflow: auto;
        }
    </style>

    <style type="text/css">
        /* Dropdown */
        .dropdown-item.active, .dropdown-item:active {
            background-color: #343a40;
        }

        /* Select2 */
        .select2-container--default .select2-results__option--highlighted[aria-selected] {
            background-color: #343a40;
        }

        .input-group > .select2-container--default {
            width: auto;
            flex: 1 1 auto;
        }

        /* MultiSelect */
        .ms-container {
            width: 100%;
        }

        .ms-container .ms-selectable li.ms-hover, .ms-container .ms-selection li.ms-hover {
            background-color: #343a40;
        }

        /* ContextMenu */
        .contextmenu-item-custom-style {
            font-size: 80%;
            font-weight: 400;
            color: #212529 !important;
        }

        .contextmenu-item-custom-style i {
            color: #212529 !important;
        }

        .contextmenu-item-custom-style:hover {
            color: #ffffff !important;
            background-color: #343a40 !important;
        }

        .contextmenu-item-custom-style:hover i {
            color: #ffffff !important;
        }

        /* Tables */
        .table .thead-default th {
            color: #343a40;
            border-top: none;
            border-bottom: none;
            background-color: #e8e8e8;
        }

        /* Scrollable Tables */
        .table-scrollable {
            overflow: auto;
            max-width: 100%;
            max-height: 500px;
            font-size: 80% !important;
            white-space: nowrap !important;
        }

        .table-scrollable table {
            cursor: pointer;
            margin: 0 !important;
        }

        .table-scrollable table thead tr th {
            top: 0;
            position: sticky !important;
        }

        .table-scrollable th {
            border-bottom: none;
            border-right: 1px solid #dee2e6;
        }

        /* DataTables */
        /* Buttons */
        .dt-buttons {
            padding-top: 0px;
            margin-bottom: 0px;
        }

        .dataTables_wrapper .dataTables_filter input {
            border: none;
            margin-left: 10px;
        }

        table.dataTable thead > tr > th.sorting_asc::before, table.dataTable thead > tr > th.sorting_desc::after, table.dataTable thead > tr > td.sorting_asc::before, table.dataTable thead > tr > td.sorting_desc::after {
            opacity: 1;
        }

        table.dataTable tbody tr.selected > * {
            color: #212529;
            box-shadow: inset 0 0 0 9999px #cfcfcf;
        }

        table.dataTable.no-footer {
            border-bottom: none;
        }
    </style>
</head>