<script>
    //-- Wizard Hashtag -->
    type = "text/javascript" >
        $(document).ready(function() {
            $("textarea").hashtags();
        })
    //-- Wizard Script create-->
    //Custom design form example
    $(".tab-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onFinished: function(event, currentIndex) {
            console.log("OKAS")
        }
    });

    var form = $(".validation-wizard").show();

    $(".validation-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
        },
        onFinishing: function(event, currentIndex) {
            return form.validate().settings.ignore = ":disabled", form.valid()
        },
        onFinished: function(event, currentIndex) {
            /* Step 1 */
            let id = $('#filter_id').val();
            let name = $('#name').val();
            let description = $('#description').val();
            var res = $('#builder').queryBuilder('getSQL', $(this).data('stmt'), false);
            var sql = res.sql + (res.params ? '\n\n' + JSON.stringify(res.params, undefined, 2) : '');
            /* Step 2 */
            var template = new Map();

            $('#dynamic_field tr').each(function() {
                var values = [];
                $(this).find(":input").each(function() {
                    values.push(this.value);
                });
                template.set(values[0], values[1]);
            });

            const result = Array.from(template).map(([name, value]) => ({
                name,
                value
            }))

            /* Step 3 */
            let subject = $('#subject').val();
            let cuenta = $('#account').val();
            let tag = $('#tag').val();
            let to = $('#to').val();
            let cc = $('#cc').val();
            let check_template = $('#check_template').is(":checked");
            //let affair = $('#affair').val();
            let _token = $('meta[name="csrf-token"]').attr('content');

            /** Envio de datos al controlador FilterController **/
            if (window.location.href.includes("filters/create")) {
                $.ajax({
                    type: 'POST',
                    url: "{{ URL::route('filter.store') }}",
                    data: {
                        _token: _token,
                        name: name,
                        description: description,
                        sql: sql,
                        template: JSON.stringify(result),
                        cuenta: cuenta,
                        tag: tag,
                        to: to,
                        cc: cc,
                        subject: subject,
                        check_template: check_template,
                    },
                    success: function() {
                        console.log('Success');
                        window.location.replace("/itoctools/public/filters")
                    }
                })
            } else {
                $.ajax({
                    type: 'PUT',
                    url: "/itoctools/public/filters/" + id,
                    data: {
                        _token: _token,
                        name: name,
                        description: description,
                        sql: sql,
                        template: JSON.stringify(result),
                        cuenta: cuenta,
                        tag: tag,
                        to: to,
                        cc: cc,
                        subject: subject,
                        check_template: check_template,
                    },
                    success: function() {
                        console.log('Success');
                        window.location.replace("/itoctools/public/filters")
                    }
                })
            }
        }
    }), $(".validation-wizard").validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger",
        successClass: "text-success",
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element)
        },
        rules: {
            email: {
                email: !0
            }
        }
    })

    /*-- Query Builder --*/

    /* Get attributes */
    let attr = [];

    $(function() {
        $.ajax({
            type: "GET",
            url: "{{ URL::route('attribute.all') }}",
            success: function(data) {
                let obj = [];
                $.each(data, function(key, value) {
                    let json = {
                        id: value.label,
                        label: value.label,
                        type: value.type,
                        operators: JSON.parse(value.operators)
                    }
                    obj.push(json)
                })
                setAttribute(obj)
            }
        })

        function setAttribute(obj) {
            $('#builder').queryBuilder({
                plugins: ['sortable', 'bt-tooltip-errors'],

                operators: [
                    'equal', 'not_equal', 'contains', 'not_contains',
                    {
                        type: 'contains_any',
                        nb_inputs: 2,
                        multiple: true,
                        apply_to: ['string']
                    },
                ],

                filters: obj
            });

            // Set values
            if (window.location.href.includes("filters/create")) {
                return
            } else {
                $('#builder').queryBuilder('setRulesFromSQL', $('#sql').val());
            }
        }
    });
</script>
