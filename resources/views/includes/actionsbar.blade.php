                <div class="row page-titles">
                    <div class="col-md-5 col-sm-5 align-self-center">
                        <h4 class="text-themecolor">{{ $title}}</h4>
                    </div>
                    <div class="col-md-7 col-sm-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                                @foreach ($breadcrumb as $breadcrumb)
                                @if ($loop->last)
                                <li class="breadcrumb-item active">{{ $breadcrumb['title'] }}</li>
                                @else
                                <li class="breadcrumb-item"><a href="{{ $breadcrumb['url'] }}">{{ $breadcrumb['title'] }}</a></li>
                                @endif
                                @endforeach
                            </ol>
                            <!--
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                            -->
                            <div class="d-flex justify-content-end align-items-center">
                                <!--
                                <button
                                    type="button"
                                    class="btn btn-cyan btn-circle btn-lg d-none d-lg-block m-l-15"
                                >
                                    <i class="fa fa-plus"></i>
                                </button>
                                -->
                            </div>
                        </div>
                    </div>
                </div>