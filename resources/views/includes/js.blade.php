<script src="{{ asset('theme/assets/node_modules/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('theme/assets/node_modules/jqueryui/jquery-ui.min.js') }}"></script>

<!-- This is DataTables -->
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/Buttons-2.2.3/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/JSZip-2.5.0/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/Buttons-2.2.3/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/Buttons-2.2.3/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/Select-1.4.0/dataTables.select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/ColReorder-1.5.6/dataTables.colReorder.min.js') }}"></script>

<!-- Vue app -->
@if (Request::is('aws/*'))
    <script src="{{ URL::asset('js/app.js') }}"></script>
@endif

<!-- ColResize -->
<script src="{{ asset('theme/assets/node_modules/colResizable/colResizable.js') }}"></script>

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('theme/assets/node_modules/popper/popper.min.js') }}"></script>
<script src="{{ asset('theme/assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<!-- Slimscrollbar scrollbar JavaScript -->
<script src="{{ asset('theme/ecommerce/dist/js/perfect-scrollbar.jquery.min.js') }}"></script>

<!-- Wave Effects -->
<script src="{{ asset('theme/ecommerce/dist/js/waves.js') }}"></script>

<!-- Menu sidebar -->
<script src="{{ asset('theme/ecommerce/dist/js/sidebarmenu.js') }}"></script>

<!-- Sticky kit -->
<script src="{{ asset('theme/assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

<!-- Sparkline -->
<script src="{{ asset('theme/assets/node_modules/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('theme/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Summer Note -->
<script src="{{ asset('theme/assets/node_modules/summernote/dist/summernote-bs4.min.js') }}"></script>

<!-- Toast -->
<script src="{{ asset('theme/assets/node_modules/toast-master/js/jquery.toast.js') }}"></script>

<!-- Switch -->
<script src="{{ asset('theme/assets/node_modules/bootstrap4-toggle/js/bootstrap4-toggle.min.js') }}"></script>

@for ($i = 1; $i <= 1000; $i++)
    @if (Request::is('filters/'.$i.'/edit'))
        <script src="{{ asset('theme/ecommerce/dist/js/bootstrap/3.3.7/js/bootstrap.min.js') }}"></script>

        <!-- jQuery querybuilder -->
        <script src="{{ asset('theme/assets/node_modules/jquery-query-builder/dist/js/query-builder.standalone.min.js') }}"></script>
        <script src="{{ asset('theme/assets/node_modules/sql-parser-mistic/browser/sql-parser.js') }}"></script>
        <script src="{{ asset('theme/assets/node_modules/interactjs/dist/interact.js') }}"></script>

        <!-- Wizard -->
        <script src="{{ asset('theme/assets/node_modules/wizard/jquery.steps.min.js') }}"></script>
        <script src="{{ asset('theme/assets/node_modules/wizard/jquery.validate.min.js') }}"></script>

        <!-- Tags Input -->
        <script src="{{ asset('theme/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>

        <!-- Query template hashtag -->
        <script src="{{ asset('theme/assets/node_modules/hashtag/js/autosize.min.js') }}"></script>
        <script src="{{ asset('theme/assets/node_modules/hashtag/js/jquery.hashtags.js') }}"></script>
    @endif
@endfor

@if (Request::is('filters/create'))
    <script src="{{ asset('theme/ecommerce/dist/js/bootstrap/3.3.7/js/bootstrap.min.js') }}"></script>

    <!-- jQuery querybuilder -->
    <script src="{{ asset('theme/assets/node_modules/jquery-query-builder/dist/js/query-builder.standalone.min.js') }}"></script>
    <script src="{{ asset('theme/assets/node_modules/sql-parser-mistic/browser/sql-parser.js') }}"></script>
    <script src="{{ asset('theme/assets/node_modules/interactjs/dist/interact.js') }}"></script>

    <!-- Wizard -->
    <script src="{{ asset('theme/assets/node_modules/wizard/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('theme/assets/node_modules/wizard/jquery.validate.min.js') }}"></script>

    <!-- Tags Input -->
    <script src="{{ asset('theme/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>

    <!-- Query template hashtag -->
    <script src="{{ asset('theme/assets/node_modules/hashtag/js/autosize.min.js') }}"></script>
    <script src="{{ asset('theme/assets/node_modules/hashtag/js/jquery.hashtags.js') }}"></script>
@endif

<!-- Custom JavaScript -->
<script src="{{ asset('theme/ecommerce/dist/js/custom.js') }}"></script>

<!-- Select2 -->
<script src="{{ asset('theme/assets/node_modules/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('theme/assets/node_modules/select2/dist/js/i18n/es.js') }}"></script>

<!-- MultiSelect -->
<script src="{{ asset('theme/ecommerce/dist/js/jquery.multi-select.js') }}"></script>

<!-- Bootstrap Select -->
<script src="{{ asset('theme/ecommerce/dist/js/bootstrap-select.min.js') }}"></script>

<script src="{{ asset('theme/jstree/jstree.min.js') }}"></script>

<!-- Font Awesome -->
<script src="https://kit.fontawesome.com/e9f94371e8.js" crossorigin="anonymous"></script>

<!-- Croppie -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.min.js" integrity="sha512-Gs+PsXsGkmr+15rqObPJbenQ2wB3qYvTHuJO6YJzPe/dTLvhy0fmae2BcnaozxDo5iaF8emzmCZWbQ1XXiX2Ig==" crossorigin="anonymous"></script>

@if (Request::is('user/profile'))
    <script src="{{ asset('js/croppie-photo-profile.js') }}"></script>
@endif

@if (Request::is('show_reports'))
    <script src="{{ asset('js/reports_module.js') }}"></script>
@endif

<!-- ContextMenu 2.x -->
<script src="https://rawgithub.com/swisnl/jQuery-contextMenu/master/dist/jquery.contextMenu.js"></script>

<!-- jQuery Validate -->
<script type="text/javascript" src="{{ asset('custom/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/jquery-validation/localization/messages_es.min.js') }}"></script>

<!-- Moment.js -->
<script type="text/javascript" src="{{ asset('custom/plugins/Moment.js/moment.min.js') }}"></script>

<!-- waitMe -->
<script type="text/javascript" src="{{ asset('custom/plugins/waitMe/waitMe.min.js') }}"></script>

<script>
    @if (Session::has('notification'))
        // Toast messages
        $.toast({
            heading: "{{ Session::get('notification.header') }}",
            text: "{{ Session::get('notification.message') }}",
            textAlign: 'left',
            position: 'top-right',
            loader: true,
            loaderBg: '#ff6849',
            icon: "{{ Session::get('notification.type', 'info') }}",
            showHideTransition: 'fade',
            allowToastClose: true, // fade, slide or plain
            hideAfter: 1800,
            stack: 5
        });
    @endif

    $(function() {
        // For select 2
        $('.select2').select2({
            width: '100%',
            language: 'es'
        });

        $('.selectpicker').selectpicker();

        $('img').addClass('img-responsive');
    });
</script>

<script>
    function onOffFilter(id_filter) {
        let _token = $('meta[name="csrf-token"]').attr('content');
        let checked = document.getElementById('toggle-event' + id_filter).checked;

        $.ajax({
            type: 'PUT',
            url: "{{ URL::route('filter.switch') }}",
            data: {
                _token: _token,
                id_filter: id_filter,
                state: checked.toString()
            },
            success: function() {
                console.log('Success');
            }
        });
    }
</script>
