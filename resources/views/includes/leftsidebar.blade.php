<aside class="left-sidebar" style="padding-top: 0 !important;">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User Profile-->
        <div class="user-profile" style="margin-top: 0 !important;">
            <div class="user-pro-body">
                <div>
                    <img src="{{ URL::asset('theme/assets/images/logo-icon.png') }}" alt="homepage"
                         style="width: 50px; display: block; margin: 10px auto 10px;"/>
                </div>
                <div>
                    <a href="javascript:void(0)" class="u-dropdown link">{{ Auth::user()->name }}</a>
                </div>
            <!-- <div class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle u-dropdown link hide-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}<span class="caret"></span></a>
                    <div class="dropdown-menu animated fadeIn"> -->
                <!-- text-->
            <!-- <a href="{{ URL::to('/user/profile') }}" class="dropdown-item"><i class="ti-user"></i> Mi perfil</a> -->
                <!-- text-->
                <!-- <a href="javascript:void(0)" class="dropdown-item"><i class="ti-email"></i> Inbox</a> -->
                <!-- text-->
                <!-- <div class="dropdown-divider"></div> -->
                <!-- text-->
            <!--  @if (!Auth::user()->can('operador') && !Auth::user()->can('lead_operator'))
                <a href="javascript:void(0)" class="dropdown-item"><i class="ti-settings"></i> Configuración</a>
@endif -->
                <!-- text-->
                <!-- <div class="dropdown-divider"></div> -->
                <!-- text-->
                <!--
                        <a href="pages-login.html" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                        -->
            <!-- <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i> Salir</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                </form> -->
                <!-- text-->
                <!--  </div>
                </div> -->
            </div>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!--<li class="nav-small-cap">--- Zenoss</li>-->
                @if (!Auth::user()->can('operador') && !Auth::user()->can('lead_operator'))
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="fas fa-grip-vertical pl-1"></i>
                            <span class="hide-menu">Aplicaciones
                            <span class="badge badge-pill badge-cyan text-white ml-auto">3</span>
                        </span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li>
                                <a href="{{ URL::to('/documents') }}">
                                    <i class="far fa-copy ml-1"></i>
                                    <span class="ml-1">Documentos</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::to('/scripts') }}">
                                    <i class="fas fa-code"></i>
                                    <span class="ml-1">Scripts</span>
                                </a>
                            </li>
                            <li><a href="{{ URL::to('/devices') }}">
                                    <i class="fas fa-network-wired"></i>
                                    <span class="ml-1">Dispositivos</span>
                                </a>
                            </li>
                            <li><a href="javascript:void(0)">
                                <!-- href="{{ URL::to('/show_reports') }}"> -->
                                    <i class="fa fa-file-pdf-o"></i>
                                    <span class="ml-1">Reportes</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if (!Auth::user()->can('operador'))
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="fas fa-cog"></i>
                            <span class="hide-menu"> Administración
                            <span class="badge badge-pill badge-cyan ml-auto">3</span>
                        </span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            @if (!Auth::user()->can('operador') && !Auth::user()->can('lead_operator'))
                                <li><a href="#">
                                        <i class="fas fa-stream ml-1"></i>
                                        <span class="ml-1">Catálogos</span>
                                    </a>
                                </li>
                                <li><a href="{{URL::to('integrations')}}">
                                        <i class="fas fa-server"></i>
                                        <span class="ml-1">Integraciones</span>
                                    </a>
                                </li>
                                <li><a href="{{URL::to('aws')}}">
                                        <i class="fas fa-cloud"></i>
                                        <span class="ml-1">AWS</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)"
                                       aria-expanded="false">
                                        <i class="fas fa-bell"></i>
                                        <span class="ml-1">Notificacion
                                    <span class="badge badge-pill badge-cyan ml-auto">2</span>
                                </span>
                                    </a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li>
                                            <a href="{{ URL::to('filters') }}">
                                                <i class="fas fa-filter ml-1"></i>
                                                <span class="ml-1">Filtros</span>
                                            </a>
                                        </li>
                                        <li><a href="{{ URL::to('attributes') }}">
                                                <i class="far fa-list-alt"></i>
                                                <span class="ml-1">Atributos</span>
                                            </a>
                                    </ul>
                                </li>
                                <li>
                                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)"
                                       aria-expanded="false">
                                        <i class="fas fa-address-book ml-1"></i>
                                        <span class="ml-1">Contactos
                                    <span class="badge badge-pill badge-cyan ml-auto">2</span>
                                </span>
                                    </a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li>
                                            <a href="{{ URL::to('contacts') }}">
                                                <i class="fas fa-users"></i>
                                                <span class="ml-1">Correos</span>
                                            </a>
                                        </li>
                                        <li><a href="{{ URL::to('groups') }}">
                                                <i class="fas fa-shapes ml-1"></i>
                                                <span class="ml-1">Grupos</span>
                                            </a>
                                    </ul>
                                </li>
                            @endif
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)"
                                   aria-expanded="false">
                                    <i class="fas fa-user-shield"></i>
                                    <span class="ml-1">Accesos
                                    <span class="badge badge-pill badge-cyan ml-auto">3</span>
                                </span>
                                </a>
                                <ul aria-expanded="false" class="collapse">
                                    <li>
                                        <a href="{{ URL::to('admin/users') }}">
                                            <i class="fas fa-users"></i>
                                            <span class="ml-1">Usuarios</span>
                                        </a>
                                    </li>
                                    <li><a href="{{ URL::to('admin/roles') }}">
                                            <i class="fas fa-shapes ml-1"></i>
                                            <span class="ml-1">Roles</span>
                                        </a>
                                    </li>
                                    <li><a href="{{ URL::to('admin/abilities') }}">
                                            <i class="fas fa-check-double ml-1"></i>
                                            <span class="ml-1">Permisos</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
            @endif
            <!--  ------------------------- Eventos -->
                <li>
                    <a class="waves-effect waves-dark" href="{{ URL::to('/events') }}" aria-expanded="false">
                        <i class="fab fa-firefox"></i>
                        <span class="hide-menu"> Eventos
                        </span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" aria-expanded="false" href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off text-danger"></i><span class="hide-menu"> Salir </span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
