<script>
    window.DATATABLE = {
        scrollY_45: '45vh',
        scrollY_50: '50vh',
        scrollY_60: '60vh',
        scrollY: '500px',
        scrollX: true,
        scrollCollapse: true,
        bDestroy: true,
        paging: true,
        domButtons: '<B><"clear"><lf>rtip',
        oLanguage: {
            sProcessing: 'Procesando...',
            sLengthMenu: 'Mostrar _MENU_',
            sZeroRecords: 'No se encontraron resultados',
            sSearch: 'Buscar',
            sEmptyTable: 'No hay datos disponibles',
            sInfo: 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_',
            sInfoEmpty: 'Mostrando registros del 0 al 0 de un total de 0',
            sInfoFiltered: '(Filtrado de un total de _MAX_)',
            sInfoThousands: ',',
            sLoadingRecords: 'Cargando...',
            oPaginate: {
                sFirst: 'Primero',
                sLast: 'Ultimo',
                sNext: 'Siguiente',
                sPrevious: 'Anterior'
            }
        },
        buttons: [{
                extend: 'excel',
                className: 'btn btn-light btn-circle',
                text: '<i class="far fa-file-excel fa-lg"></i>',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdf',
                className: 'btn btn-light btn-circle',
                text: '<i class="far fa-file-pdf fa-lg"></i>',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'print',
                className: 'btn btn-light btn-circle',
                //text: '<i class="icon-printer"></i>',
                text: '<i class="fas fa-print fa-lg"></i>',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            }
        ],
        aLengthMenu: [
            [15, 25, 50, 100, 250, -1],
            [15, 25, 50, 100, 250, 'Todo']
        ],
        aLengthMenuAll: [
            [-1],
            ['Todo']
        ]
    };
    Object.freeze(DATATABLE);

    $('#documents-table').DataTable({
        dom: DATATABLE.domButtons,
        bDestroy: DATATABLE.bDestroy,
        oLanguage: DATATABLE.oLanguage,
        buttons: DATATABLE.buttons,
        aLengthMenu: DATATABLE.aLengthMenu,
        processing: true,
        serverSide: true,

        ajax: '{{ url('documents-list') }}',
        order: [
            [0, 'asc']
        ],
        columns: [{
                title: 'ID',
                data: 'id'
            },
            {
                title: 'Nombre',
                data: function(data) {
                    url = '<a href="{{ URL::route('documents.show', ':id') }}">' + data
                        .title + '</a>';
                    url = url.replace(':id', data.id);
                    return url;
                }
            },
            {
                title: 'Autor',
                data: function(data) {
                    return data.user.name;
                }
            },
            {
                title: 'Alta',
                data: function(data) {
                    return '<span class="label label-info">' + data.created_at +
                        '</span>';
                }
            }
        ]
    });

    // Summer Note
    $('#content').summernote({
        height: 500,
        lang: 'es-ES',
        dialogsInBody: true,
        dialogsFade: false,
        //placeholder: '...Y aquí algo muy interesante y útil...',
        //*
        callbacks: {
            onInit: function() {
                var noteBtn =
                    '<button id="makeTag" type="button" class="btn btn-primary btn-sm btn-small" title="Tag" data-event="something" tabindex="-1"><i class="fa fa-tag"></i></button>';
                var fileGroup = '<div class="note-file btn-group">' + noteBtn + '</div>';
                $(fileGroup).appendTo($('.note-toolbar'));
                $('#makeTag').tooltip({
                    container: 'body',
                    placement: 'bottom'
                });
                $('#makeTag').click(function(event) {
                    var highlight = window.getSelection();
                    spn = document.createElement('span');
                    range = highlight.getRangeAt(0);

                    spn.innerHTML = highlight;
                    spn.className = 'label label-primary';

                    range.deleteContents();
                    range.insertNode(spn);
                });
            },
            onPaste: function(image) {
                // TODO. Soporte para copy paste, cambiar Base64.
            },
            onImageUpload: function(image) {
                imageUpload(image[0]);
            },
        },

        toolbar: [
            ['style', ['style']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            //['insert', ['picture', 'link', 'video']],
            ['insert', ['picture', 'link', 'hr', 'video']],
            ['table', ['table']],
            ['help', ['help']],
            ['fullscreen', ['fullscreen']],
            ['codeview', ['codeview']]
        ]
    }).on('change keyup keydown paste cut', 'textarea', function() {
        $(this).height(0).height(this.scrollHeight);
    }).find('textarea').change();

    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());
    });

    changeTemplate();

    function imageUpload(image) {
        var data = new FormData();
        data.append("image", image);
        $.ajax({
            data: data,
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: "POST",
            url: "{{ url('document-image-upload') }}",
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                $('.summernote').summernote("insertImage", url);
            },
            error: function(data) {}
        });
    }

    // --- Del archivo scripts_create.blade.php se agrego el evento a onchange
    function changeTemplate() {
        var data = "";
        var text = $('#template_id').val();
        if (text) {
            data = jQuery.parseJSON(text)['description'];
        }
        $("#template_description").html(data);
    }
    
</script>