<script>
    window.DATATABLE = {
        scrollY_45: '45vh',
        scrollY_50: '50vh',
        scrollY_60: '60vh',
        scrollY: '500px',
        scrollX: true,
        scrollCollapse: true,
        bDestroy: true,
        paging: true,
        domButtons: '<B><"clear"><lf>rtip',
        oLanguage: {
            sProcessing: 'Procesando...',
            sLengthMenu: 'Mostrar _MENU_',
            sZeroRecords: 'No se encontraron resultados',
            sSearch: 'Buscar',
            sEmptyTable: 'No hay datos disponibles',
            sInfo: 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_',
            sInfoEmpty: 'Mostrando registros del 0 al 0 de un total de 0',
            sInfoFiltered: '(Filtrado de un total de _MAX_)',
            sInfoThousands: ',',
            sLoadingRecords: 'Cargando...',
            oPaginate: {
                sFirst: 'Primero',
                sLast: 'Ultimo',
                sNext: 'Siguiente',
                sPrevious: 'Anterior'
            }
        },
        buttons: [{
                extend: 'excel',
                className: 'btn btn-light btn-circle',
                text: '<i class="far fa-file-excel fa-lg"></i>',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdf',
                className: 'btn btn-light btn-circle',
                text: '<i class="far fa-file-pdf fa-lg"></i>',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'print',
                className: 'btn btn-light btn-circle',
                //text: '<i class="icon-printer"></i>',
                text: '<i class="fas fa-print fa-lg"></i>',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            }
        ],
        aLengthMenu: [
            [15, 25, 50, 100, 250, -1],
            [15, 25, 50, 100, 250, 'Todo']
        ],
        aLengthMenuAll: [
            [-1],
            ['Todo']
        ]
    };
    Object.freeze(DATATABLE);

    $('#devices-table').DataTable({
        dom: DATATABLE.domButtons,
        bDestroy: DATATABLE.bDestroy,
        oLanguage: DATATABLE.oLanguage,
        buttons: DATATABLE.buttons,
        aLengthMenu: DATATABLE.aLengthMenu,
        processing: true,
        serverSide: true,

        ajax: '{{ url('devices-list') }}',
        order: [
            [0, 'desc']
        ],
        columns: [{
                title: 'ID',
                data: 'id'
            },
            {
                title: 'Hostname',
                data: function(data) {
                    url = '<a href="{{ URL::route('documents.show', ':id') }}">' +
                        data.hostname + '</a>';
                    url = url.replace(':id', data.id);
                    return url;
                }
            },
            {
                title: 'IP',
                data: 'address'
            },
            {
                title: 'Descripción',
                data: 'description'
            },
            {
                title: 'Alta',
                data: 'created_at'
            },
        ]
    });

    //Toggle fullscreen
    $(".fullscreen-btn").click(function(e) {
        e.preventDefault();
        var $this = $(this);
        $this.children('i')
            .toggleClass('fa-expand')
            .toggleClass('fa-compress');
        $(this).closest('.card').toggleClass('panel-fullscreen');
    });

    $('#connect').on('click', function() {
        host = $('#host').val();
        os = $('#os').val();
        user = $('#user').val();
        password = $('#password').val();
        $('#host_error').text('');
        $('#os_error').text('');
        $('#user_error').text('');
        $('#password_error').text('');
        $.ajax({
            data: {
                host: host,
                os: os,
                user: user,
                password: password
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: "POST",
            url: "{{ url('devices-connect') }}",
            success: function(data) {
                //console.log(data);
                window.open(data, '_blank');
            },
            error: function(data) {
                if (data.status === 422) {
                    var errors = data.responseJSON.errors;
                    $.each(errors, function(key, value) {
                        $("#" + key + "_error").text(value[0]);
                    });
                }
            }
        });
    });

    $('#devices-tree').jstree({
        core: {
            check_callback: true,
            multiple: false,
            data: {
                url: '{{ url('devices-tree-list') }}',
                dataType: 'json',
            },
        },
        plugins: ['types', 'state'],
        types: {
            branch: {
                icon: 'far fa-folder'
            },
            leaf: {
                icon: 'far fa-hdd fa-sm',
                'max_depth': 0
            }
        }
    }).on('loaded.jstree', function(e, data) {
        var tree = $(this).jstree();
        //$(this).jstree('open_all');
        $('#devices-tree-new-folder').on('click', function() {
            tree.get_selected().forEach(function(id) {
                $.ajax({
                    data: {
                        parent: id,
                        text: 'Directorio',
                        type: 'branch'
                    },
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    type: "POST",
                    dataType: 'JSON',
                    url: "{{ URL::route('devices-tree.store') }}",
                    success: function(node) {
                        tree.create_node(node.parent_id, {
                            id: node.id,
                            text: node.name,
                            type: node.type
                        }, 'last', function(id) {
                            node = tree.get_node(id);
                            tree.edit(node);
                            tree.deselect_node(node);
                        });
                    },
                    error: function(data) {}
                });
            });
        });

        $('#devices-tree-new-host').on('click', function() {
            tree.get_selected().forEach(function(id) {
                $.ajax({
                    data: {
                        parent: id,
                        text: 'Dispositivo',
                        type: 'leaf'
                    },
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    type: "POST",
                    dataType: 'JSON',
                    url: "{{ URL::route('devices-tree.store') }}",
                    success: function(node) {
                        tree.create_node(node.parent_id, {
                            id: node.id,
                            text: node.name,
                            type: node.type
                        }, 'last', function(id) {
                            node = tree.get_node(id);
                            tree.edit(node);
                            tree.deselect_node(node);
                        });
                    },
                    error: function(data) {}
                });
            });
        });

        $("#devices-tree-delete").on('click', function() {
            tree.get_selected().forEach(function(id) {
                node = tree.get_node(id);
                if (node.parent != '#') {
                    if (!tree.is_parent(node)) {
                        url = "{{ URL::route('devices-tree.destroy', ':id') }}";
                        url = url.replace(':id', id);
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            type: "DELETE",
                            url: url,
                            success: function(data) {
                                if (data == 'SUCCESS') {
                                    tree.delete_node(node);
                                } else {}
                            },
                            error: function(data) {}
                        });
                    }
                }
            });
        });

    }).on('select_node.jstree', function(e, data) {

        console.log(data);

        if (data.node.type == 'leaf') {
            //Formulario deshabilitado: false
            $('#device_disabled').prop('disabled', false);
            //ID del host: data.node.id
            $('#id').val(data.node.id);
            //Path del host: data.node
            var path = data.instance.get_path(data.node, '  >  ');
            $('#device_path').html('<code>' + path + '</code>');
            //TODO. Carga asincrona info del host... here
        } else {
            //Formulario deshabilitado: true
            $('#device_disabled').prop('disabled', true);
            //ID del host: null
            $('#id').val(null);
            //Path del host: null
            $('#device_path').html(null);
        }
    }).on('dblclick.jstree', function(e) {
        var tree = $(this).jstree();
        node = tree.get_node(e.target);
        if (node.parent != '#') {
            tree.edit(node);
        }
    }).on('rename_node.jstree', function(e, data) {
        if (data.text != data.old) {
            url = "{{ URL::route('devices-tree.update', ':id') }}";
            url = url.replace(':id', data.node.id);
            $.ajax({
                data: {
                    text: data.node.text
                },
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                type: "PUT",
                url: url,
                success: function(data) {
                    if (data == 'SUCCESS') {
                        console.log('ACTUALIZADO');
                    } else {
                        console.log('Error');
                    }
                },
                error: function(data) {}
            });
        }
    });

    $('#save').on('click', function() {
        host = $('#host').val();
        os = $('#os').val();
        user = $('#user').val();
        password = $('#password').val();
        description = $('#description').val();
        $('#host_error').text('');
        $('#os_error').text('');
        $('#user_error').text('');
        $('#password_error').text('');
        $.ajax({
            data: {
                host: host,
                os: os,
                user: user,
                password: password,
                description: description
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: "POST",
            url: "{{ url('devices-save') }}",
            success: function(data) {
                console.log(data);
            },
            error: function(data) {
                if (data.status === 422) {
                    var errors = data.responseJSON.errors;
                    $.each(errors, function(key, value) {
                        $("#" + key + "_error").text(value[0]);
                    });
                } else {
                    console.log(data);
                }
            }
        });
    });

</script>