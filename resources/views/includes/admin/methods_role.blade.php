<script>
    window.DATATABLE = {
        scrollY_45: '45vh',
        scrollY_50: '50vh',
        scrollY_60: '60vh',
        scrollY: '500px',
        scrollX: true,
        scrollCollapse: true,
        bDestroy: true,
        paging: true,
        domButtons: '<B><"clear"><lf>rtip',
        oLanguage: {
            sProcessing: 'Procesando...',
            sLengthMenu: 'Mostrar _MENU_',
            sZeroRecords: 'No se encontraron resultados',
            sSearch: 'Buscar',
            sEmptyTable: 'No hay datos disponibles',
            sInfo: 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_',
            sInfoEmpty: 'Mostrando registros del 0 al 0 de un total de 0',
            sInfoFiltered: '(Filtrado de un total de _MAX_)',
            sInfoThousands: ',',
            sLoadingRecords: 'Cargando...',
            oPaginate: {
                sFirst: 'Primero',
                sLast: 'Ultimo',
                sNext: 'Siguiente',
                sPrevious: 'Anterior'
            }
        },
        buttons: [{
                extend: 'excel',
                className: 'btn btn-light btn-circle',
                text: '<i class="far fa-file-excel fa-lg"></i>',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdf',
                className: 'btn btn-light btn-circle',
                text: '<i class="far fa-file-pdf fa-lg"></i>',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'print',
                className: 'btn btn-light btn-circle',
                //text: '<i class="icon-printer"></i>',
                text: '<i class="fas fa-print fa-lg"></i>',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            }
        ],
        aLengthMenu: [
            [15, 25, 50, 100, 250, -1],
            [15, 25, 50, 100, 250, 'Todo']
        ],
        aLengthMenuAll: [
            [-1],
            ['Todo']
        ]
    };
    Object.freeze(DATATABLE);

    $('#roles-table').DataTable({
        dom: DATATABLE.domButtons,
        bDestroy: DATATABLE.bDestroy,
        oLanguage: DATATABLE.oLanguage,
        buttons: DATATABLE.buttons,
        aLengthMenu: DATATABLE.aLengthMenu,
        processing: true,
        serverSide: true,

        ajax: '{{ url('roles-list') }}',
        order: [
            [0, 'asc']
        ],
        columns: [{
                title: 'ID',
                data: 'id'
            },
            {
                title: 'Nombre',
                data: function(data) {
                    url = '<a href="{{ URL::route('roles.show', ':id') }}">' + data
                        .name + '</a>';
                    url = url.replace(':id', data.id);
                    return url;
                }
            },
            {
                title: 'Titulo',
                data: 'title'
            },
            {
                title: 'Creado',
                data: 'created_at'
            }
        ]
    });
</script>