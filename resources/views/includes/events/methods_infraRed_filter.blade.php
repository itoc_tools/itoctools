<script>
    var table = $('#events-table').DataTable({
        colReorder: true,
        dom: '<B><"clear"><lf>rtip',
        bDestroy: true,
        oLanguage: {
            sProcessing: 'Procesando...',
            sLengthMenu: 'Mostrar _MENU_',
            sZeroRecords: 'No se encontraron resultados',
            sSearch: 'Buscar',
            sEmptyTable: 'No hay datos disponibles',
            sInfo: 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_',
            sInfoEmpty: 'Mostrando registros del 0 al 0 de un total de 0',
            sInfoFiltered: '(Filtrado de un total de _MAX_)',
            sInfoThousands: ',',
            sLoadingRecords: 'Cargando...'
        },
        buttons: [{
                extend: 'excel',
                className: 'btn btn-light btn-circle',
                text: '<i class="far fa-file-excel fa-lg"></i>',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdf',
                className: 'btn btn-light btn-circle',
                text: '<i class="far fa-file-pdf fa-lg"></i>',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'print',
                className: 'btn btn-light btn-circle',
                text: '<i class="fas fa-print fa-lg"></i>',
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            }
        ],
        aLengthMenu: [
            [-1],
            ['Todo']
        ],
        scrollX: true,
        scrollY: '500px',
        scrollCollapse: true,
        paging: false,
        ajax: "{{ route('infraRedReload') }}",
        columns: [{
            data: function(data) {
                url = `<input id="checkbox-${ data.id_event }" type="checkbox" onclick="selectedByUser(this, '${ data.id_event }')">`
                return url;
            }
        }, {
            data: function(data) {
                if (data.im) {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'IM " + data.id_event + "','width=1050,height=450');\" style='width:90%; height:90%;' class='label label-info'><small style='white-space: nowrap'>" +
                        data.im + "</small></a>";
                    url = url.replace(':id', data.id_event);
                    return url;
                } else if (data.im_fake) {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'IM " + data.id_event + "','width=1050,height=450');\" style='width:90%; height:90%;' class='label label-warning'><small style='white-space: nowrap'>" +
                        data.im_fake + "</small></a>";
                    url = url.replace(':id', data.id_event);
                    return url;
                } else if (data.im_empty) {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.edit', ':id') }}', 'IM " + data.id_event + "','width=700,height=650');\" style='width:90%; height:90%;' class='label label-danger'><small style='white-space: nowrap'>" +
                        data.im_empty + "</small></a>";
                    url = url.replace(':id', data.id_event);
                    return url;
                } else {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.edit', ':id') }}', 'IM " + data.id_event + "','width=700,height=650');\" style='width:90%; height:90%;' class='label label-danger'> <small style='white-space: nowrap'>" +
                        "Crear IM" + "</small></a>";
                    url = url.replace(':id', data.id_event);
                    return url;
                }
            }
        }, {
            data: function(data) {
                if (data.severity_critical) {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'> <small>" +
                        "<img src='{{url('/images/status/stat_error.png')}}' width='16' height='16'></small></a>";
                    url = url.replace(':id', data.id_event);
                    return url;
                } else if (data.severity_major) {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'> <small>" +
                        "<img src='{{url('/images/status/stat_majo.png')}}' width='16' height='16'></small></a>";
                    url = url.replace(':id', data.id_event);
                    return url;
                } else if (data.severity_minor) {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'> <small>" +
                        "<img src='{{url('/images/status/stat_mino.png')}}' width='16' height='16'></small></a>";
                    url = url.replace(':id', data.id_event);
                    return url;
                } else if (data.severity_warning) {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'> <small>" +
                        "<img src='{{url('/images/status/stat_warn.png')}}' width='16' height='16'></small></a>";
                    url = url.replace(':id', data.id_event);
                    return url;
                } else if (data.severity_normal) {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'> <small>" +
                        "<img src='{{url('/images/status/stat_norm.png')}}' width='16' height='16'></small></a>";
                    url = url.replace(':id', data.id_event);
                    return url;
                } else {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'> <small></small> </a></td>";
                    url = url.replace(':id', data.id_event);
                    return url;
                }
            }
        }, {
            data: function(data) {
                url = "<th class='p-0' style=' white-space: nowrap'><a  href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', '" + data.id_event + "','width=1050,height=450');\" class='link m-r-10'><small class='p-0' style='white-space: nowrap'>" +
                    data.priority + "</small></a></th>";
                url = url.replace(':id', data.id_event);
                return url;
            }
        }, {
            data: function(data) {
                url = "<a  href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'><small class='p-0' style='white-space: nowrap'>" +
                    data.application + "</small></a>";
                url = url.replace(':id', data.id_event);
                return url;
            }
        }, {
            data: function(data) {
                url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'><small class='p-0' style='white-space: nowrap'>" +
                    data.state + "</small></a>";
                url = url.replace(':id', data.id_event);
                return url;
            }
        }, {
            data: function(data) {
                if (data.symptom_list_c) {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'><small><svg xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' width='18' height='18' viewBox='0 0 172 172' style=' fill:#000000;'><g fill='none' fill-rule='nonzero' stroke='none' stroke-width='1' stroke-linecap='butt' stroke-linejoin='miter' stroke-miterlimit='10' stroke-dasharray=' stroke-dashoffset='0' font-family='none' font-weight='none' font-size='none' text-anchor='none' style='mix-blend-mode: normal'><path d='M0,172v-172h172v172z' fill='none'></path><g fill='#2ecc71'>" +
                        "<path d='M35.83333,21.5c-7.83362,0 -14.33333,6.49972 -14.33333,14.33333v21.5c0,7.83362 6.49972,14.33333 14.33333,14.33333h28.66667c7.83362,0 14.33333,-6.49972 14.33333,-14.33333v-21.5c0,-7.83362 -6.49972,-14.33333 -14.33333,-14.33333zM35.83333,35.83333h28.66667v21.5h-28.66667zM107.5,71.66667c-7.83362,0 -14.33333,6.49972 -14.33333,14.33333v50.16667c0,7.83362 6.49972,14.33333 14.33333,14.33333h28.66667c7.83362,0 14.33333,-6.49972 14.33333,-14.33333v-50.16667c0,-7.83362 -6.49972,-14.33333 -14.33333,-14.33333zM35.83333,86v14.33333h14.33333v-14.33333zM107.5,86h28.66667v50.16667h-28.66667zM35.83333,114.66667v14.33333h14.33333v-14.33333zM64.5,114.66667v14.33333h14.33333v-14.33333zM121.83333,114.66667c-3.95804,0 -7.16667,3.20863 -7.16667,7.16667c0,3.95804 3.20863,7.16667 7.16667,7.16667c3.95804,0 7.16667,-3.20863 7.16667,-7.16667c0,-3.95804 -3.20863,-7.16667 -7.16667,-7.16667z'></path></g></g></svg></small></a>";
                    url = url.replace(':id', data.id_event);
                    return url;
                } else {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'> <small></small> </a></td>";
                    url = url.replace(':id', data.id_event);
                    return url;
                }
            }
        }, {
            data: function(data) {
                if (data.annotation_list_n) {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'><small><svg xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' width='18' height='18' viewBox='0 0 172 172' style=' fill:#000000;'><g fill='none' fill-rule='nonzero' stroke='none' stroke-width='1' stroke-linecap='butt' stroke-linejoin='miter' stroke-miterlimit='10' stroke-dasharray=' stroke-dashoffset='0' font-family='none' font-weight='none' font-size='none' text-anchor='none' style='mix-blend-mode: normal'><path d='M0,172v-172h172v172z' fill='none'></path><g fill='#f1c40f'>" +
                        "<path d='M141.65347,50.4132l-37.26667,-37.26667c-1.07787,-1.07787 -2.53413,-1.67987 -4.05347,-1.67987h-60.2c-6.33533,0 -11.46667,5.13133 -11.46667,11.46667v126.13333c0,6.33533 5.13133,11.46667 11.46667,11.46667h91.73333c6.33533,0 11.46667,-5.13133 11.46667,-11.46667v-94.6c0,-1.51933 -0.602,-2.9756 -1.67987,-4.05347zM103.2,120.4h-45.86667c-3.1648,0 -5.73333,-2.56853 -5.73333,-5.73333c0,-3.1648 2.56853,-5.73333 5.73333,-5.73333h45.86667c3.1648,0 5.73333,2.56853 5.73333,5.73333c0,3.1648 -2.56853,5.73333 -5.73333,5.73333zM114.66667,97.46667h-57.33333c-3.1648,0 -5.73333,-2.56853 -5.73333,-5.73333c0,-3.1648 2.56853,-5.73333 5.73333,-5.73333h57.33333c3.1648,0 5.73333,2.56853 5.73333,5.73333c0,3.1648 -2.56853,5.73333 -5.73333,5.73333zM103.2,57.33333c-3.1648,0 -5.73333,-2.56853 -5.73333,-5.73333v-29.21707l34.9504,34.9504z'></path></g></g></svg></small></a>";
                    url = url.replace(':id', data.id_event);
                    return url;
                } else {
                    url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'> <small></small> </a></td>";
                    url = url.replace(':id', data.id_event);
                    return url;
                }
            }
        }, {
            data: function(data) {
                if (data.user_name == '&lt;unknown&gt;') {
                    url = "<th class='p-0' style=' white-space: nowrap'><a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', '" + data.id_event + "','width=1050,height=450');\" class='link m-r-10'><small style='white-space: nowrap'>" +
                        data.user_name + "</small></a></th>";
                } else {
                    url = "<th class='p-0' style=' white-space: nowrap'><a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', '" + data.id_event + "','width=1050,height=450');\" class='link m-r-10'><small class='font-weight-bold' style='white-space: nowrap'>" +
                        data.user_name + "</small></a></th>";
                }
                url = url.replace(':id', data.id_event);
                return url;
            }
        }, {
            data: function(data) {
                url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'><small class='p-0' style='white-space: nowrap'>" +
                    data.event_age + "</small></a>";
                url = url.replace(':id', data.id_event);
                return url;
            }
        }, {
            data: function(data) {
                url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'><small class='p-0' style='white-space: nowrap'>" +
                    data.time_received_label + "</small></a>";
                url = url.replace(':id', data.id_event);
                return url;
            }
        }, {
            data: function(data) {
                if (data.cResponsable) {
                    cResponsable = data.cResponsable;
                } else {
                    cResponsable = '';
                }
                url = "<th class='p-0' style=' white-space: nowrap'><a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', '" + data.id_event + "','width=1050,height=450');\" class='link m-r-10'><small class='p-0' style='white-space: nowrap'>" +
                    cResponsable + "</small></a></th>";
                url = url.replace(':id', data.id_event);
                return url;
            }
        }, {
            data: function(data) {
                if (data.EventoZenoss_Id) {
                    EventoZenoss_Id = data.EventoZenoss_Id;
                } else {
                    EventoZenoss_Id = '';
                }
                url = "<th class='p-0' style=' white-space: nowrap'><a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', '" + data.id_event + "','width=1050,height=450');\" class='link m-r-10'><small class='p-0' style='white-space: nowrap'>" +
                    EventoZenoss_Id + "</small></a></th>";
                url = url.replace(':id', data.id_event);
                return url;
            }
        }, {
            data: function(data) {
                if (data.CO) {
                    CO = data.CO;
                } else {
                    CO = '';
                }
                url = "<th class='p-0' style=' white-space: nowrap'><a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', '" + data.id_event + "','width=1050,height=450');\" class='link m-r-10'><small class='p-0' style='white-space: nowrap'>" +
                    CO + "</small></a></th>";
                url = url.replace(':id', data.id_event);
                return url;
            }
        }, {
            data: function(data) {
                if (data.object != null) {
                    object = data.object;
                } else {
                    object = '';
                }
                url = "<th class='p-0' style=' white-space: nowrap'><a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', '" + data.id_event + "','width=1050,height=450');\" class='link m-r-10'><small class='p-0' style=' white-space: nowrap'>" +
                    object + "</small></a></th>";
                url = url.replace(':id', data.id_event);
                return url;
            }
        }, {
            data: function(data) {
                url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'><small class='p-0' style=' white-space: nowrap'>" +
                    data.title + "</small></a>";
                url = url.replace(':id', data.id_event);
                return url;
            }
        }, {
            data: function(data) {
                url = "<a href='javascript:void(0)' onclick=\"window.open('{{ route('events.show', ':id') }}', 'Details','width=1050,height=450');\" class='link m-r-10'><small class='p-0' style='white-space: nowrap'>" +
                    data.id_event + "</small></a>";
                url = url.replace(':id', data.id_event);
                return url;
            }
        }]
    });

    setInterval(function() {
        table.ajax.reload(function() {
            updateSelection();
        }, false);
    }, 15000);

    $('#events-table tbody').on('dblclick', 'tr', function() {
        var data = table.row(this).data();
        var id_event_clicked = data['id_event'] ? data['id_event'] : '';
        var url = "{{ route('events.show', ':id_event_clicked') }}";
        url = url.replace(':id_event_clicked', id_event_clicked);
        var popup = window.open(url, "", "width=1050, height=450");
        popup.focus();
    });
</script>