<script>
    refreshTableGraphics();

    function refreshTableGraphics() {
        $('#events-graphics').load('graphicsReload', function() {
            setTimeout(refreshTableGraphics, 15000);
        });
    }
</script>