<script>
    function GetSelected(evnt, userBSM, passBSM) {
        var ids = "";
        var message = "";
        var data = eventsDataTable.rows({
            selected: true
        }).data().toArray();

        if (data.length <= 0) {
            alert('No hay eventos seleccionados');

            evnt.stopPropagation();

            return;
        }

        data.forEach(e => {
            ids += `${ e.id_event}/`;

            message += `* ${ e.id_event }\t${ e.application }\t${ e.title }\n`;
        });

        document.getElementById("ids").value = ids;
        document.getElementById("idsTextBox").value = message;
        document.getElementById("User_OMI_CAM").value = userBSM;
        document.getElementById("Password_OMI_CAM").value = passBSM;
    }

    function GetSelectedAM(evnt, userBSM, passBSM) {
        var ids = "";
        var message = "";
        var data = eventsDataTable.rows({
            selected: true
        }).data().toArray();

        if (data.length <= 0) {
            alert('No hay eventos seleccionados');

            evnt.stopPropagation();

            return;
        }

        data.forEach(e => {
            ids += `${ e.id_event}/`;

            message += `* ${ e.id_event }\t${ e.application }\t${ e.title }\n`;
        });

        document.getElementById("idsAM").value = ids;
        document.getElementById("idsAMText").value = message;
        document.getElementById("User_OMI_AM").value = userBSM;
        document.getElementById("Password_OMI_AM").value = passBSM;
    }

    function GetSelectedAssing(evnt, userBSM, passBSM) {
        var ids = "";
        var message = "";
        var data = eventsDataTable.rows({
            selected: true
        }).data().toArray();

        if (data.length <= 0) {
            alert('No hay eventos seleccionados');

            evnt.stopPropagation();

            return;
        }

        data.forEach(e => {
            ids += `${ e.id_event}/`;

            message += `* ${ e.id_event }\t${ e.application }\t${ e.title }\n`;
        });

        document.getElementById("idsAssign").value = ids;
        document.getElementById("idsAssignText").value = message;
        document.getElementById("User_OMI_Assign").value = userBSM;
        document.getElementById("Password_OMI_Assign").value = passBSM;
        /* Funcion para evitar el modal y pase directo al loading del proceso */
        $('#saveAssign').click();
        document.getElementById("responsive-modal-assign-main").style.display = "none";
    }

    function GetSelectedState(evnt, userBSM, passBSM) {
        var ids = "";
        var message = "";
        var data = eventsDataTable.rows({
            selected: true
        }).data().toArray();

        if (data.length <= 0) {
            alert('No hay eventos seleccionados');

            evnt.stopPropagation();

            return;
        }

        data.forEach(e => {
            ids += `${ e.id_event}/`;

            message += `* ${ e.id_event }\t${ e.application }\t${ e.title }\n`;
        });

        document.getElementById("idState").value = ids;
        document.getElementById("idStateText").value = message;
        document.getElementById("User_OMI_State").value = userBSM;
        document.getElementById("Password_OMI_Status").value = passBSM;
    }

    document.getElementById("save-CAM").onclick = function() {
        document.getElementById("responsive-modal-custom-attributes_main").style.display = "none";
    }

    document.getElementById("saveAM").onclick = function() {
        document.getElementById("responsive-modal-annotation_main").style.display = "none";
    }

    /* Funcion para evitar el modal y pase directo al loading del proceso */
    /* document.getElementById("saveAssign").onclick = function() {
        document.getElementById("responsive-modal-assign-main").style.display = "none";
    } */

    document.getElementById("saveState").onclick = function() {
        document.getElementById("responsive-modal-state-main").style.display = "none";
    }
</script>