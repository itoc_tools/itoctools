@extends('layouts.master')

@section('content')
    <div class="col pb-3 text-right">
        <a href="{{URL::to('groups/create')}}" class="btn bg-secondary">Nuevo
            Grupo</a>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($groups as $group)
                            <tr>
                                <td>{{$group->id}}</td>
                                <td>
                                    <a href="{{route('groups.show', [$group->id])}}">{{$group->name}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
