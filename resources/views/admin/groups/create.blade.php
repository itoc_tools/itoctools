@extends('layouts.master')

@section('content')
    <div class="col">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Nuevo Grupo</h4>
                <form action="{{route('group.create')}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <div class="col">
                            <label>Nombre</label>
                            <input type="text" name="name" class="form-control" placeholder="Nombre" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <label for="validationCustom01">Correo</label>
                            <input type="email" name="email" class="form-control" placeholder="Correo" required>
                        </div>
                        <div class="col">
                            <label for="validationCustom01">Teléfono</label>
                            <input type="tel" name="telephone" class="form-control" placeholder="Teléfono" required>
                        </div>
                    </div>
                    <div class="col">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                        <a href="{{ route('contact.index') }}" class="btn btn-warning">Cancelar </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
