@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <h4 class="card-title">NUEVO USUARIO</h4>
                <h6 class="card-subtitle">Para ingresar un nuevo usuario debe completar los campos.</h6>
                <form class="form-material m-t-40" method="POST" action="{{ route('users.store') }}">
                    {{ csrf_field() }}
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Nombre</label>
                        <div class="col-10">
                            <input type="text" class="form-control form-control-line" placeholder="nombre" id="name" name="name" value="{{ old('name') }}" required>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Email</label>
                        <div class="col-10">
                            <input type="email" class="form-control form-control-line" placeholder="email@email.com" id="email" name="email" value="{{ old('email') }}" required>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Password</label>
                        <div class="col-10">
                            <input type="password" class="form-control form-control-line" placeholder="password" id="password" name="password" required>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Confirmar Password</label>
                        <div class="col-10">
                            <input type="password" class="form-control form-control-line" placeholder="confirmar password" id="password_confirmation" name="password_confirmation" required>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Usuario BSM</label>
                        <div class="col-10">
                            <input type="text" class="form-control form-control-line" placeholder="BSM" id="us_bsm" name="us_bsm">
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Password BSM</label>
                        <div class="col-10">
                            <input type="text" class="form-control form-control-line" placeholder="Password BSM" id="pass_bsm" name="pass_bsm">
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Roles</label>
                        <div class="col-10">
                            <select class="select2 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose" id="roles[]" name="roles[]" required>
                                @foreach($roles as $role)
                                <option old('roles')>{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Permisos</label>
                        <div class="col-10">
                            <select class="select2 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose" id="abilities[]" name="abilities[]">
                                @foreach($abilities as $abilitie)
                                <option old('abilities')>{{$abilitie->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row text-right">
                        <div class="col">
                            <button type="submit" class="btn btn-success" id="save">
                                Guardar
                            </button>
                            <a href="{{ route('users.index') }}" class="btn btn-warning">Cancelar </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection