@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <h4 class="card-title">Editar el usuario: {{ $user->id }} </h4>
                <h6 class="card-subtitle">Editar el usuario.</h6>
                <form class="form-material m-t-40" method="POST" action="{{ route('users.update', $user->id) }}">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Nombre</label>
                        <div class="col-10">
                            <input type="text" class="form-control form-control-line" placeholder="Nombre" id="name" name="name" value="{{ $user->name }}" required>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Email</label>
                        <div class="col-10">
                            <input type="text" class="form-control form-control-line" placeholder="email" id="editEmail" name="email" value="{{ $user->email }}" required>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Usuario BSM</label>
                        <div class="col-10">
                            @if(isset($user_bsm))
                            <input type="text" class="form-control form-control-line" placeholder="Usuario BSM" id="us_bsm" name="us_bsm" value="{{ $user_bsm->us_bsm }}">
                            @else
                            <input type="text" class="form-control form-control-line" placeholder="Usuario BSM" id="us_bsm" name="us_bsm" value="">
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Password BSM</label>
                        <div class="col-10">
                            @if(isset($user_bsm))
                            <input type="text" class="form-control form-control-line" placeholder="Password BSM" id="pass_bsm" name="pass_bsm" value="{{ $user_bsm->pass_bsm }}">
                            @else
                            <input type="text" class="form-control form-control-line" placeholder="Password BSM" id="pass_bsm" name="pass_bsm" value="">
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Roles</label>
                        <div class="col-10">
                            <select class="select2 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose" id="roles[]" name="roles[]" required>
                                @foreach($roles as $role)
                                <option old('roles') @foreach($user->roles()->pluck('id', 'id') as $roleUser)
                                    @if($role->id == $roleUser)
                                    selected = "selected"
                                    @endif
                                    @endforeach>{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Permisos</label>
                        <div class="col-10">
                            <select class="select2 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose" id="abilities[]" name="abilities[]">
                                @foreach($abilities as $abilitie)
                                <option old('abilities') @foreach($user->abilities->pluck('id', 'id') as $roleAbilitie)
                                    @if($abilitie->id == $roleAbilitie)
                                    selected = "selected"
                                    @endif
                                    @endforeach>{{ $abilitie->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row text-right">
                        <div class="col">
                            <button type="submit" class="btn btn-success" id="save">
                                Actualizar
                            </button>
                            <a href="{{ route('users.index') }}" class="btn btn-warning">Cancelar </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection