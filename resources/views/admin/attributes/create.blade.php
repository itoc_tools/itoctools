@extends('layouts.master')

@section('content')
    <div class="col">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Nuevo atributo</h4>
                <form action="{{route('attribute.store')}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Nombre</label>
                            <input type="text" id="label" name="label" class="form-control" required>
                        </div>
                    </div>
                    <div class="col">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                        <a href="{{ route('attribute.index') }}" class="btn btn-warning">Cancelar </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
