@extends('layouts.master')

@section('content')
    <div class="col pb-3 text-right">
        <a href="{{route('attribute.create')}}" class="btn bg-secondary">Nuevo atributo</a>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($attributes as $key => $val)
                            <tr>
                                <td>{{$val->id}}</td>
                                <td>{{$val->label}}</td>
                                <td>
                                    <div class="dropdown">
                                        <a href="{{URL::to('attribute/'. $val->id . '/edit')}}">Editar</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
