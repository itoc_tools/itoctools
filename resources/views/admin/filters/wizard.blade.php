@extends('layouts.master')

<!-- Style Query Builders -->
<style>
    .query-builder .rules-group-container {
        width: 100% !important;
        background-color: #FFFFFF !important;
        border-color: #D7DBDD !important;
        margin-bottom: 25px !important;
    }

    .rule-container {
        background-color: #F4F4F4 !important;
    }

    .select2-selection__choice {
        background-color: #FFFFFF !important;
        color: #000000 !important;
        border-color: #000000 !important;
    }


    .jqueryHashtags .highlighter {
        white-space: pre-wrap;
        color: transparent;
        overflow: hidden;
        position: absolute;
        padding-left: 7px;
        padding-top: 5px;
        font-size: 14px;
        line-height: 20px;
    }

    .jqueryHashtags .theSelector {
        background-color: transparent;
        position: relative;
        direction: ltr;
        font-size: 14px;
        resize: none !important;
        /* -webkit-transition: height 0.2s; */
        border: 1px solid #e9ecef;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        padding: 4px 6px;
        vertical-align: middle;
        min-height: 32px !important;
        line-height: 20px;
        width: 100%;
    }

    .jqueryHashtags .hashtag {
        background: -webkit-linear-gradient(#dce6f8, #bdcff1);
        border-radius: 2px;
        box-shadow: 0 0 0 1px #a3bcea;
        font-size: 14px;
        white-space: pre-wrap;
        word-break: break-word;
        line-height: 20px;
    }
</style>

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div id="wizard-content" class="card-body wizard-content">
                <h4 class="card-title">Correlación de eventos</h4>
                <form id="profileForm" method="post" class="validation-wizard wizard-circle">
                    <!-- Step 1 -->
                    <h6>Filtro</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="intType1">Nombre:</label>
                                    <input type="text" class="form-control" name="name" id="name" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="intType1">Descripción:</label>
                                    <input type="text" class="form-control" name="description" id="description" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="builder"></div>
                            </div>
                        </div>
                    </section>
                    <!-- Step 2 -->
                    <h6>Plantilla</h6>
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-header" style="background-color: #dbf5f9">
                                            <h3 style="color: #386d99" class="font-weight-bold p-3">Centro Integral
                                                de Monitoreo y Gestión
                                                ITOC</h3>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="dynamic_field">
                                                <tr>
                                                    <td>
                                                        <input type="text" class="form-control" name="property" required>
                                                    </td>
                                                    <td>
                                                        <textarea rows="1" name="attributes" required></textarea>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-success" type="button" onclick="template_fields();"><i class="fa fa-plus"></i></button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Step 3 -->
                    <h6>Notificación</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Cuenta:</label>
                                    <select class="form-control" name="account" id="account">
                                        <option value="apm.itoc@triara.com">apm.itoc@triara.com</option>
                                        <option value="notificacion.itoc@triara.com">notificacion.itoc@triara.com</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Etiqueta personalizada:</label>
                                    <textarea rows="0" name="tag" id="tag" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Para:</label>
                                    <input type="text" data-role="tagsinput" placeholder="Agregar correo" id="to" name="to" required />
                                </div>
                                <div class="form-group">
                                    <label>Cc:</label>
                                    <input type="text" data-role="tagsinput" placeholder="Agregar correo" id="cc" name="cc" />
                                </div>
                                <div class="form-group">
                                    <label>Asunto:</label>
                                    <textarea rows="0" name="subject" id="subject" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Opciones:</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="check_template" name="check_template">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            Plantilla sin formato
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Script Plantilla -->
<script>
    var i = 0;

    function template_fields() {

        i++;
        $("select.select2").select2('destroy');
        $('#dynamic_field').append('<tr id="row' + i + '" class="template2"> <td> <input type="text" class="form-control" name="property" required> </td> <td> <textarea rows="1" name="attributes" required></textarea> </td> <td> <button class="btn btn-danger" type="button" onclick="remove_template_fields(' + i + ');"> <i class="fa fa-minus"></i> </button> </td> </tr>');
        $("select.select2").select2();
        $("textarea").hashtags();
    }

    function remove_template_fields(rid) {
        $('#row' + rid).remove();
    }
</script>

@endsection

@section('allJquery')
@include('includes.filters.methods_wizard')
@endsection
