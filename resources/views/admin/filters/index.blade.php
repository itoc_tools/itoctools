@extends('layouts.master')

@section('content')
    <div class="col pb-3 text-right">
        <a href="{{route('filter.create')}}" class="btn bg-secondary">Nuevo Filtro</a>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Filtro</th>
                            <th>Descripción</th>
                            <th>Opciones</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($filters as $filter)
                            <tr>
                                <td>{{$filter->id}}</td>
                                <td>{{$filter->name}}</td>
                                <td>{{$filter->description}}</td>
                                <td>
                                    <div class="dropdown">
                                        <a href="{{route('filter.replicate', $filter->id)}}" title="Duplicar"><i
                                                class="fas fa-clone"></i></a>
                                        <a href="{{route('filter.edit', $filter->id)}}" title="Editar"><i
                                                class="far fa-edit"></i></a>
                                        <a href="{{route('filter.destroy', $filter->id)}}" title="Eliminar"><i
                                                class="fas fa-trash"></i></a>
                                    </div>
                                </td>
                                <td>
                                    <input id="toggle-event{{$filter->id}}" type="checkbox" data-toggle="toggle"
                                           data-size="xs"
                                           onchange="onOffFilter({{$filter->id}})" {{$filter->state == true ? "checked" : ""}}>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
