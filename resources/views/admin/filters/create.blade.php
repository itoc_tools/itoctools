<html>
<head>
    <title>filter</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="https://cdn.jsdelivr.net/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- jQuery querybuilder plugin -->
    <script src="{{ URL::asset('theme/assets/node_modules/jquery-query-builder/dist/js/query-builder.standalone.min.js') }}"></script>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.jsdelivr.net/jquery.query-builder/2.4.1/css/query-builder.default.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sql-parser-mistic@1.2.3/browser/sql-parser.js"></script>
    <style>
        .save {
            padding-top: 20px;
        }

        .save-btn {
            background-color: #80C342;
            color: #FFFFFF;
        }

        .save-btn:hover {
            background-color: #FFFFFF;
            color: #000000;
            border: 1px solid #000000;
        }
    </style>

</head>
<body>
<div class="container-fluid">
    <form class="row g-3">
        <div class="form-group col-md-6">
            <label class="font-weight-bold">Nombre</label>
            <input id="name" type="text" class="form-control" name="name"
                   aria-describedby="emailHelp" placeholder="Ingresa un nombre" required>
        </div>
        <div class="form-group col-md-6">
            <label class="font-weight-bold">Descripción</label>
            <input id="description" type="text" class="form-control" name="description"
                   aria-describedby="emailHelp" placeholder="Ingresa una descripción" required>
        </div>
        <div class="form-group col-md-12">
            <div id="builder"></div>
        </div>
    </form>
    <div class="col-md-12">
        <div class="col save text-right ">
            <button class="btn btn-secondary save-btn parse-sql" data-stmt="false">Guardar</button>
        </div>
    </div>
</div>

<script>
    // Add your javascript here
    $(function () {
        $('#builder').queryBuilder({
            plugins: ['bt-tooltip-errors'],

            operators: [
                'equal', 'not_equal', 'is_null',
                {type: 'contains_any', nb_inputs: 2, multiple: true, apply_to: ['string']},
            ],

            filters: [{
                id: 'assignedto',
                label: 'Assigned To',
                type: 'string',
                operators: ['equal', 'not_equal', 'is_null']
            }, {
                id: 'category',
                label: 'Category',
                type: 'string',
                operators: ['equal', 'not_equal', 'is_null']
            }, {
                id: 'correlation',
                label: 'Correlation',
                type: 'string',
                operators: ['equal', 'not_equal', 'is_null']
            }, {
                id: 'Description',
                label: 'Description',
                type: 'string',
                operators: ['equal', 'not_equal', 'is_null']
            }, {
                id: 'lcs',
                label: 'Lifecycle State',
                type: 'string',
                operators: ['equal', 'not_equal', 'is_null']
            }, {
                id: 'priority',
                label: 'Priority',
                type: 'string',
                operators: ['equal', 'not_equal', 'is_null']
            }, {
                id: 'severity',
                label: 'Severity',
                type: 'string',
                operators: ['equal', 'not_equal', 'is_null']
            }, {
                id: 'subcategory',
                label: 'Subcategory',
                type: 'string',
                operators: ['equal', 'not_equal', 'is_null']
            }, {
                id: 'title',
                label: 'Title',
                type: 'string',
                operators: ['equal', 'not_equal', 'is_null']
            }, {
                id: 'type',
                label: 'Type',
                type: 'string',
                operators: ['equal', 'not_equal', 'is_null']
            }, {
                id: 'application',
                label: 'Application',
                type: 'string',
                operators: ['equal', 'not_equal', 'is_null']
            },
            ]
        });


        // get rules from SQL
        $('.parse-sql').on('click', function () {
            let name = $('#name').val();
            let description = $('#description').val();
            var res = $('#builder').queryBuilder('getSQL', $(this).data('stmt'), false);
            var sql = res.sql + (res.params ? '\n\n' + JSON.stringify(res.params, undefined, 2) : '');
            let _token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: "{{ URL::route('filter.store') }}",
                data: {
                    _token: _token,
                    name: name,
                    description: description,
                    sql: sql
                },
                success: function () {
                    console.log('Success');
                    opener.location.reload();
                    window.close();
                }
            })
        });
    });
</script>
</body>
</html>
