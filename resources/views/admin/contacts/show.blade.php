@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 text-right">
                            @if ((Auth::user()->can('users_manage')))
                                <a href="{{ route('contact.edit', $contact->id), 'edit' }}">
                                    <button type="button" class="btn btn-circle btn-primary" onclick="javascript();">
                                        <i class="mdi mdi-pencil"></i>
                                    </button>
                                </a>
                            @endif
                        </div>
                    </div>
                    <h4 class="card-title font-weight-bold">Detalle Contacto </h4>
                    <div class="col-12 form-group m-t-40 row">
                        <label class="col-auto col-form-label text-right font-weight-bold">Nombre:</label>
                        <label class="col-auto col-form-label">{{ $contact->name . ' ' . $contact->last_name }}</label>
                    </div>
                    <div class="col-12 form-group m-t-10 row">
                        <label class="col-auto col-form-label text-right font-weight-bold">Correo:</label>
                        <label class="col-auto col-form-label">{{ $contact->email }}</label>
                    </div>
                    <div class="col-12 form-group m-t-10 row">
                        <label class="col-auto col-form-label text-right font-weight-bold">Telefono:</label>
                        <label class="col-auto col-form-label">{{ $contact->telephone }}</label>
                    </div>
                    <br>
                    <div class="row text-right">
                        <div class="col">
                            <a href="{{ route('contact.index') }}" class="btn btn-warning">Cancelar </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
