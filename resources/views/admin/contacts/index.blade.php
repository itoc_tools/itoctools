@extends('layouts.master')

@section('content')
    <div class="col pb-3 text-right">
        <a href="{{URL::to('contacts/create')}}" class="btn bg-secondary">Nuevo
            Contacto</a>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Número de teléfono</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contacts as $contact)
                            <tr>
                                <td>{{$contact->id}}</td>
                                <td>
                                    <a href="{{route('contact.show', [$contact->id])}}">{{$contact->name . ' ' . $contact->last_name}}</a>
                                </td>
                                <th>{{$contact->email}}</th>
                                <th>{{$contact->telephone}}</th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
