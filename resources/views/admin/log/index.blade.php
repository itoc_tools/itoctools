@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">

            </div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Asunto</th>
                    <th>URL</th>
                    <th>Metodo</th>
                    <th>Ip</th>
                    <th width="300px">Agente</th>
                    <th>Usuario</th>
                    <th>Fecha</th>
                </tr>
                </thead>
                <tbody>
                @if($logs->count())
                    @foreach($logs as $key => $log)
                        <tr>
                            <td>{{ $log->id }}</td>
                            <td>{{ $log->subject }}</td>
                            <td class="text-success">{{ $log->url }}</td>
                            <td><label class="label label-info">{{ $log->method }}</label></td>
                            <td>{{ $log->ip }}</td>
                            <td>{{ $log->agent }}</td>
                            <td>{{ $log->user_name }}</td>
                            <td>{{ $log->created_at }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
