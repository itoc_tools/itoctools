@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-body" style="height: 80vh">
            <div class="form-group">
                <div class="form-group m-t-40 row">
                    <label for="name" class="col-2 col-form-label text-right">Nombre</label>
                    <div class="col-6">
                        <input type="text" class="form-control form-control-line" placeholder="nombre" id="name"
                               name="name" value="{{ old('name') }}" required>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
