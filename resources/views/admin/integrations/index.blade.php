@extends('layouts.master')

<style>
    .add-integration {
        width: 100vh;
        cursor: pointer;
        text-align: center;
        margin: 0 !important;
    }

    .add-integration-border {
        border: dashed #dddddd !important;
    }

    .add-integration-border:hover {
        border: dashed #000000 !important;
    }

    .card-integration {
        margin: 0 !important;
        cursor: pointer !important;
        text-align: center !important;
        background-color: #F4F4F4 !important;
        box-shadow: 0px 10px 15px -3px rgba(0, 0, 0, 0.1) !important;
    }

    .card-integration:hover {
        box-shadow: 0px 10px 15px -3px rgba(0, 0, 0, 0.2) !important;
    }

    .img-integrations {
        height: auto !important;
        max-width: 250px !important;
    }
</style>

@section('content')
@if (session()->get('success'))
    <div id="alert-message" class="alert alert-success">
        <i class="fas fa-check"></i> {{ session()->get('success') }}
    </div>
@endif
@if ($errors->any())
    <div id="alert-message" class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <i class="fas fa-times"></i> {{ $error }}<br>
        @endforeach
    </div>
@endif
<div class="card">
    <div class="card-body" style="height: 80vh;">
        @if ($integrations->isEmpty())
            <div class="d-flex align-items-center justify-content-center" style="height: 80vh;">
                <div class="text-center">
                    <p>
                        <img alt="integrations" class="img-integrations" src="{{ asset('./images/svg/integrations.svg') }}">
                    </p>
                    <h5>
                        <p>Incorpora las integraciones que necesites para enviar notificaciones</p>
                    </h5>
                    <button class="btn btn-outline-dark" type="button" data-target="#create-modal" data-toggle="modal">
                        Añadir Integración
                    </button>
                </div>
            </div>
        @else
            <div class="row" style="row-gap: 1rem;">
                @foreach ($integrations as $integration)
                    <div class="col-3">
                        <div class="card card-integration">
                            <div class="card-header">
                                <div class="card-actions">
                                    <button class="btn btn-outline-success" title="Comprobar Conexión" type="button" onclick="check_connection({{ $integration->id }});">
                                        <i class="fas fa-globe"></i>
                                    </button>
                                    <button class="btn btn-outline-warning" title="Editar" type="button" onclick="update_record({{ $integration->id }});">
                                        <i class="fas fa-pencil-alt"></i>
                                    </button>
                                    <button class="btn btn-outline-danger" title="Eliminar" type="button" onclick="delete_record({{ $integration->id }});">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <h6 class="card-title">{{ $integration->name }}</h6>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-3 d-flex align-items-center">
                    <div class="card add-integration">
                        <div class="card-body add-integration-border">
                            <button class="btn btn-outline-dark" type="button" data-target="#create-modal" data-toggle="modal">
                                Añadir Integración
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

<div id="create-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true" aria-labelledby="create-modal-label" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="create-modal-label" class="modal-title">Añadir Integración</h5>
            </div>
            <form id="create-form" action="{{ route('integrations.store') }}" autocomplete="off" method="POST">
                @csrf
                <input id="update-method" type="hidden" name="_method" value="PATCH">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="name">Nombre</label>
                        <div class="col-sm-9">
                            <input id="name" class="form-control" name="name" placeholder="Ingresa el nombre de la integración" type="text" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="description">Descripción</label>
                        <div class="col-sm-9">
                            <textarea id="description" class="form-control" name="description" placeholder="Ingresa una descripción para la integración" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <select id="method" class="form-control form-select" name="method" required>
                                <option value="GET" selected>GET</option>
                                <option value="POST">POST</option>
                            </select>
                        </div>
                        <div class="col-sm-9">
                            <input id="endpoint" class="form-control" name="endpoint" placeholder="Ingresa la dirección de la petición" type="text" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <ul id="tab" class="nav nav-tabs" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a id="auth-tab" class="nav-link" href="#auth" role="tab" aria-controls="auth" aria-selected="false" data-toggle="tab">Authorization</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a id="headers-tab" class="nav-link" href="#headers" role="tab" aria-controls="headers" aria-selected="false" data-toggle="tab">Headers</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a id="body-tab" class="nav-link" href="#body" role="tab" aria-controls="body" aria-selected="false" data-toggle="tab">Body</a>
                                </li>
                            </ul>
                            <div id="tab-content" class="tab-content">
                                <div id="auth" class="tab-pane fade mt-4" role="tabpanel" aria-labelledby="auth-tab">
                                    <div class="form-group row">
                                        <label for="type" class="col-sm-3 col-form-label">Type</label>
                                        <div class="col-sm-9">
                                            <select id="type" class="form-control form-select" name="auth_type" required>
                                                <option value="" disabled hidden selected>Type</option>
                                                <option value="Basic Auth">Basic Auth</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="username" class="col-sm-3 col-form-label">Username</label>
                                        <div class="col-sm-9">
                                            <input id="username" class="form-control" name="auth_params[username]" placeholder="Username" type="text" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="col-sm-3 col-form-label">Password</label>
                                        <div class="col-sm-9">
                                            <input id="password" class="form-control" name="auth_params[password]" placeholder="Password" type="text" required>
                                        </div>
                                    </div>
                                </div>
                                <div id="headers" class="tab-pane fade mt-4" role="tabpanel" aria-labelledby="headers-tab">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Key</label>
                                        <label class="col-sm-8 col-form-label">Value</label>
                                        <div class="col-sm-1">
                                            <button class="btn btn-success h-100 w-100" type="button" onclick="create_header();">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <input id="Content-Type-Key" class="form-control" name="headers[0][key]" placeholder="Key" type="text" value="Content-Type" readonly>
                                        </div>
                                        <div class="col-sm-8">
                                            <input id="Content-Type-Value" class="form-control" name="headers[0][value]" placeholder="Value" type="text" value="application/json" readonly>
                                        </div>
                                        <div class="col-sm-1"></div>
                                    </div>
                                </div>
                                <div id="body" class="tab-pane fade mt-4" role="tabpanel" aria-labelledby="body-tab">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <div class="custom-control custom-radio">
                                                <input id="radio-1" class="custom-control-input" name="body_type" type="radio" value="params" required>
                                                <label class="custom-control-label" for="radio-1">params</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="custom-control custom-radio">
                                                <input id="radio-2" class="custom-control-input" name="body_type" type="radio" value="raw" required>
                                                <label class="custom-control-label" for="radio-2">raw</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <select id="body-params-type" class="form-control form-select" name="body_params_type" disabled required>
                                                <option value="" disabled hidden selected>Type</option>
                                                <option value="text/html">HTML</option>
                                                <option value="application/javascript">JavaScript</option>
                                                <option value="application/json">JSON</option>
                                                <option value="text/plain">Text</option>
                                                <option value="application/xml">XML</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="params">
                                        <div class="form-group row">
                                            <label class="col-sm-4 offset-sm-3 col-form-label">Key</label>
                                            <label class="col-sm-4 col-form-label">Value</label>
                                            <div class="col-sm-1">
                                                <button class="btn btn-success h-100 w-100" type="button" onclick="create_param();">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="raw">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <textarea id="summernote-area" name="body_params" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-outline-dark" type="submit">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="delete-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true" aria-labelledby="delete-modal-label" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="delete-modal-label" class="modal-title">Eliminar Integración</h5>
            </div>
            <form id="delete-form" autocomplete="off" method="POST">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <p>¿Estás seguro que deseas eliminar esta integración?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-outline-dark" type="submit">Eliminar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="connection-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true" aria-labelledby="connection-modal-label" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="connection-modal-label" class="modal-title">Comprobar Conexión</h5>
            </div>
            <form id="connection-form" autocomplete="off" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nombre</label>
                        <div class="col-sm-9">
                            <input id="connection-name" class="form-control" placeholder="Nombre de la integración" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Descripción</label>
                        <div class="col-sm-9">
                            <textarea id="connection-description" class="form-control" placeholder="Descripción para la integración" disabled></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <input id="connection-method" class="form-control" placeholder="Método de la petición" type="text" disabled>
                        </div>
                        <div class="col-sm-9">
                            <input id="connection-endpoint" class="form-control" placeholder="Dirección de la petición" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a id="connect-auth-tab" class="nav-link" href="#connect-auth" role="tab" aria-controls="connect-auth" aria-selected="false" data-toggle="tab">Authorization</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a id="connect-headers-tab" class="nav-link" href="#connect-headers" role="tab" aria-controls="connect-headers" aria-selected="false" data-toggle="tab">Headers</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a id="connect-body-tab" class="nav-link" href="#connect-body" role="tab" aria-controls="connect-body" aria-selected="false" data-toggle="tab">Body</a>
                                </li>
                                <li class="nav-item" role="presentation" style="display: none;">
                                    <a id="connect-response-tab" class="nav-link" href="#connect-response" role="tab" aria-controls="connect-response" aria-selected="false" data-toggle="tab">Response</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="connect-auth" class="tab-pane fade mt-4" role="tabpanel" aria-labelledby="connect-auth-tab">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Type</label>
                                        <div class="col-sm-9">
                                            <input id="connection-auth_type" class="form-control" placeholder="Tipo de autorización" type="text" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div id="connect-headers" class="tab-pane fade mt-4" role="tabpanel" aria-labelledby="connect-headers-tab"></div>
                                <div id="connect-body" class="tab-pane fade mt-4" role="tabpanel" aria-labelledby="connect-body-tab"></div>
                                <div id="connect-response" class="tab-pane fade mt-4" role="tabpanel" aria-labelledby="connect-response-tab">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <textarea id="summernote-response"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default connection-button" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-outline-dark connection-button" type="submit">Comprobar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('allJquery')
@include('includes.admin.methods_documents')
<script>
    let dynamic = 1;

    $(function() {
        $('#summernote-area').summernote({
            height: 150, 
            maxHeight: 150, 
            toolbar: false, 
            callbacks: {
                onBlurCodeview: function(contents, $editable) {
                    $('#summernote-area').val(contents);
                }, 
                onInit: function() {
                    $('#summernote-area').summernote('codeview.toggle');
                }
            }
        });

        $('#summernote-response').summernote({
            height: 150, 
            maxHeight: 150, 
            toolbar: false, 
            callbacks: {
                onInit: function() {
                    $('#summernote-response').summernote('codeview.toggle');

                    $('#summernote-response').summernote('code', '');
                }
            }
        });

        $('#create-modal').on('hidden.bs.modal', function() {
            reset_form();
        });

        $('#connection-modal').on('hidden.bs.modal', function() {
            $('.dynamic-element').remove();

            $('#connect-response-tab').closest('li').hide();
        });

        $('input[type="radio"][name="body_type"]').on('change', function() {
            let val = $('input[type="radio"][name="body_type"]:checked').val();

            $('.params-input, #body-params-type, #summernote-area').prop('disabled', true);

            $('#params, #raw').hide();

            if (val === 'params') {
                $('#params').show();

                $('.params-input').prop('disabled', false);
            } else if (val === 'raw') {
                $('#raw').show();

                $('#body-params-type, #summernote-area').prop('disabled', false);
            }
        });

        $('#body-params-type').on('change', function() {
            let content = 'application/json';

            if ($(this).val() !== null) {
                content = $(this).val();
            }

            $('#Content-Type-Value').val(content);
        });

        $('#connection-form').on('submit', function(e) {
            $('.connection-button').prop('disabled', true);

            $('.nav-tabs a[href="#connect-body"]').tab('show');

            $('#connect-response-tab').closest('li').hide();

            e.preventDefault();

            let form = $(e.currentTarget);

            let json = {};

            $.each(form.serializeArray(), function() {
                json[this.name] = this.value || '';
            });

            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: json,
                success: function(response) {
                    console.log(response);
                    $('#summernote-response').summernote('code', response);
                },
                error: function(error) {
                    $('#summernote-response').summernote('code', error.responseJSON);
                },
                complete: function() {
                    $('#connect-response-tab').closest('li').show();

                    $('.nav-tabs a[href="#connect-response"]').tab('show');

                    $('.connection-button').prop('disabled', false);
                },
            });

            return false;
        });

        setTimeout(function() {
            $('#alert-message').slideUp('normal', function() {
                $(this).remove();
            });
        }, 5000);

        reset_form();
    });

    function check_connection(id) {
        let show_route = "{{ route('integrations.show', 'id') }}";
        show_route = show_route.replace('id', id);

        let connection_route = "{{ route('integrations.connect', 'id') }}";
        connection_route = connection_route.replace('id', id);

        $.get(show_route, function(integration) {
            Object.keys(integration).forEach(key => {
                $(`#connection-${ key }`).val(integration[key]);
            });

            let auth_params = JSON.parse(integration.auth_params);

            Object.keys(auth_params).forEach(key => {
                create_element('#connect-auth', key, auth_params[key]);
            });

            let headers = JSON.parse(integration.headers);

            Object.keys(headers).forEach(key => {
                create_element('#connect-headers', key, headers[key]);
            });

            if (integration.body_type === 'params') {
                JSON.parse(integration.body_params).forEach(element => {
                    create_body_element(element.key, element.value, element.required);
                });
            } else {
                $('#connect-body').append(`
                    <div class="form-group row dynamic-element">
                        <div class="col-sm-12">
                            <textarea id="summernote-dynamic" name="body"></textarea>
                        </div>
                    </div>
                `);

                $('#summernote-dynamic').summernote({
                    height: 150, 
                    maxHeight: 150, 
                    toolbar: false, 
                    callbacks: {
                        onBlurCodeview: function(contents, $editable) {
                            $('#summernote-dynamic').val(contents);
                        }
                    }
                });

                $('#summernote-dynamic').summernote('codeview.toggle');

                $('#summernote-dynamic').summernote('code', integration.body_params);
            }

            $('.nav-tabs a[href="#connect-body"]').tab('show');

            $('#connection-form').attr('action', connection_route);

            $('#connection-modal').modal('show');
        });
    }

    function update_record(id) {
        let show_route = "{{ route('integrations.show', 'id') }}";
        show_route = show_route.replace('id', id);

        let edit_route = "{{ route('integrations.update', 'id') }}";
        edit_route = edit_route.replace('id', id);

        $.get(show_route, function(integration) {
            $('#name').val(integration.name);
            $('#description').val(integration.description);
            $('#method').val(integration.method).trigger('change');
            $('#endpoint').val(integration.endpoint);
            $('#type').val(integration.auth_type);
            $('input[name="body_type"]').val([integration.body_type]).trigger('change');

            let auth = JSON.parse(integration.auth_params);

            $('#username').val(auth.username);
            $('#password').val(auth.password);

            let headers = $.map(JSON.parse(integration.headers), function(value, key) {
                return [[key, value]];
            });

            headers.forEach(header => {
                create_header(header);
            });

            if (integration.body_type === 'params') {
                let body_params = JSON.parse(integration.body_params);

                body_params.forEach(data => {
                    create_param(data);
                });
            } else {
                $('#body-params-type').val(integration.body_params_type).trigger('change');

                $('#summernote-area').summernote('code', integration.body_params);
            }

            $('#update-method').prop('disabled', false);

            $('#create-modal-label').text('Editar Integración');

            $('#create-form').attr('action', edit_route);

            $('#create-modal').modal('show');
        });
    }

    function delete_record(id) {
        let route = "{{ route('integrations.destroy', 'id') }}";
        route = route.replace('id', id);

        $('#delete-form').attr('action', route);

        $('#delete-modal').modal('show');
    }

    function reset_form() {
        let route = "{{ route('integrations.store') }}";

        $('#create-form').trigger('reset');

        $('.dynamic-element').remove();

        $('#body-params-type').val('').change();

        $('#summernote-area').summernote('code', '');

        $('#radio-1').change();

        $('#update-method').prop('disabled', true);

        $('#create-modal-label').text('Añadir Integración');

        $('#create-form').attr('action', route);

        $('.nav-tabs a[href="#auth"]').tab('show');

        dynamic = 1;
    }

    function create_header(pair = []) {
        if (pair[0] !== undefined) {
            if (pair[0].toLowerCase() === 'content-type') {
                return;
            }
        }

        $('#headers').append(`
            <div class="form-group row dynamic-element">
                <div class="col-sm-3">
                    <input class="form-control" name="headers[${ dynamic }][key]" placeholder="Key" type="text" value="${ pair[0] !== undefined ? pair[0] : '' }" required>
                </div>
                <div class="col-sm-8">
                    <input class="form-control" name="headers[${ dynamic }][value]" placeholder="Value" type="text" value="${ pair[1] !== undefined ? pair[1] : '' }" required>
                </div>
                <div class="col-sm-1">
                    <button class="btn btn-danger h-100 w-100" type="button" onclick="destroy_element(this);">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
        `);

        dynamic++;
    }

    function create_param(params = []) {
        $('#params').append(`
            <div class="form-group row dynamic-element">
                <div class="col-sm-3">
                    <div class="custom-control custom-checkbox">
                        <input id="params-radio-${ dynamic }" class="custom-control-input params-input" name="body_params[${ dynamic }][required]" type="checkbox" ${ params['required'] !== undefined ? 'checked' : '' }>
                        <label class="custom-control-label" for="params-radio-${ dynamic }">Requerido</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <input class="form-control params-input" name="body_params[${ dynamic }][key]" placeholder="Key" type="text" value="${ params['key'] !== undefined ? params['key'] : '' }" required>
                </div>
                <div class="col-sm-4">
                    <input class="form-control params-input" name="body_params[${ dynamic }][value]" placeholder="Value" type="text" value="${ params['value'] !== undefined ? params['value'] : '' }" required>
                </div>
                <div class="col-sm-1">
                    <button class="btn btn-danger h-100 w-100" type="button" onclick="destroy_element(this);">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
        `);

        dynamic++;
    }

    function destroy_element(element) {
        let row = $(element).closest('div.form-group.row');

        row.remove();
    }

    function create_element(element, key, value, required = null) {
        $(element).append(`
            <div class="form-group row dynamic-element">
                <label class="col-sm-3 col-form-label">${ key }</label>
                <div class="col-sm-9">
                    <input class="form-control" placeholder="${ key }" type="text" value="${ value }" disabled>
                </div>
            </div>
        `);
    }

    function create_body_element(key, value, required) {
        $('#connect-body').append(`
            <div class="form-group row dynamic-element">
                <label class="col-sm-3 col-form-label">${ key }</label>
                <div class="col-sm-9">
                    <input class="form-control" name="${ key }" placeholder="${ key }" type="text" value="${ value }" ${ required !== undefined ? 'required' : '' }>
                </div>
            </div>
        `);
    }
</script>
@endsection