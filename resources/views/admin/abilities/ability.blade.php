@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <!--<h4 class="card-title">Habilidades</h4>-->

                @if (Auth::user()->can('users_manage') || Auth::user()->can('lead_operator'))
                <div class="row">
                    <a href="{{ route('abilities.create') }}">
                        <button type="button" class="btn btn-rounded btn-primary">Crear nuevo</button>
                    </a>
                </div>
                @endif
                <div class="row">
                    <div class="col">
                        <table class="table color-table  table-responsive table-striped table-hover w-100 d-block d-md-table table-md" id="abilities-table"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('allJquery')
@include('includes.admin.methods_abilities')
@endsection