@extends('layouts.master')
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">AWS</h4>
                    <h6 class="card-subtitle">Detalles de servicios de cliente <b>{{$client}}</b></h6>
                    <div class="row justify-content-between mb-2">
                        <div class="col-3">
                            <button type="button" class="btn btn-primary">Configurar Alarma Masiva</button>
                        </div>

                        <div class="col-2 float-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#listaAlarmasModal">Lista de Alarmas</button>
                        </div>
                    </div>
                    <div>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th><input type="checkbox" name="" id=""></th>
                                <th><small class="font-weight-bold">Id de Instancia</small></th>
                                <th><small class="font-weight-bold">Tag</small></th>
                                <th><small class="font-weight-bold">Status</small></th>
                                <th><small class="font-weight-bold">Tipo</small></th>
                                <th><small class="font-weight-bold">Servicio</small></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($instances as $val)
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td>
                                        {{$val['InstanceId']}}
                                    </td>
                                    <td>
                                        {{$val['Tag']}}
                                    </td>
                                    <td>
                                        {{$val['InstanceStatus']}}
                                    </td>
                                    <td>
                                        {{$val['Type']}}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#verServicioModal">Ver</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


<!-- Modal Ver Servicio -->
<div class="modal fade" id="verServicioModal" tabindex="-1" role="dialog" aria-labelledby="verServicioModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="verServicioModalLabel">Servicio</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            ...
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        </div>
        </div>
    </div>
</div>


<!-- Modal Lista Alarmas -->
<div class="modal fade" id="listaAlarmasModal" tabindex="-1" role="dialog" aria-labelledby="listaAlarmasModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="listaAlarmasModalLabel">Lista de Alarmas para configurar</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            ...
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        </div>
        </div>
    </div>
</div>


@endsection
