@extends('layouts.master')
@section('content')

	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">AWS</h4>
					<h6 class="card-subtitle">Servicios de cliente <b>{{$clientName}}</b></h6>
					<div class="row">
						@foreach($services as $key => $val)
							@if($val['TOTAL'] > 0)
								<div class="col-3">
									<div class="card text-white bg-info">
										<img class="card-img-top" src="{{url('/images/aws/Amazon_AWS_' . strtoupper($key) . '.png')}}" style="height: 160px !important;" height="160px" alt="{{strtoupper($key)}}">
										<div class="card-body">
											<h5 class="card-title">Instancias {{strtoupper($key)}}</h5>
											<p class="card-text">Instancias Iniciadas: {{$val['INITIALIZED']}}<br>
											Instancias Detenidas: {{$val['STOPPED']}}<br>
											Total Instancias: {{$val['TOTAL']}}</p>
											<a href="/aws/instances/{{strtolower($key)}}/{{$accountId}}/{{$clientId}}" class="btn btn-primary">Ver Instancias</a>
										</div>
									</div>
								</div>
							@endif
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
