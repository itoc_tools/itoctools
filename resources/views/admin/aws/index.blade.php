@extends('layouts.master')
@section('content')

	@if ($p_users['statusCode'] === 'fail' || $p_alerts['statusCode'] === 'fail' || $p_clients['statusCode'] === 'fail')
		<div class="row">
			<div class="col-12">
				<div class="card text-white bg-warning">
					<div class="card-body">
						@if ($p_users['statusCode'] === 'fail')
							{{$p_users['statusMessage']}}<br>
						@endif
						@if ($p_alerts['statusCode'] === 'fail')
							{{$p_alerts['statusMessage']}}<br>
						@endif
						@if ($p_clients['statusCode'] === 'fail')
							{{$p_clients['statusMessage']}}<br>
						@endif
					</div>
				</div>
			</div>
		</div>
	@endif

	<div class="row">
		<div class="col-12">
			<div class="card-group">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<div class="d-flex no-block align-items-center">
									<div>
										<h3>
											<i class="fa fa-users text-primary"></i>
										</h3>
										<p class="text-muted">Usuarios</p>
									</div>
									<div class="ml-auto">
										<h2 class="counter text-primary">{{count($p_users['data'])}}</h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<div class="d-flex no-block align-items-center">
									<div>
										<h3>
											<i class="fa fa-star text-success"></i>
										</h3>
										<p class="text-muted">Clientes</p>
									</div>
									<div class="ml-auto">
										<h2 class="counter text-success">{{count($p_clients['data'])}}</h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<div class="d-flex no-block align-items-center">
									<div>
										<h3>
											<i class="fa fa-bell text-warning"></i>
										</h3>
										<p class="text-muted">Alertas</p>
									</div>
									<div class="ml-auto">
										<h2 class="counter text-warning">{{count($p_alerts['data'])}}</h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title">Usuarios AWS</h5>
				</div>
				@if (count($p_users['data']) > 0)
					<table class="table table-hover no-wrap">
						<thead>
							<tr>
								<th class="text-jutify"><small class="font-weight-bold">Usuario</small></th>
								<th class="text-jutify"><small class="font-weight-bold">Id AWS</small></th>
								<th class="text-jutify"><small class="font-weight-bold">Región</small></th>
								<th class="text-jutify"><small class="font-weight-bold">Cuentas</small></th>
							</tr>
						</thead>
						<tbody>
						@foreach ($p_users['data'] as $val)
							<tr>
								<td>
									<span class="text-monospace">{{$val['Usuario']}}</span>
								</td>
								<td>
									{{$val['Idaws']}}
								</td>
								<td>
									<span class="badge badge-success badge-pill">{{$val['Region']}}</span>
								</td>
								<td>
									<a class="btn btn-sm btn-primary" role="button" href="/aws/accounts/{{$val['Idaws']}}">
										<i class="icon-user"></i>
										<span class="ml-1">Cuentas</span>
									</a>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				@else
					<p class="card-text text-center">No se encontraron usuarios</p>
				@endif
			</div>
		</div>
		<!--
		<div class="col-6">
			<div class="card">
				<h2>Chart de Instancias</h2>
			</div>
		</div>
		-->
	</div>

	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title">Alertas configuradas por instancia</h5>
				</div>
				<div style="height: 300px !important;">
					<div class="scrollable-box">
						<div class="scrollable-container">
							<table class="table table-hover no-wrap">
								<thead>
									<tr>
										<th class="text-jutify"><small class="font-weight-bold">Nombre</small></th>
										<th class="text-jutify"><small class="font-weight-bold">Métrica</small></th>
										<th class="text-jutify"><small class="font-weight-bold">Namespace</small></th>
										<th class="text-jutify"><small class="font-weight-bold">Instancia</small></th>
									</tr>
								</thead>
								<tbody>
									@foreach ($p_alerts['data'] as $alert)
										<tr>
											<td>
												<span class="text-monospace">{{$alert['AlarmName']}}</span>
											</td>
											<td>
												{{$alert['MetricName']}}
											</td>
											<td>
												<span class="badge badge-success badge-pill">{{$alert['Namespace']}}</span>
											</td>
											<td>
												@if( is_array($alert['Dimensions']) && count($alert['Dimensions']) > 0 )
													@for($i = 0; $i < count($alert['Dimensions']); $i++ )
														@if( is_array($alert['Dimensions'][$i]) && count($alert['Dimensions'][$i]) > 0 && array_key_exists('Name', $alert['Dimensions'][$i]) && ($alert['Dimensions'][$i]['Name'] == 'InstanceId' || $alert['Dimensions'][$i]['Name'] == 'DBInstanceIdentifier'))
															{{$alert['Dimensions'][$i]['Value']}}
															@break
														@endif
													@endfor
												@endif
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<style>
		.scrollable-box {
			position: relative;
			width: 100%;
			height: 100%;
			margin: 0px;
			padding: 0px;
			overflow-y: scroll;
			background-color: none;
		}
		.scrollable-box > .scrollable-container {
			width: 100%;
			margin: 0px;
			padding: 0px;
			background-color: none;
		}
	</style>

@endsection
