@extends('layouts.master')
@section('content')

	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">AWS</h4>
					<h6 class="card-subtitle">Detalles de cuentas de clientes</h6>
				</div>
				<table class="table table-striped">
					<thead>
						<tr>
							<th><small class="font-weight-bold">Id AWS</small></th>
							<th><small class="font-weight-bold">Nombre</small></th>
							<th><small class="font-weight-bold">Cliente</small></th>
							<th><small class="font-weight-bold">Email</small></th>
							<th><small class="font-weight-bold">Status</small></th>
							<th><small class="font-weight-bold">Servicios</small></th>
						</tr>
					</thead>
					<tbody>
					@foreach($instances as $val)
						<tr>
							<td>
								{{$val['Id']}}
							</td>
							<td>
								{{$val['Name']}}
							</td>
							<td>
								{{$val['Client']}}
							</td>
							<td>
								{{$val['Email']}}
							</td>
							<td>
								@if ($val['Status'] === 'ACTIVE')
									<span><i class="text-success fa fa-circle" title="Cuenta Activa"></i></span>
								@else
									<span><i class="text-danger fa fa-circle" title="Acuenta Inactiva"></i></span>
								@endif
								</td>
							<td>
								<a class="btn btn-primary" role="button" href="/aws/services/{{$userId}}/{{$val['Id']}}">
									<i class="icon-wrench"></i>
									<span class="ml-2">Servicios</span>
								</a>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

@endsection
