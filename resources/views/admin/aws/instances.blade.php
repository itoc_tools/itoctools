@extends('layouts.master')
@section('content')

<div id="app">

	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">AWS</h4>
					<h6 class="card-subtitle">Instancias de {{strtoupper($instanceType)}}</h6>
					<div class="row">
						<div class="col-12">
							<div class="row justify-content-between mb-2">
								<div class="col-3">
									<button type="button" id="alertasMasivasModalButton" disabled class="btn btn-primary" data-toggle="modal" data-target="#alertasMasivasModal" @click="$refs.childref3.loadAlertsTemplates()">Configurar Alerta Masiva</button>
								</div>
								<div class="col-2 float-right">
									<button type="button" class="btn btn-success" data-toggle="modal" data-target="#listaAlarmasModal" @click="$refs.childref2.openListAlarmTemplate('{{$instanceType}}')">Lista de Alertas</button>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>
											<button class="btn btn-primary" id="selectButton" @click="$refs.childref3.selectAllCheckbox()">Select All</button>
										</th>
										<th class="text-jutify">
											<small class="font-weight-bold">Id Instancia</small>
										</th>
										<th class="text-jutify">
											<small class="font-weight-bold">Estatus</small>
										</th>
										<th class="text-jutify">
											<small class="font-weight-bold">Tag</small>
										</th>
										<th class="text-jutify">
											<small class="font-weight-bold">Ver</small>
										</th>
									</tr>
								</thead>
								<tbody>
									@foreach($instancesList as $instance)
										<tr>
											<td>
												<input type="checkbox" id="checked_{{$instance['InstanceId']}}" value="{{$instance['InstanceId']}}" @click="$refs.childref3.checkChangeFunction('{{$instance['InstanceId']}}')">
											</td>
											<td>
												{{$instance['InstanceId']}}
											</td>
											<td>
												<h3>
													<li class="fa fa-arrow-circle-up text-success" title="Running" v-if="'{{strtolower($instance['InstanceStatus'])}}' === 'running'"></li>
												</h3>
												<h3>
													<li class="fa fa-arrow-circle-down text-warning" title="Stopped" v-if="'{{strtolower($instance['InstanceStatus'])}}' === 'stopped'"></li>
												</h3>
											</td>
											<td>
												{{$instance['NameInstance']}}
											</td>
											<td>
												<button type="button" class="btn btn-primary" @click="$refs.childref.loadAlarmsByInstance('{{$instance['InstanceId']}}', '{{$accountId}}', '{{$clientId}}', '{{$instanceType}}')" data-toggle="modal" data-target="#verServicioModal">Ver</button>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Alertas Masivas -->
	<div class="modal fade" id="alertasMasivasModal" tabindex="-1" role="dialog" aria-labelledby="alertasMasivasModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="alertasMasivasModalLabel">Configuración de Alertas Masivas</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-12">
							<configuracionalertasmasivas-component ref="childref3"></configuracionalertasmasivas-component>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal" @click="$refs.childref.closeCreateForm()">Cerrar</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Lista Plantilla Alarmas -->
	<div class="modal fade" id="listaAlarmasModal" tabindex="-1" role="dialog" aria-labelledby="listaAlarmasModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="listaAlarmasModalLabel">Lista de Alertas para configurar</h5>
				<button type="button" class="close" data-dismiss="modal" @click="$refs.childref2.closeMetricTemplate()" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-12">
						<listaplantillaalertas-component ref="childref2"></listaplantillaalertas-component>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" @click="$refs.childref2.closeMetricTemplate()">Cerrar</button>
			</div>
			</div>
		</div>
	</div>

	<!-- Modal Alarmas Instancia -->
	<div class="modal fade" id="verServicioModal" tabindex="-1" role="dialog" aria-labelledby="verServicioModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="verServicioModalLabel">Instancia</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="$refs.childref.closeCreateForm()">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-12">
							<alertasdeservicios-component ref="childref"></alertasdeservicios-component>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal" @click="$refs.childref.closeCreateForm()">Cerrar</button>
				</div>
			</div>
		</div>
	</div>

</div>

@endsection