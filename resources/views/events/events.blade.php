@extends('layouts.master')

@section('content')
    <div class="row">
        @include('events.filters.general')
    </div>
@endsection

@section('allJquery')
    @include('events.common_components.scripts_functions')
    @include('includes.events.methods_massive_attributes')
    @include('includes.events.methods_graphics_reload')
@endsection