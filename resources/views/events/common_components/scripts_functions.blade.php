<script>
    let eventsDataTable = null;

    $.validator.addMethod('notIn', function(value, element, params) {
        let isContained = params.some(element => {
            return element.toLowerCase() === value.toLowerCase();
        });

        return !isContained;
    }, 'El valor ingresado no está permitido.');

    $.contextMenu({
        events: {
            preShow: function(options) {
                let index = eventsDataTable.row(options[0]).index();

                let selection = eventsDataTable.rows({
                    selected: true
                }).indexes().toArray();

                return selection.includes(index);
            }
        },
        items: {
            attribute: {
                className: 'contextmenu-item-custom-style',
                icon: 'fas fa-list',
                name: 'Añadir Atributo Personalizado...'
            },
            annotation: {
                className: 'contextmenu-item-custom-style',
                icon: 'fas fa-sticky-note',
                name: 'Añadir Anotación...'
            },
            state: {
                className: 'contextmenu-item-custom-style',
                icon: 'fas fa-recycle',
                name: 'Cambiar Estado...'
            },
            assign: {
                className: 'contextmenu-item-custom-style',
                icon: 'fas fa-user',
                name: 'Asignar a... Mi'
            },
            relate: {
                className: 'contextmenu-item-custom-style',
                icon: 'fas fa-network-wired',
                name: 'Relacionar Eventos'
            }
        },
        selector: '#events-table tbody tr',
        trigger: 'right',
        // callbacks
        callback: function(itemKey, opt) {
            switch (itemKey) {
                case 'annotation':
                    $('#massive-annotation-button').click();
                    break;
                case 'assign':
                    // $('#atm-button').click();
                    break;
                case 'attribute':
                    $('#massive-attribute-button').click();
                    break;
                case 'relate':
                    showEventGroupingModal();
                    break;
                case 'state':
                    $('#massive-state-change-button').click();
                    break;
            }
        }
    });

    $(function() {
        $('div.alert').fadeOut(5000);
        // NOTE: Objeto con la configuración por default para cada DataTable

        /* DOM. Cada elemento en DataTables tiene una letra asociada. Cada una de estas letras es utilizada para indicar donde debe aparecer cada elemento en el DOM.
            * l - length changing input control
            * f - filtering input
            * t - the table
            * i - table information summary
            * p - pagination control
            * r - processing display element
        Cada letra puede ser utlizada múltiples veces (con excepción de la tabla en sí). */

        eventsDataTable = $('#events-table').DataTable({
            ajax: '{{ $ajaxDTRoute }}',
            destroy: true,
            buttons: [{
                    className: 'btn btn-warning',
                    extend: 'excel',
                    text: '<i class="fa-regular fa-file-excel fa-lg"></i>'
                },
                {
                    className: 'btn btn-warning',
                    extend: 'pdf',
                    text: '<i class="fa-regular fa-file-pdf fa-lg"></i>'
                },
                {
                    className: 'btn btn-warning',
                    extend: 'print',
                    text: '<i class="fa-regular fa-print fa-lg"></i>'
                },
                {
                    className: 'btn btn-warning',
                    text: '<i class="fa-regular fa-table-pivot fa-lg"></i>',
                    action: function(e, dt, node, config) {
                        showColReorderModal();
                    }

                }
            ],
            colReorder: {
                enable: false
            },
            columnDefs: [{
                    className: 'text-center',
                    targets: [0, 1, 2, 4, 5, 6]
                },
                {
                    className: 'text-justify',
                    targets: [3, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                },
            ],
            columns: [{
                data: function(row, type, set, meta) {
                    //console.log(row);

                    if (row.im) {
                        return row.im;
                    } else if (row.im_fake) {
                        return row.im_fake;
                    }

                    return '';
                },
                render: function(data, type, row, meta) {
                    if (/^(IM|SD|RFC)\d+$/.test(data)) {
                        return `<span class="badge badge-info rounded w-100">${ data }</span>`;
                    } else if (data === '') {
                        return `<a class="badge badge-success rounded w-100" href="javascript:void(0)" onclick="window.open('{{ route('events.edit', ':event') }}', '_blank', 'height=650,width=700')">Crear IM</a>`.replace(':event', row.id_event);
                    }

                    return `<span class="badge badge-dark rounded w-100">${ data }</span>`;
                }
            }, {
                data: 'severity',
                orderable: false,
                render: function(data, type, row, meta) {
                    switch (data) {
                        case 'critical':
                            return '<img src="{{ asset("images/status/stat_error.png") }}" height="16" width="16">';
                        case 'major':
                            return '<img src="{{ asset("images/status/stat_majo.png") }}" height="16" width="16">';
                        case 'minor':
                            return '<img src="{{ asset("images/status/stat_mino.png") }}" height="16" width="16">';
                        case 'warning':
                            return '<img src="{{ asset("images/status/stat_warn.png") }}" height="16" width="16">';
                        case 'normal':
                            return '<img src="{{ asset("images/status/stat_norm.png") }}" height="16" width="16">';
                        default:
                            return '<img src="{{ asset("images/status/stat_unkn_11.png") }}" height="16" width="16">';
                    }
                },
                searchable: false
            }, {
                data: 'priority'
            }, {
                data: 'application'
            }, {
                data: 'state'
            }, {
                data: 'symptom_list',
                orderable: false,
                render: function(data, type, row, meta) {
                    if (data !== null) {
                        return '<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="18" height="18" viewBox="0 0 172 172" style="fill: #000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray=" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal;"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#2ecc71"><path d="M35.83333,21.5c-7.83362,0 -14.33333,6.49972 -14.33333,14.33333v21.5c0,7.83362 6.49972,14.33333 14.33333,14.33333h28.66667c7.83362,0 14.33333,-6.49972 14.33333,-14.33333v-21.5c0,-7.83362 -6.49972,-14.33333 -14.33333,-14.33333zM35.83333,35.83333h28.66667v21.5h-28.66667zM107.5,71.66667c-7.83362,0 -14.33333,6.49972 -14.33333,14.33333v50.16667c0,7.83362 6.49972,14.33333 14.33333,14.33333h28.66667c7.83362,0 14.33333,-6.49972 14.33333,-14.33333v-50.16667c0,-7.83362 -6.49972,-14.33333 -14.33333,-14.33333zM35.83333,86v14.33333h14.33333v-14.33333zM107.5,86h28.66667v50.16667h-28.66667zM35.83333,114.66667v14.33333h14.33333v-14.33333zM64.5,114.66667v14.33333h14.33333v-14.33333zM121.83333,114.66667c-3.95804,0 -7.16667,3.20863 -7.16667,7.16667c0,3.95804 3.20863,7.16667 7.16667,7.16667c3.95804,0 7.16667,-3.20863 7.16667,-7.16667c0,-3.95804 -3.20863,-7.16667 -7.16667,-7.16667z"></path></g></g></svg>';
                    }

                    return '';
                },
                searchable: false
            }, {
                data: 'annotation_list',
                orderable: false,
                render: function(data, type, row, meta) {
                    if (data !== null) {
                        return `<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="18" height="18" viewBox="0 0 172 172" style="fill: #000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray=" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal;"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#f1c40f"><path d="M141.65347,50.4132l-37.26667,-37.26667c-1.07787,-1.07787 -2.53413,-1.67987 -4.05347,-1.67987h-60.2c-6.33533,0 -11.46667,5.13133 -11.46667,11.46667v126.13333c0,6.33533 5.13133,11.46667 11.46667,11.46667h91.73333c6.33533,0 11.46667,-5.13133 11.46667,-11.46667v-94.6c0,-1.51933 -0.602,-2.9756 -1.67987,-4.05347zM103.2,120.4h-45.86667c-3.1648,0 -5.73333,-2.56853 -5.73333,-5.73333c0,-3.1648 2.56853,-5.73333 5.73333,-5.73333h45.86667c3.1648,0 5.73333,2.56853 5.73333,5.73333c0,3.1648 -2.56853,5.73333 -5.73333,5.73333zM114.66667,97.46667h-57.33333c-3.1648,0 -5.73333,-2.56853 -5.73333,-5.73333c0,-3.1648 2.56853,-5.73333 5.73333,-5.73333h57.33333c3.1648,0 5.73333,2.56853 5.73333,5.73333c0,3.1648 -2.56853,5.73333 -5.73333,5.73333zM103.2,57.33333c-3.1648,0 -5.73333,-2.56853 -5.73333,-5.73333v-29.21707l34.9504,34.9504z"></path></g></g></svg>`;
                    }

                    return '';
                },
                searchable: false
            }, {
                data: 'user_name',
                render: function(data, type, row, meta) {
                    if (data !== null) {
                        return `<span class="font-weight-bold">${ row.user_name }</span>`;
                    }

                    return data;
                }
            }, {
                data: 'event_age',
                render: function(data, type, row, meta) {
                    if (type === 'sort' || type === 'type') {
                        return moment().diff(moment(row.time_received_dt), 'seconds');
                    }

                    return data;
                }
            }, {
                data: 'time_reception',
                render: function(data, type, row, meta) {
                    if (type === 'sort' || type === 'type') {
                        return data;
                    }

                    return moment(data).format('DD/MM/YYYY hh:mm:ss A');
                }
            }, {
                data: function(row, type, set, meta) {
                    if (row.cResponsable) {
                        return row.cResponsable;
                    }

                    return '';
                }
            }, {
                data: function(row, type, set, meta) {
                    if (row.EventoZenoss_Id) {
                        return row.EventoZenoss_Id;
                    }

                    return '';
                }
            }, {
                data: function(row, type, set, meta) {
                    if (row.CO) {
                        return row.CO;
                    }

                    return '';
                }
            }, {
                data: 'object'
            }, {
                data: 'title'
            }, {
                data: 'id_event'
            }],
            dom: '<"row"<"#buttons-container.col d-flex justify-content-between"B>><"row mb-2"<"col"f>><"row"<"col"<"table-scrollable"rt>>>i',
            language: {
                url: '{{ asset("custom/plugins/DataTables/i18n/es-MX.json") }}'
            },
            order: [],
            paging: false,
            rowId: 'id_event',
            select: {
                blurable: true,
                info: false,
                style: 'os'
            },
            // callbacks
            initComplete: function(settings, json) {
                /* $('#events-table').colResizable({
                    liveDrag: true
                }); */
            }
        });

        $('#colreorder-form').validate({
            errorClass: 'error-feedback',
            errorPlacement: function(error, element) {
                error.appendTo(element.parents('div.form-group'));
            },
            rules: {
                name: {
                    notIn: ['default', 'predeterminada', 'predeterminado', 'por defecto']
                }
            },
            submitHandler: function(form) {
                let state = alterTableState();

                $(form).find('input[name="state"]').val(JSON.stringify(state));

                runWaitMe($(form).find('div.modal-content'));

                return true;
            }
        });

        $('#events-correlation-form').validate({
            errorClass: 'error-feedback',
            errorPlacement: function(error, element) {
                error.appendTo(element.parents('div.modal-body'));
            },
            submitHandler: function(form) {
                runWaitMe($(form).find('div.modal-content'));

                return true;
            }
        });

        $('#colreorder-name').select2({
            dropdownParent: $('#colreorder-modal'),
            language: 'es',
            placeholder: 'Selecciona una opción',
            tags: true,
            width: 'auto'
        });

        $('#colreorder-selector').multiSelect({
            dblClick: true,
            keepOrder: true,
            selectableHeader: `<div class="d-flex align-items-center justify-content-between bg-secondary px-2 py-1">
                    <span class="text-left">Columnas disponibles</span>
                    <button class="btn waves-effect waves-light btn-xs btn-dark" title="Mostrar todas las columnas" type="button" onclick="showAllColumns()">
                        <i class="fa-solid fa-chevron-right"></i>
                    </button>
                </div>`,
            selectionHeader: `<div class="d-flex align-items-center justify-content-between bg-secondary px-2 py-1">
                    <button class="btn waves-effect waves-light btn-xs btn-dark" title="Ocultar todas las columnas" type="button" onclick="hideAllColumns()">
                        <i class="fa-solid fa-chevron-left"></i>
                    </button>
                    <span class="text-right">Mostrar estas columnas</span>
                </div>`,
            // callbacks
            afterInit: function(container) {
                let headers = eventsDataTable.columns().header().map(h => h.textContent).toArray();

                headers.forEach((value, key) => {
                    $('#colreorder-selector').multiSelect('addOption', {
                        value: `${ key }`,
                        text: value
                    });
                });

                $('.ms-selection > ul.ms-list').sortable({
                    axis: 'y'
                });
            }
        });

        $('#events-table tbody').on('dblclick', 'tr', function() {
            let data = eventsDataTable.row(this).data();
            let id = data['id_event'] ? data['id_event'] : '';

            let url = '{{ route("events.show", ":id") }}';
            url = url.replace(':id', id);

            let popup = window.open(url, data.id_event, 'height=450,width=1050');
            popup.focus();
        });

        $('#events-table tbody').on('contextmenu', 'tr', function(e) {
            e.preventDefault();
        });

        $('#colreorder-name').on('change', function() {
            let select = $(this);
            let state = select.find(':selected').data('state');

            hideAllColumns();

            select.parent().find('button.btn-outline-danger').prop('disabled', true);

            if (typeof state !== 'undefined') {
                state.visible.forEach(col => {
                    $('#colreorder-selector').multiSelect('select', col.toString());
                });

                select.parent().find('button.btn-outline-danger').prop('disabled', false);
            } else if (select.val() === 'reset') {
                showAllColumns();
            }
        });

        setInterval(function() {
            eventsDataTable.ajax.reload();
        }, 15000);
    });

    $(document).on('preInit.dt', function(e, settings) {
        // Move custom buttons container inside buttons container
        let buttons = $('#custom-buttons-container').detach();
        $('#buttons-container').prepend(buttons);

        // Remove additional classes from buttons
        $('.btn').removeClass('dt-button');

        // Load table config
        $.ajax({
            type: 'get',
            url: '{{ route("preferences.getEventsTablePreferences") }}'
        }).then(function(data) {
            Object.keys(data).forEach(key => {
                $('#colreorder-name').append($('<option/>', {
                    value: key,
                    text: data[key].current ? key + ' (actual)' : key,
                    // selected: data[key].current ? true : false,
                    'data-state': data[key].state
                }));

                if (data[key].current) {
                    alterTableState(JSON.parse(data[key].state));
                }
            });
        });
    });

    function runWaitMe(element) {
        element.waitMe({
            effect: 'roundBounce'
        });
    }

    /* <---------- START: Functions for columns reorder ----------> */
    function showColReorderModal() {
        $('#colreorder-name').val(null);

        $('#colreorder-name option').filter(function() {
            return $(this).text().includes('(actual)');
        }).prop('selected', true);

        $('#colreorder-name').trigger('change');

        $('#colreorder-modal').modal('show');
    }

    function getColumnsOrder() {
        let columns = {
            'visible': [],
            'hidden': []
        };

        $('.ms-selection > ul.ms-list').children('li').each(function() {
            let li = $(this);

            let value = $('#colreorder-selector option').filter(function() {
                return $(this).html() === li.text();
            }).val();

            if (li.is(':visible')) {
                columns['visible'].push(parseInt(value));
            } else {
                columns['hidden'].push(parseInt(value));
            }
        });

        return columns;
    }

    function showAllColumns() {
        $('#colreorder-selector').multiSelect('select_all');
    }

    function hideAllColumns() {
        $('#colreorder-selector').multiSelect('deselect_all');
    }

    function alterTableState(columns = null) {
        if (columns === null) {
            columns = getColumnsOrder();
        }

        eventsDataTable.colReorder.reset();

        eventsDataTable.columns(columns['hidden']).visible(false);
        eventsDataTable.columns(columns['visible']).visible(true);

        eventsDataTable.colReorder.order(columns['visible'].concat(columns['hidden']));

        return columns;
    }

    function setDefaultConfig() {
        if ($('#colreorder-name').find('option[value="reset"]').length) {
            $('#colreorder-name').val('reset').trigger('change');
        } else {
            let option = new Option('', 'reset', true, true);

            $('#colreorder-name').append(option).trigger('change');
        }

        $('#colreorder-form').submit();
    }
    /* <---------- END: Functions for columns reorder ----------> */

    function showEventGroupingModal() {
        $('#events-correlation-table tbody').empty();

        let events = eventsDataTable.rows({
            selected: true
        }).data().toArray();

        if (events.length > 1) {
            events.forEach(event => {
                let severity = '<i class="fa-solid fa-circle-question fa-lg txt-unknown" title="Desconocido"></i>';

                switch (event.severity) {
                    case 'critical':
                        severity = '<img src="{{ asset("images/status/stat_error.png") }}" height="16" width="16">';
                        break;
                    case 'major':
                        severity = '<img src="{{ asset("images/status/stat_majo.png") }}" height="16" width="16">';
                        break;
                    case 'minor':
                        severity = '<img src="{{ asset("images/status/stat_mino.png") }}" height="16" width="16">';
                        break;
                    case 'warning':
                        severity = '<img src="{{ asset("images/status/stat_warn.png") }}" height="16" width="16">';
                        break;
                    case 'normal':
                        severity = '<img src="{{ asset("images/status/stat_norm.png") }}" height="16" width="16">';
                        break;
                }

                $('#events-correlation-table tbody').append(
                    `<tr>
                        <td class="text-center">
                            <input name="events[]" type="hidden" value="${ event.id_event }">
                            <div class="form-check">
                                <input class="form-check-input position-static" name="cause" type="radio" value="${ event.id_event }" required>
                            </div>
                        </td>
                        <td class="text-center">${ severity }</td>
                        <td class="text-justify">${ event.title }</td>
                        <td class="text-justify">${ moment(event.time_received).format('DD/MM/YYYY hh:mm:ss A') }</td>
                    </tr>`
                );
            });

            $('#events-correlation-modal').modal('show');
        } else {
            alert('warning', 'No haz seleccionado una cantidad adecuada de eventos para ejecutar esta acción.');
        }
    }
</script>