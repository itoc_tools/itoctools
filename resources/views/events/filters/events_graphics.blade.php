<style>
    .card-filters {
        border: solid;
        border-width: 1px;
        box-shadow: rgba(0, 0, 0, 0.16) 1px 1px 4px;
        border-radius: 5px;
        width: auto;
        min-width: 120px;
        height: 66px;
        font-size: 13px;
    }

    .flex-filters {
        display: flex;
        flex-wrap: wrap;
        gap: 10px;
        justify-content: center;
    }
</style>

<div id="events-graphics">
    <div class="flex-filters">
        <a href="{{ route('telmexSlim') }}">
            @if(isset($graphicsReloadNum['telmexSlim']) && $graphicsReloadNum['telmexSlim'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">TELMEX SLIM:
                <b>{{$graphicsReloadNum['telmexSlim']}}<br></b>
                @elseif(isset($graphicsReloadNum['telmexSlim']) && $graphicsReloadNum['telmexSlim'] > 0 )
                <button type="button" class="btn card-filters btn-danger">TELMEX SLIM:
                    <b>{{$graphicsReloadNum['telmexSlim']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">TELMEX SLIM: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' height='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['telmexSlimCritical']))
                        {{$graphicsReloadNum['telmexSlimCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['telmexSlimMajor']))
                        {{$graphicsReloadNum['telmexSlimMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['telmexSlimMinor']))
                        {{$graphicsReloadNum['telmexSlimMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!-- @if(isset($graphicsReloadNum['telmexSlimWarning']))
                        {{$graphicsReloadNum['telmexSlimWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['telmexSlimNormal']))
                        {{$graphicsReloadNum['telmexSlimNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['telmexSlimUnknown']))
                        {{$graphicsReloadNum['telmexSlimUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('openException') }}">
            @if(isset($graphicsReloadNum['openException']) && $graphicsReloadNum['openException'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Abiertos Exception:
                <b>{{$graphicsReloadNum['openException']}}<br></b>
                @elseif(isset($graphicsReloadNum['openException']) && $graphicsReloadNum['openException'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Abiertos Exception:
                    <b>{{$graphicsReloadNum['openException']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Abiertos Exception: <b>-
                            <br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['openExceptionCritical']))
                        {{$graphicsReloadNum['openExceptionCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['openExceptionMajor']))
                        {{$graphicsReloadNum['openExceptionMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['openExceptionMinor']))
                        {{$graphicsReloadNum['openExceptionMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!-- @if(isset($graphicsReloadNum['openExceptionWarning']))
                        {{$graphicsReloadNum['openExceptionWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['openExceptionNormal']))
                        {{$graphicsReloadNum['openExceptionNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['openExceptionUnknown']))
                        {{$graphicsReloadNum['openExceptionUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('exchangeCriticas') }}">
            @if(isset($graphicsReloadNum['exchangeCriticas']) && $graphicsReloadNum['exchangeCriticas'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Exchange Críticas Reportar:
                <b>{{$graphicsReloadNum['exchangeCriticas']}}<br></b>
                @elseif(isset($graphicsReloadNum['exchangeCriticas']) && $graphicsReloadNum['exchangeCriticas'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Exchange Críticas Reportar:
                    <b>{{$graphicsReloadNum['exchangeCriticas']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Exchange Críticas Reportar:
                        <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['exchangeCriticasCritical']))
                        {{$graphicsReloadNum['exchangeCriticasCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['exchangeCriticasMajor']))
                        {{$graphicsReloadNum['exchangeCriticasMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['exchangeCriticasMinor']))
                        {{$graphicsReloadNum['exchangeCriticasMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!-- @if(isset($graphicsReloadNum['exchangeCriticasWarning']))
                        {{$graphicsReloadNum['exchangeCriticasWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['exchangeCriticasNormal']))
                        {{$graphicsReloadNum['exchangeCriticasNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['exchangeCriticasUnknown']))
                        {{$graphicsReloadNum['exchangeCriticasUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('alertsWithOutIMSDInProgress') }}">
            @if(isset($graphicsReloadNum['alertsWithOutIMSDInProgress']) && $graphicsReloadNum['alertsWithOutIMSDInProgress'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Excepcion sin IM o SD en Progreso:
                <b>{{$graphicsReloadNum['alertsWithOutIMSDInProgress']}}<br></b>
                @elseif(isset($graphicsReloadNum['alertsWithOutIMSDInProgress']) && $graphicsReloadNum['alertsWithOutIMSDInProgress'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Excepcion sin IM o SD en Progreso:
                    <b>{{$graphicsReloadNum['alertsWithOutIMSDInProgress']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Excepcion sin IM o SD en
                        Progreso: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['alertsWithOutIMSDInProgressCritical']))
                        {{$graphicsReloadNum['alertsWithOutIMSDInProgressCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['alertsWithOutIMSDInProgressMajor']))
                        {{$graphicsReloadNum['alertsWithOutIMSDInProgressMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['alertsWithOutIMSDInProgressMinor']))
                        {{$graphicsReloadNum['alertsWithOutIMSDInProgressMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!-- @if(isset($graphicsReloadNum['alertsWithOutIMSDInProgressWarning']))
                        {{$graphicsReloadNum['alertsWithOutIMSDInProgressWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['alertsWithOutIMSDInProgressNormal']))
                        {{$graphicsReloadNum['alertsWithOutIMSDInProgressNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['alertsWithOutIMSDInProgressUnknown']))
                        {{$graphicsReloadNum['alertsWithOutIMSDInProgressUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('alertsCriticasChat') }}">
            @if(isset($graphicsReloadNum['alertsCriticasChat']) && $graphicsReloadNum['alertsCriticasChat'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Alertas Críticas CHAT:
                <b>{{$graphicsReloadNum['alertsCriticasChat']}}<br></b>
                @elseif(isset($graphicsReloadNum['alertsCriticasChat']) && $graphicsReloadNum['alertsCriticasChat'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Alertas Críticas CHAT:
                    <b>{{$graphicsReloadNum['alertsCriticasChat']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Alertas Críticas CHAT: <b>-
                            <br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['alertsCriticasChatCritical']))
                        {{$graphicsReloadNum['alertsCriticasChatCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['alertsCriticasChatMajor']))
                        {{$graphicsReloadNum['alertsCriticasChatMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['alertsCriticasChatMinor']))
                        {{$graphicsReloadNum['alertsCriticasChatMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!-- @if(isset($graphicsReloadNum['alertsCriticasChatWarning']))
                        {{$graphicsReloadNum['alertsCriticasChatWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['alertsCriticasChatNormal']))
                        {{$graphicsReloadNum['alertsCriticasChatNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['alertsCriticasChatUnknown']))
                        {{$graphicsReloadNum['alertsCriticasChatUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('infraHardware') }}">
            @if(isset($graphicsReloadNum['infraHardware']) && $graphicsReloadNum['infraHardware'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Infraestructura Hardware:
                <b>{{$graphicsReloadNum['infraHardware']}}<br></b>
                @elseif(isset($graphicsReloadNum['infraHardware']) && $graphicsReloadNum['infraHardware'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Infraestructura Hardware:
                    <b>{{$graphicsReloadNum['infraHardware']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Infraestructura Hardware:
                        <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['infraHardwareCritical']))
                        {{$graphicsReloadNum['infraHardwareCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['infraHardwareMajor']))
                        {{$graphicsReloadNum['infraHardwareMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['infraHardwareMinor']))
                        {{$graphicsReloadNum['infraHardwareMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!-- @if(isset($graphicsReloadNum['infraHardwareWarning']))
                        {{$graphicsReloadNum['infraHardwareWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['infraHardwareNormal']))
                        {{$graphicsReloadNum['infraHardwareNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['infraHardwareUnknown']))
                        {{$graphicsReloadNum['infraHardwareUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('infraRed') }}">
            @if(isset($graphicsReloadNum['infraRed']) && $graphicsReloadNum['infraRed'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Infraestructura de Red:
                <b>{{$graphicsReloadNum['infraRed']}}<br></b>
                @elseif(isset($graphicsReloadNum['infraRed']) && $graphicsReloadNum['infraRed'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Infraestructura de Red:
                    <b>{{$graphicsReloadNum['infraRed']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Infraestructura de Red: <b>-
                            <br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['infraRedCritical']))
                        {{$graphicsReloadNum['infraRedCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['infraRedMajor']))
                        {{$graphicsReloadNum['infraRedMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['infraRedMinor']))
                        {{$graphicsReloadNum['infraRedMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!-- @if(isset($graphicsReloadNum['infraRedWarning']))
                        {{$graphicsReloadNum['infraRedWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['infraRedNormal']))
                        {{$graphicsReloadNum['infraRedNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['infraRedUnknown']))
                        {{$graphicsReloadNum['infraRedUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('infraSeguridad') }}">
            @if(isset($graphicsReloadNum['infraSeguridad']) && $graphicsReloadNum['infraSeguridad'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Infraestructura de Seguridad:
                <b>{{$graphicsReloadNum['infraSeguridad']}}<br></b>
                @elseif(isset($graphicsReloadNum['infraSeguridad']) && $graphicsReloadNum['infraSeguridad'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Infraestructura de Seguridad:
                    <b>{{$graphicsReloadNum['infraSeguridad']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Infraestructura de
                        Seguridad: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['infraSeguridadCritical']))
                        {{$graphicsReloadNum['infraSeguridadCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['infraSeguridadMajor']))
                        {{$graphicsReloadNum['infraSeguridadMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['infraSeguridadMinor']))
                        {{$graphicsReloadNum['infraSeguridadMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['infraSeguridadWarning']))
                        {{$graphicsReloadNum['infraSeguridadWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!-- @if(isset($graphicsReloadNum['infraSeguridadNormal']))
                        {{$graphicsReloadNum['infraSeguridadNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['infraSeguridadUnknown']))
                        {{$graphicsReloadNum['infraSeguridadUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('zenossQro') }}">
            @if(isset($graphicsReloadNum['zenossQro']) && $graphicsReloadNum['zenossQro'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Zenoss Qro:
                <b>{{$graphicsReloadNum['zenossQro']}}<br></b>
                @elseif(isset($graphicsReloadNum['zenossQro']) && $graphicsReloadNum['zenossQro'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Zenoss Qro:
                    <b>{{$graphicsReloadNum['zenossQro']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Zenoss Qro: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['zenossQroCritical']))
                        {{$graphicsReloadNum['zenossQroCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['zenossQroMajor']))
                        {{$graphicsReloadNum['zenossQroMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['zenossQroMinor']))
                        {{$graphicsReloadNum['zenossQroMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!-- @if(isset($graphicsReloadNum['zenossQroWarning']))
                        {{$graphicsReloadNum['zenossQroWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['zenossQroNormal']))
                        {{$graphicsReloadNum['zenossQroNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['zenossQroUnknown']))
                        {{$graphicsReloadNum['zenossQroUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('zenossMtyItoc') }}">
            @if(isset($graphicsReloadNum['zenossMtyItoc']) && $graphicsReloadNum['zenossMtyItoc'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Zenoss ITOC:
                <b>{{$graphicsReloadNum['zenossMtyItoc']}}<br></b>
                @elseif(isset($graphicsReloadNum['zenossMtyItoc']) && $graphicsReloadNum['zenossMtyItoc'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Zenoss ITOC:
                    <b>{{$graphicsReloadNum['zenossMtyItoc']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Zenoss ITOC: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['zenossMtyItocCritical']))
                        {{$graphicsReloadNum['zenossMtyItocCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['zenossMtyItocMajor']))
                        {{$graphicsReloadNum['zenossMtyItocMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['zenossMtyItocMinor']))
                        {{$graphicsReloadNum['zenossMtyItocMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!-- @if(isset($graphicsReloadNum['zenossMtyItocWarning']))
                        {{$graphicsReloadNum['zenossMtyItocWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['zenossMtyItocNormal']))
                        {{$graphicsReloadNum['zenossMtyItocNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['zenossMtyItocUnknown']))
                        {{$graphicsReloadNum['zenossMtyItocUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('zenossSRE') }}">
            @if(isset($graphicsReloadNum['zenossSRE']) && $graphicsReloadNum['zenossSRE'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Zenoss SRE:
                <b>{{$graphicsReloadNum['zenossSRE']}}<br></b>
                @elseif(isset($graphicsReloadNum['zenossSRE']) && $graphicsReloadNum['zenossSRE'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Zenoss SRE:
                    <b>{{$graphicsReloadNum['zenossSRE']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Zenoss SRE: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['zenossSRECritical']))
                        {{$graphicsReloadNum['zenossSRECritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['zenossSREMajor']))
                        {{$graphicsReloadNum['zenossSREMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['zenossSREMinor']))
                        {{$graphicsReloadNum['zenossSREMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!-- @if(isset($graphicsReloadNum['zenossSREWarning']))
                        {{$graphicsReloadNum['zenossSREWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['zenossSRENormal']))
                        {{$graphicsReloadNum['zenossSRENormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['zenossSREUnknown']))
                        {{$graphicsReloadNum['zenossSREUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('zenossGBMV') }}">
            @if(isset($graphicsReloadNum['zenossGBMV']) && $graphicsReloadNum['zenossGBMV'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Zenoss GBMV:
                <b>{{$graphicsReloadNum['zenossGBMV']}}<br></b>
                @elseif(isset($graphicsReloadNum['zenossGBMV']) && $graphicsReloadNum['zenossGBMV'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Zenoss GBMV:
                    <b>{{$graphicsReloadNum['zenossGBMV']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Zenoss GBMV: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['zenossGBMVCritical']))
                        {{$graphicsReloadNum['zenossGBMVCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['zenossGBMVMajor']))
                        {{$graphicsReloadNum['zenossGBMVMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['zenossGBMVMinor']))
                        {{$graphicsReloadNum['zenossGBMVMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['zenossGBMVWarning']))
                        {{$graphicsReloadNum['zenossGBMVWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['zenossGBMVNormal']))
                        {{$graphicsReloadNum['zenossGBMVNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['zenossGBMVUnknown']))
                        {{$graphicsReloadNum['zenossGBMVUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('ingHerramientas') }}">
            @if(isset($graphicsReloadNum['ingHerramientas']) && $graphicsReloadNum['ingHerramientas'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Ingeniería Herramientas:
                <b>{{$graphicsReloadNum['ingHerramientas']}}<br></b>
                @elseif(isset($graphicsReloadNum['ingHerramientas']) && $graphicsReloadNum['ingHerramientas'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Ingeniería Herramientas:
                    <b>{{$graphicsReloadNum['ingHerramientas']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Ingeniería Herramientas: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['ingHerramientasCritical']))
                        {{$graphicsReloadNum['ingHerramientasCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['ingHerramientasMajor']))
                        {{$graphicsReloadNum['ingHerramientasMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['ingHerramientasMinor']))
                        {{$graphicsReloadNum['ingHerramientasMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['ingHerramientasWarning']))
                        {{$graphicsReloadNum['ingHerramientasWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['ingHerramientasNormal']))
                        {{$graphicsReloadNum['ingHerramientasNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['ingHerramientasUnknown']))
                        {{$graphicsReloadNum['ingHerramientasUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('ITOC_VCloud') }}">
            @if(isset($graphicsReloadNum['ITOC_VCloud']) && $graphicsReloadNum['ITOC_VCloud'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">ITOC_VCloud:
                <b>{{$graphicsReloadNum['ITOC_VCloud']}}<br></b>
                @elseif(isset($graphicsReloadNum['ITOC_VCloud']) && $graphicsReloadNum['ITOC_VCloud'] > 0 )
                <button type="button" class="btn card-filters btn-danger">ITOC_VCloud:
                    <b>{{$graphicsReloadNum['ITOC_VCloud']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">ITOC_VCloud: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['ITOC_VCloudCritical']))
                        {{$graphicsReloadNum['ITOC_VCloudCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['ITOC_VCloudMajor']))
                        {{$graphicsReloadNum['ITOC_VCloudMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['ITOC_VCloudMinor']))
                        {{$graphicsReloadNum['ITOC_VCloudMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['ITOC_VCloudWarning']))
                        {{$graphicsReloadNum['ITOC_VCloudWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['ITOC_VCloudNormal']))
                        {{$graphicsReloadNum['ITOC_VCloudNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['ITOC_VCloudUnknown']))
                        {{$graphicsReloadNum['ITOC_VCloudUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventPlataformasClientes') }}">
            @if(isset($graphicsReloadNum['EventPlataformasClientes']) && $graphicsReloadNum['EventPlataformasClientes'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Plataformas | Clientes Críticos:
                <b>{{$graphicsReloadNum['EventPlataformasClientes']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventPlataformasClientes']) && $graphicsReloadNum['EventPlataformasClientes'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Plataformas | Clientes Críticos:
                    <b>{{$graphicsReloadNum['EventPlataformasClientes']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Plataformas | Clientes Críticos: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventPlataformasClientesCritical']))
                        {{$graphicsReloadNum['EventPlataformasClientesCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventPlataformasClientesMajor']))
                        {{$graphicsReloadNum['EventPlataformasClientesMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventPlataformasClientesMinor']))
                        {{$graphicsReloadNum['EventPlataformasClientesMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventPlataformasClientesWarning']))
                        {{$graphicsReloadNum['EventPlataformasClientesWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventPlataformasClientesNormal']))
                        {{$graphicsReloadNum['EventPlataformasClientesNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventPlataformasClientesUnknown']))
                        {{$graphicsReloadNum['EventPlataformasClientesUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventPingReinicios') }}">
            @if(isset($graphicsReloadNum['EventPingReinicios']) && $graphicsReloadNum['EventPingReinicios'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Ping y Reinicios:
                <b>{{$graphicsReloadNum['EventPingReinicios']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventPingReinicios']) && $graphicsReloadNum['EventPingReinicios'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Ping Y Reinicios:
                    <b>{{$graphicsReloadNum['EventPingReinicios']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Ping y Reinicios: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventPingReiniciosCritical']))
                        {{$graphicsReloadNum['EventPingReiniciosCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventPingReiniciosMajor']))
                        {{$graphicsReloadNum['EventPingReiniciosMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventPingReiniciosMinor']))
                        {{$graphicsReloadNum['EventPingReiniciosMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventPingReiniciosWarning']))
                        {{$graphicsReloadNum['EventPingReiniciosWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventPingReiniciosNormal']))
                        {{$graphicsReloadNum['EventPingReiniciosNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventPingReiniciosUnknown']))
                        {{$graphicsReloadNum['EventPingReiniciosUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventExcepcionSinAtender') }}">
            @if(isset($graphicsReloadNum['EventExcepcionSinAtender']) && $graphicsReloadNum['EventExcepcionSinAtender'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Excepción Sin Atender > 15 min:
                <b>{{$graphicsReloadNum['EventExcepcionSinAtender']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventExcepcionSinAtender']) && $graphicsReloadNum['EventExcepcionSinAtender'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Excepción Sin Atender > 15 min:
                    <b>{{$graphicsReloadNum['EventExcepcionSinAtender']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Excepción Sin Atender > 15 min: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventExcepcionSinAtenderCritical']))
                        {{$graphicsReloadNum['EventExcepcionSinAtenderCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventExcepcionSinAtenderMajor']))
                        {{$graphicsReloadNum['EventExcepcionSinAtenderMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventExcepcionSinAtenderMinor']))
                        {{$graphicsReloadNum['EventExcepcionSinAtenderMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventExcepcionSinAtenderWarning']))
                        {{$graphicsReloadNum['EventExcepcionSinAtenderWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventExcepcionSinAtenderNormal']))
                        {{$graphicsReloadNum['EventExcepcionSinAtenderNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventExcepcionSinAtenderUnknown']))
                        {{$graphicsReloadNum['EventExcepcionSinAtenderUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventSitescope') }}">
            @if(isset($graphicsReloadNum['EventSitescope']) && $graphicsReloadNum['EventSitescope'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Sitescope:
                <b>{{$graphicsReloadNum['EventSitescope']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventSitescope']) && $graphicsReloadNum['EventSitescope'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Sitescope:
                    <b>{{$graphicsReloadNum['EventSitescope']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Sitescope: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventSitescopeCritical']))
                        {{$graphicsReloadNum['EventSitescopeCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventSitescopeMajor']))
                        {{$graphicsReloadNum['EventSitescopeMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventSitescopeMinor']))
                        {{$graphicsReloadNum['EventSitescopeMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventSitescopeWarning']))
                        {{$graphicsReloadNum['EventSitescopeWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventSitescopeNormal']))
                        {{$graphicsReloadNum['EventSitescopeNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventSitescopeUnknown']))
                        {{$graphicsReloadNum['EventSitescopeUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventNagiosURLsZabbix') }}">
            @if(isset($graphicsReloadNum['EventNagiosURLsZabbix']) && $graphicsReloadNum['EventNagiosURLsZabbix'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Nagios URLs | Zabbix:
                <b>{{$graphicsReloadNum['EventNagiosURLsZabbix']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventNagiosURLsZabbix']) && $graphicsReloadNum['EventNagiosURLsZabbix'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Nagios URLs | Zabbix:
                    <b>{{$graphicsReloadNum['EventNagiosURLsZabbix']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Nagios URLs | Zabbix: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventNagiosURLsZabbixCritical']))
                        {{$graphicsReloadNum['EventNagiosURLsZabbixCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventNagiosURLsZabbixMajor']))
                        {{$graphicsReloadNum['EventNagiosURLsZabbixMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventNagiosURLsZabbixMinor']))
                        {{$graphicsReloadNum['EventNagiosURLsZabbixMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventNagiosURLsZabbixWarning']))
                        {{$graphicsReloadNum['EventNagiosURLsZabbixWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventNagiosURLsZabbixNormal']))
                        {{$graphicsReloadNum['EventNagiosURLsZabbixNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventNagiosURLsZabbixUnknown']))
                        {{$graphicsReloadNum['EventNagiosURLsZabbixUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventAWSNewRelic') }}">
            @if(isset($graphicsReloadNum['EventAWSNewRelic']) && $graphicsReloadNum['EventAWSNewRelic'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">AWS | New Relic:
                <b>{{$graphicsReloadNum['EventAWSNewRelic']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventAWSNewRelic']) && $graphicsReloadNum['EventAWSNewRelic'] > 0 )
                <button type="button" class="btn card-filters btn-danger">AWS | New Relic:
                    <b>{{$graphicsReloadNum['EventAWSNewRelic']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">AWS | New Relic: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventAWSNewRelicCritical']))
                        {{$graphicsReloadNum['EventAWSNewRelicCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventAWSNewRelicMajor']))
                        {{$graphicsReloadNum['EventAWSNewRelicMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventAWSNewRelicMinor']))
                        {{$graphicsReloadNum['EventAWSNewRelicMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventAWSNewRelicWarning']))
                        {{$graphicsReloadNum['EventAWSNewRelicWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventAWSNewRelicNormal']))
                        {{$graphicsReloadNum['EventAWSNewRelicNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventAWSNewRelicUnknown']))
                        {{$graphicsReloadNum['EventAWSNewRelicUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventEventosdeRFC') }}">
            @if(isset($graphicsReloadNum['EventEventosdeRFC']) && $graphicsReloadNum['EventEventosdeRFC'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Eventos de RFC:
                <b>{{$graphicsReloadNum['EventEventosdeRFC']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventEventosdeRFC']) && $graphicsReloadNum['EventEventosdeRFC'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Eventos de RFC:
                    <b>{{$graphicsReloadNum['EventEventosdeRFC']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Eventos de RFC: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventEventosdeRFCCritical']))
                        {{$graphicsReloadNum['EventEventosdeRFCCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventEventosdeRFCMajor']))
                        {{$graphicsReloadNum['EventEventosdeRFCMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventEventosdeRFCMinor']))
                        {{$graphicsReloadNum['EventEventosdeRFCMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventEventosdeRFCWarning']))
                        {{$graphicsReloadNum['EventEventosdeRFCWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventEventosdeRFCNormal']))
                        {{$graphicsReloadNum['EventEventosdeRFCNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventEventosdeRFCUnknown']))
                        {{$graphicsReloadNum['EventEventosdeRFCUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventZenossCloudVIP') }}">
            @if(isset($graphicsReloadNum['EventZenossCloudVIP']) && $graphicsReloadNum['EventZenossCloudVIP'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Zenoss Cloud | VIP:
                <b>{{$graphicsReloadNum['EventZenossCloudVIP']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventZenossCloudVIP']) && $graphicsReloadNum['EventZenossCloudVIP'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Zenoss Cloud | VIP:
                    <b>{{$graphicsReloadNum['EventZenossCloudVIP']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Zenoss Cloud | VIP: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventZenossCloudVIPCritical']))
                        {{$graphicsReloadNum['EventZenossCloudVIPCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventZenossCloudVIPMajor']))
                        {{$graphicsReloadNum['EventZenossCloudVIPMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventZenossCloudVIPMinor']))
                        {{$graphicsReloadNum['EventZenossCloudVIPMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventZenossCloudVIPWarning']))
                        {{$graphicsReloadNum['EventZenossCloudVIPWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventZenossCloudVIPNormal']))
                        {{$graphicsReloadNum['EventZenossCloudVIPNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventZenossCloudVIPUnknown']))
                        {{$graphicsReloadNum['EventZenossCloudVIPUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventEventosResueltos') }}">
            @if(isset($graphicsReloadNum['EventEventosResueltos']) && $graphicsReloadNum['EventEventosResueltos'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Eventos Resueltos:
                <b>{{$graphicsReloadNum['EventEventosResueltos']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventEventosResueltos']) && $graphicsReloadNum['EventEventosResueltos'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Eventos Resueltos:
                    <b>{{$graphicsReloadNum['EventEventosResueltos']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Eventos Resueltos: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventEventosResueltosCritical']))
                        {{$graphicsReloadNum['EventEventosResueltosCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventEventosResueltosMajor']))
                        {{$graphicsReloadNum['EventEventosResueltosMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventEventosResueltosMinor']))
                        {{$graphicsReloadNum['EventEventosResueltosMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventEventosResueltosWarning']))
                        {{$graphicsReloadNum['EventEventosResueltosWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventEventosResueltosNormal']))
                        {{$graphicsReloadNum['EventEventosResueltosNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventEventosResueltosUnknown']))
                        {{$graphicsReloadNum['EventEventosResueltosUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventClientesSAP') }}">
            @if(isset($graphicsReloadNum['EventClientesSAP']) && $graphicsReloadNum['EventClientesSAP'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Clientes SAP:
                <b>{{$graphicsReloadNum['EventClientesSAP']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventClientesSAP']) && $graphicsReloadNum['EventClientesSAP'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Clientes SAP:
                    <b>{{$graphicsReloadNum['EventClientesSAP']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Clientes SAP: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventClientesSAPCritical']))
                        {{$graphicsReloadNum['EventClientesSAPCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventClientesSAPMajor']))
                        {{$graphicsReloadNum['EventClientesSAPMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventClientesSAPMinor']))
                        {{$graphicsReloadNum['EventClientesSAPMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventClientesSAPWarning']))
                        {{$graphicsReloadNum['EventClientesSAPWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventClientesSAPNormal']))
                        {{$graphicsReloadNum['EventClientesSAPNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventClientesSAPUnknown']))
                        {{$graphicsReloadNum['EventClientesSAPUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventAlertasAvaya') }}">
            @if(isset($graphicsReloadNum['EventAlertasAvaya']) && $graphicsReloadNum['EventAlertasAvaya'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Alertas Avaya:
                <b>{{$graphicsReloadNum['EventAlertasAvaya']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventAlertasAvaya']) && $graphicsReloadNum['EventAlertasAvaya'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Alertas Avaya:
                    <b>{{$graphicsReloadNum['EventAlertasAvaya']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Alertas Avaya: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventAlertasAvayaCritical']))
                        {{$graphicsReloadNum['EventAlertasAvayaCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventAlertasAvayaMajor']))
                        {{$graphicsReloadNum['EventAlertasAvayaMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventAlertasAvayaMinor']))
                        {{$graphicsReloadNum['EventAlertasAvayaMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventAlertasAvayaWarning']))
                        {{$graphicsReloadNum['EventAlertasAvayaWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventAlertasAvayaNormal']))
                        {{$graphicsReloadNum['EventAlertasAvayaNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventAlertasAvayaUnknown']))
                        {{$graphicsReloadNum['EventAlertasAvayaUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventIntegracionesITOCTOOLs') }}">
            @if(isset($graphicsReloadNum['EventIntegracionesITOCTOOLs']) && $graphicsReloadNum['EventIntegracionesITOCTOOLs'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Integraciones ITOC TOOLs:
                <b>{{$graphicsReloadNum['EventIntegracionesITOCTOOLs']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventIntegracionesITOCTOOLs']) && $graphicsReloadNum['EventIntegracionesITOCTOOLs'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Integraciones ITOC TOOLs:
                    <b>{{$graphicsReloadNum['EventIntegracionesITOCTOOLs']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Integraciones ITOC TOOLs: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventIntegracionesITOCTOOLsCritical']))
                        {{$graphicsReloadNum['EventIntegracionesITOCTOOLsCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventIntegracionesITOCTOOLsMajor']))
                        {{$graphicsReloadNum['EventIntegracionesITOCTOOLsMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventIntegracionesITOCTOOLsMinor']))
                        {{$graphicsReloadNum['EventIntegracionesITOCTOOLsMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventIntegracionesITOCTOOLsWarning']))
                        {{$graphicsReloadNum['EventIntegracionesITOCTOOLsWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventIntegracionesITOCTOOLsNormal']))
                        {{$graphicsReloadNum['EventIntegracionesITOCTOOLsNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventIntegracionesITOCTOOLsUnknown']))
                        {{$graphicsReloadNum['EventIntegracionesITOCTOOLsUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventAlertasNA') }}">
            @if(isset($graphicsReloadNum['EventAlertasNA']) && $graphicsReloadNum['EventAlertasNA'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Alertas N/A:
                <b>{{$graphicsReloadNum['EventAlertasNA']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventAlertasNA']) && $graphicsReloadNum['EventAlertasNA'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Alertas N/A:
                    <b>{{$graphicsReloadNum['EventAlertasNA']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Alertas N/A: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventAlertasNACritical']))
                        {{$graphicsReloadNum['EventAlertasNACritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventAlertasNAMajor']))
                        {{$graphicsReloadNum['EventAlertasNAMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventAlertasNAMinor']))
                        {{$graphicsReloadNum['EventAlertasNAMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventAlertasNAWarning']))
                        {{$graphicsReloadNum['EventAlertasNAWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventAlertasNANormal']))
                        {{$graphicsReloadNum['EventAlertasNANormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventAlertasNAUnknown']))
                        {{$graphicsReloadNum['EventAlertasNAUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventEventosDummyPrueba') }}">
            @if(isset($graphicsReloadNum['EventEventosDummyPrueba']) && $graphicsReloadNum['EventEventosDummyPrueba'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Eventos Dummy | Prueba:
                <b>{{$graphicsReloadNum['EventEventosDummyPrueba']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventEventosDummyPrueba']) && $graphicsReloadNum['EventEventosDummyPrueba'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Eventos Dummy | Prueba:
                    <b>{{$graphicsReloadNum['EventEventosDummyPrueba']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Eventos Dummy | Prueba: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventEventosDummyPruebaCritical']))
                        {{$graphicsReloadNum['EventEventosDummyPruebaCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventEventosDummyPruebaMajor']))
                        {{$graphicsReloadNum['EventEventosDummyPruebaMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventEventosDummyPruebaMinor']))
                        {{$graphicsReloadNum['EventEventosDummyPruebaMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventEventosDummyPruebaWarning']))
                        {{$graphicsReloadNum['EventEventosDummyPruebaWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventEventosDummyPruebaNormal']))
                        {{$graphicsReloadNum['EventEventosDummyPruebaNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventEventosDummyPruebaUnknown']))
                        {{$graphicsReloadNum['EventEventosDummyPruebaUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ route('eventPoleos') }}">
            @if(isset($graphicsReloadNum['EventPoleos']) && $graphicsReloadNum['EventPoleos'] == 0 )
            <button type="button" class="btn card-filters btn-secondary.disabled">Poleos:
                <b>{{$graphicsReloadNum['EventPoleos']}}<br></b>
                @elseif(isset($graphicsReloadNum['EventPoleos']) && $graphicsReloadNum['EventPoleos'] > 0 )
                <button type="button" class="btn card-filters btn-danger">Poleos:
                    <b>{{$graphicsReloadNum['EventPoleos']}}<br></b>
                    @else
                    <button type="button" class="btn card-filters btn-secondary.disabled">Poleos: <b>-<br></b>
                        @endif
                        <img src='{{url("/images/status/stat_error.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_majo.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_mino.png")}}' width='14' width='14'>
                        <img src='{{url("/images/status/stat_warn.png")}}' width='14' width='14'>
                        <!-- <img src='{{url("/images/status/stat_norm.png")}}' width='14' width='14'> -->
                        <img src='{{url("/images/status/stat_unkn_11.png")}}' width='14' width='14'>
                        <br>
                        @if(isset($graphicsReloadNum['EventPoleosCritical']))
                        {{$graphicsReloadNum['EventPoleosCritical']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventPoleosMajor']))
                        {{$graphicsReloadNum['EventPoleosMajor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventPoleosMinor']))
                        {{$graphicsReloadNum['EventPoleosMinor']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        @if(isset($graphicsReloadNum['EventPoleosWarning']))
                        {{$graphicsReloadNum['EventPoleosWarning']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif
                        <!--  @if(isset($graphicsReloadNum['EventPoleosNormal']))
                        {{$graphicsReloadNum['EventPoleosNormal']}}&nbsp;&nbsp;
                        @else
                        0&nbsp;&nbsp;
                        @endif -->
                        @if(isset($graphicsReloadNum['EventPoleosUnknown']))
                        {{$graphicsReloadNum['EventPoleosUnknown']}}&nbsp;
                        @else
                        0&nbsp;
                        @endif
                    </button>
        </a>
        <a href="{{ URL::to('/events') }}">
            <button type="button" class="btn card-filters btn-success">No Filter</button>
        </a>
    </div>
</div>