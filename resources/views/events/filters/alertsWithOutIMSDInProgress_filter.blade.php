@extends('layouts.master')

@section('content')

<div class="row">
    @include('events.filters.general')
</div>

@endsection

@section('allJquery')
@include('includes.events.methods_alertsWithOutIMSDInProgress_filter')
@include('includes.events.methods_massive_attributes')
@include('includes.events.methods_graphics_reload')
@endsection