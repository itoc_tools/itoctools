<div id="events-page-container" class="col-12">
    <div class="card">
        <div class="card-body">
            @include('events.filters.events_graphics')
            @if (session()->has('serviceErrorData'))
                <div class="row">
                    <script>
                        alert("Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.");
                    </script>
                </div>
            @elseif (session()->has('serviceErrorConnection'))
                <div class="row">
                    <script>
                        alert("Error de conexión, inténtelo más tarde.");
                    </script>
                </div>
            @endif
            <div class="row">
                <div class="col">
                    <div id="responsive-modal-annotation_loading_main" role="dialog" style="display: none;" tabindex="-1" aria-hidden="true" aria-labelledby="mySmallModalLabel">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 id="mySmallModalLabel" class="modal-title font-weight-bold text-center">Procesando Annotation Masivos....</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="responsive-modal-annotation_main" class="modal" role="dialog" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('events.masiveAnnotation') }}" class="form-horizontal form-material" method="POST">
                                @csrf
                                @method('POST')
                                <div class="modal-header">
                                    <h4 class="modal-title">Crear Annotations Masivos</h4>
                                    <button class="close" type="button" aria-hidden="true" data-dismiss="modal">×</button>
                                </div>
                                <div class="modal-body">
                                    <input id="User_OMI_AM" name="User_OMI_AM" type="hidden" value="{{ $user_bsm->us_bsm ?? '' }}">
                                    <input id="Password_OMI_AM" name="Password_OMI_AM" type="hidden" value="{{ $user_bsm->pass_bsm ?? '' }}">
                                    <div class="form-group">
                                        <label class="control-label" for="time_AM">Time Created</label>
                                        <input id="time_AM" class="form-control" name="time_AM" placeholder="{{ Carbon\Carbon::now(new DateTimeZone('America/Mexico_City')) }}" type="text" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="text_AM">Text</label>
                                        <textarea id="text_AM" class="form-control border border-dark" name="text_AM" rows="3"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="idsAMText">ID's de los eventos a los que se agregará el Annotation</label>
                                        <textarea id="idsAM" class="form-control border border-dark d-none" name="idsAM" rows="1" readonly></textarea>
                                        <textarea id="idsAMText" class="form-control border border-dark" name="idsAMText" rows="5" wrap="off" readonly></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="saveAM" class="btn btn-danger" type="submit" data-target="#responsive-modal-annotation_loading_main" data-toggle="modal">OK</button>
                                    <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div id="responsive-modal-main" role="dialog" style="display: none;" tabindex="-1" aria-hidden="true" aria-labelledby="mySmallModalLabel">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 id="mySmallModalLabel" class="modal-title font-weight-bold text-center">Procesando Custom Attributes Masivos....</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="responsive-modal-custom-attributes_main" class="modal" role="dialog" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('events.masiveCustomAttributes') }}" class="form-horizontal form-material" method="POST">
                                @csrf
                                @method('POST')
                                <div class="modal-header">
                                    <h4 class="modal-title">Crear Custom Attribute Masivos</h4>
                                    <button class="close" type="button" aria-hidden="true" data-dismiss="modal">×</button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label class="control-label" for="name-CAM">Name</label>
                                        <input id="name-CAM" class="form-control" name="name-CAM" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="value-CAM">Value</label>
                                        <textarea id="value-CAM" class="form-control" name="value-CAM" rows="1"></textarea>
                                    </div>
                                    <input id="User_OMI_CAM" name="User_OMI_CAM" type="hidden" value="{{ $user_bsm->us_bsm ?? '' }}">
                                    <input id="Password_OMI_CAM" name="Password_OMI_CAM" type="hidden" value="{{ $user_bsm->pass_bsm ?? '' }}">
                                    <div class="form-group">
                                        <label class="control-label" for="idsTextBox">ID's de los eventos a los que se agregará el Custom Attribute</label>
                                        <textarea id="ids" class="form-control border border-dark d-none" name="ids" rows="1" readonly></textarea>
                                        <textarea id="idsTextBox" class="form-control border border-dark" name="idsTextBox" rows="5" wrap="off" readonly></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="save-CAM" class="btn btn-danger" type="submit" data-target="#responsive-modal-main" data-toggle="modal">OK</button>
                                    <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div id="responsive-modal-assign-loading-main" role="dialog" style="display: none;" tabindex="-1" aria-hidden="true" aria-labelledby="mySmallModalLabel">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 id="mySmallModalLabel" class="modal-title font-weight-bold text-center">Procesando Assign To Me Masivos....</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="responsive-modal-assign-main" class="modal" role="dialog" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('events.massiveAssign') }}" class="form-horizontal form-material" method="POST">
                                @csrf
                                @method('POST')
                                <div class="modal-header">
                                    <h4 class="modal-title">Assign To Me... Masivo</h4>
                                    <button class="close" type="button" aria-hidden="true" data-dismiss="modal">×</button>
                                </div>
                                <div class="modal-body">
                                    <input id="User_OMI_Assign" name="User_OMI_Assign" type="hidden" value="{{ $user_bsm->us_bsm ?? '' }}">
                                    <input id="Password_OMI_Assign" name="Password_OMI_Assign" type="hidden" value="{{ $user_bsm->pass_bsm ?? '' }}">
                                    <div class="form-group">
                                        <label class="control-label font-weight-bold mb-2" for="assigned_user">Assigned User</label>
                                        <select id="assigned_user" class="select2 form-control form-select" name="assigned_user" style="width: 100%;">
                                            <option>{{ $user_bsm->us_bsm ?? '' }}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="idsAssignText">ID's de los eventos que se asignarán al usuario</label>
                                        <textarea id="idsAssign" class="form-control border border-dark d-none" name="idsAssign" rows="1" readonly></textarea>
                                        <textarea id="idsAssignText" class="form-control border border-dark" name="idsAssignText" rows="5" wrap="off" readonly></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="saveAssign" class="btn btn-danger" type="submit" data-target="#responsive-modal-assign-loading-main" data-toggle="modal">OK</button>
                                    <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div id="responsive-modal-state-loading-main" role="dialog" style="display: none;" tabindex="-1" aria-hidden="true" aria-labelledby="mySmallModalLabel">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 id="mySmallModalLabel" class="modal-title font-weight-bold text-center">Procesando Lifecycle Status Masivos....</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="responsive-modal-state-main" class="modal" role="dialog" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('events.massiveState') }}" class="form-horizontal form-material" method="POST">
                                @csrf
                                @method('POST')
                                <div class="modal-header">
                                    <h4 class="modal-title">Cambiar State (Lifecycle State) Masivo</h4>
                                    <button class="close" type="button" aria-hidden="true" data-dismiss="modal">×</button>
                                </div>
                                <div class="modal-body">
                                    <input id="User_OMI_State" name="User_OMI_State" type="hidden" value="{{ $user_bsm->us_bsm ?? '' }}">
                                    <input id="Password_OMI_Status" name="Password_OMI_Status" type="hidden" value="{{ $user_bsm->pass_bsm ?? '' }}">
                                    <div class="form-group">
                                        <label class="control-label font-weight-bold mb-2" for="state_massive">Lifecycle State</label>
                                        <select id="state_massive" class="select2 form-control form-select" name="state_massive" style="width: 100%;">
                                            <option disabled hidden selected></option>
                                            <option>open</option>
                                            <option>in_progress</option>
                                            <option>resolved</option>
                                            <option>closed</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="idStateText">ID's de los eventos a los que se cambiará el State</label>
                                        <textarea id="idState" class="form-control border border-dark d-none" name="idState" rows="1" readonly></textarea>
                                        <textarea id="idStateText" class="form-control border border-dark" name="idStateText" rows="5" wrap="off" readonly></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="saveState" class="btn btn-danger" type="submit" data-target="#responsive-modal-state-loading-main" data-toggle="modal">OK</button>
                                    <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3 mb-3">
                <div class="col">
                    <h4 class="font-weight-bold text-themecolor text-center">{{ $name_graphic }}</h4>
                </div>
            </div>
            <div id="custom-buttons-container" class="button-group">
                <button id="cam-button" class="btn btn-warning" type="button" data-target="#responsive-modal-custom-attributes_main" data-toggle="modal" onclick="GetSelected(event, '{{ $user_bsm->us_bsm ?? NULL }}', '{{ $user_bsm->pass_bsm ?? NULL }}')">
                    Atributo Personalizado Masivo
                </button>
                <button id="am-button" class="btn btn-warning" type="button" data-target="#responsive-modal-annotation_main" data-toggle="modal" onclick="GetSelectedAM(event, '{{ $user_bsm->us_bsm ?? NULL }}', '{{ $user_bsm->pass_bsm ?? NULL }}')">
                    Anotación Masiva
                </button>
                <button id="lsm-button" class="btn btn-warning" type="button" data-target="#responsive-modal-state-main" data-toggle="modal" onclick="GetSelectedState(event, '{{ $user_bsm->us_bsm ?? NULL }}', '{{ $user_bsm->pass_bsm ?? NULL }}')">
                    Cambio de Estado Masivo
                </button>
                <button id="atm-button" class="btn btn-warning" type="button" onclick="GetSelectedAssing(event, '{{ $user_bsm->us_bsm ?? NULL }}', '{{ $user_bsm->pass_bsm ?? NULL }}')">
                    Asignar a ... Mi
                </button>
            </div>
            <div class="row">
                <div class="col-12">
                    @include('events.filters.events_table')
                </div>
            </div>
            @include ('nc.layout.components.colreorder-modal')
            @include ('nc.events.events-correlation-modal')
        </div>
    </div>
</div>