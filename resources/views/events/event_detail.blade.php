<!DOCTYPE html>
<html lang="en">
@include('includes.head')

<body id="bodyMaster" class="skin-default fixed-layout">
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <style>
        .table-hover>tbody>tr:hover {
            background-color: #cfcfcf !important;
        }

        .table-hover>tbody>tr:hover td small {
            font-weight: 500 !important;
        }
    </style>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs profile-tab" role="tablist">
                    @if (isset($im_details['details']))
                        <li class="nav-item"><a class="nav-link active" href="#im-details" role="tab" data-toggle="tab">IM Details</a></li>
                    @endif
                    <li class="nav-item"><a class="nav-link {{ isset($im_details['details']) ? '' : 'active' }}" href="#general" role="tab" data-toggle="tab">General</a></li>
                    <li class="nav-item"><a class="nav-link" href="#custom-attributes" role="tab" data-toggle="tab">Custom Attributes</a></li>
                    <li class="nav-item"><a class="nav-link" href="#annotation" role="tab" data-toggle="tab">Annotations</a></li>
                    <li class="nav-item"><a class="nav-link" href="#history" role="tab" data-toggle="tab">History</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    @if (session()->has('serviceErrorData') || session()->has('serviceErrorConnection'))
                        <div class="row">
                            <script>
                                alert("Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.");
                            </script>
                        </div>
                    @endif
                    <!-- general pane -->
                    @if (isset($im_details['details']))
                        <!-- im:details pane -->
                        <div id="im-details" class="tab-pane active" role="tabpanel">
                            <div class="card-body">
                                <form>
                                    <div class="row mb-2">
                                        <div class="col">
                                            <h4 class="font-weight-bold">General</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        @foreach ($im_details['details'] as $key => $value)
                                            <div class="col-12 col-sm-6 mb-2">
                                                @if ($key === 'IncidentID')
                                                    <p class="font-weight-bold">IncidentID: {{ $value }}</p>
                                                @else
                                                    <label class="form-label font-weight-bold">{{ $key }}</label>
                                                    <textarea rows="{{ ($key === 'JournalUpdates') ? 5 : 1 }}" class="form-control" disabled>{{ $value }}</textarea>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row">
                                        <div class="col text-right">
                                            <input class="btn btn-default" type="button" value="Cancelar" onclick="window.close();">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endif
                    <!-- general pane -->
                    <div id="general" class="tab-pane {{ isset($im_details['details']) ? '' : 'active' }}" role="tabpanel">
                        <div class="card-body">
                            <form action="{{ route('events.update', $general['id']) }}" method="POST">
                                @csrf
                                @method('PATCH')
                                <div class="row">
                                    <div class="col">
                                        <h4 class="font-weight-bold">
                                            General
                                            @switch ($general['severity'])
                                                @case ('critical')
                                                    <img height="16" src="{{ url('/images/status/stat_error.png') }}" width="16">
                                                    @break
                                                @case ('major')
                                                    <img height="16" src="{{ url('/images/status/stat_majo.png') }}" width="16">
                                                    @break
                                                @case ('minor')
                                                    <img height="16" src="{{ url('/images/status/stat_mino.png') }}" width="16">
                                                    @break
                                                @case ('warning')
                                                    <img height="16" src="{{ url('/images/status/stat_warn.png') }}" width="16">
                                                    @break
                                                @case ('normal')
                                                    <img height="16" src="{{ url('/images/status/stat_norm.png') }}" width="16">
                                                    @break
                                            @endswitch
                                        </h4>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-2 font-weight-bold">ID</div>
                                            <div class="col-10">{{ $general['id'] }}</div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <label class="form-label font-weight-bold">Severity</label>
                                        <select id="severity" class="select2 form-control form-select" name="severity" style="width: 100%;">
                                            <option disabled hidden selected></option>
                                            <option>critical</option>
                                            <option>major</option>
                                            <option>minor</option>
                                            <option>warning</option>
                                            <option>normal</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col">
                                        <label class="form-label font-weight-bold">Lifecycle State</label>
                                        <input id="state_initial" class="form-control" name="state_initial" type="text" value="{{ $general['state'] }}" style="display: none;">
                                        <select id="state" class="select2 form-control form-select" name="state" style="width: 100%;" required>
                                            <option disabled hidden selected></option>
                                            <option>open</option>
                                            <option>in_progress</option>
                                            <option>resolved</option>
                                            <option>closed</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label class="form-label font-weight-bold">Priority</label>
                                        <select id="priority" class="select2 form-control form-select" name="priority" style="width: 100%;" required>
                                            <option disabled hidden selected></option>
                                            <option>lowest</option>
                                            <option>low</option>
                                            <option>medium</option>
                                            <option>high</option>
                                            <option>highest</option>
                                            <option>none</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col">
                                        <label class="form-label font-weight-bold">Assigned Group</label>
                                        <input class="form-control" type="text" value="{{ $general['assigned_group'] }}" disabled>
                                    </div>
                                    <div class="col">
                                        <label class="form-label font-weight-bold">Assigned User</label>
                                        <input id="assigned_user_logged" class="form-control" name="assigned_user_logged" type="text" value="{{ $user_bsm->us_bsm ?? '' }}" style="display: none;">
                                        <select id="assigned_user" class="select2 form-control form-select" name="assigned_user" style="width: 100%;" required>
                                            <option disabled hidden selected></option>
                                            <option>andrea.valencia</option>
                                            <option>angela.galvan</option>
                                            <option>andres.sanchez</option>
                                            <option>angelica.hernandez</option>
                                            <option>alan.torres</option>
                                            <option>alexis.torres</option>
                                            <option>arkadi.padilla</option>
                                            <option>artemio.urbina</option>
                                            <option>Brayan Sanchez</option>
                                            <option>cesar.gutierrez</option>
                                            <option>cgonzalez</option>
                                            <option>darwin.diaz</option>
                                            <option>david.martinez</option>
                                            <option>enrique.quezada</option>
                                            <option>erik.cerda</option>
                                            <option>hector.rodriguez</option>
                                            <option>Ignacio.rodriguez</option>
                                            <option>israel.gonzalez</option>
                                            <option>jose.medina</option>
                                            <option>juan.cruz</option>
                                            <option>Juan Benitez</option>
                                            <option>juan.arriaga</option>
                                            <option>juan.palma</option>
                                            <option>lorena.cruz</option>
                                            <option>luis.manzo</option>
                                            <option>maria.meneses</option>
                                            <option>mario.gutierrez</option>
                                            <option>osvaldo.martinez</option>
                                            <option>pablo.gutierrez</option>
                                            <option>Ramiro Estrada</option>
                                            <option>OPC.Zenoss</option>
                                            <option>operaciones.itoc</option>
                                            <option>capacitacion.itoc</option>
                                            <option>capacitacion.itoc2</option>
                                            <option>capacitacion.itoc3</option>
                                            <option>unknown</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col">
                                        <label class="form-label font-weight-bold">Category</label>
                                        <input class="form-control" type="text" value="{{ $general['category'] }}" readonly>
                                    </div>
                                    <div class="col">
                                        <label class="form-label font-weight-bold">Time Created</label>
                                        <input class="form-control" type="text" value="{{ $general['time_created'] }}" readonly>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col">
                                        <label class="form-label font-weight-bold">Title</label>
                                        <textarea class="form-control" rows="3" readonly>{{ $general['title'] }}</textarea>
                                    </div>
                                </div>
                                <div class="row mt-4 mb-2">
                                    <div class="col">
                                        <h4 class="font-weight-bold">Información Adicional</h4>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col">
                                        <label class="form-label font-weight-bold">Application</label>
                                        <input class="form-control" type="text" value="{{ $general['application'] }}" readonly>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col">
                                        <label class="form-label font-weight-bold">Description</label>
                                        @if (isset($general['description_is_URL']))
                                            <a class="d-block text-info" href="{{ $general['description_is_URL'] }}" rel="noopener noreferrer" target="_blank">{{ $general['description_is_URL'] }}</a>
                                        @else
                                            <textarea class="form-control" rows="3" readonly>"{{ $general['description'] }}"</textarea>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    @if (!isset($im_details['details']))
                                        <div class="col text-left">
                                            <a class="btn btn-info" href="{{ route('events.edit', $general['id']) }}" target="IM Create" title="Crear IM" onclick="window.open(this.href, this.target, 'width=700,height=650'); return false;">
                                                Crear IM
                                            </a>
                                        </div>
                                    @endif
                                    <div class="col text-right">
                                        <input class="btn btn-default" type="button" value="Cancelar" onclick="window.close();">
                                        <button id="save-main" class="btn btn-success" type="submit">Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- custom-attributes pane -->
                    <div id="custom-attributes" class="tab-pane" role="tabpanel">
                        <div class="card-body">
                            <div id="loading-custom-attribute-modal" class="modal" role="dialog" tabindex="-1">
                                <div class="modal-dialog modal-sm modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title font-weight-bold text-center">Procesando Custom Attributes....</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="create-custom-attribute-modal" class="modal" role="dialog" tabindex="-1">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form action="{{ route('events.manageCustomAttributes', [$general['id'], 'POST']) }}" method="POST">
                                            @csrf
                                            @method('POST')
                                            <div class="modal-header">
                                                <h4 class="modal-title">Agregar Custom Attribute</h4>
                                                <button class="close" type="button" data-dismiss="modal">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <input name="User_OMI_CA" type="hidden" value="{{ $user_bsm->us_bsm ?? '' }}">
                                                <input name="Password_OMI_CA" type="hidden" value="{{ $user_bsm->pass_bsm ?? '' }}">
                                                <div class="form-group">
                                                    <label class="control-label" for="name">Name</label>
                                                    <input id="name" class="form-control" name="name" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="value">Value</label>
                                                    <textarea id="value" class="form-control" name="value" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
                                                <button id="create-attribute-button" class="btn btn-success" type="submit">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div id="edit-custom-attribute-modal" class="modal" role="dialog" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('events.manageCustomAttributes', [$general['id'], 'PUT']) }}" method="POST">
                                            @csrf
                                            @method('POST')
                                            <div class="modal-header">
                                                <h4 class="modal-title">Editar Custom Attribute</h4>
                                                <button class="close" type="button" data-dismiss="modal">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <input name="User_OMI_Edit_CA" type="hidden" value="{{ $user_bsm->us_bsm ?? '' }}">
                                                <input name="Password_OMI_Edit_CA" type="hidden" value="{{ $user_bsm->pass_bsm ?? '' }}">
                                                <div class="form-group">
                                                    <label class="control-label" for="name-edit">Name</label>
                                                    <input id="name-edit" class="form-control" name="name-edit" type="text" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="value-edit">Value</label>
                                                    <textarea id="value-edit" class="form-control" name="value-edit" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="w-100 d-flex justify-content-between">
                                                    <button class="btn btn-danger mr-auto" type="button" onclick="deleteCustomAttribute()">Eliminar</button>
                                                    <div>
                                                        <button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
                                                        <button id="edit-attribute-button" class="btn btn-success" type="submit">Guardar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div id="delete-custom-attribute-modal" class="modal" role="dialog" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('events.manageCustomAttributes', [$general['id'], 'DELETE']) }}" method="POST">
                                            @csrf
                                            @method('POST')
                                            <div class="modal-header">
                                                <h4 class="modal-title">Eliminar Custom Attribute</h4>
                                                <button class="close" type="button" data-dismiss="modal">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <input name="User_OMI_Delete_CA" type="hidden" value="{{ $user_bsm->us_bsm ?? '' }}">
                                                <input name="Password_OMI_Delete_CA" type="hidden" value="{{ $user_bsm->pass_bsm ?? '' }}">
                                                <div class="form-group">
                                                    <label class="control-label" for="name-delete">Name</label>
                                                    <input id="name-delete" class="form-control" name="name-delete" type="text" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="value-delete">Value</label>
                                                    <textarea id="value-delete" class="form-control" name="value-delete" rows="3" readonly></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
                                                <button id="delete-attribute-button" class="btn btn-danger" type="submit">Eliminar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="table-responsive">
                                        <table id="table-custom-attribute" class="table table-striped table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th class="p-l-0" scope="col"><small class="font-weight-bold">Name</small></th>
                                                    <th class="p-l-0" scope="col"><small class="font-weight-bold">Value</small></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($custom_attribute as $item)
                                                    <tr style="cursor: pointer;">
                                                        <td class="p-0"><small>{{ $item['name'] }}</small></td>
                                                        <td class="p-0"><small class="text-truncate" title="{{ $item['value'] }}">{{ $item['value'] }}</small></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                @if (!isset($im_details['details']))
                                    <div class="col text-left">
                                        <a class="btn btn-info" href="{{ route('events.edit', $general['id']) }}" target="IM Create" title="Crear IM" onclick="window.open(this.href, this.target, 'width=700,height=650'); return false;">
                                            Crear IM
                                        </a>
                                    </div>
                                @endif
                                <div class="col text-right">
                                    <input class="btn btn-default" type="button" value="Cancelar" onclick="window.close();">
                                    <button class="btn btn-success" data-target="#create-custom-attribute-modal" data-toggle="modal">
                                        Agregar Atributo
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- annotation pane -->
                    <div id="annotation" class="tab-pane" role="tabpanel">
                        <div class="card-body">
                            <div id="loading-annotation-modal" class="modal" role="dialog" tabindex="-1">
                                <div class="modal-dialog modal-sm modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title font-weight-bold text-center">Procesando Annotations....</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="create-annotation-modal" class="modal" role="dialog" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('events.createAnnotation', [$general['id'], 'POST']) }}" method="POST">
                                            @csrf
                                            @method('POST')
                                            <div class="modal-header">
                                                <h4 class="modal-title">Agregar Annotation</h4>
                                                <button class="close" type="button" data-dismiss="modal">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <input name="User_OMI" type="hidden" value="{{ $user_bsm->us_bsm ?? '' }}">
                                                <input name="Password_OMI" type="hidden" value="{{ $user_bsm->pass_bsm ?? '' }}">
                                                <div class="form-group">
                                                    <label class="control-label" for="timeAnnotation">Time Created</label>
                                                    <input id="timeAnnotation" class="form-control" name="time" type="text" value="{{ Carbon\Carbon::now(new DateTimeZone('America/Mexico_City')) }}" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="text">Text</label>
                                                    <textarea id="text" class="form-control" name="text" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
                                                <button id="create-annotation-button" class="btn btn-success" type="submit">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div id="edit-annotation-modal" class="modal" role="dialog" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('events.createAnnotation', [$general['id'], 'PUT']) }}" method="POST">
                                            @csrf
                                            @method('POST')
                                            <div class="modal-header">
                                                <h4 class="modal-title">Editar Annotation</h4>
                                                <button class="close" type="button" data-dismiss="modal">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <input name="User_OMI_Edit" type="hidden" value="{{ $user_bsm->us_bsm ?? '' }}">
                                                <input name="Password_OMI_Edit" type="hidden" value="{{ $user_bsm->pass_bsm ?? '' }}">
                                                <div class="form-group">
                                                    <label class="control-label" for="timeAnnotationEdit">Time Created</label>
                                                    <input id="timeAnnotationEdit" class="form-control" name="time" type="text" value="{{ Carbon\Carbon::now(new DateTimeZone('America/Mexico_City')) }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="id_annotation_edit">ID Annotation</label>
                                                    <input id="id_annotation_edit" class="form-control" name="id_annotation_edit" type="text" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="text-edit">Text</label>
                                                    <textarea id="text-edit" class="form-control" name="text-edit" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="w-100 d-flex justify-content-between">
                                                    <button class="btn btn-danger mr-auto" type="button" onclick="deleteAnnotation()">Eliminar</button>
                                                    <div>
                                                        <button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
                                                        <button id="edit-annotation-button" class="btn btn-success" type="submit">Guardar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div id="delete-annotation-modal" class="modal" role="dialog" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('events.createAnnotation', [$general['id'], 'DELETE']) }}" method="POST">
                                            @csrf
                                            @method('POST')
                                            <div class="modal-header">
                                                <h4 class="modal-title">Eliminar Annotation</h4>
                                                <button class="close" type="button" data-dismiss="modal">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <input name="User_OMI_Delete" type="hidden" value="{{ $user_bsm->us_bsm ?? '' }}">
                                                <input name="Password_OMI_Delete" type="hidden" value="{{ $user_bsm->pass_bsm ?? '' }}">
                                                <div class="form-group">
                                                    <label class="control-label" for="timeAnnotationDelete">Time Created</label>
                                                    <input id="timeAnnotationDelete" class="form-control" name="time" type="text" value="{{ Carbon\Carbon::now(new DateTimeZone('America/Mexico_City')) }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="id_annotation_delete">ID Annotation</label>
                                                    <input id="id_annotation_delete" class="form-control" name="id_annotation_delte" type="text" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="text-delete">Text</label>
                                                    <textarea id="text-delete" class="form-control" name="text-delete" rows="3" readonly></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
                                                <button id="delete-annotation-button" class="btn btn-danger" type="submit">Eliminar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="table-responsive">
                                        <table id="table-annotation" class="table table-striped table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th class="col-sm-2 p-l-0" scope="col"><small class="font-weight-bold">User</small></th>
                                                    <th class="col-sm-2 p-l-0" scope="col"><small class="font-weight-bold">Time Created</small></th>
                                                    <th class="d-none" scope="col"><small class="font-weight-bold">ID Annotation</small></th>
                                                    <th class="col-sm-8 p-l-0" scope="col"><small class="font-weight-bold">Text</small></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($att_annotation as $itemAnnotation)
                                                    <tr style="cursor: pointer;">
                                                        <td class="p-0"><small>{{ $itemAnnotation['author'] }}</small></td>
                                                        <td class="p-0"><small>{{ $itemAnnotation['time_created'] }}</small></td>
                                                        <td class="d-none"><small>{{ $itemAnnotation['id'] }}</small></td>
                                                        <td class="p-0"><small class="text-truncate">{{ $itemAnnotation['text'] }}</small></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                @if (!isset($im_details['details']))
                                    <div class="col text-left">
                                        <a class="btn btn-info" href="{{ route('events.edit', $general['id']) }}" target="IM Create" title="Crear IM" onclick="window.open(this.href, this.target, 'width=700,height=650'); return false;">
                                            Crear IM
                                        </a>
                                    </div>
                                @endif
                                <div class="col text-right">
                                    <input class="btn btn-default" type="button" value="Cancelar" onclick="window.close();">
                                    <button class="btn btn-success" data-target="#create-annotation-modal" data-toggle="modal">
                                        Agregar Anotación
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- history pane -->
                    <div id="history" class="tab-pane" role="tabpanel">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="table-responsive">
                                        <table id="history-table" class="table table-striped table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th class="col-sm-2 p-l-0" scope="col"><small class="font-weight-bold">Modification Date</small></th>
                                                    <th class="col-sm-2 p-l-0" scope="col"><small class="font-weight-bold">Modified By</small></th>
                                                    <th class="col-sm-8 p-l-0" scope="col"><small class="font-weight-bold">Action</small></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($history_lines as $history)
                                                    <tr>
                                                        <td class="p-0"><small>{{ $history['time_changed'] }}</small></td>
                                                        <td class="p-0"><small>{{ $history['modified_by'] }}</small></td>
                                                        <td class="p-0">
                                                            <small class="text-truncate">
                                                                La propiedad <span class="font-weight-bold">{{ $history['property'] }}</span> ha cambiado su valor
                                                                @if ($history['previous_value'] !== '')
                                                                de <span class="font-weight-bold">{{ $history['previous_value'] }}</span>
                                                                @endif
                                                                a <span class="font-weight-bold">{{ $history['current_value'] }}</span>.
                                                            </small>
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td class="p-0" colspan="3">No se encontraron resultados</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end Tab panes-->
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    @include('includes.js')
    <script>
        function deleteCustomAttribute() {
            $('#edit-custom-attribute-modal').modal('hide');

            $('#delete-custom-attribute-modal').modal('show');
        }

        function deleteAnnotation() {
            $('#edit-annotation-modal').modal('hide');

            $('#delete-annotation-modal').modal('show');
        }

        $(function() {
            // Set severity
            $('#severity').val("{{ $general['severity'] }}").trigger('change');

            // Set state
            $('#state').val("{{ $general['state'] }}").trigger('change');

            // Set priority
            $('#priority').val("{{ $general['priority'] }}").trigger('change');

            // Set assigned user
            $('#assigned_user').val("{{ $general['assigned_user'] }}").trigger('change');

            $('#table-custom-attribute tbody tr').on('dblclick', function(e) {
                let name = $(e.currentTarget).find('td:eq(0)').text();
                let value = $(e.currentTarget).find('td:eq(1)').text();

                $('#name-edit, #name-delete').val(name);
                $('#value-edit, #value-delete').val(value);

                $('#edit-custom-attribute-modal').modal('show');
            });

            $('#create-attribute-button, #edit-attribute-button, #delete-attribute-button').on('click', function(e) {
                $('#create-custom-attribute-modal, #edit-custom-attribute-modal, #delete-custom-attribute-modal').modal('hide');

                $('#loading-custom-attribute-modal').modal('show');
            });

            $('#table-annotation tbody tr').on('dblclick', function(e) {
                let annotation = $(e.currentTarget).find('td:eq(2)').text();
                let text = $(e.currentTarget).find('td:eq(3)').text();

                $('#id_annotation_edit, #id_annotation_delete').val(annotation);
                $('#text-edit, #text-delete').val(text);

                $('#edit-annotation-modal').modal('show');
            });

            $('#create-annotation-button, #edit-annotation-button, #delete-annotation-button').on('click', function(e) {
                $('#create-annotation-modal, #edit-annotation-modal, #delete-annotation-modal').modal('hide');

                $('#loading-annotation-modal').modal('show');
            });
        });
    </script>
    <!-- ============================================================== -->
    <!-- End all Jquery -->
    <!-- ============================================================== -->
</body>
</html>