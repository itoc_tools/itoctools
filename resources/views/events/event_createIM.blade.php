<!DOCTYPE html>
<html lang="en">
@include('includes.head')

<body id="bodyMaster" class="skin-default fixed-layout">
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if(isset($is_error))
                    <h4 class="card-title"><b>{{$is_error}}</b></h4>
                    <h3 class="card-subtitle"><b>¡¡¡Llenar los campos correctamente!!!</b></h3>
                    @else
                    <h4 class="card-title">Creación de IM para el eveto</h4>
                    <h5 class="card-subtitle"><b>Para crear el evento debe completar todos los campos.</b></h5>
                    <button id="create-IM-detail" class="btn btn-default" type="button">Mostrar datos</button>
                    @endif
                    <form class="form-material m-t-40" method="POST" action="{{ route('events.store') }}">
                        {{ csrf_field() }}
                        <table class="table">
                            <tr id="tr-1-IM-detail" style="display: none;">
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-4 col-form-label text-center"><b>Contacto:</b></label>
                                        <div class="col-8">
                                            <input type="text" class="form-control form-control-line" placeholder="ITOC MONITOREO" id="Contacto" name="Contacto" value="ITOC MONITOREO" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-3 col-form-label text-center"><b>Email:</b></label>
                                        <div class="col-9">
                                            <input type="email" class="form-control form-control-line" placeholder="notificacion.itoc@triara.com" id="callbakemail" name="callbakemail" value="notificacion.itoc@triara.com" required>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="tr-2-IM-detail" style="display: none;">
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-4 col-form-label text-center"><b>Assigned To Group:</b></label>
                                        <div class="col-8">
                                            <input type="text" class="form-control form-control-line" placeholder="Inc / Mesa de Servicio" id="AssignmentGroup" name="AssignmentGroup" value="Inc / Mesa de Servicio" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-4 col-form-label text-center"><b>Affected Service:</b></label>
                                        <div class="col-8">
                                            <input type="text" class="form-control form-control-line" placeholder="OP. ADMIN DE MONITOREO" id="Affectedservice" name="Affectedservice" value="OP. ADMIN DE MONITOREO" required>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="tr-3-IM-detail" style="display: none;">
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-4 col-form-label text-center"><b>Affected CI:</b></label>
                                        <div class="col-8">
                                            <input type="text" class="form-control form-control-line" placeholder="PDS-TDP-TDP-99999" id="AffectedCI" name="AffectedCI" value="PDS-TDP-TDP-99999" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-4 col-form-label text-center"><b>Category:</b></label>
                                        <div class="col-8">
                                            <input type="text" class="form-control form-control-line" placeholder="Solicitud de Servicio" id="Category" name="Category" value="Solicitud de Servicio" required>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="tr-4-IM-detail" style="display: none;">
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-4 col-form-label text-center"><b>Impact:</b></label>
                                        <div class="col-8">
                                            <input type="text" class="form-control form-control-line" placeholder="4" id="Impacto" name="Impacto" value="4" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-4 col-form-label text-center"><b>Urgency:</b></label>
                                        <div class="col-8">
                                            <input type="text" class="form-control form-control-line" placeholder="2" id="Urgency" name="Urgency" value="2" required>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="tr-5-IM-detail" style="display: none;">
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-4 col-form-label text-center"><b>Medium:</b></label>
                                        <div class="col-8">
                                            <input type="text" class="form-control form-control-line" placeholder="Zenoss" id="mediu" name="mediu" value="Zenoss" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-4 col-form-label text-center"><b>Source:</b></label>
                                        <div class="col-8">
                                            <input type="text" class="form-control form-control-line" placeholder="Auto" id="Source" name="Source" value="Auto" required>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="tr-6-IM-detail" style="display: none;">
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-6 col-form-label text-center"><b>Requested At:</b></label>
                                        <div class="col-6">
                                            <input type="text" class="form-control form-control-line" placeholder="Monterrey" id="site" name="site" value="Monterrey" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-4 col-form-label text-center"><b>Service:</b></label>
                                        <div class="col-8">
                                            <input type="text" class="form-control form-control-line" placeholder="OP. ADMIN DE MONITOREO" id="Service" name="Service" value="OP. ADMIN DE MONITOREO" required>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="tr-7-IM-detail" style="display: none;">
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-6 col-form-label text-center"><b>Subcategory 1:</b></label>
                                        <div class="col-6">
                                            <input type="text" class="form-control form-control-line" placeholder="Monitoreo" id="Subcategory" name="Subcategory" value="Monitoreo" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-6 col-form-label text-center"><b>Subcategory 2:</b></label>
                                        <div class="col-6">
                                            <input type="text" class="form-control form-control-line" placeholder="Eventos" id="Subcat2" name="Subcat2" value="Eventos" required>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="tr-8-IM-detail" style="display: none;">
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-6 col-form-label text-center"><b>Subcategory 3:</b></label>
                                        <div class="col-6">
                                            <input type="text" class="form-control form-control-line" placeholder="Registro de eventos" id="productype" name="productype" value="Registro de eventos" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-6 col-form-label text-center"><b>TicketOwner:</b></label>
                                        <div class="col-6">
                                            <input type="text" class="form-control form-control-line" placeholder="TicketOwner" id="TicketOwner" name="TicketOwner" value="">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- Estos datos no deberán mostrarse -->
                            <tr id="tr-9-IM-detail" style="display: none;">
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-5 col-form-label text-center"><b>usuario:</b></label>
                                        <div class="col-7">
                                            <input type="text" class="form-control form-control-line" placeholder="sas.integracion" id="usuario" name="usuario" value="sas.integracion" required>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-5 col-form-label text-center"><b>password:</b></label>
                                        <div class="col-7">
                                            <input type="password" class="form-control form-control-line" placeholder="abc12345" id="password" name="password" value="abc12345" required>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="tr-10-IM-detail" style="display: none;">
                                <td>
                                    <div class="form-group m-t-40 row">
                                        <label class="col-3 col-form-label text-center"><b>idEvent:</b></label>
                                        <div class="col-9">
                                            <input type="text" class="form-control form-control-line" placeholder="{{$idEvent}}" id="idEvent" name="idEvent" value="{{$idEvent}}" required>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!--   --------------------------   -->
                        </table>
                        <br>
                        <div class="form-group m-t-40 row">
                            <label class="col-2 col-form-label text-left"><b>Titulo:</b></label>
                            <div class="col-10">
                                <textarea rows="4" class="form-control form-control-line" placeholder="Titulo" id="Titulo" name="Titulo" value=" " required></textarea>
                            </div>
                        </div>
                        <div class="form-group m-t-40 row">
                            <label class="col-2 col-form-label text-left"><b>Descripcion:</b></label>
                            <div class="col-10">
                                <textarea rows="6" class="form-control form-control-line" placeholder="Descripcion" id="Descripcion" name="Descripcion" value=" " required></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row text-right">
                            <div class="col">
                                <button type="submit" class="btn btn-success" id="save" title="Crear IM">
                                    Crear IM
                                </button>
                                <a onclick="window.close()" class="btn btn-warning">Cancelar </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    @include('includes.js')
    <!-- ============================================================== -->
    <!-- End all Jquery -->
    <!-- ============================================================== -->
    <script>
        document.getElementById("create-IM-detail").onclick = function() {
            var im_d = document.getElementById(id = "tr-1-IM-detail");
            if (im_d.style.display == 'none') {
                document.getElementById("tr-1-IM-detail").style.display = "";
                document.getElementById("tr-2-IM-detail").style.display = "";
                document.getElementById("tr-3-IM-detail").style.display = "";
                document.getElementById("tr-4-IM-detail").style.display = "";
                document.getElementById("tr-5-IM-detail").style.display = "";
                document.getElementById("tr-6-IM-detail").style.display = "";
                document.getElementById("tr-7-IM-detail").style.display = "";
                document.getElementById("tr-8-IM-detail").style.display = "";
                document.getElementById("tr-9-IM-detail").style.display = "";
                document.getElementById("tr-10-IM-detail").style.display = "";
            } else {
                document.getElementById("tr-1-IM-detail").style.display = "none";
                document.getElementById("tr-2-IM-detail").style.display = "none";
                document.getElementById("tr-3-IM-detail").style.display = "none";
                document.getElementById("tr-4-IM-detail").style.display = "none";
                document.getElementById("tr-5-IM-detail").style.display = "none";
                document.getElementById("tr-6-IM-detail").style.display = "none";
                document.getElementById("tr-7-IM-detail").style.display = "none";
                document.getElementById("tr-8-IM-detail").style.display = "none";
                document.getElementById("tr-9-IM-detail").style.display = "none";
                document.getElementById("tr-10-IM-detail").style.display = "none";
            }
        }
    </script>

</body>

</html>