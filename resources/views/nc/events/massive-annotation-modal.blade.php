<div id="massive-annotation-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <form action="{{ route('events.masiveAnnotation') }}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Anotación Masiva</h5>
                    <button class="close" type="button" aria-label="Close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="form-label">Anotación</label>
                        <textarea class="form-control" placeholder="Anotación" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Eventos Seleccionados</label>
                        <textarea class="form-control events-container" placeholder="Eventos Seleccionados" rows="3" readonly></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-outline-success" type="button">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>