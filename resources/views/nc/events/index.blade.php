@extends ('nc.layout.main')

@section ('title', 'Eventos')

@section ('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/assets/node_modules/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/contextMenu/jquery.contextMenu.min.css') }}">
@include ('nc.layout.css.datatables')
@endsection

@section ('styles')
<style type="text/css">
    .chart-container {
        cursor: pointer;
        background-color: #edf1f5;
        background-clip: content-box;
    }

    .chart-legend.d-flex {
        display: flex !important;
    }

    .legend-icon {
        width: 15px;
    }

    .legend-text {
        font-size: 12;
        color: #666666;
        font-weight: bold;
        font-family: 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif;
    }
</style>
@endsection

@section ('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row d-none">
                    <div class="col-6 col-md-3 col-xl-2 rounded chart-container">
                        <div class="row">
                            <div class="col-8">
                                <canvas id="ctx"></canvas>
                            </div>
                            <div id="legend" class="col-4 d-flex align-items-center chart-legend"></div>
                        </div>
                    </div>
                </div>
                <div id="custom-buttons-container" class="button-group">
                    <button id="massive-attribute-button" class="btn btn-outline-dark massive-action" title="Atributo Personalizado Masivo" type="button" data-modal-id="#massive-attribute-modal">
                        <i class="fa-regular fa-shapes fa-lg"></i>
                    </button>
                    <button id="massive-annotation-button" class="btn btn-outline-dark massive-action" title="Anotación Masiva" type="button" data-modal-id="#massive-annotation-modal">
                        <i class="fa-regular fa-file-lines fa-lg"></i>
                    </button>
                    <button id="massive-state-change-button" class="btn btn-outline-dark massive-action" title="Cambio de Estado Masivo" type="button" data-modal-id="#massive-state-change-modal">
                        <i class="fa-regular fa-repeat fa-lg"></i>
                    </button>
                    <button class="btn btn-outline-dark" title="Asignar a... Mi" type="button">
                        <i class="fa-regular fa-user-plus fa-lg"></i>
                    </button>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table id="events-table" class="table table-sm table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="text-center">MS</th>
                                    <th class="text-center">Gravedad</th>
                                    <th class="text-center">Prioridad</th>
                                    <th class="text-center">Aplicación</th>
                                    <th class="text-center">Estado</th>
                                    <th class="text-center">Correlación</th>
                                    <th class="text-center">Anotaciones</th>
                                    <th class="text-center">Usuario</th>
                                    <th class="text-center">Antigüedad</th>
                                    <th class="text-center">Fecha de Recepción</th>
                                    <th class="text-center">Responsable</th>
                                    <th class="text-center">ID Evento Zenoss</th>
                                    <th class="text-center">CO</th>
                                    <th class="text-center">Objecto</th>
                                    <th class="text-center">Título</th>
                                    <th class="text-center">ID</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include ('nc.events.massive-attribute-modal')
@include ('nc.events.massive-annotation-modal')
@include ('nc.events.massive-state-change-modal')
@include ('nc.events.events-correlation-modal')
@include ('nc.layout.components.colreorder-modal')
@endsection

@section ('plugins')
<script type="text/javascript" src="{{ asset('theme/assets/node_modules/Chart.js/Chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/ecommerce/dist/js/jquery.multi-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/jquery-validation/localization/messages_es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/contextMenu/jquery.contextMenu.min.js') }}"></script>
@include ('nc.layout.js.datatables')
@endsection

@section ('scripts')
<script type="text/javascript">
    let eventsDataTable = null;

    $.validator.addMethod('notIn', function(value, element, params) {
        let isContained = params.some(element => {
            return element.toLowerCase() === value.toLowerCase();
        });

        return !isContained;
    }, 'El valor ingresado no está permitido.');

    $.contextMenu({
        events: {
            preShow: function(options) {
                let index = eventsDataTable.row(options[0]).index();

                let selection = eventsDataTable.rows({
                    selected: true
                }).indexes().toArray();

                return selection.includes(index);
            }
        },
        items: {
            attribute: {
                className: 'contextmenu-item-custom-style',
                icon: 'far fa-shapes',
                name: 'Crear Atributo Personalizado...'
            },
            annotation: {
                className: 'contextmenu-item-custom-style',
                icon: 'far fa-file-lines',
                name: 'Crear Anotación...'
            },
            state: {
                className: 'contextmenu-item-custom-style',
                icon: 'far fa-repeat',
                name: 'Cambiar Estado...'
            },
            assign: {
                className: 'contextmenu-item-custom-style',
                icon: 'far fa-user-plus',
                name: 'Asignar a... Mi'
            },
            relate: {
                className: 'contextmenu-item-custom-style',
                icon: 'far fa-diagram-nested',
                name: 'Relacionar Eventos'
            }
        },
        selector: '#events-table tbody tr',
        trigger: 'right',
        // callbacks
        callback: function(itemKey, opt) {
            switch (itemKey) {
                case 'annotation':
                    $('#massive-annotation-button').click();
                    break;
                case 'assign':
                    // $('#atm-button').click();
                    break;
                case 'attribute':
                    $('#massive-attribute-button').click();
                    break;
                case 'relate':
                    showEventGroupingModal();
                    break;
                case 'state':
                    $('#massive-state-change-button').click();
                    break;
            }
        }
    });

    $(function() {
        eventsDataTable = $('#events-table').DataTable({
            ajax: '{{ route("alertsWithOutIMSDInProgressReload") }}',
            destroy: true,
            buttons: [{
                    className: 'btn btn-outline-dark',
                    extend: 'excel',
                    text: '<i class="fa-regular fa-file-excel fa-lg"></i>'
                },
                {
                    className: 'btn btn-outline-dark',
                    extend: 'pdf',
                    text: '<i class="fa-regular fa-file-pdf fa-lg"></i>'
                },
                {
                    className: 'btn btn-outline-dark',
                    extend: 'print',
                    text: '<i class="fa-regular fa-print fa-lg"></i>'
                },
                {
                    className: 'btn btn-outline-dark',
                    text: '<i class="fa-regular fa-table-pivot fa-lg"></i>',
                    action: function(e, dt, node, config) {
                        showColReorderModal();
                    }

                }
            ],
            colReorder: {
                enable: false
            },
            columnDefs: [{
                    className: 'text-center',
                    targets: [0, 1, 2, 4, 5, 6]
                },
                {
                    className: 'text-justify',
                    targets: [3, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                },
            ],
            columns: [{
                data: function(row, type, set, meta) {
                    if (row.im) {
                        return row.im;
                    } else if (row.im_fake) {
                        return row.im_fake;
                    }

                    return '';
                },
                render: function(data, type, row, meta) {
                    if (/^(IM|SD|RFC)\d+$/.test(data)) {
                        return `<span class="badge badge-info rounded w-100">${ data }</span>`;
                    } else if (data === '') {
                        return `<a class="badge badge-success rounded w-100" href="javascript:void(0)" onclick="window.open('{{ route('events.edit', ':event') }}', '_blank', 'height=650,width=700')">Crear IM</a>`.replace(':event', row.id_event);
                    }

                    return `<span class="badge badge-dark rounded w-100">${ data }</span>`;
                }
            }, {
                data: 'severity',
                orderable: false,
                render: function(data, type, row, meta) {
                    switch (data) {
                        case 'critical':
                            return '<i class="fa-solid fa-circle-xmark fa-lg txt-critical" title="Crítico"></i>';
                        case 'major':
                            return '<i class="fa-solid fa-triangle-exclamation fa-lg txt-major" title="Principal"></i>';
                        case 'minor':
                            return '<i class="fa-solid fa-triangle-exclamation fa-lg txt-minor" title="Menor"></i>';
                        case 'warning':
                            return '<i class="fa-solid fa-triangle-exclamation fa-lg txt-warning" title="Advertencia"></i>';
                        case 'normal':
                            return '<i class="fa-solid fa-circle-check fa-lg txt-normal" title="Normal"></i>';
                        default:
                            return '<i class="fa-solid fa-circle-question fa-lg txt-unknown" title="Desconocido"></i>';
                    }
                },
                searchable: false
            }, {
                data: 'priority',
                orderable: false,
                render: function(data, type, row, meta) {
                    switch (data) {
                        case 'highest':
                            return '<i class="fa-solid fa-chevrons-up fa-lg" title="Mayor"></i>';
                        case 'high':
                            return '<i class="fa-solid fa-chevron-up fa-lg" title="Alto"></i>';
                        case 'medium':
                            return '<i class="fa-solid fa-diamond fa-lg" title="Medio"></i>';
                        case 'low':
                            return '<i class="fa-solid fa-chevron-down fa-lg" title="Bajo"></i>';
                        case 'lowest':
                            return '<i class="fa-solid fa-chevrons-down fa-lg" title="Menor"></i>';
                        default:
                            return '<i class="fa-solid fa-minus fa-lg" title="Ninguno"></i>';
                    }
                },
                searchable: false
            }, {
                data: 'application'
            }, {
                data: 'state',
                orderable: false,
                render: function(data, type, row, meta) {
                    switch (data) {
                        case 'open':
                            return '<i class="fa-regular fa-square fa-lg txt-open" title="Abierto"></i>';
                        case 'in_progress':
                            return '<i class="fa-solid fa-wrench fa-lg txt-in-progress" title="En Curso"></i>';
                        case 'resolved':
                            return '<i class="fa-solid fa-square-check fa-lg txt-resolved" title="Resuelto"></i>';
                        case 'closed':
                            return '<i class="fa-solid fa-square-xmark fa-lg txt-closed" title="Cerrado"></i>';
                        default:
                            return '';
                    }
                },
                searchable: false
            }, {
                data: 'symptom_list',
                orderable: false,
                render: function(data, type, row, meta) {
                    if (data !== null) {
                        return '<i class="fa-solid fa-diagram-nested fa-lg text-success"></i>';
                    }

                    return '';
                },
                searchable: false
            }, {
                data: 'annotation_list',
                orderable: false,
                render: function(data, type, row, meta) {
                    if (data !== null) {
                        return '<i class="fa-solid fa-file-lines fa-lg text-warning"></i>';
                    }

                    return '';
                },
                searchable: false
            }, {
                data: 'user_name',
                render: function(data, type, row, meta) {
                    if (data != '&lt;unknown&gt;') {
                        return `<span class="font-weight-bold">${ row.user_name }</span>`;
                    }

                    return data;
                }
            }, {
                data: 'event_age',
                render: function(data, type, row, meta) {
                    if (type === 'sort' || type === 'type') {
                        return moment().diff(moment(row.time_received), 'seconds');
                    }

                    return data;
                }
            }, {
                data: 'time_received',
                render: function(data, type, row, meta) {
                    if (type === 'sort' || type === 'type') {
                        return data;
                    }

                    return moment(data).format('DD/MM/YYYY hh:mm:ss A');
                }
            }, {
                data: function(row, type, set, meta) {
                    if (row.cResponsable) {
                        return row.cResponsable;
                    }

                    return '';
                }
            }, {
                data: function(row, type, set, meta) {
                    if (row.EventoZenoss_Id) {
                        return row.EventoZenoss_Id;
                    }

                    return '';
                }
            }, {
                data: function(row, type, set, meta) {
                    if (row.CO) {
                        return row.CO;
                    }

                    return '';
                }
            }, {
                data: 'object'
            }, {
                data: 'title'
            }, {
                data: 'id_event'
            }],
            dom: '<"row"<"#buttons-container.col d-flex justify-content-between"B>><"row"<"col"f>><"row"<"col"<"table-scrollable"rt>>>i',
            language: {
                url: '{{ asset("custom/plugins/DataTables/i18n/es-MX.json") }}'
            },
            order: [],
            paging: false,
            rowId: 'id_event',
            select: {
                blurable: true,
                info: false,
                style: 'os'
            }
        });

        /* let chart = new Chart($('#ctx'), {
            type: 'bar',
            data: {
                labels: ['Eventos'],
                datasets: [{
                    label: 'Critical',
                    backgroundColor: '#d30011',
                    data: [6]
                }, {
                    label: 'Major',
                    backgroundColor: '#fd6c1c',
                    data: [4]
                }, {
                    label: 'Minor',
                    backgroundColor: '#fdaa24',
                    data: [8]
                }, {
                    label: 'Warning',
                    backgroundColor: '#25b396',
                    data: [3]
                }, {
                    label: 'Normal',
                    backgroundColor: '#359c55',
                    data: [7]
                }, {
                    label: 'Unknown',
                    backgroundColor: '#1668bc',
                    data: [5]
                }]
            },
            options: {
                legend: {
                    display: false,
                    // position: 'right'
                },
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        display: false,
                        stacked: true
                    }],
                    yAxes: [{
                        display: false,
                        stacked: true,
                    }]
                },
                title: {
                    display: true,
                    text: 'TELMEX SLIM'
                },


                legendCallback: function(chart) {
                    let label = '<div class="w-100">';

                    for (var i = 0; i < chart.data.datasets.length; i++) {
                        let dataset = chart.data.datasets[i];
                        label +=
                            `<span class="d-block text-center">
                                <img alt="${ dataset.label }" class="legend-icon mr-2" src="{{ asset('') }}custom/images/${ dataset.label }.png" title="${ dataset.label }">
                                <span class="legend-text">${ dataset.data }</span>
                            </span>`;
                    }

                    return label + '</div>';
                }
            }
        });
        $('#legend').html(chart.generateLegend()); */

        $('#colreorder-form').validate({
            errorClass: 'error-feedback',
            errorPlacement: function(error, element) {
                error.appendTo(element.parents('div.form-group'));
            },
            rules: {
                name: {
                    notIn: ['default', 'predeterminada', 'predeterminado', 'por defecto']
                }
            },
            submitHandler: function(form) {
                let state = alterTableState();

                $(form).find('input[name="state"]').val(JSON.stringify(state));

                runWaitMe($(form).find('div.modal-content'));

                return true;
            }
        });

        $('#events-correlation-form').validate({
            errorClass: 'error-feedback',
            errorPlacement: function(error, element) {
                error.appendTo(element.parents('div.modal-body'));
            },
            submitHandler: function(form) {
                runWaitMe($(form).find('div.modal-content'));

                return true;
            }
        });

        $('#state-selector').select2({
            dropdownParent: $('#massive-state-change-modal'),
            minimumResultsForSearch: Infinity,
            language: 'es',
            placeholder: 'Selecciona una opción',
            templateResult: function(data, container) {
                return $(`<span><i class="${ $(data.element).data('icon') }"></i>${ data.text }</span>`);
            },
            templateSelection: function(data, container) {
                return $(`<span><i class="${ $(data.element).data('icon') }"></i>${ data.text }</span>`);
            },
            width: '100%'
        });

        $('#colreorder-name').select2({
            dropdownParent: $('#colreorder-modal'),
            language: 'es',
            placeholder: 'Selecciona una opción',
            tags: true,
            width: 'auto'
        });

        $('#colreorder-selector').multiSelect({
            dblClick: true,
            keepOrder: true,
            selectableHeader: `<div class="d-flex align-items-center justify-content-between bg-secondary px-2 py-1">
                    <span class="text-left">Columnas disponibles</span>
                    <button class="btn waves-effect waves-light btn-xs btn-dark" title="Mostrar todas las columnas" type="button" onclick="showAllColumns()">
                        <i class="fa-solid fa-chevron-right"></i>
                    </button>
                </div>`,
            selectionHeader: `<div class="d-flex align-items-center justify-content-between bg-secondary px-2 py-1">
                    <button class="btn waves-effect waves-light btn-xs btn-dark" title="Ocultar todas las columnas" type="button" onclick="hideAllColumns()">
                        <i class="fa-solid fa-chevron-left"></i>
                    </button>
                    <span class="text-right">Mostrar estas columnas</span>
                </div>`,
            // callbacks
            afterInit: function(container) {
                let headers = eventsDataTable.columns().header().map(h => h.textContent).toArray();

                headers.forEach((value, key) => {
                    $('#colreorder-selector').multiSelect('addOption', {
                        value: `${ key }`,
                        text: value
                    });
                });

                $('.ms-selection > ul.ms-list').sortable({
                    axis: 'y'
                });
            }
        });

        $('#events-table tbody').on('dblclick', 'tr', function() {
            let data = eventsDataTable.row(this).data();
            let id = data['id_event'] ? data['id_event'] : '';

            let url = '{{ route("events.show", ":id") }}';
            url = url.replace(':id', id);

            let popup = window.open(url, data.id_event, 'height=570,width=1140');
            popup.focus();
        });

        $('#events-table tbody').on('contextmenu', 'tr', function(e) {
            e.preventDefault();
        });

        $('.massive-action').on('click', function(e) {
            let modal = $(this).data('modal-id');

            let data = eventsDataTable.rows({
                selected: true
            }).data().map(r => r.id_event).toArray();

            if (data.length > 0) {
                $(modal).find('textarea.events-container').val(data.join('\n'));

                $(modal).modal('show');
            } else {
                drawAlert('warning', 'No haz seleccionado ningún evento para ejecutar esta acción sobre él.');
            }
        });

        $('#colreorder-name').on('change', function() {
            let select = $(this);
            let state = select.find(':selected').data('state');

            hideAllColumns();

            select.parent().find('button.btn-outline-danger').prop('disabled', true);

            if (typeof state !== 'undefined') {
                state.visible.forEach(col => {
                    $('#colreorder-selector').multiSelect('select', col.toString());
                });

                select.parent().find('button.btn-outline-danger').prop('disabled', false);
            } else if (select.val() === 'reset') {
                showAllColumns();
            }
        });

        setInterval(function() {
            eventsDataTable.ajax.reload();
        }, 15000);
    });

    $(document).on('preInit.dt', function(e, settings) {
        // Move custom buttons container inside buttons container
        let buttons = $('#custom-buttons-container').detach();
        $('#buttons-container').prepend(buttons);

        // Remove additional classes from buttons
        $('.btn').removeClass('dt-button');

        // Load table config
        $.ajax({
            type: 'get',
            url: '{{ route("preferences.getEventsTablePreferences") }}'
        }).then(function(data) {
            Object.keys(data).forEach(key => {
                $('#colreorder-name').append($('<option/>', {
                    value: key,
                    text: data[key].current ? key + ' (actual)' : key,
                    // selected: data[key].current ? true : false,
                    'data-state': data[key].state
                }));

                if (data[key].current) {
                    alterTableState(JSON.parse(data[key].state));
                }
            });
        });
    });

    /* <---------- START: Functions for columns reorder ----------> */
    function showColReorderModal() {
        $('#colreorder-name').val(null);

        $('#colreorder-name option').filter(function() {
            return $(this).text().includes('(actual)');
        }).prop('selected', true);

        $('#colreorder-name').trigger('change');

        $('#colreorder-modal').modal('show');
    }

    function getColumnsOrder() {
        let columns = {
            'visible': [],
            'hidden': []
        };

        $('.ms-selection > ul.ms-list').children('li').each(function() {
            let li = $(this);

            let value = $('#colreorder-selector option').filter(function() {
                return $(this).html() === li.text();
            }).val();

            if (li.is(':visible')) {
                columns['visible'].push(parseInt(value));
            } else {
                columns['hidden'].push(parseInt(value));
            }
        });

        return columns;
    }

    function showAllColumns() {
        $('#colreorder-selector').multiSelect('select_all');
    }

    function hideAllColumns() {
        $('#colreorder-selector').multiSelect('deselect_all');
    }

    function alterTableState(columns = null) {
        if (columns === null) {
            columns = getColumnsOrder();
        }

        eventsDataTable.colReorder.reset();

        eventsDataTable.columns(columns['hidden']).visible(false);
        eventsDataTable.columns(columns['visible']).visible(true);

        eventsDataTable.colReorder.order(columns['visible'].concat(columns['hidden']));

        return columns;
    }

    function setDefaultConfig() {
        if ($('#colreorder-name').find('option[value="reset"]').length) {
            $('#colreorder-name').val('reset').trigger('change');
        } else {
            let option = new Option('', 'reset', true, true);

            $('#colreorder-name').append(option).trigger('change');
        }

        $('#colreorder-form').submit();
    }
    /* <---------- END: Functions for columns reorder ----------> */

    function showEventGroupingModal() {
        $('#events-correlation-table tbody').empty();

        let events = eventsDataTable.rows({
            selected: true
        }).data().toArray();

        if (events.length > 1) {
            events.forEach(event => {
                let severity = '<i class="fa-solid fa-circle-question fa-lg txt-unknown" title="Desconocido"></i>';

                switch (event.severity) {
                    case 'critical':
                        severity = '<i class="fa-solid fa-circle-xmark fa-lg txt-critical" title="Crítico"></i>';
                        break;
                    case 'major':
                        severity = '<i class="fa-solid fa-triangle-exclamation fa-lg txt-major" title="Principal"></i>';
                        break;
                    case 'minor':
                        severity = '<i class="fa-solid fa-triangle-exclamation fa-lg txt-minor" title="Menor"></i>';
                        break;
                    case 'warning':
                        severity = '<i class="fa-solid fa-triangle-exclamation fa-lg txt-warning" title="Advertencia"></i>';
                        break;
                    case 'normal':
                        severity = '<i class="fa-solid fa-circle-check fa-lg txt-normal" title="Normal"></i>';
                        break;
                }

                $('#events-correlation-table tbody').append(
                    `<tr>
                        <td class="text-center">
                            <input name="events[]" type="hidden" value="${ event.id_event }">
                            <div class="form-check">
                                <input class="form-check-input position-static" name="cause" type="radio" value="${ event.id_event }" required>
                            </div>
                        </td>
                        <td class="text-center">${ severity }</td>
                        <td class="text-justify">${ event.title }</td>
                        <td class="text-justify">${ moment(event.time_received).format('DD/MM/YYYY hh:mm:ss A') }</td>
                    </tr>`
                );
            });

            $('#events-correlation-modal').modal('show');
        } else {
            drawAlert('warning', 'No haz seleccionado una cantidad adecuada de eventos para ejecutar esta acción.');
        }
    }
</script>
@endsection