<div id="events-correlation-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <form id="events-correlation-form" action="{{ route('events.create-correlation') }}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Relacionar Eventos</h5>
                    <button class="close" type="button" aria-label="Close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6 class="card-subtitle mb-2">Selecciona el evento causa</h6>
                    <div class="table-scrollable table-scrollable-sm">
                        <table id="events-correlation-table" class="table table-sm table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="text-center">Causa</th>
                                    <th class="text-center">Gravedad</th>
                                    <th class="text-center">Título</th>
                                    <th class="text-center">Fecha de Recepción</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-outline-success" type="submit">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>