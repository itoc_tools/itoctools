<div id="massive-state-change-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <form action="{{ route('events.massiveState') }}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cambio de Estado Masivo</h5>
                    <button class="close" type="button" aria-label="Close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="form-label">Estado</label>
                        <select id="state-selector" class="form-control form-select">
                            <option value="open" data-icon="fa-regular fa-square txt-open mr-2">Abierto</option>
                            <option value="in_progress" data-icon="fa-solid fa-wrench txt-in-progress mr-2">En Curso</option>
                            <option value="resolved" data-icon="fa-solid fa-square-check txt-resolved mr-2">Resuelto</option>
                            <option value="closed" data-icon="fa-solid fa-square-xmark txt-closed mr-2">Cerrado</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Eventos Seleccionados</label>
                        <textarea class="form-control events-container" placeholder="Eventos Seleccionados" rows="3" readonly></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-outline-success" type="button">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>