<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{ asset('custom/images/favicon.png') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('custom/images/favicon.png') }}" type="image/x-icon">
    <title>ITOC+ | Información de Evento</title>
    <!-- Font Awesome 6 -->
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/font-awesome-6/css/font-awesome-6.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/assets/node_modules/select2/dist/css/select2.min.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/ecommerce/dist/css/style.min.css') }}">
    <!-- Alexis Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/css/custom.css') }}">
    <style>
        .btn, .col-form-label, .customtab li a.nav-link, .form-control, .select2-container {
            font-size: 12px;
        }

        .highlight {
            font-weight: 500;
        }

        .table {
            font-size: 12px;
            table-layout: fixed;
        }

        .table td {
            vertical-align: middle;
        }

        .select2-container--default .select2-selection--single {
            height: calc(1.64844rem + 2px);
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 2;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 26px;
        }

        .w-10 {
            width: 10%;
        }

        .w-15 {
            width: 15%;
        }

        .w-20 {
            width: 20%;
        }

        #event-identifier {
            font-weight: 500;
            background-image: none;
        }

        @media screen and (max-width: 780px) {
            .table thead {
                padding: 0;
                width: 1px;
                height: 1px;
                border: none;
                margin: -1px;
                overflow: hidden;
                position: absolute;
                clip: rect(0 0 0 0);
            }

            .table tr {
                display: block;
                margin-bottom: 0.5rem;
            }

            .table tr:nth-of-type(2n+1) {
                background: none !important;
            }

            .table td {
                display: block;
                text-align: justify !important;
            }

            .table td::before {
                width: 25%;
                float: left;
                font-weight: 500;
                min-width: 100px;
                margin-right: 10px;
                content: attr(data-label);
            }

            .table td:last-child {
                border-bottom: 1px solid #dee2e6;
            }
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="skin-default card-no-border">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">I T O C +</p>
        </div>
    </div>
    <!-- Main wrapper - style you can find in pages.scss -->
    <section id="wrapper">
        <div class="container-fluid px-0">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-body p-0">
                            <ul class="nav nav-tabs customtab" role="tablist">
                                <li class="nav-item">
                                    <a id="general-tab" class="nav-link active" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">
                                        <span class="hidden-sm-up">
                                            <i class="fa-regular fa-window"></i>
                                        </span>
                                        <span class="hidden-xs-down d-none d-md-inline">Información</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="attribute-tab" class="nav-link" data-toggle="tab" href="#attribute" role="tab" aria-controls="attribute" aria-selected="false">
                                        <span class="hidden-sm-up">
                                            <i class="fa-regular fa-shapes"></i>
                                        </span>
                                        <span class="hidden-xs-down d-none d-md-inline">Atributos Personalizados</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="annotation-tab" class="nav-link" data-toggle="tab" href="#annotation" role="tab" aria-controls="annotation" aria-selected="false">
                                        <span class="hidden-sm-up">
                                            <i class="fa-regular fa-file-lines"></i>
                                        </span>
                                        <span class="hidden-xs-down d-none d-md-inline">Anotaciones</span>
                                    </a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a id="annotation-tab" class="nav-link" data-toggle="tab" href="#annotation" role="tab" aria-controls="annotation" aria-selected="false">
                                        <span class="hidden-sm-up">
                                            <i class="fa-regular fa-file-lines"></i>
                                        </span>
                                        <span class="hidden-xs-down d-none d-md-inline">Anotaciones</span>
                                    </a>
                                </li> -->
                                <li class="nav-item">
                                    <a id="history-tab" class="nav-link" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="false">
                                        <span class="hidden-sm-up">
                                            <i class="fa-regular fa-timeline"></i>
                                        </span>
                                        <span class="hidden-xs-down d-none d-md-inline">Historial</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="general" class="tab-pane fade p-20 show active" role="tabpanel" aria-labelledby="general-tab">
                                    <form action="{{ route('events.update', $event['id']) }}" method="POST">
                                        @csrf
                                        @method('PATCH')
                                        <div class="row">
                                            <div class="col d-flex no-block justify-content-between align-items-center">
                                                <h4 class="text-small-caps font-medium text-muted mb-0">General</h4>
                                                <button class="btn btn-sm btn-outline-info" type="button">Crear IM</button>
                                            </div>
                                        </div>
                                        <hr class="my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-form-label">ID</label>
                                                    <div class="col-8 form-material">
                                                        <input id="event-identifier" class="form-control form-control-sm" type="text" value="{{ $event['id'] }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-form-label" for="severity-selector">Gravedad</label>
                                                    <div class="col-8">
                                                        <select id="severity-selector" class="form-control form-select select-with-icon" name="severity" required>
                                                            <option value="critical" data-icon="fa-solid fa-circle-xmark txt-critical mr-2">Crítico</option>
                                                            <option value="major" data-icon="fa-solid fa-triangle-exclamation txt-major mr-2">Principal</option>
                                                            <option value="minor" data-icon="fa-solid fa-triangle-exclamation txt-minor mr-2">Menor</option>
                                                            <option value="warning" data-icon="fa-solid fa-triangle-exclamation txt-warning mr-2">Advertencia</option>
                                                            <option value="normal" data-icon="fa-solid fa-circle-check txt-normal mr-2">Normal</option>
                                                            <option value="unknown" data-icon="fa-solid fa-circle-question txt-unknown mr-2">Desconocido</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-form-label" for="state-selector">Estado</label>
                                                    <div class="col-8">
                                                        <input name="state_initial" type="hidden" value="{{ $event['state'] }}">
                                                        <select id="state-selector" class="form-control form-select select-with-icon" name="state" required>
                                                            <option value="open" data-icon="fa-regular fa-square txt-open mr-2">Abierto</option>
                                                            <option value="in_progress" data-icon="fa-solid fa-wrench txt-in-progress mr-2">En Curso</option>
                                                            <option value="resolved" data-icon="fa-solid fa-square-check txt-resolved mr-2">Resuelto</option>
                                                            <option value="closed" data-icon="fa-solid fa-square-xmark txt-closed mr-2">Cerrado</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-form-label" for="priority-selector">Prioridad</label>
                                                    <div class="col-8">
                                                        <select id="priority-selector" class="form-control form-select select-with-icon" name="priority" required>
                                                            <option value="highest" data-icon="fa-solid fa-chevrons-up mr-2">Mayor</option>
                                                            <option value="high" data-icon="fa-solid fa-chevron-up mr-2">Alto</option>
                                                            <option value="medium" data-icon="fa-solid fa-diamond mr-2">Medio</option>
                                                            <option value="low" data-icon="fa-solid fa-chevron-down mr-2">Bajo</option>
                                                            <option value="lowest" data-icon="fa-solid fa-chevrons-down mr-2">Menor</option>
                                                            <option value="none" data-icon="fa-solid fa-minus mr-2">Ninguno</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-form-label">Categoría</label>
                                                    <div class="col-8">
                                                        <input class="form-control form-control-sm" type="text" value="{{ $event['category'] }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-form-label">Subcategoría</label>
                                                    <div class="col-8">
                                                        <input class="form-control form-control-sm" type="text" value="{{ $event['sub_category'] }}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-form-label" for="group-selector">Grupo Asignado</label>
                                                    <div class="col-8">
                                                        <input class="form-control form-control-sm" type="text" value="{{ $event['assigned_group']['name'] ?? '' }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-form-label" for="user-selector">Usuario Asignado</label>
                                                    <div class="col-8">
                                                        <input name="assigned_user_logged" type="hidden" value="{{ $bsmUser->us_bsm ?? '' }}">
                                                        <select id="user-selector" class="form-control form-control-sm form-select" name="assigned_user" required>
                                                            <option>andrea.valencia</option>
                                                            <option>angela.galvan</option>
                                                            <option>andres.sanchez</option>
                                                            <option>angelica.hernandez</option>
                                                            <option>alan.torres</option>
                                                            <option>alexis.torres</option>
                                                            <option>arkadi.padilla</option>
                                                            <option>artemio.urbina</option>
                                                            <option>Brayan Sanchez</option>
                                                            <option>cesar.gutierrez</option>
                                                            <option>cgonzalez</option>
                                                            <option>darwin.diaz</option>
                                                            <option>david.martinez</option>
                                                            <option>enrique.quezada</option>
                                                            <option>erik.cerda</option>
                                                            <option>hector.rodriguez</option>
                                                            <option>Ignacio.rodriguez</option>
                                                            <option>israel.gonzalez</option>
                                                            <option>jose.medina</option>
                                                            <option>juan.cruz</option>
                                                            <option>Juan Benitez</option>
                                                            <option>juan.arriaga</option>
                                                            <option>juan.palma</option>
                                                            <option>lorena.cruz</option>
                                                            <option>luis.manzo</option>
                                                            <option>maria.meneses</option>
                                                            <option>mario.gutierrez</option>
                                                            <option>osvaldo.martinez</option>
                                                            <option>pablo.gutierrez</option>
                                                            <option>Ramiro Estrada</option>
                                                            <option>OPC.Zenoss</option>
                                                            <option>operaciones.itoc</option>
                                                            <option>capacitacion.itoc</option>
                                                            <option>capacitacion.itoc2</option>
                                                            <option>capacitacion.itoc3</option>
                                                            <option>unknown</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-form-label">Fecha de Creación</label>
                                                    <div class="col-8">
                                                        <input class="form-control form-control-sm moment" type="text" value="{{ $event['time_created'] }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-form-label">Fecha de Recepción</label>
                                                    <div class="col-8">
                                                        <input class="form-control form-control-sm moment" type="text" value="{{ $event['time_received'] }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-form-label">Fecha de Cambio de Estado</label>
                                                    <div class="col-8">
                                                        <input class="form-control form-control-sm moment" type="text" value="{{ $event['time_state_changed'] }}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-md-2 col-form-label">Título</label>
                                                    <div class="col-8 col-md-10">
                                                        <textarea class="form-control form-control-sm" rows="1" disabled>{{ $event['title'] }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <h4 class="text-small-caps font-medium text-muted mb-0">Adicional</h4>
                                            </div>
                                        </div>
                                        <hr class="my-2">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-md-2 col-form-label">Aplicación</label>
                                                    <div class="col-8 col-md-10">
                                                        <input class="form-control form-control-sm" type="text" value="{{ $event['application'] }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-1">
                                                    <label class="col-4 col-md-2 col-form-label">Descripción</label>
                                                    <div class="col-8 col-md-10">
                                                        <textarea class="form-control form-control-sm" rows="2" disabled>{{ $event['description'] }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col text-right">
                                                <button class="btn btn-sm btn-outline-secondary" type="button" onclick="window.close();">Cancelar</button>
                                                <button class="btn btn-sm btn-outline-success" type="submit">Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="attribute" class="tab-pane fade p-20" role="tabpanel" aria-labelledby="attribute-tab">
                                    <div class="row">
                                        <div class="col d-flex no-block justify-content-between align-items-center">
                                            <button class="btn btn-sm btn-outline-success" type="button">Crear Atributo Personalizado</button>
                                            <button class="btn btn-sm btn-outline-info" type="button">Crear IM</button>
                                        </div>
                                    </div>
                                    <hr class="my-2">
                                    <div class="row">
                                        <div class="col-12">
                                            <table id="attributes-table" class="table table-sm table-striped mb-0">
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th class="text-center w-15" scope="col">Nombre</th>
                                                        <th class="text-center" scope="col">Valor</th>
                                                        <th class="text-center w-10" scope="col">Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse ($event['custom_attributes'] as $attribute)
                                                    <tr>
                                                        <td class="text-justify" scope="row" data-label="Nombre">{{ $attribute['name'] }}</td>
                                                        <td class="text-justify text-truncate" data-label="Valor">{{ $attribute['value'] }}</td>
                                                        <td class="text-center" data-label="Acciones">
                                                            <div class="btn-group" role="group">
                                                                <button type="button" class="btn btn-sm btn-outline-warning">
                                                                    <i class="fa-regular fa-pencil fa-lg"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-sm btn-outline-danger">
                                                                    <i class="fa-regular fa-trash-can fa-lg"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @empty
                                                    <tr>
                                                        <td class="text-center" colspan="3" data-label="">No se encontraron resultados</td>
                                                    </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="annotation" class="tab-pane fade p-20" role="tabpanel" aria-labelledby="annotation-tab">
                                    <div class="row">
                                        <div class="col d-flex no-block justify-content-between align-items-center">
                                            <button class="btn btn-sm btn-outline-success" type="button">Crear Anotación</button>
                                            <button class="btn btn-sm btn-outline-info" type="button">Crear IM</button>
                                        </div>
                                    </div>
                                    <hr class="my-2">
                                    <div class="row">
                                        <div class="col-12">
                                            <table id="annotations-table" class="table table-sm table-striped mb-0 table-with-date">
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th class="text-center w-20" scope="col">Creación</th>
                                                        <th class="text-center w-20" scope="col">Usuario</th>
                                                        <th class="text-center w-50" scope="col">Texto</th>
                                                        <th class="text-center w-10" scope="col">Acciones</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse ($event['annotations'] as $annotation)
                                                    <tr>
                                                        <td class="text-justify" scope="row" data-label="Creación">{{ $annotation['time_created'] }}</td>
                                                        <td class="text-justify" data-label="Autor">{{ $annotation['author'] }}</td>
                                                        <td class="text-justify text-truncate" data-label="Anotación">{{ mb_convert_encoding($annotation['text'], 'ISO-8859-1') }}</td>
                                                        <td class="text-center" data-label="Acciones">
                                                            <div class="btn-group" role="group">
                                                                <button type="button" class="btn btn-sm btn-outline-warning">
                                                                    <i class="fa-regular fa-pencil fa-lg"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-sm btn-outline-danger">
                                                                    <i class="fa-regular fa-trash-can fa-lg"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @empty
                                                    <tr>
                                                        <td class="text-center" colspan="4" data-label="">No se encontraron resultados</td>
                                                    </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="history" class="tab-pane fade p-20" role="tabpanel" aria-labelledby="history-tab">
                                    <div class="row">
                                        <div class="col-12">
                                            <table id="history-table" class="table table-sm table-striped mb-0 table-with-date">
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th class="text-center w-15" scope="col">Modificación</th>
                                                        <th class="text-center w-15" scope="col">Usuario</th>
                                                        <th class="text-center" scope="col">Acción</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse ($event['history'] as $history)
                                                    <tr>
                                                        <td class="text-justify" scope="row" data-label="Modificación">{{ $history['time_changed'] }}</td>
                                                        <td class="text-justify" scope="row" data-label="Usuario">{{ $history['modified_by'] }}</td>
                                                        <td class="text-justify text-truncate" data-label="Acción">
                                                            La propiedad <span class="highlight">{{ $history['property'] }}</span> ha cambiado su valor
                                                            @if ($history['previous_value'] !== '')
                                                            de <span class="highlight">{{ $history['previous_value'] }}</span>
                                                            @endif
                                                            a <span class="highlight">{{ $history['current_value'] }}</span>.
                                                        </td>
                                                    </tr>
                                                    @empty
                                                    <tr>
                                                        <td class="text-center" colspan="3" data-label="">No se encontraron resultados</td>
                                                    </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- All Jquery -->
    <script type="text/javascript" src="{{ asset('theme/assets/node_modules/jquery/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script type="text/javascript" src="{{ asset('theme/assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Select2 -->
    <script type="text/javascript" src="{{ asset('theme/assets/node_modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('theme/assets/node_modules/select2/dist/js/i18n/es.js') }}"></script>
    <!-- Moment.js -->
    <script type="text/javascript" src="{{ asset('custom/plugins/Moment.js/moment.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $('.select-with-icon').select2({
                minimumResultsForSearch: Infinity,
                language: 'es',
                placeholder: 'Selecciona una opción',
                templateResult: renderIcon,
                templateSelection: renderIcon,
                width: '100%'
            });

            // Set event attributes
            $('#state-selector').val('{{ $event["state"] }}').trigger('change');
            $('#severity-selector').val('{{ $event["severity"] }}').trigger('change');
            $('#priority-selector').val('{{ $event["priority"] }}').trigger('change');

            $('input.moment').each(function() {
                let date = moment($(this).val());

                $(this).val(date.format('DD/MM/YYYY hh:mm:ss A'));
            });

            $('.table-with-date tbody tr td:first-child').each(function() {
                let date = moment($(this).text());

                let format = date.format('DD/MM/YY hh:mm:ss A');

                if (format !== 'Invalid date') {
                    $(this).text(format);
                }
            });

            setTimeout(function() {
                $('.preloader').fadeOut();
            }, 2000);

            console.log(@json($event));
            console.log(@json($bsmUser));
        });

        function renderIcon(data, container) {
            return $(`<span><i class="${ $(data.element).data('icon') }"></i>${ data.text }</span>`);
        }
    </script>
</body>

</html>