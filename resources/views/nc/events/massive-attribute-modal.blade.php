<div id="massive-attribute-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <form action="{{ route('events.masiveCustomAttributes') }}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Atributo Personalizado Masivo</h5>
                    <button class="close" type="button" aria-label="Close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="form-label">Nombre</label>
                        <input class="form-control" placeholder="Nombre" type="text">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Valor</label>
                        <input class="form-control" placeholder="Valor" type="text">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Eventos Seleccionados</label>
                        <textarea class="form-control events-container" placeholder="Eventos Seleccionados" rows="3" readonly></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-outline-success" type="button">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>