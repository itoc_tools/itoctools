<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- <li>
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="fa-regular fa-grip-vertical text-center"></i>
                        <span class="hide-menu">Aplicaciones</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{ url('documents') }}">
                                <i class="fa-regular fa-files text-center"></i>
                                <span class="ml-1">Documentos</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('scripts') }}">
                                <i class="fa-regular fa-code text-center"></i>
                                <span class="ml-1">Scripts</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('devices') }}">
                                <i class="fa-regular fa-network-wired text-center"></i>
                                <span class="ml-1">Dispositivos</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="fa-regular fa-file-pdf text-center"></i>
                                <span class="ml-1">Reportes</span>
                            </a>
                        </li>
                    </ul>
                </li> -->
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="fa-regular fa-chart-network text-center"></i>
                        <span class="hide-menu">Operaciones</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{ url('test-application') }}">
                                <i class="fa-regular fa-window"></i>
                                <span class="ml-1">Eventos</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="javascript:void(0)" onclick="document.getElementById('logout-form').submit();">
                        <i class="fa-regular fa-arrow-right-from-bracket text-center"></i>
                        <span class="hide-menu">Cerrar Sesión</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" class="d-none" method="POST">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>