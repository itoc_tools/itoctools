<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/DataTables/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/DataTables/Buttons-2.2.3/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/DataTables/Select-1.4.0/select.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/DataTables/ColReorder-1.5.6/colReorder.dataTables.min.css') }}">