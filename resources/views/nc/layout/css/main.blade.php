@yield ('stylesheets')

<!-- Font Awesome 6 -->
<link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/font-awesome-6/css/font-awesome-6.min.css') }}">

<!-- toast -->
<link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/toast/jquery.toast.min.css') }}">

<!-- waitMe -->
<link rel="stylesheet" type="text/css" href="{{ asset('custom/plugins/waitMe/waitMe.min.css') }}">

<!-- Select2 -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/assets/node_modules/select2/dist/css/select2.min.css') }}">

<!-- Custom CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/ecommerce/dist/css/style.min.css') }}">

<!-- Alexis Custom CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('custom/css/custom.css') }}">

@yield ('styles')