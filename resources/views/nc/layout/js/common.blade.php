<script type="text/javascript">
    function drawAlert(type, text) {
        let heading = '';
        let bgColor = '#212529';
        let loaderBg = '#4f5467';

        switch (type) {
            case 'error':
                bgColor = '#cc0000';
                heading = '¡Error!';
                loaderBg = '#ff4444';
                break;
            case 'error':
                bgColor = '#0099cc';
                heading = '¡Atención!';
                loaderBg = '#33b5e5';
                break;
            case 'success':
                bgColor = '#007e33';
                heading = '¡Correcto!';
                loaderBg = '#00c851';
                break;
            case 'warning':
                bgColor = '#ff8800';
                heading = '¡Advertencia!';
                loaderBg = '#ffbb33';
                break;
            default:
                type = '';
                break;
        }

        $.toast({
            allowToastClose: true,
            bgColor: bgColor,
            hideAfter: 5000,
            icon: type,
            loaderBg: loaderBg,
            position: 'bottom-right',
            text: text,
            textColor: 'white'
        });
    }

    function runWaitMe(element) {
        element.waitMe({
            effect: 'roundBounce'
        });
    }
</script>