<!-- DataTables -->
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/Buttons-2.2.3/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/JSZip-2.5.0/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/Buttons-2.2.3/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/Buttons-2.2.3/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/Select-1.4.0/dataTables.select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom/plugins/DataTables/ColReorder-1.5.6/dataTables.colReorder.min.js') }}"></script>