<script type="text/javascript" src="{{ asset('theme/assets/node_modules/jquery/jquery-3.2.1.min.js') }}"></script>

<!-- Bootstrap tether Core JavaScript -->
<script type="text/javascript" src="{{ asset('theme/assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>

<!-- slimscrollbar scrollbar JavaScript -->
<script type="text/javascript" src="{{ asset('theme/ecommerce/dist/js/perfect-scrollbar.jquery.min.js') }}"></script>

<!--Wave Effects -->
<script type="text/javascript" src="{{ asset('theme/ecommerce/dist/js/waves.js') }}"></script>

<!-- Menu sidebar -->
<script type="text/javascript" src="{{ asset('theme/ecommerce/dist/js/sidebarmenu.js') }}"></script>

<!-- stickey kit -->
<script type="text/javascript" src="{{ asset('theme/assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/assets/node_modules/sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Custom JavaScript -->
<script type="text/javascript"  src="{{ asset('theme/ecommerce/dist/js/custom.js') }}"></script>

<!-- toast -->
<script type="text/javascript" src="{{ asset('custom/plugins/toast/jquery.toast.min.js') }}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{ asset('theme/assets/node_modules/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/assets/node_modules/select2/dist/js/i18n/es.js') }}"></script>

<!-- jQuery UI -->
<script type="text/javascript" src="{{ asset('theme/assets/node_modules/jqueryui/jquery-ui.min.js') }}"></script>

<!-- Moment.js -->
<script type="text/javascript" src="{{ asset('custom/plugins/Moment.js/moment.min.js') }}"></script>

<!-- waitMe -->
<script type="text/javascript" src="{{ asset('custom/plugins/waitMe/waitMe.min.js') }}"></script>

@yield ('plugins')

@include ('nc.layout.js.common')

@yield ('scripts')