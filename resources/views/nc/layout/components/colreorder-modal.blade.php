<div id="colreorder-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <form id="colreorder-form" action="{{ route('preferences.saveEventsTableState') }}" autocomplete="off" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Configuración de Columnas</h5>
                    <button class="close" type="button" aria-label="Close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="colreorder-name">
                            Selecciona o ingresa un nombre para este orden
                        </label>
                        <div class="input-group">
                            <select id="colreorder-name" class="form-control" name="name" required></select>
                            <div class="input-group-append">
                                <!-- <button class="btn btn-outline-danger" title="Eliminar Configuración" type="button" onclick="deleteConfig()">
                                    <i class="fa-regular fa-trash-can fa-lg"></i>
                                </button> -->
                                <button class="btn btn-outline-dark" title="Restablecer Configuración" type="button" onclick="setDefaultConfig()">
                                    <i class="fa-regular fa-rotate-right fa-lg"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="colreorder-selector">
                            Selecciona las columnas que deseas mostrar
                        </label>
                        <select id="colreorder-selector" multiple="multiple" name="order" required>
                        </select>
                        <input id="colreorder-state" type="hidden" name="state" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-outline-success" type="submit">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>