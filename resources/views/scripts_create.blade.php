@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <h4 class="card-title">Nuevo scripts</h4>
                <h6 class="card-subtitle"> Ingresar un nuevo <code> scripts</code> y configurar sus parametros.</h6>
                <form class="form-material m-t-40" method="POST" action="{{ route('scripts.store') }}">
                    {{ csrf_field() }}
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Nombre</label>
                        <div class="col-10">
                            <input type="text" class="form-control form-control-line" placeholder="Capacidades de red"
                                id="name" name="name" value="{{ old('name') }}" require>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Descripción</label>
                        <div class="col-10">
                            <textarea class="form-control" rows="3" id="description" name="description"
                                require>{{ old('description') }}</textarea>
                            <span class="help-block text-muted">
                                <small>Agregar una descripcion del script para identificar su funcionamiento.</small>
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Tipo</label>
                        <div class="col-10">
                            <select class="form-control" id="type" name="type">
                                <option>SOAP</option>
                                <option>RESTful</option>
                                <option>SSH</option>
                                <option>Gearman</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">API</label>
                        <div class="col-10">
                            <select class="form-control" id="api" name="api">
                                <option>NuSOAP</option>
                                <option>SLIM</option>
                                <option>SSH2</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label">Autor</label>
                        <input type="text" class="form-control form-control-line" id="author" name="author"
                            placeholder="Autor">
                    </div> -->
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Version</label>
                        <div class="col-10">
                            <input type="numeric" class="form-control form-control-line" id="version" name="version"
                                placeholder="1.00" value="{{ old('version') }}" require>
                        </div>
                    </div>

                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Lenguaje</label>
                        <div class="col-10">
                            <select class="form-control" id="language" name="language">
                                <option>PHP</option>
                                <option>JAVA</option>
                                <option>C#</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">URL</label>
                        <div class="col-10">
                            <input type="text" class="form-control form-control-line" id="url" name="url"
                                placeholder="http://localhost" value="{{ old('url') }}" require>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Template</label>
                        <div class="col-3">
                            <select class="form-control" id="template_id" name="template_id"
                                onchange="changeTemplate()">
                                <option value=""> Sin template </option>
                                @foreach ($templates as $template)
                                <option value="{{ $template }}"> {{ $template->name }} </option>
                                @endforeach
                            </select>
                        </div>
                        <label class="col-7 col-form-label" id="template_description"> </label>
                    </div>
                    <br>
                    <div class="row text-right">
                        <div class="col">
                            <button type="submit" class="btn btn-success" id="save">
                                Guardar
                            </button>
                            <a href="{{ route('scripts.index') }}" class="btn btn-warning">Cancelar </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('allJquery')
@include('includes.admin.methods_scripts')
@endsection


<!-- function changTemplate(obj) {
    var combo = document.getElementById("template_id");
    var selected = combo.options[combo.selectedIndex].value;
    var obj = JSON.parse(selected);
    document.getElementById('template_description').innerHTML = obj['description'];
} -->