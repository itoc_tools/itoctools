@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 text-right">
                        @if ((Auth::user()->can('documents_edit')))
                        <a href="{{ route('scripts.edit', $script->id) }}">
                            <button type="button" class="btn btn-circle btn-primary" onclick="javascript();">
                                <i class="mdi mdi-pencil"></i>
                            </button>
                        </a>
                        @endif
                    </div>
                </div>
                <h4 class="card-title">Detalle del scripts con el id: {{ $script->id }} </h4>
                <h6 class="card-subtitle"> Detalle del <code> scripts</code> y sus parametros.</h6>
                <div class="form-group m-t-40 row">
                    <label class="col-2 col-form-label text-right">Nombre</label>
                    <label class="col-10 col-form-label">{{ $script->name }}</label>
                </div>
                <div class="form-group m-t-10 row">
                    <label class="col-2 col-form-label text-right">Descripción</label>
                    <label class="col-10 col-form-label">{{ $script->description }}</label>
                </div>
                <div class="form-group m-t-10 row">
                    <label class="col-2 col-form-label text-right">Tipo</label>
                    <label class="col-10 col-form-label">{{ $script->type }}</label>
                </div>
                <div class="form-group m-t-10 row">
                    <label class="col-2 col-form-label text-right">API</label>
                    <label class="col-10 col-form-label">{{ $script->api }}</label>
                </div>
                <div class="form-group m-t-10 row">
                    <label class="col-2 col-form-label text-right">Version</label>
                    <label class="col-10 col-form-label">{{ $script->version }}</label>
                </div>
                <div class="form-group m-t-10 row">
                    <label class="col-2 col-form-label text-right">Lenguaje</label>
                    <label class="col-10 col-form-label">{{ $script->language }}</label>
                </div>
                <div class="form-group m-t-10 row">
                    <label class="col-2 col-form-label text-right">URL</label>
                    <label class="col-10 col-form-label">{{ $script->url }}</label>
                </div>
                <div class="form-group m-t-10 row">
                    <label class="col-2 col-form-label text-right">Template</label>
                    <!-- Valida que exista un registro si no asigna un valor en blanco -->
                    <label class="col-3 col-form-label">{{ isset($template->name) ? $template->name : '' }}</label>
                    <label class="col-7 col-form-label">{{ isset($template->name) ? $template->description : '' }}</label>
                </div>
                <br>
                <div class="row text-right">
                    <div class="col">
                        <a href="{{ route('scripts.index') }}" class="btn btn-warning">Cancelar </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection