@extends('layouts.master')

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- Snippet para implementar vistas intercambiables: listado y cuadrícula
		<div class="row">
	        <div class="col-lg-4">
	            <div class="card">
	                <img class="card-img-top img-responsive" src="../assets/images/big/img1.jpg" alt="Card image cap">
	                <div class="card-body">
	                    <ul class="list-inline font-14">
	                        <li class="p-l-0">20 May 2016</li>
	                        <li><a href="javascript:void(0)" class="link">3 Comment</a></li>
	                    </ul>
	                    <h3 class="font-normal">Featured Hydroflora Pots Garden &amp; Outdoors</h3>
	                    <p class="m-b-0 m-t-10">Titudin venenatis ipsum ac feugiat. Vestibulum ullamcorper quam.</p>
	                    <button class="btn btn-success btn-rounded waves-effect waves-light m-t-20">Leer</button>
	                </div>
	            </div>
	        </div>

	        <div class="col-lg-4">
	            <div class="card">
	                <img class="card-img-top img-responsive" src="../assets/images/big/img2.jpg" alt="Card image cap">
	                <div class="card-body">
	                    <ul class="list-inline font-14">
	                        <li class="p-l-0">20 May 2016</li>
	                        <li><a href="javascript:void(0)" class="link">3 Comment</a></li>
	                    </ul>
	                    <h3 class="font-normal">Featured Hydroflora Pots Garden &amp; Outdoors</h3>
	                    <p class="m-b-0 m-t-10">Titudin venenatis ipsum ac feugiat. Vestibulum ullamcorper quam.</p>
	                    <button class="btn btn-success btn-rounded waves-effect waves-light m-t-20">Leer</button>
	                </div>
	            </div>
	        </div>

	        <div class="col-lg-4">
	            <div class="card">
	                <img class="card-img-top img-responsive" src="../assets/images/big/img4.jpg" alt="Card image cap">
	                <div class="card-body">
	                    <ul class="list-inline font-14">
	                        <li class="p-l-0">20 May 2016</li>
	                        <li><a href="javascript:void(0)" class="link">3 Comment</a></li>
	                    </ul>
	                    <h3 class="font-normal">Featured Hydroflora Pots Garden &amp; Outdoors</h3>
	                    <p class="m-b-0 m-t-10">Titudin venenatis ipsum ac feugiat. Vestibulum ullamcorper quam.</p>
	                    <button class="btn btn-success btn-rounded waves-effect waves-light m-t-20">Leer</button>
	                </div>
	            </div>
	        </div>

	    </div>
		-->
		<div class="card">
			<div class="card-body bg-light border-bottom">
				<div class="row">
					@if ((Auth::user()->can('documents_edit')))
					<div class="col-sm-8 media">
						<img src="{{ URL::asset('theme/assets/images/users/admin.png') }}" alt="user" class="pull-left img-circle" width="45">
						<div class="media-body p-l-10">
							<h5><strong>{{ $document->user->name }}</strong></h5>
							<span class="card-subtitle p-r-5">{{ $document->created_at->format("F j, Y") }}</span>
							<i class="fas fa-unlock-alt fa-sm"></i>
						</div>
					</div>
					<div class="col-sm-3 text-right">
						<div class="btn-group" role="group" aria-label="Opciones">
							<button type="button" class="btn btn-sm btn-circle btn-secondary"><i class="mdi mdi-download"></i></button>
							<button type="button" class="btn btn-sm btn-circle btn-secondary"><i class="mdi mdi-share-variant"></i></button>
							<button type="button" class="btn btn-sm btn-sm btn-circle btn-secondary fullscreen-btn" id="panel-fullscreen">
								<i class="fas fa-expand"></i>
							</button>
						</div>
					</div>
					<div class="col-sm-1 text-right">
						<a href="{{ route('documents.edit', $document) }}">
							<button type="button" class="btn btn-circle btn-primary" onclick="javascript();"><i class="mdi mdi-pencil"></i></button>
						</a>
					</div>
					@else
					<div class="col-md-5 col-sm-5 media">
						<img src="{{ URL::asset('theme/assets/images/users/4.jpg') }}" alt="user" class="pull-left img-circle" width="45">
						<div class="media-body p-l-10">
							<h5><strong>{{ $document->user->name }}</strong></h5>
							<span class="card-subtitle p-r-5">{{ $document->created_at->format("F j, Y") }}</span>
							<i class="fas fa-unlock-alt fa-sm"></i>
						</div>
					</div>
					<div class="col-md-7 col-sm-7 text-right">
						<div class="btn-group" role="group" aria-label="Opciones">
							<button type="button" class="btn btn-sm btn-circle btn-secondary"><i class="mdi mdi-download"></i></button>
							<button type="button" class="btn btn-sm btn-circle btn-secondary"><i class="mdi mdi-share-variant"></i></button>
							<button type="button" class="btn btn-sm btn-sm btn-circle btn-secondary fullscreen-btn" id="panel-fullscreen">
								<i class="fas fa-expand"></i>
							</button>
						</div>
					</div>
					@endif
				</div>
			</div>
			<div class="card-body">
				<div class="w-100">&nbsp;</div>
				<h1>{{ $document->title }}</h1>
				<h6 class="card-subtitle"><i>{{ $document->description }}</i></h6>

				<div class="row">
					<div class="col text-right">
						<span class="small p-r-5 text-muted">{{ $document->updated_at->format("F j, Y") }}</span>
						<span class="label label-success small">Actualizado</span>
					</div>
				</div>
				<hr style="border-top: dotted 1px #ddd;" />
				<div>{!! $document->content !!}</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('allJquery')
@include('includes.admin.methods_devices')
@endsection