@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div>

                        <div class="container">
                            <br>
                            <div class="row d-flex justify-content-center">
                                <div class="col-sm">
                                </div>
                                <div class="col-sm my-auto">
                                    <div class="dropdown">

                                        <div class="btn-group" role="group">
                                            <button class="btn btn-success dropdown-toggle" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                Selecciona el formato a descargar
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                                <a class="dropdown-item"><i class="far fa-file-pdf"></i> PDF</a>

                                                <a class="dropdown-item"><i class="far fa-file-excel"></i> XLS</a>

                                                <a class="dropdown-item"><i class="fab fa-html5"></i> HTML</a>
                                            </div>
                                            <button type="button" id="btnDownloadReport" class="btn btn-info"><i class="fa fa-download"
                                                    aria-hidden="true"></i>
                                                Descargar</button>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-sm">
                                </div>
                            </div>
                        </div>

                        <br>

                        <div>
                            {!! $reporte_html !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        const url_report_pdf = "{{ $url_path_report_pdf }}";
        const url_report_html = "{{ $url_path_report_html }}";
        const url_report_xls = "{{ $url_path_report_xls }}";
    </script>
@endsection
