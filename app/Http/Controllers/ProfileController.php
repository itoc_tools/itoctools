<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Auth;

class ProfileController extends Controller
{

    public function breadcrumb()
    {
        return view('user.profile')->with([
            'title' => 'Usuarios',
            'breadcrumb' => [
                /*  [
                    'title' => 'Usuarios',
                    'url' => URL::route('users.index'),
                ], */
                [
                    'title' => Auth::user()->name,
                ],
            ],
        ]);
    }
}
