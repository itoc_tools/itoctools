<?php

namespace App\Http\Controllers\ExposedServices;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;

class ExternalController extends Controller
{
    protected $events;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(\App\Repositories\Events $events)
    {
        $this->events = $events;
    }

    public function index(string $idEvent)
    {
        Log::info('................ Exposed Services Request:');
        Log::info('................ ID:  ' . $idEvent);
        $event = $this->events->find($idEvent);
        $event = html_entity_decode($event);
        $event = json_decode($event, true);
        Log::info('................ Responde el OMI correctamente');
        return $event;
    }


    public function addcustomattribute(Request $request)
    {
        Log::info('................ Exposed Services Request: addcustomattribute');
        //error_log($request);
        $idEvent = $request->input('idEvent');
        $name = $request->input('name');
        $value = $request->input('value');
        Log::info('................ ID:  ' . $idEvent);
        $xml = new \DomDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xml_enviar = $xml->createElement('custom_attribute');
        $xml_enviar->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
        $xml_enviar->setAttribute('self', 'http://bsmtelmexit.triara.mexico/opr-web/rest/9.10/event_list/' . $idEvent .
            '/custom_attribute_list/' . $name);
        $xml_enviar->setAttribute('type', 'urn:x-hp:2009:software:data_model:opr:type:event:custom_attribute');
        $xml_enviar->setAttribute('version', '1.0');
        $xml_enviar->appendChild($xml->createElement('name', $name));
        $xml_enviar->appendChild($xml->createElement('value', $value));
        $xml->appendChild($xml_enviar);
        $xml_body = $xml->saveXML();
        Log::info($xml_body);
        //error_log($xml_body);
        $ca_created = [];
        try {
            $client = new Client();
            $res = $client->request('POST', 'http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/' . $idEvent .
                '/custom_attribute_list/?alt=json', [
                'auth' => ['OPC.Zenoss', '+0ruge5.X3m137-'],
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8'
                ],
                'body'   => $xml_body
            ]);
            $ca_created = $res->getBody();
            $ca_created = $ca_created->getContents();
            $ca_created = html_entity_decode($ca_created);
            $ca_created = json_decode($ca_created, true);
        } catch (ConnectException $e) {
            Log::info('Error Message: ' . $e->getMessage());
            error_log('Error Message: ' . $e->getMessage());
            $ca_created = array(
                "status" => "error",
                "Error Message" => $e->getMessage()
            );
        } catch (RequestException $er) {
            Log::info('Error Message: ' . $er->getMessage());
            error_log('Error Message: ' . $er->getMessage());
            $ca_created = array(
                "status" => "error",
                "Error Message" => $er->getMessage()
            );
        } catch (ClientException $err) {
            Log::info('Error Message: ' . $err->getMessage());
            error_log('Error Message: ' . $err->getMessage());
            $ca_created = array(
                "status" => "error",
                "Error Message" => $err->getMessage()
            );
        }
        return $ca_created;
    }

    public function addannotation(Request $request)
    {
        Log::info('................ Exposed Services Request: addannotation');
        //error_log($request);
        $idEvent = $request->input('idEvent');
        $value = $request->input('text');
        Log::info('................ ID:  ' . $idEvent);
        //error_log('................ ID:  ' . $idEvent);
        $xml = new \DomDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xml_enviar = $xml->createElement('annotation');
        $xml_enviar->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
        $xml_enviar->appendChild($xml->createElement('text', $value));
        $xml->appendChild($xml_enviar);
        $xml_body = $xml->saveXML();
        //error_log($xml_body);
        Log::info($xml_body);
        $ca_created = [];
        try {
            $client = new Client();
            $res = $client->request('POST', "http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/{$idEvent}/annotation_list?alt=json", [
                'auth' => ['OPC.Zenoss', '+0ruge5.X3m137-'],
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8'
                ],
                'body'   => $xml_body
            ]);
            $ca_created = $res->getBody();
            $ca_created = $ca_created->getContents();
            $ca_created = html_entity_decode($ca_created);
            $ca_created = json_decode($ca_created, true);
            $ca_created = $ca_created['annotation'];
        } catch (ConnectException $e) {
            Log::info('Error Message: ' . $e->getMessage());
            //error_log('Error Message: ' . $e->getMessage());
            $ca_created = array(
                "status" => "error",
                "Error Message" => $e->getMessage()
            );
        } catch (RequestException $er) {
            Log::info('Error Message: ' . $er->getMessage());
            //error_log('Error Message: ' . $er->getMessage());
            $ca_created = array(
                "status" => "error",
                "Error Message" => $er->getMessage()
            );
        } catch (ClientException $err) {
            Log::info('Error Message: ' . $err->getMessage());
            //error_log('Error Message: ' . $err->getMessage());
            $ca_created = array(
                "status" => "error",
                "Error Message" => $err->getMessage()
            );
        }
        return $ca_created;
    }
}
