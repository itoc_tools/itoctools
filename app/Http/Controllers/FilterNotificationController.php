<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Filter;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FilterNotificationController extends Controller
{
    /**
     * @param Request $request
     * Se obtienen los eventos a validar con los filtros
     */
    public function store(Request $request)
    {
        $filters = Filter::all();

        foreach ($filters as $filter) {
            $events = DB::select(DB::raw('select * from events where ' . $filter->sql));

            if ($events) {
                foreach ($events as $event) {
                    $this->sendMail($event);
                }
            } else {
                Log::info('No cumple');
            }
        }
    }

    /**
     * API envio de correos
     */
    public function sendMail($event)
    {
        $client = new Client(['base_uri' => 'https://itoc-api.triara.com:8088/']);

        $response = $client->request('POST', '/notificacion/mail/solotext', ['form_params' => [
            'subjetc' => 'OMI',
            'from' => 'apm.itoc@triara.com',
            'mensaje' => html_entity_decode($this->fillTemplate($event)),
            'grupo_envio' => 'rodrigo.mendoza@triara.com',
        ]]);

        return $response->getStatusCode();
    }


    public function fillTemplate($event)
    {
        date_default_timezone_set("America/Mexico_City");
        $hora_event = date("d-m-Y h:i:s");

        $template = file_get_contents('../resources/views/templates/mail.html');

        $template = str_replace('${evt/OMI_client}', $event->application, $template);
        $template = str_replace('${evt/OMI_platform}', $event->category, $template);
        $template = str_replace('${evt/OMI_description}', $event->description, $template);
        $template = str_replace('${evt/OMI_hostname}', $event->application, $template);
        $template = str_replace('${evt/OMI_firstTime}', $event->time_created_label, $template);
        $template = str_replace('${evt/OMI_application}', $event->application, $template);
        $template = str_replace('${evt/OMI_follow}', $event->application, $template);
        $template = str_replace('${evt/OMI_eventKey}', $event->id_event, $template);
        $template = str_replace('${evt/OMI_cloudwatch}', $event->application, $template);

        return $template;
    }
}
