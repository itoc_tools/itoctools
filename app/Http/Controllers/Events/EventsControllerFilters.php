<?php

namespace App\Http\Controllers\Events;

use App\UsersBsm;
use App\EventsFilters;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class EventsControllerFilters extends Controller
{

    protected $events;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(\App\Repositories\Events $events)
    {
        $this->middleware('auth');
        $this->events = $events;
    }

    /**
     * Display a listing of the resource for TelmexSlim.
     *
     * @return array $events_reviewed
     */
    public function telmexSlim()
    {
        $num = $this->getTelmexSlimNum();
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        return view('events.events')->with([
            'title' => 'Eventos: TELMEX SLIM',
            'name_graphic' => 'TELMEX SLIM',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: TELMEX SLIM'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('telmexSlimReload')
        ]);
    }

    /**
     * Get a listing of the resource for TelmexSlim.
     *
     */
    public function telmexSlimReload()
    {
        $events = EventsFilters::where('filter_name', 'EventTelmexSlim')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events)->toJson();
    }

    /**
     * Get a Num of the resource for TelmexSlim.
     *
     */
    public function getTelmexSlimNum()
    {
        $eventsNum = EventsFilters::where('filter_name', 'EventTelmexSlim')
            ->count();
        return $eventsNum;
    }

    /**
     * Get a Num of Critical for TelmexSlim.
     *
     */
    public function getTelmexSlimCriticalNum()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventTelmexSlim')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for TelmexSlim.
     *
     */
    public function getTelmexSlimMajorNum()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventTelmexSlim')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }
    /**
     * Get a Num of Minor for TelmexSlim.
     *
     */

    public function getTelmexSlimMinorNum()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventTelmexSlim')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for TelmexSlim.
     *
     */
    public function getTelmexSlimWarningNum()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventTelmexSlim')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }
    /**
     * Get a Num of Normal for TelmexSlim.
     *
     */
    public function getTelmexSlimNormalNum()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventTelmexSlim')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for TelmexSlim.
     *
     */
    public function getTelmexSlimUnknownNum()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventTelmexSlim')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for Alertas Abiertas en Excepcion.
     *
     * @return array $events_reviewed
     */
    public function openException()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getOpenExceptionReloadNum();
        return view('events.events')->with([
            'title' => 'Eventos: Abiertos Excepcion',
            'name_graphic' => 'Abiertos Excepcion',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: Abiertos Excepcion'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('openExceptionReload')
        ]);
    }

    /**
     * Get a Reload of the resource for Alertas Abiertas en Excepcion.
     *
     * @return array $events_reviewed
     */
    public function openExceptionReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventAbiertosExcepcion')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a Num of the resource for Alertas Abiertas en Excepcion.
     *
     * @return string $events_query
     */
    public function getOpenExceptionReloadNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventAbiertosExcepcion')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventAbiertosExcepcion.
     *
     */
    public function getOpenExceptionReloadNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventAbiertosExcepcion')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventAbiertosExcepcion.
     *
     */
    public function getOpenExceptionReloadNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventAbiertosExcepcion')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }
    /**
     * Get a Num of Minor for EventAbiertosExcepcion.
     *
     */

    public function getOpenExceptionReloadNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventAbiertosExcepcion')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventAbiertosExcepcion.
     *
     */
    public function getOpenExceptionReloadNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventAbiertosExcepcion')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }
    /**
     * Get a Num of Normal for EventAbiertosExcepcion.
     *
     */
    public function getOpenExceptionReloadNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventAbiertosExcepcion')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventAbiertosExcepcion.
     *
     */
    public function getOpenExceptionReloadNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventAbiertosExcepcion')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for Excepcion sin IM o SD en Progreso.
     *
     * @return array $events_reviewed
     */
    public function alertsWithOutIMSDInProgress()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getAlertsWithOutIMSDInProgressNum();
        return view('events.events')->with([
            'title' => 'Eventos: Excepcion sin IM o SD en Progreso',
            'name_graphic' => 'Excepcion sin IM o SD en Progreso',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: Excepcion sin IM o SD en Progreso'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('alertsWithOutIMSDInProgressReload')
        ]);
    }

    /**
     * Get a listing of the resource for Alertas sin IM o SD en Progreso.
     *
     * @return array $events_query
     */
    public function alertsWithOutIMSDInProgressReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventExcepcionSinIMoSD')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a Num of the resource for Alertas sin IM o SD en Progreso.
     *
     * @return string $events_query
     */
    public function getAlertsWithOutIMSDInProgressNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventExcepcionSinIMoSD')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for Alertas sin IM o SD en Progreso.
     *
     */
    public function getAlertsWithOutIMSDInProgressNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventExcepcionSinIMoSD')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for Alertas sin IM o SD en Progreso.
     *
     */
    public function getAlertsWithOutIMSDInProgressNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventExcepcionSinIMoSD')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }
    /**
     * Get a Num of Minor for Alertas sin IM o SD en Progreso.
     *
     */

    public function getAlertsWithOutIMSDInProgressNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventExcepcionSinIMoSD')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for Alertas sin IM o SD en Progreso.
     *
     */
    public function getAlertsWithOutIMSDInProgressNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventExcepcionSinIMoSD')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }
    /**
     * Get a Num of Normal for Alertas sin IM o SD en Progreso.
     *
     */
    public function getAlertsWithOutIMSDInProgressNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventExcepcionSinIMoSD')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for Alertas sin IM o SD en Progreso.
     *
     */
    public function getAlertsWithOutIMSDInProgressNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventExcepcionSinIMoSD')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for Infraestructura Hardware.
     *
     * @return array $events_reviewed
     */
    public function infraHardware()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getInfraHardwareNum();
        return view('events.events')->with([
            'title' => 'Eventos: Infraestructura Hardware',
            'name_graphic' => 'Infraestructura Hardware',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: Infraestructura Hardware'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('infraHardwaresReload')
        ]);
    }

    /**
     * Get a listing of the resource for Infraestructura Hardware.
     *
     * @return array $events_query
     */
    public function infraHardwaresReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventInfraHardware')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a Num of the resource for Infraestructura Hardware.
     *
     * @return string $events_query
     */
    public function getInfraHardwareNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventInfraHardware')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for Infraestructura Hardware.
     *
     */
    public function getInfraHardwareNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventInfraHardware')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for Infraestructura Hardware.
     *
     */
    public function getInfraHardwareNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventInfraHardware')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }
    /**
     * Get a Num of Minor for Infraestructura Hardware.
     *
     */

    public function getInfraHardwareNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventInfraHardware')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for Infraestructura Hardware.
     *
     */
    public function getInfraHardwareNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventInfraHardware')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }
    /**
     * Get a Num of Normal for Infraestructura Hardware.
     *
     */
    public function getInfraHardwareNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventInfraHardware')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for Infraestructura Hardware.
     *
     */
    public function getInfraHardwareNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventInfraHardware')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for Infraestructura de Red.
     *
     * @return array $events_reviewed
     */
    public function infraRed()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getInfraRedNum();
        return view('events.events')->with([
            'title' => 'Eventos: Infraestructura de Red',
            'name_graphic' => 'Infraestructura de Red',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: Infraestructura de Red'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('infraRedReload')
        ]);
    }

    /**
     * Get a listing of the resource for Infraestructura de Red.
     *
     * @return array $events_query
     */
    public function infraRedReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventInfraRed')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a Num of the resource for Infraestructura de Red.
     *
     * @return string $events_query
     */
    public function getInfraRedNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventInfraRed')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for Infraestructura Red.
     *
     */
    public function getInfraRedNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventInfraRed')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for Infraestructura Red.
     *
     */
    public function getInfraRedNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventInfraRed')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }
    /**
     * Get a Num of Minor for Infraestructura Red.
     *
     */

    public function getInfraRedNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventInfraRed')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for Infraestructura Red.
     *
     */
    public function getInfraRedNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventInfraRed')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }
    /**
     * Get a Num of Normal for Infraestructura Red.
     *
     */
    public function getInfraRedNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventInfraRed')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for Infraestructura Red.
     *
     */
    public function getInfraRedNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventInfraRed')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for Infraestructura de Seguridad.
     *
     * @return array $events_reviewed
     */
    public function infraSeguridad()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getInfraSeguridadNum();
        return view('events.events')->with([
            'title' => 'Eventos: Infraestructura de Seguridad',
            'name_graphic' => 'Infraestructura de Seguridad',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: Infraestructura de Seguridad'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('infraSeguridadReload')
        ]);
    }

    /**
     * Get a listing of the resource for Infraestructura de Seguridad.
     *
     * @return array $events_query
     */
    public function infraSeguridadReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventInfraSeguridad')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a Num of the resource for Infraestructura de Seguridad.
     *
     * @return string $events_query
     */
    public function getInfraSeguridadNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventInfraSeguridad')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for Infraestructura Seguridad.
     *
     */
    public function getInfraSeguridadNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventInfraSeguridad')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for Infraestructura Seguridad.
     *
     */
    public function getInfraSeguridadNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventInfraSeguridad')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }
    /**
     * Get a Num of Minor for Infraestructura Seguridad.
     *
     */

    public function getInfraSeguridadNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventInfraSeguridad')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for Infraestructura Seguridad.
     *
     */
    public function getInfraSeguridadNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventInfraSeguridad')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }
    /**
     * Get a Num of Normal for Infraestructura Seguridad.
     *
     */
    public function getInfraSeguridadNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventInfraSeguridad')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for Infraestructura Seguridad.
     *
     */
    public function getInfraSeguridadNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventInfraSeguridad')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for Zenoss Qro.
     *
     * @return array $events_reviewed
     */
    public function zenossQro()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getZenossQroNum();
        return view('events.events')->with([
            'title' => 'Eventos: Zenoss Qro',
            'name_graphic' => 'Zenoss Qro',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: Zenoss Qro'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('zenossQroReload')
        ]);
    }

    /**
     * Get a listing of the resource for Zenoss Qro.
     *
     * @return array $events_query
     */
    public function zenossQroReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventZenossQro')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for Zenoss Qro.
     *
     * @return string $events_query
     */
    public function getZenossQroNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventZenossQro')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for Zenoss Qro.
     *
     */
    public function getZenossQroNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventZenossQro')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for Zenoss Qro.
     *
     */
    public function getZenossQroNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventZenossQro')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }
    /**
     * Get a Num of Minor for Zenoss Qro.
     *
     */

    public function getZenossQroNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventZenossQro')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for Zenoss Qro
     *
     */
    public function getZenossQroNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventZenossQro')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }
    /**
     * Get a Num of Normal for Zenoss Qro.
     *
     */
    public function getZenossQroNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventZenossQro')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for Zenoss Qro.
     *
     */
    public function getZenossQroNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventZenossQro')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for Zenoss Mty ITOC.
     *
     * @return array $events_reviewed
     */
    public function zenossMtyItoc()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getZenossMtyItocNum();
        return view('events.events')->with([
            'title' => 'Eventos: Zenoss ITOC',
            'name_graphic' => 'Zenoss ITOC',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: Zenoss ITOC'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('zenossMtyItocReload')
        ]);
    }

    /**
     * Get a listing of the resource for Zenoss Mty ITOC.
     *
     * @param array $events_query
     */
    public function zenossMtyItocReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventZenossMtyItoc')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for Zenoss Mty ITOC.
     *
     * @param string $events_query
     */
    public function getZenossMtyItocNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventZenossMtyItoc')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for Zenoss Mty ITOC.
     *
     */
    public function getZenossMtyItocNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventZenossMtyItoc')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for Zenoss Mty ITOC.
     *
     */
    public function getZenossMtyItocNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventZenossMtyItoc')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }
    /**
     * Get a Num of Minor for Zenoss Mty ITOC.
     *
     */

    public function getZenossMtyItocNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventZenossMtyItoc')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for Zenoss Mty ITOC
     *
     */
    public function getZenossMtyItocNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventZenossMtyItoc')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }
    /**
     * Get a Num of Normal for Zenoss Mty ITOC.
     *
     */
    public function getZenossMtyItocNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventZenossMtyItoc')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for Zenoss Mty ITOC.
     *
     */
    public function getZenossMtyItocNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventZenossMtyItoc')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for Zenoss SRE.
     *
     * @return array $events_reviewed
     */
    public function zenossSRE()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getZenossSRENum();
        return view('events.events')->with([
            'title' => 'Eventos: Zenoss SRE',
            'name_graphic' => 'Zenoss SRE',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: Zenoss SRE'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('zenossSREReload')
        ]);
    }

    /**
     * Get a listing of the resource for Zenoss SRE.
     *
     * @return array $events_query
     */
    public function zenossSREReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventZenossSRE')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for Zenoss SRE.
     *
     * @return string $events_query
     */
    public function getZenossSRENum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventZenossSRE')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for Zenoss SRE.
     *
     */
    public function getZenossSRENumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventZenossSRE')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for Zenoss SRE.
     *
     */
    public function getZenossSRENumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventZenossSRE')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }
    /**
     * Get a Num of Minor for Zenoss SRE.
     *
     */

    public function getZenossSRENumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventZenossSRE')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for Zenoss SRE.
     *
     */
    public function getZenossSRENumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventZenossSRE')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }
    /**
     * Get a Num of Normal for Zenoss SRE.
     *
     */
    public function getZenossSRENumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventZenossSRE')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for Zenoss SRE.
     *
     */
    public function getZenossSRENumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventZenossSRE')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for Zenoss GBMV.
     *
     * @return array $events_reviewed
     */
    public function zenossGBMV()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getZenossGBMVNum();
        return view('events.events')->with([
            'title' => 'Eventos: Zenoss GBMV',
            'name_graphic' => 'Zenoss GBMV',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: Zenoss GBMV'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('zenossGBMVReload')
        ]);
    }

    /**
     * Get a listing of the resource for Zenoss GBMV.
     *
     * @return array $events_query
     */
    public function zenossGBMVReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventZenossGBMV')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for Zenoss GBMV.
     *
     * @return string $events_query
     */
    public function getZenossGBMVNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventZenossGBMV')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for Zenoss GBMV.
     *
     */
    public function getZenossGBMVNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventZenossGBMV')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for Zenoss GBMV.
     *
     */
    public function getZenossGBMVNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventZenossGBMV')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }
    /**
     * Get a Num of Minor for Zenoss GBMV.
     *
     */

    public function getZenossGBMVNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventZenossGBMV')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for Zenoss GBMV.
     *
     */
    public function getZenossGBMVNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventZenossGBMV')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }
    /**
     * Get a Num of Normal for Zenoss GBMV.
     *
     */
    public function getZenossGBMVNumumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventZenossGBMV')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for Zenoss GBMV.
     *
     */
    public function getZenossGBMVNumNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventZenossGBMV')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for Alertas Criticas CHAT.
     *
     * @return array $events_reviewed
     */
    public function alertsCriticasChat()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getAlertsCriticasChatNum();
        return view('events.events')->with([
            'title' => 'Eventos: Alertas Críticas CHAT',
            'name_graphic' => 'Alertas Críticas CHAT',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: Alertas Críticas CHAT'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('alertsCriticasChatReload')
        ]);
    }

    /**
     * Get a listing of the resource for Alertas Criticas CHAT.
     *
     * @return array $events_query
     */
    public function alertsCriticasChatReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventAlertsCriticasChat')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a listing of the resource for Alertas Criticas CHAT.
     *
     * @return array $events_query
     */
    public function getAlertsCriticasChatNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventAlertsCriticasChat')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for Alertas Criticas CHAT.
     *
     */
    public function getAlertsCriticasChatNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventAlertsCriticasChat')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for Alertas Criticas CHAT.
     *
     */
    public function getAlertsCriticasChatNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventAlertsCriticasChat')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }
    /**
     * Get a Num of Minor for Alertas Criticas CHAT.
     *
     */

    public function getAlertsCriticasChatNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventAlertsCriticasChat')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for Alertas Criticas CHAT.
     *
     */
    public function getAlertsCriticasChatNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventAlertsCriticasChat')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }
    /**
     * Get a Num of Normal for Alertas Criticas CHAT.
     *
     */
    public function getAlertsCriticasChatNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventAlertsCriticasChat')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for Alertas Criticas CHAT.
     *
     */
    public function getAlertsCriticasChatNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventAlertsCriticasChat')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for Exchange Críticas Reportar.
     *
     * @return array $events_reviewed
     */
    public function exchangeCriticas()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getExchangeCriticasNum();
        return view('events.events')->with([
            'title' => 'Eventos: Exchange Críticas Reportar',
            'name_graphic' => 'Exchange Críticas Reportar',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: Exchange Críticas Reportar'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('exchangeCriticasReload')
        ]);
    }

    /**
     * Get a listing of the resource for Críticas Reportar.
     *
     * @return array $events_query
     */
    public function exchangeCriticasReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventExchangeCriticas')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for Críticas Reportar.
     *
     * @return array $events_query
     */
    public function getExchangeCriticasNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventExchangeCriticas')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for Críticas Reportar.
     *
     */
    public function getExchangeCriticasNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventExchangeCriticas')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for Críticas Reportar.
     *
     */
    public function getExchangeCriticasNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventExchangeCriticas')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }
    /**
     * Get a Num of Minor for Críticas Reportar.
     *
     */

    public function getExchangeCriticasNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventExchangeCriticas')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for Críticas Reportar.
     *
     */
    public function getExchangeCriticasNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventExchangeCriticas')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }
    /**
     * Get a Num of Normal for Críticas Reportar.
     *
     */
    public function getExchangeCriticasNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventExchangeCriticas')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for Críticas Reportar.
     *
     */
    public function getExchangeCriticasNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventExchangeCriticas')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for Ingeniería Herramientas.
     *
     * @return array $events_reviewed
     */
    public function ingHerramientas()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getIngHerramientasNum();
        return view('events.events')->with([
            'title' => 'Eventos: Ingeniería Herramientas',
            'name_graphic' => 'Ingeniería Herramientas',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: Ingeniería Herramientas'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('ingHerramientasReload')
        ]);
    }

    /**
     * Get a listing of the resource for Ingeniería Herramientas.
     *
     * @return array $events_query
     */
    public function ingHerramientasReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventIngHerramientas')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for Ingeniería Herramientas.
     *
     * @return array $events_query
     */
    public function getIngHerramientasNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventIngHerramientas')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for Ingeniería Herramientas.
     *
     */
    public function getIngHerramientasNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventIngHerramientas')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for Ingeniería Herramientas.
     *
     */
    public function getIngHerramientasNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventIngHerramientas')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for Ingeniería Herramientas.
     *
     */
    public function getIngHerramientasNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventIngHerramientas')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for Ingeniería Herramientas.
     *
     */
    public function getIngHerramientasNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventIngHerramientas')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for Críticas Reportar.
     *
     */
    public function getIngHerramientasNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventIngHerramientas')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for Críticas Reportar.
     *
     */
    public function getIngHerramientassNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventIngHerramientas')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for ITOC_VCloud.
     *
     * @return array $events_reviewed
     */
    public function ITOC_VCloud()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getITOC_VCloudNum();
        return view('events.events')->with([
            'title' => 'Eventos: ITOC_VCloud',
            'name_graphic' => 'ITOC_VCloud',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: ITOC_VCloud'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('ITOC_VCloudReload')
        ]);
    }

    /**
     * Get a listing of the resource for ITOC_VCloud.
     *
     * @return array $events_query
     */
    public function ITOC_VCloudReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventITOC_VCloud')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for ITOC_VCloud.
     *
     * @return array $events_query
     */
    public function getITOC_VCloudNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventITOC_VCloud')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for ITOC_VCloud.
     *
     */
    public function getITOC_VCloudNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventITOC_VCloud')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for ITOC_VCloud.
     *
     */
    public function getITOC_VCloudNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventITOC_VCloud')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for ITOC_VCloud.
     *
     */
    public function getITOC_VCloudNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventITOC_VCloud')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for ITOC_VCloud.
     *
     */
    public function getITOC_VCloudNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventITOC_VCloud')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for ITOC_VCloud.
     *
     */
    public function getITOC_VCloudNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventITOC_VCloud')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for ITOC_VCloud.
     *
     */
    public function getITOC_VCloudNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventITOC_VCloud')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventPlataformasClientes.
     *
     * @return array $events_reviewed
     */
    public function eventPlataformasClientes()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventPlataformasClientesNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventPlataformasClientes',
            'name_graphic' => 'EventPlataformasClientes',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventPlataformasClientes'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventPlataformasClientesReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventPlataformasClientes.
     *
     * @return array $events_query
     */
    public function eventPlataformasClientesReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventPlataformasClientes')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventPlataformasClientes.
     *
     * @return array $events_query
     */
    public function getEventPlataformasClientesNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventPlataformasClientes')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventPlataformasClientes.
     *
     */
    public function getEventPlataformasClientesNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventPlataformasClientes')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventPlataformasClientes.
     *
     */
    public function getEventPlataformasClientesNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventPlataformasClientes')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventPlataformasClientes.
     *
     */
    public function getEventPlataformasClientesNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventPlataformasClientes')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventPlataformasClientes.
     *
     */
    public function getEventPlataformasClientesNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventPlataformasClientes')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventPlataformasClientes.
     *
     */
    public function getEventPlataformasClientesNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventPlataformasClientes')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventPlataformasClientes.
     *
     */
    public function getEventPlataformasClientesNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventPlataformasClientes')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventPingReinicios.
     *
     * @return array $events_reviewed
     */
    public function eventPingReinicios()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventPingReiniciosNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventPingReinicios',
            'name_graphic' => 'EventPingReinicios',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventPingReinicios'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventPingReiniciosReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventPingReinicios.
     *
     * @return array $events_query
     */
    public function eventPingReiniciosReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventPingReinicios')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventPingReinicios.
     *
     * @return array $events_query
     */
    public function getEventPingReiniciosNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventPingReinicios')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventPingReinicios.
     *
     */
    public function getEventPingReiniciosNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventPingReinicios')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventPingReinicios.
     *
     */
    public function getEventPingReiniciosNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventPingReinicios')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventPingReinicios.
     *
     */
    public function getEventPingReiniciosNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventPingReinicios')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventPingReinicios.
     *
     */
    public function getEventPingReiniciosNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventPingReinicios')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventPingReinicios.
     *
     */
    public function getEventPingReiniciosNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventPingReinicios')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventPingReinicios.
     *
     */
    public function getEventPingReiniciosNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventPingReinicios')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventExcepcionSinAtender.
     *
     * @return array $events_reviewed
     */
    public function eventExcepcionSinAtender()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventExcepciónSinAtenderNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventExcepciónSinAtender',
            'name_graphic' => 'EventExcepciónSinAtender',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventExcepciónSinAtender'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventExcepcionSinAtenderReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventExcepcionSinAtender.
     *
     * @return array $events_query
     */
    public function eventExcepcionSinAtenderReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventExcepcionSinAtender')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventExcepcionSinAtender.
     *
     * @return array $events_query
     */
    public function getEventExcepcionSinAtenderNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventExcepcionSinAtender')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventExcepcionSinAtender.
     *
     */
    public function getEventExcepcionSinAtenderNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventExcepcionSinAtender')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventExcepcionSinAtender.
     *
     */
    public function getEventExcepcionSinAtenderNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventExcepcionSinAtender')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventExcepcionSinAtender.
     *
     */
    public function getEventExcepcionSinAtenderNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventExcepcionSinAtender')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventExcepcionSinAtender.
     *
     */
    public function getEventExcepcionSinAtenderNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventExcepcionSinAtender')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventExcepcionSinAtender.
     *
     */
    public function getEventExcepcionSinAtenderNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventExcepcionSinAtender')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventExcepcionSinAtender.
     *
     */
    public function getEventExcepcionSinAtenderNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventExcepcionSinAtender')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventSitescope.
     *
     * @return array $events_reviewed
     */
    public function eventSitescope()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventSitescopeNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventSitescope',
            'name_graphic' => 'EventSitescope',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventSitescope'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventSitescopeReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventSitescope.
     *
     * @return array $events_query
     */
    public function eventSitescopeReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventSitescope')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventSitescope.
     *
     * @return array $events_query
     */
    public function getEventSitescopeNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventSitescope')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventSitescope.
     *
     */
    public function getEventSitescopeNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventSitescope')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventSitescope.
     *
     */
    public function getEventSitescopeNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventSitescope')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventSitescope.
     *
     */
    public function getEventSitescopeNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventSitescope')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventSitescope.
     *
     */
    public function getEventSitescopeNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventSitescope')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventSitescope.
     *
     */
    public function getEventSitescopeNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventSitescope')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventSitescope.
     *
     */
    public function getEventSitescopeNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventSitescope')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventNagiosURLsZabbix.
     *
     * @return array $events_reviewed
     */
    public function eventNagiosURLsZabbix()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventNagiosURLsZabbixNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventNagiosURLsZabbix',
            'name_graphic' => 'EventNagiosURLsZabbix',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventNagiosURLsZabbix'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventNagiosURLsZabbixReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventNagiosURLsZabbix.
     *
     * @return array $events_query
     */
    public function eventNagiosURLsZabbixReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventNagiosURLsZabbix')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventNagiosURLsZabbix.
     *
     * @return array $events_query
     */
    public function getEventNagiosURLsZabbixNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventNagiosURLsZabbix')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventNagiosURLsZabbix.
     *
     */
    public function getEventNagiosURLsZabbixNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventNagiosURLsZabbix')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventNagiosURLsZabbix.
     *
     */
    public function getEventNagiosURLsZabbixNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventNagiosURLsZabbix')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventNagiosURLsZabbix.
     *
     */
    public function getEventNagiosURLsZabbixNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventNagiosURLsZabbix')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventNagiosURLsZabbix.
     *
     */
    public function getEventNagiosURLsZabbixNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventNagiosURLsZabbix')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventNagiosURLsZabbix.
     *
     */
    public function getEventNagiosURLsZabbixNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventNagiosURLsZabbix')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventNagiosURLsZabbix.
     *
     */
    public function getEventNagiosURLsZabbixNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventNagiosURLsZabbix')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventAWSNewRelic.
     *
     * @return array $events_reviewed
     */
    public function eventAWSNewRelic()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventAWSNewRelicNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventAWSNewRelic',
            'name_graphic' => 'EventAWSNewRelic',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventAWSNewRelic'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventAWSNewRelicReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventAWSNewRelic.
     *
     * @return array $events_query
     */
    public function eventAWSNewRelicReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventAWSNewRelic')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventAWSNewRelic.
     *
     * @return array $events_query
     */
    public function getEventAWSNewRelicNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventAWSNewRelic')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventAWSNewRelic.
     *
     */
    public function getEventAWSNewRelicNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventAWSNewRelic')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventAWSNewRelic.
     *
     */
    public function getEventAWSNewRelicNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventAWSNewRelic')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventAWSNewRelic.
     *
     */
    public function getEventAWSNewRelicNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventAWSNewRelic')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventAWSNewRelic.
     *
     */
    public function getEventAWSNewRelicNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventAWSNewRelic')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventAWSNewRelic.
     *
     */
    public function getEventAWSNewRelicNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventAWSNewRelic')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventAWSNewRelic.
     *
     */
    public function getEventAWSNewRelicNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventAWSNewRelic')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventEventosdeRFC.
     *
     * @return array $events_reviewed
     */
    public function eventEventosdeRFC()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventEventosdeRFCNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventEventosdeRFC',
            'name_graphic' => 'EventEventosdeRFC',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventEventosdeRFC'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventEventosdeRFCReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventEventosdeRFC.
     *
     * @return array $events_query
     */
    public function eventEventosdeRFCReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventEventosdeRFC')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventEventosdeRFC.
     *
     * @return array $events_query
     */
    public function getEventEventosdeRFCNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventEventosdeRFC')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventEventosdeRFC.
     *
     */
    public function getEventEventosdeRFCNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventEventosdeRFC')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventEventosdeRFC.
     *
     */
    public function getEventEventosdeRFCNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventEventosdeRFC')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventEventosdeRFC.
     *
     */
    public function getEventEventosdeRFCNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventEventosdeRFC')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventEventosdeRFC.
     *
     */
    public function getEventEventosdeRFCNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventEventosdeRFC')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventEventosdeRFC.
     *
     */
    public function getEventEventosdeRFCNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventEventosdeRFC')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventEventosdeRFC.
     *
     */
    public function getEventEventosdeRFCNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventEventosdeRFC')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventZenossCloudVIP.
     *
     * @return array $events_reviewed
     */
    public function eventZenossCloudVIP()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventZenossCloudVIPNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventZenossCloudVIP',
            'name_graphic' => 'EventZenossCloudVIP',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventZenossCloudVIP'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventZenossCloudVIPReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventZenossCloudVIP.
     *
     * @return array $events_query
     */
    public function eventZenossCloudVIPReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventZenossCloudVIP')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventZenossCloudVIP.
     *
     * @return array $events_query
     */
    public function getEventZenossCloudVIPNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventZenossCloudVIP')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventZenossCloudVIP.
     *
     */
    public function getEventZenossCloudVIPNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventZenossCloudVIP')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventZenossCloudVIP.
     *
     */
    public function getEventZenossCloudVIPNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventZenossCloudVIP')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventZenossCloudVIP.
     *
     */
    public function getEventZenossCloudVIPNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventZenossCloudVIP')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventZenossCloudVIP.
     *
     */
    public function getEventZenossCloudVIPNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventZenossCloudVIP')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventZenossCloudVIP.
     *
     */
    public function getEventZenossCloudVIPNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventZenossCloudVIP')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventZenossCloudVIP.
     *
     */
    public function getEventZenossCloudVIPNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventZenossCloudVIP')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventEventosResueltos.
     *
     * @return array $events_reviewed
     */
    public function eventEventosResueltos()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventEventosResueltosNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventEventosResueltos',
            'name_graphic' => 'EventEventosResueltos',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventEventosResueltos'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventEventosResueltosReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventEventosResueltos.
     *
     * @return array $events_query
     */
    public function eventEventosResueltosReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventEventosResueltos')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventEventosResueltos.
     *
     * @return array $events_query
     */
    public function getEventEventosResueltosNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventEventosResueltos')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventEventosResueltos.
     *
     */
    public function getEventEventosResueltosNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventEventosResueltos')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventEventosResueltos.
     *
     */
    public function getEventEventosResueltosNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventEventosResueltos')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventEventosResueltos.
     *
     */
    public function getEventEventosResueltosNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventEventosResueltos')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventEventosResueltos.
     *
     */
    public function getEventEventosResueltosNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventEventosResueltos')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventEventosResueltos.
     *
     */
    public function getEventEventosResueltosNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventEventosResueltos')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventEventosResueltos.
     *
     */
    public function getEventEventosResueltosNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventEventosResueltos')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventClientesSAP.
     *
     * @return array $events_reviewed
     */
    public function eventClientesSAP()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventClientesSAPNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventClientesSAP',
            'name_graphic' => 'EventClientesSAP',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventClientesSAP'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventClientesSAPReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventClientesSAP.
     *
     * @return array $events_query
     */
    public function eventClientesSAPReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventClientesSAP')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventClientesSAP.
     *
     * @return array $events_query
     */
    public function getEventClientesSAPNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventClientesSAP')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventClientesSAP.
     *
     */
    public function getEventClientesSAPNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventClientesSAP')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventClientesSAP.
     *
     */
    public function getEventClientesSAPNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventClientesSAP')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventClientesSAP.
     *
     */
    public function getEventClientesSAPNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventClientesSAP')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventClientesSAP.
     *
     */
    public function getEventClientesSAPNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventClientesSAP')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventClientesSAP.
     *
     */
    public function getEventClientesSAPNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventClientesSAP')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventClientesSAP.
     *
     */
    public function getEventClientesSAPNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventClientesSAP')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventAlertasAvaya.
     *
     * @return array $events_reviewed
     */
    public function eventAlertasAvaya()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventAlertasAvayaNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventAlertasAvaya',
            'name_graphic' => 'EventAlertasAvaya',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventAlertasAvaya'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventAlertasAvayaReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventAlertasAvaya.
     *
     * @return array $events_query
     */
    public function eventAlertasAvayaReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventAlertasAvaya')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventAlertasAvaya.
     *
     * @return array $events_query
     */
    public function getEventAlertasAvayaNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventAlertasAvaya')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventAlertasAvaya.
     *
     */
    public function getEventAlertasAvayaNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventAlertasAvaya')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventAlertasAvaya.
     *
     */
    public function getEventAlertasAvayaNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventAlertasAvaya')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventAlertasAvaya.
     *
     */
    public function getEventAlertasAvayaNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventAlertasAvaya')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventAlertasAvaya.
     *
     */
    public function getEventAlertasAvayaNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventAlertasAvaya')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventAlertasAvaya.
     *
     */
    public function getEventAlertasAvayaNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventAlertasAvaya')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventAlertasAvaya.
     *
     */
    public function getEventAlertasAvayaNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventAlertasAvaya')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventIntegracionesITOCTOOLs.
     *
     * @return array $events_reviewed
     */
    public function eventIntegracionesITOCTOOLs()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventIntegracionesITOCTOOLsNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventIntegracionesITOCTOOLs',
            'name_graphic' => 'EventIntegracionesITOCTOOLs',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventIntegracionesITOCTOOLs'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventIntegracionesITOCTOOLsReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventIntegracionesITOCTOOLs.
     *
     * @return array $events_query
     */
    public function eventIntegracionesITOCTOOLsReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventIntegracionesITOCTOOLs')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventIntegracionesITOCTOOLs.
     *
     * @return array $events_query
     */
    public function getEventIntegracionesITOCTOOLsNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventIntegracionesITOCTOOLs')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventIntegracionesITOCTOOLs.
     *
     */
    public function getEventIntegracionesITOCTOOLsNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventIntegracionesITOCTOOLs')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventIntegracionesITOCTOOLs.
     *
     */
    public function getEventIntegracionesITOCTOOLsNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventIntegracionesITOCTOOLs')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventIntegracionesITOCTOOLs.
     *
     */
    public function getEventIntegracionesITOCTOOLsNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventIntegracionesITOCTOOLs')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventIntegracionesITOCTOOLs.
     *
     */
    public function getEventIntegracionesITOCTOOLsNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventIntegracionesITOCTOOLs')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventIntegracionesITOCTOOLs.
     *
     */
    public function getEventIntegracionesITOCTOOLsNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventIntegracionesITOCTOOLs')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventIntegracionesITOCTOOLs.
     *
     */
    public function getEventIntegracionesITOCTOOLsNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventIntegracionesITOCTOOLs')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventAlertasNA.
     *
     * @return array $events_reviewed
     */
    public function eventAlertasNA()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventAlertasNANum();
        return view('events.events')->with([
            'title' => 'Eventos: EventAlertasNA',
            'name_graphic' => 'EventAlertasNA',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventAlertasNA'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventAlertasNAReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventAlertasNA.
     *
     * @return array $events_query
     */
    public function eventAlertasNAReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventAlertasNA')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventAlertasNA.
     *
     * @return array $events_query
     */
    public function getEventAlertasNANum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventAlertasNA')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventAlertasNA.
     *
     */
    public function getEventAlertasNANumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventAlertasNA')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventAlertasNA.
     *
     */
    public function getEventAlertasNANumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventAlertasNA')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventAlertasNA.
     *
     */
    public function getEventAlertasNANumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventAlertasNA')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventAlertasNA.
     *
     */
    public function getEventAlertasNANumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventAlertasNA')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventAlertasNA.
     *
     */
    public function getEventAlertasNANumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventAlertasNA')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventAlertasNA.
     *
     */
    public function getEventAlertasNANumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventAlertasNA')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventEventosDummyPrueba.
     *
     * @return array $events_reviewed
     */
    public function eventEventosDummyPrueba()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventEventosDummyPruebaNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventEventosDummyPrueba',
            'name_graphic' => 'EventEventosDummyPrueba',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventEventosDummyPrueba'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventEventosDummyPruebaReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventEventosDummyPrueba.
     *
     * @return array $events_query
     */
    public function eventEventosDummyPruebaReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventEventosDummyPrueba')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventEventosDummyPrueba.
     *
     * @return array $events_query
     */
    public function getEventEventosDummyPruebaNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventEventosDummyPrueba')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventEventosDummyPrueba.
     *
     */
    public function getEventEventosDummyPruebaNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventEventosDummyPrueba')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventEventosDummyPrueba.
     *
     */
    public function getEventEventosDummyPruebaNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventEventosDummyPrueba')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventEventosDummyPrueba.
     *
     */
    public function getEventEventosDummyPruebaNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventEventosDummyPrueba')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventEventosDummyPrueba.
     *
     */
    public function getEventEventosDummyPruebaNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventEventosDummyPrueba')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventEventosDummyPrueba.
     *
     */
    public function getEventEventosDummyPruebaNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventEventosDummyPrueba')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventEventosDummyPrueba.
     *
     */
    public function getEventEventosDummyPruebaNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventEventosDummyPrueba')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a listing of the resource for EventPoleos.
     *
     * @return array $events_reviewed
     */
    public function eventPoleos()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        $num = $this->getEventPoleosNum();
        return view('events.events')->with([
            'title' => 'Eventos: EventPoleos',
            'name_graphic' => 'EventPoleos',
            'num' => $num,
            'breadcrumb' => [
                [
                    'title' => 'Eventos: EventPoleos'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('eventPoleosReload')
        ]);
    }

    /**
     * Get a listing of the resource for EventPoleos.
     *
     * @return array $events_query
     */
    public function eventPoleosReload()
    {
        $events_query = EventsFilters::where('filter_name', 'EventPoleos')
            ->orderByDesc("time_received")
            ->get();
        $api_date = $this->apiDate();
        foreach ($events_query as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events_query)->toJson();
    }

    /**
     * Get a num of the resource for EventPoleos.
     *
     * @return array $events_query
     */
    public function getEventPoleosNum()
    {
        $events_query = EventsFilters::where('filter_name', 'EventPoleos')
            ->count();
        return $events_query;
    }

    /**
     * Get a Num of Critical for EventPoleos.
     *
     */
    public function getEventPoleosNumCritical()
    {
        $eventsNumCritical = EventsFilters::where('filter_name', 'EventPoleos')
            ->where('severity', 'critical')
            ->count();
        return $eventsNumCritical;
    }

    /**
     * Get a Num of Major for EventPoleos.
     *
     */
    public function getEventPoleosNumMajor()
    {
        $eventsNumMajor = EventsFilters::where('filter_name', 'EventPoleos')
            ->where('severity', 'major')
            ->count();
        return $eventsNumMajor;
    }

    /**
     * Get a Num of Minor for EventPoleos.
     *
     */
    public function getEventPoleosNumMinor()
    {
        $eventsNumMinor = EventsFilters::where('filter_name', 'EventPoleos')
            ->where('severity', 'minor')
            ->count();
        return $eventsNumMinor;
    }

    /**
     * Get a Num of Warning for EventPoleos.
     *
     */
    public function getEventPoleosNumWarning()
    {
        $eventsNumWarning = EventsFilters::where('filter_name', 'EventPoleos')
            ->where('severity', 'warning')
            ->count();
        return $eventsNumWarning;
    }

    /**
     * Get a Num of Normal for EventPoleos.
     *
     */
    public function getEventPoleosNumNormal()
    {
        $eventsNumNormal = EventsFilters::where('filter_name', 'EventPoleos')
            ->where('severity', 'normal')
            ->count();
        return $eventsNumNormal;
    }

    /**
     * Get a Num of Unknown for EventPoleos.
     *
     */
    public function getEventPoleosNumUnknown()
    {
        $eventsNumUnknown = EventsFilters::where('filter_name', 'EventPoleos')
            ->where('severity', 'unknown')
            ->count();
        return $eventsNumUnknown;
    }

    /**
     * Display a Reload of the resource for Filters.
     *
     * @return \Illuminate\Http\Response
     */
    public function graphicsReload()
    {
        $graphicsReloadNum = array();
        $graphicsReloadNum['telmexSlim']  = $this->getTelmexSlimNum();
        $graphicsReloadNum['telmexSlimCritical']  = $this->getTelmexSlimCriticalNum();
        $graphicsReloadNum['telmexSlimMajor']  = $this->getTelmexSlimMajorNum();
        $graphicsReloadNum['telmexSlimMinor']  = $this->getTelmexSlimMinorNum();
        /* $graphicsReloadNum['telmexSlimWarning']  = $this->getTelmexSlimWarningNum();
        $graphicsReloadNum['telmexSlimNormal']  = $this->getTelmexSlimNormalNum(); */
        $graphicsReloadNum['telmexSlimUnknown']  = $this->getTelmexSlimUnknownNum();

        $graphicsReloadNum['openException'] = $this->getOpenExceptionReloadNum();
        $graphicsReloadNum['openExceptionCritical'] = $this->getOpenExceptionReloadNumCritical();
        $graphicsReloadNum['openExceptionMajor'] = $this->getOpenExceptionReloadNumMajor();
        $graphicsReloadNum['openExceptionMinor'] = $this->getOpenExceptionReloadNumMinor();
        /* $graphicsReloadNum['openExceptionWarning'] = $this->getOpenExceptionReloadNumWarning();
        $graphicsReloadNum['openExceptionNormal'] = $this->getOpenExceptionReloadNumNormal(); */
        $graphicsReloadNum['openExceptionUnknown'] = $this->getOpenExceptionReloadNumUnknown();

        $graphicsReloadNum['exchangeCriticas'] = $this->getExchangeCriticasNum();
        $graphicsReloadNum['exchangeCriticasCritical'] = $this->getExchangeCriticasNumCritical();
        $graphicsReloadNum['exchangeCriticasMajor'] = $this->getExchangeCriticasNumMajor();
        $graphicsReloadNum['exchangeCriticasMinor'] = $this->getExchangeCriticasNumMinor();
        /* $graphicsReloadNum['exchangeCriticasWarning'] = $this->getExchangeCriticasNumWarning();
        $graphicsReloadNum['exchangeCriticasNormal'] = $this->getExchangeCriticasNumNormal(); */
        $graphicsReloadNum['exchangeCriticasUnknown'] = $this->getExchangeCriticasNumUnknown();

        $graphicsReloadNum['alertsWithOutIMSDInProgress'] = $this->getAlertsWithOutIMSDInProgressNum();
        $graphicsReloadNum['alertsWithOutIMSDInProgressCritical'] = $this->getAlertsWithOutIMSDInProgressNumCritical();
        $graphicsReloadNum['alertsWithOutIMSDInProgressMajor'] = $this->getAlertsWithOutIMSDInProgressNumMajor();
        $graphicsReloadNum['alertsWithOutIMSDInProgressMinor'] = $this->getAlertsWithOutIMSDInProgressNumMinor();
        /* $graphicsReloadNum['alertsWithOutIMSDInProgressWarning'] = $this->getAlertsWithOutIMSDInProgressNumWarning();
        $graphicsReloadNum['alertsWithOutIMSDInProgressNormal'] = $this->getAlertsWithOutIMSDInProgressNumNormal(); */
        $graphicsReloadNum['alertsWithOutIMSDInProgressUnknown'] = $this->getAlertsWithOutIMSDInProgressNumUnknown();

        $graphicsReloadNum['alertsCriticasChat'] = $this->getAlertsCriticasChatNum();
        $graphicsReloadNum['alertsCriticasChatCritical'] = $this->getAlertsCriticasChatNumCritical();
        $graphicsReloadNum['alertsCriticasChatMajor'] = $this->getAlertsCriticasChatNumMajor();
        $graphicsReloadNum['alertsCriticasChatMinor'] = $this->getAlertsCriticasChatNumMinor();
        /* $graphicsReloadNum['alertsCriticasChatWarning'] = $this->getAlertsCriticasChatNumWarning();
        $graphicsReloadNum['alertsCriticasChatNormal'] = $this->getAlertsCriticasChatNumNormal(); */
        $graphicsReloadNum['alertsCriticasChatUnknown'] = $this->getAlertsCriticasChatNumUnknown();

        $graphicsReloadNum['infraHardware'] = $this->getInfraHardwareNum();
        $graphicsReloadNum['infraHardwareCritical'] = $this->getInfraHardwareNumCritical();
        $graphicsReloadNum['infraHardwareMajor'] = $this->getInfraHardwareNumMajor();
        $graphicsReloadNum['infraHardwareMinor'] = $this->getInfraHardwareNumMinor();
        /* $graphicsReloadNum['infraHardwareWarning'] = $this->getInfraHardwareNumWarning();
        $graphicsReloadNum['infraHardwareNormal'] = $this->getInfraHardwareNumNormal(); */
        $graphicsReloadNum['infraHardwareUnknown'] = $this->getInfraHardwareNumUnknown();

        $graphicsReloadNum['infraRed'] = $this->getInfraRedNum();
        $graphicsReloadNum['infraRedCritical'] = $this->getInfraRedNumCritical();
        $graphicsReloadNum['infraRedMajor'] = $this->getInfraRedNumMajor();
        $graphicsReloadNum['infraRedMinor'] = $this->getInfraRedNumMinor();
        /* $graphicsReloadNum['infraRedWarning'] = $this->getInfraRedNumWarning();
        $graphicsReloadNum['infraRedNormal'] = $this->getInfraRedNumNormal(); */
        $graphicsReloadNum['infraRedUnknown'] = $this->getInfraRedNumUnknown();

        $graphicsReloadNum['infraSeguridad'] = $this->getInfraSeguridadNum();
        $graphicsReloadNum['infraSeguridadCritical'] = $this->getInfraSeguridadNumCritical();
        $graphicsReloadNum['infraSeguridadMajor'] = $this->getInfraSeguridadNumMajor();
        $graphicsReloadNum['infraSeguridadMinor'] = $this->getInfraSeguridadNumMinor();
        $graphicsReloadNum['infraSeguridadWarning'] = $this->getInfraSeguridadNumWarning();
        /* $graphicsReloadNum['infraSeguridadNormal'] = $this->getInfraSeguridadNumNormal(); */
        $graphicsReloadNum['infraSeguridadUnknown'] = $this->getInfraSeguridadNumUnknown();

        $graphicsReloadNum['zenossQro'] = $this->getZenossQroNum();
        $graphicsReloadNum['zenossQroCritical'] = $this->getZenossQroNumCritical();
        $graphicsReloadNum['zenossQroMajor'] = $this->getZenossQroNumMajor();
        $graphicsReloadNum['zenossQroMinor'] = $this->getZenossQroNumMinor();
        /*  $graphicsReloadNum['zenossQroWarning'] = $this->getZenossQroNumWarning();
        $graphicsReloadNum['zenossQroNormal'] = $this->getZenossQroNumNormal(); */
        $graphicsReloadNum['zenossQroUnknown'] = $this->getZenossQroNumUnknown();

        $graphicsReloadNum['zenossMtyItoc'] = $this->getZenossMtyItocNum();
        $graphicsReloadNum['zenossMtyItocCritical'] = $this->getZenossMtyItocNumCritical();
        $graphicsReloadNum['zenossMtyItocMajor'] = $this->getZenossMtyItocNumMajor();
        $graphicsReloadNum['zenossMtyItocMinor'] = $this->getZenossMtyItocNumMinor();
        /* $graphicsReloadNum['zenossMtyItocWarning'] = $this->getZenossMtyItocNumWarning();
        $graphicsReloadNum['zenossMtyItocNormal'] = $this->getZenossMtyItocNumNormal(); */
        $graphicsReloadNum['zenossMtyItocUnknown'] = $this->getZenossMtyItocNumUnknown();

        $graphicsReloadNum['zenossSRE'] = $this->getZenossSRENum();
        $graphicsReloadNum['zenossSRECritical'] = $this->getZenossSRENumCritical();
        $graphicsReloadNum['zenossSREMajor'] = $this->getZenossSRENumMajor();
        $graphicsReloadNum['zenossSREMinor'] = $this->getZenossSRENumMinor();
        /* $graphicsReloadNum['zenossSREWarning'] = $this->getZenossSRENumWarning();
        $graphicsReloadNum['zenossSRENormal'] = $this->getZenossSRENumNormal(); */
        $graphicsReloadNum['zenossSREUnknown'] = $this->getZenossSRENumUnknown();

        $graphicsReloadNum['zenossGBMV'] = $this->getZenossGBMVNum();
        $graphicsReloadNum['zenossGBMVCritical'] = $this->getZenossGBMVNumCritical();
        $graphicsReloadNum['zenossGBMVMajor'] = $this->getZenossGBMVNumMajor();
        $graphicsReloadNum['zenossGBMVMinor'] = $this->getZenossGBMVNumMinor();
        /* $graphicsReloadNum['zenossGBMVWarning'] = $this->getZenossGBMVNumWarning();
        $graphicsReloadNum['zenossGBMVNormal'] = $this->getZenossGBMVNumumNormal(); */
        $graphicsReloadNum['zenossGBMVUnknown'] = $this->getZenossGBMVNumNumUnknown();

        $graphicsReloadNum['ingHerramientas'] = $this->getIngHerramientasNum();
        $graphicsReloadNum['ingHerramientasCritical'] = $this->getIngHerramientasNumCritical();
        $graphicsReloadNum['ingHerramientasMajor'] = $this->getIngHerramientasNumMajor();
        $graphicsReloadNum['ingHerramientasMinor'] = $this->getIngHerramientasNumMinor();
        $graphicsReloadNum['ingHerramientasWarning'] = $this->getIngHerramientasNumWarning();
        /* $graphicsReloadNum['ingHerramientasNormal'] = $this->getIngHerramientasNumNormal(); */
        $graphicsReloadNum['ingHerramientasUnknown'] = $this->getIngHerramientassNumUnknown();

        $graphicsReloadNum['ITOC_VCloud'] = $this->getITOC_VCloudNum();
        $graphicsReloadNum['ITOC_VCloudCritical'] = $this->getITOC_VCloudNumCritical();
        $graphicsReloadNum['ITOC_VCloudMajor'] = $this->getITOC_VCloudNumMajor();
        $graphicsReloadNum['ITOC_VCloudMinor'] = $this->getITOC_VCloudNumMinor();
        $graphicsReloadNum['ITOC_VCloudWarning'] = $this->getITOC_VCloudNumWarning();
        /* $graphicsReloadNum['ITOC_VCloudNormal'] = $this->getITOC_VCloudNumNormal(); */
        $graphicsReloadNum['ITOC_VCloudUnknown'] = $this->getITOC_VCloudNumUnknown();

        $graphicsReloadNum['EventPlataformasClientes'] = $this->getEventPlataformasClientesNum();
        $graphicsReloadNum['EventPlataformasClientesCritical'] = $this->getEventPlataformasClientesNumCritical();
        $graphicsReloadNum['EventPlataformasClientesMajor'] = $this->getEventPlataformasClientesNumMajor();
        $graphicsReloadNum['EventPlataformasClientesMinor'] = $this->getEventPlataformasClientesNumMinor();
        $graphicsReloadNum['EventPlataformasClientesWarning'] = $this->getEventPlataformasClientesNumWarning();
        /* $graphicsReloadNum['EventPlataformasClientesNormal'] = $this->getEventPlataformasClientesNumNormal(); */
        $graphicsReloadNum['EventPlataformasClientesUnknown'] = $this->getEventPlataformasClientesNumUnknown();

        $graphicsReloadNum['EventPingReinicios'] = $this->getEventPingReiniciosNum();
        $graphicsReloadNum['EventPingReiniciosCritical'] = $this->getEventPingReiniciosNumCritical();
        $graphicsReloadNum['EventPingReiniciosMajor'] = $this->getEventPingReiniciosNumMajor();
        $graphicsReloadNum['EventPingReiniciosMinor'] = $this->getEventPingReiniciosNumMinor();
        $graphicsReloadNum['EventPingReiniciosWarning'] = $this->getEventPingReiniciosNumWarning();
        /* $graphicsReloadNum['EventPingReiniciosNormal'] = $this->getEventPingReiniciosNumNormal(); */
        $graphicsReloadNum['EventPingReiniciosUnknown'] = $this->getEventPingReiniciosNumUnknown();

        $graphicsReloadNum['EventExcepcionSinAtender'] = $this->getEventExcepcionSinAtenderNum();
        $graphicsReloadNum['EventExcepcionSinAtenderCritical'] = $this->getEventExcepcionSinAtenderNumCritical();
        $graphicsReloadNum['EventExcepcionSinAtenderMajor'] = $this->getEventExcepcionSinAtenderNumMajor();
        $graphicsReloadNum['EventExcepcionSinAtenderMinor'] = $this->getEventExcepcionSinAtenderNumMinor();
        $graphicsReloadNum['EventExcepcionSinAtenderWarning'] = $this->getEventExcepcionSinAtenderNumWarning();
        /* $graphicsReloadNum['EventExcepcionSinAtenderNormal'] = $this->getEventExcepcionSinAtenderNumNormal(); */
        $graphicsReloadNum['EventExcepcionSinAtenderUnknown'] = $this->getEventExcepcionSinAtenderNumUnknown();

        $graphicsReloadNum['EventSitescope'] = $this->getEventSitescopeNum();
        $graphicsReloadNum['EventSitescopeCritical'] = $this->getEventSitescopeNumCritical();
        $graphicsReloadNum['EventSitescopeMajor'] = $this->getEventSitescopeNumMajor();
        $graphicsReloadNum['EventSitescopeMinor'] = $this->getEventSitescopeNumMinor();
        $graphicsReloadNum['EventSitescopeWarning'] = $this->getEventSitescopeNumWarning();
        /* $graphicsReloadNum['EventSitescopeNormal'] = $this->getEventSitescopeNumNormal(); */
        $graphicsReloadNum['EventSitescopeUnknown'] = $this->getEventSitescopeNumUnknown();

        $graphicsReloadNum['EventNagiosURLsZabbix'] = $this->getEventNagiosURLsZabbixNum();
        $graphicsReloadNum['EventNagiosURLsZabbixCritical'] = $this->getEventNagiosURLsZabbixNumCritical();
        $graphicsReloadNum['EventNagiosURLsZabbixMajor'] = $this->getEventNagiosURLsZabbixNumMajor();
        $graphicsReloadNum['EventNagiosURLsZabbixMinor'] = $this->getEventNagiosURLsZabbixNumMinor();
        $graphicsReloadNum['EventNagiosURLsZabbixWarning'] = $this->getEventNagiosURLsZabbixNumWarning();
        /* $graphicsReloadNum['EventNagiosURLsZabbixNormal'] = $this->getEventNagiosURLsZabbixNumNormal(); */
        $graphicsReloadNum['EventNagiosURLsZabbixUnknown'] = $this->getEventNagiosURLsZabbixNumUnknown();

        $graphicsReloadNum['EventAWSNewRelic'] = $this->getEventAWSNewRelicNum();
        $graphicsReloadNum['EventAWSNewRelicCritical'] = $this->getEventAWSNewRelicNumCritical();
        $graphicsReloadNum['EventAWSNewRelicMajor'] = $this->getEventAWSNewRelicNumMajor();
        $graphicsReloadNum['EventAWSNewRelicMinor'] = $this->getEventAWSNewRelicNumMinor();
        $graphicsReloadNum['EventAWSNewRelicWarning'] = $this->getEventAWSNewRelicNumWarning();
        /* $graphicsReloadNum['EventAWSNewRelicNormal'] = $this->getEventAWSNewRelicNumNormal(); */
        $graphicsReloadNum['EventAWSNewRelicUnknown'] = $this->getEventAWSNewRelicNumUnknown();

        $graphicsReloadNum['EventEventosdeRFC'] = $this->getEventEventosdeRFCNum();
        $graphicsReloadNum['EventEventosdeRFCCritical'] = $this->getEventEventosdeRFCNumCritical();
        $graphicsReloadNum['EventEventosdeRFCMajor'] = $this->getEventEventosdeRFCNumMajor();
        $graphicsReloadNum['EventEventosdeRFCMinor'] = $this->getEventEventosdeRFCNumMinor();
        $graphicsReloadNum['EventEventosdeRFCWarning'] = $this->getEventEventosdeRFCNumWarning();
        /* $graphicsReloadNum['EventEventosdeRFCNormal'] = $this->getEventEventosdeRFCNumNormal(); */
        $graphicsReloadNum['EventEventosdeRFCUnknown'] = $this->getEventEventosdeRFCNumUnknown();

        $graphicsReloadNum['EventZenossCloudVIP'] = $this->getEventZenossCloudVIPNum();
        $graphicsReloadNum['EventZenossCloudVIPCritical'] = $this->getEventZenossCloudVIPNumCritical();
        $graphicsReloadNum['EventZenossCloudVIPMajor'] = $this->getEventZenossCloudVIPNumMajor();
        $graphicsReloadNum['EventZenossCloudVIPMinor'] = $this->getEventZenossCloudVIPNumMinor();
        $graphicsReloadNum['EventZenossCloudVIPWarning'] = $this->getEventZenossCloudVIPNumWarning();
        /* $graphicsReloadNum['EventZenossCloudVIPNormal'] = $this->getEventZenossCloudVIPNumNormal(); */
        $graphicsReloadNum['EventZenossCloudVIPUnknown'] = $this->getEventZenossCloudVIPNumUnknown();

        $graphicsReloadNum['EventEventosResueltos'] = $this->getEventEventosResueltosNum();
        $graphicsReloadNum['EventEventosResueltosCritical'] = $this->getEventEventosResueltosNumCritical();
        $graphicsReloadNum['EventEventosResueltosMajor'] = $this->getEventEventosResueltosNumMajor();
        $graphicsReloadNum['EventEventosResueltosMinor'] = $this->getEventEventosResueltosNumMinor();
        $graphicsReloadNum['EventEventosResueltosWarning'] = $this->getEventEventosResueltosNumWarning();
        /* $graphicsReloadNum['EventEventosResueltosNormal'] = $this->getEventEventosResueltosNumNormal(); */
        $graphicsReloadNum['EventEventosResueltosUnknown'] = $this->getEventEventosResueltosNumUnknown();

        $graphicsReloadNum['EventClientesSAP'] = $this->getEventClientesSAPNum();
        $graphicsReloadNum['EventClientesSAPCritical'] = $this->getEventClientesSAPNumCritical();
        $graphicsReloadNum['EventClientesSAPMajor'] = $this->getEventClientesSAPNumMajor();
        $graphicsReloadNum['EventClientesSAPMinor'] = $this->getEventClientesSAPNumMinor();
        $graphicsReloadNum['EventClientesSAPWarning'] = $this->getEventClientesSAPNumWarning();
        /* $graphicsReloadNum['EventClientesSAPNormal'] = $this->getEventClientesSAPNumNormal(); */
        $graphicsReloadNum['EventClientesSAPUnknown'] = $this->getEventClientesSAPNumUnknown();

        $graphicsReloadNum['EventAlertasAvaya'] = $this->getEventAlertasAvayaNum();
        $graphicsReloadNum['EventAlertasAvayaCritical'] = $this->getEventAlertasAvayaNumCritical();
        $graphicsReloadNum['EventAlertasAvayaMajor'] = $this->getEventAlertasAvayaNumMajor();
        $graphicsReloadNum['EventAlertasAvayaMinor'] = $this->getEventAlertasAvayaNumMinor();
        $graphicsReloadNum['EventAlertasAvayaWarning'] = $this->getEventAlertasAvayaNumWarning();
        /* $graphicsReloadNum['EventAlertasAvayaNormal'] = $this->getEventAlertasAvayaNumNormal(); */
        $graphicsReloadNum['EventAlertasAvayaUnknown'] = $this->getEventAlertasAvayaNumUnknown();

        $graphicsReloadNum['EventIntegracionesITOCTOOLs'] = $this->getEventIntegracionesITOCTOOLsNum();
        $graphicsReloadNum['EventIntegracionesITOCTOOLsCritical'] = $this->getEventIntegracionesITOCTOOLsNumCritical();
        $graphicsReloadNum['EventIntegracionesITOCTOOLsMajor'] = $this->getEventIntegracionesITOCTOOLsNumMajor();
        $graphicsReloadNum['EventIntegracionesITOCTOOLsMinor'] = $this->getEventIntegracionesITOCTOOLsNumMinor();
        $graphicsReloadNum['EventIntegracionesITOCTOOLsWarning'] = $this->getEventIntegracionesITOCTOOLsNumWarning();
        /* $graphicsReloadNum['EventIntegracionesITOCTOOLsNormal'] = $this->getEventIntegracionesITOCTOOLsNumNormal(); */
        $graphicsReloadNum['EventIntegracionesITOCTOOLsUnknown'] = $this->getEventIntegracionesITOCTOOLsNumUnknown();

        $graphicsReloadNum['EventAlertasNA'] = $this->getEventAlertasNANum();
        $graphicsReloadNum['EventAlertasNACritical'] = $this->getEventAlertasNANumCritical();
        $graphicsReloadNum['EventAlertasNAMajor'] = $this->getEventAlertasNANumMajor();
        $graphicsReloadNum['EventAlertasNAMinor'] = $this->getEventAlertasNANumMinor();
        $graphicsReloadNum['EventAlertasNAWarning'] = $this->getEventAlertasNANumWarning();
        /* $graphicsReloadNum['EventAlertasNANormal'] = $this->getEventAlertasNANumNormal(); */
        $graphicsReloadNum['EventAlertasNAUnknown'] = $this->getEventAlertasNANumUnknown();

        $graphicsReloadNum['EventEventosDummyPrueba'] = $this->getEventEventosDummyPruebaNum();
        $graphicsReloadNum['EventEventosDummyPruebaCritical'] = $this->getEventEventosDummyPruebaNumCritical();
        $graphicsReloadNum['EventEventosDummyPruebaMajor'] = $this->getEventEventosDummyPruebaNumMajor();
        $graphicsReloadNum['EventEventosDummyPruebaMinor'] = $this->getEventEventosDummyPruebaNumMinor();
        $graphicsReloadNum['EventEventosDummyPruebaWarning'] = $this->getEventEventosDummyPruebaNumWarning();
        /* $graphicsReloadNum['EventEventosDummyPruebaNormal'] = $this->getEventEventosDummyPruebaNumNormal(); */
        $graphicsReloadNum['EventEventosDummyPruebaUnknown'] = $this->getEventEventosDummyPruebaNumUnknown();

        $graphicsReloadNum['EventPoleos'] = $this->getEventPoleosNum();
        $graphicsReloadNum['EventPoleosCritical'] = $this->getEventPoleosNumCritical();
        $graphicsReloadNum['EventPoleosMajor'] = $this->getEventPoleosNumMajor();
        $graphicsReloadNum['EventPoleosMinor'] = $this->getEventPoleosNumMinor();
        $graphicsReloadNum['EventPoleosWarning'] = $this->getEventPoleosNumWarning();
        /* $graphicsReloadNum['EventPoleosNormal'] = $this->getEventPoleosNumNormal(); */
        $graphicsReloadNum['EventPoleosUnknown'] = $this->getEventPoleosNumUnknown();

        return view('events.filters.events_graphics')->with([
            'graphicsReloadNum' => $graphicsReloadNum
        ]);
    }

    public function getDatetime($created, $api_data)
    {
        $event = new \DateTime($created);
        $interval = $event->diff($api_data);
        return ($interval->format('%a')) ? $interval->format('%ad ') . $interval->format('%hh') : $interval->format('%hh ') . $interval->format('%imin');
    }

    public function apiDate()
    {
        $res = new \DateTime(Carbon::now(new \DateTimeZone('America/Mexico_City'))->format('Y-m-d\TH:i:s.vP'));
        return $res;
    }

    /** 
     * Replacing accented characters
     * 
     * @param String  $str
     * @return String  $str_r
     */
    public function replaceStrC($str)
    {
        $search = explode(",", "á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,Ã¡,Ã©,Ã­,Ã³,Ãº,Ã±,ÃÃ¡,ÃÃ©,ÃÃ­,ÃÃ³,ÃÃº,ÃÃ±,Ã“,Ã ,Ã‰,Ã ,Ãš,â€œ,â€ ,Â¿,ü,Ã‘");
        $replace = explode(",", "á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,Ó,Á,É,Í,Ú,\",\",¿,&uuml;,Ñ");
        $str_r = str_replace($search, $replace, $str);
        return $str_r;
    }

    /** 
     * Subtract 5 hrs
     * @param String  $time
     * @return String  $sub_time
     */
    public function subtract5Hrs($time)
    {
        $sub_time = (new \DateTime($time))->sub(new \DateInterval('PT5H'))->format('Y-m-d\TH:i:s.vP');
        return $sub_time;
    }
}
