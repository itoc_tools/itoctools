<?php

namespace App\Http\Controllers\Events;

use App\Events;
use App\UsersBsm;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

class EventsController extends Controller
{

    protected $events;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(\App\Repositories\Events $events)
    {
        $this->middleware('auth');
        $this->events = $events;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        return view('events.events')->with([
            'title' => 'Eventos',
            'name_graphic' => 'General',
            'breadcrumb' => [
                [
                    'title' => 'Eventos'
                ]
            ],
            'user_bsm' => $user_bsm,
            'ajaxDTRoute' => route('reloadEventsAjax')
        ]);
    }

    /**
     * Display a listing of the resource for reload events.
     *
     * @return \Illuminate\Http\Response
     */
    public function reloadEventsAjax()
    {
        $events = Events::where('state', '!=', 'CLOSED')
            ->orderByDesc("time_received")
            ->take(500)
            ->get();
        $api_date = new \DateTime(Carbon::now(new \DateTimeZone('America/Mexico_City'))->format('Y-m-d\TH:i:s.vP'));
        foreach ($events as $row) {
            $row['time_reception'] = $this->subtract5Hrs($row['time_received']);
            $row['event_age'] = $this->getDatetime($row['time_reception'], $api_date);
            if (isset($row["custom_attribute_list"])) {
                $customs = array();
                $rowsCA = array();
                $str = $row["custom_attribute_list"];
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                foreach ($customs as $att_im) {
                    if (trim($att_im[0]) == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im[1],  $match)) {
                            $row["im"] = $match[0];
                        } else if ($att_im[1] == '') {
                            $row["im_empty"] = 'Vacío/Crear IM';
                        } else {
                            $row["im_fake"] = $att_im[1];
                        }
                    }
                    if (trim($att_im[0]) == 'cResponsable') {
                        $row["cResponsable"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'EventoZenoss_Id') {
                        $row["EventoZenoss_Id"] = $att_im[1];
                    }
                    if (trim($att_im[0]) == 'CO') {
                        $row["CO"] = $att_im[1];
                    }
                }
            }
            if (isset($row["severity"])) {
                if ($row["severity"] == 'CRITICAL') {
                    $row["severity"] = 'critical';
                } else if ($row["severity"] == 'MAJOR') {
                    $row["severity"] = 'major';
                } else if ($row["severity"] == 'MINOR') {
                    $row["severity"] = 'minor';
                } else if ($row["severity"] == 'WARNING') {
                    $row["severity"] = 'warning';
                } else if ($row["severity"] == 'NORMAL') {
                    $row["severity"] = 'normal';
                }
            }
        }
        return datatables()->of($events)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  String  $idEvent
     * @return \Illuminate\Http\Response
     */
    public function show($idEvent)
    {
        try {
            $event = $this->events->find($idEvent);
        } catch (ConnectException $connectException) {
            Log::info('ConnectException Message: ' . $connectException->getMessage());

            return redirect()->back()->with('serviceErrorConnection', $connectException->getMessage());
        } catch (RequestException $requestException) {
            Log::info('RequestException Message: ' . $requestException->getMessage());

            return redirect()->back()->with('serviceErrorConnection', $requestException->getMessage());
        } catch (ClientException $clientException) {
            Log::info('ClientException Message: ' . $clientException->getMessage());

            return redirect()->back()->with('serviceErrorConnection', $clientException->getMessage());
        }

        $event = json_decode($event, true);

        $event = $event['event'];

        /* $customEvent = [
            'annotations'           => [],
            'assigned_group'        => isset($event['assigned_group']) ? $event['assigned_group'] : '',
            'assigned_user'         => isset($event['assigned_user']) ? $event['assigned_user'] : '',
            'application'           => isset($event['application']) ? $event['application'] : '',
            'category'              => isset($event['category']) ? stripslashes($event['category']) : '',
            'custom_attributes'     => [],
            'description'           => isset($event['description']) ? stripslashes($event['description']) : '',
            'history'               => $this->getEventHistory($event['id']),
            'id'                    => isset($event['id']) ? $event['id'] : '',
            'priority'              => isset($event['priority']) ? $event['priority'] : '',
            'severity'              => isset($event['severity']) ? $event['severity'] : '',
            'state'                 => isset($event['state']) ? $event['state'] : '',
            'sub_category'          => isset($event['sub_category']) ? stripslashes($event['sub_category']) : '',
            'time_created'          => isset($event['time_created']) ? $event['time_created'] : '',
            'time_received'         => isset($event['time_received']) ? $event['time_received'] : '',
            'time_state_changed'    => isset($event['time_state_changed']) ? $event['time_state_changed'] : '',
            'title'                 => isset($event['title']) ? stripslashes($event['title']) : ''
        ];

        if (isset($event['custom_attribute_list']['custom_attribute'])) {
            $attributes = [];

            foreach ($event['custom_attribute_list']['custom_attribute'] as $key => $attribute) {
                if (gettype($key) === 'string') {
                    $attribute = $event['custom_attribute_list']['custom_attribute'];
                }

                $attributes[] = [
                    'name'  => $attribute['name'] ? stripslashes($attribute['name']) : '',
                    'value' => $attribute['value'] ? stripslashes($attribute['value']) : ''
                ];

                if (gettype($key) === 'string') {
                    break;
                }
            }

            $customEvent['custom_attributes'] = $attributes;
        }

        if (isset($event['annotation_list']['annotation'])) {
            $annotations = [];

            foreach ($event['annotation_list']['annotation'] as $key => $annotation) {
                if (gettype($key) === 'string') {
                    $annotation = $event['annotation_list']['annotation'];
                }

                $annotations[] = [
                    'author'        => isset($annotation['author']) ? $annotation['author'] : '',
                    'id'            => isset($annotation['id']) ? $annotation['id'] : '',
                    'text'          => isset($annotation['text']) ? stripslashes($annotation['text']) : '',
                    'time_created'  => isset($annotation['time_created']) ? $annotation['time_created'] : ''
                ];

                if (gettype($key) === 'string') {
                    break;
                }
            }

            $customEvent['annotations'] = $annotations;
        }

        $bsmUser = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();

        return view('nc.events.details')->with([
            'event'     => $customEvent,
            'bsmUser'   => $bsmUser
        ]); */

        //General section
        $general = array();
        if (isset($event["id"])) {
            $general["id"] = is_string($event["id"]) ? $event["id"] : ' ';
        } else {
            $general["id"] = ' ';
        }
        if (isset($event["severity"])) {
            $general["severity"] = is_string($event["severity"]) ? $event["severity"] : ' ';
        } else {
            $general["severity"] = ' ';
        }
        if (isset($event["state"])) {
            $general["state"] = is_string($event["state"]) ? $event["state"] : ' ';
        } else {
            $general["state"] = ' ';
        }
        if (isset($event["priority"])) {
            $general["priority"] = is_string($event["priority"]) ? $event["priority"] : ' ';
        } else {
            $general["priority"] = ' ';
        }
        if (isset($event["assigned_group"]['name'])) {
            $general["assigned_group"] = is_string($event["assigned_group"]['name']) ? $event["assigned_group"]['name'] : ' ';
        } else {
            $general["assigned_group"] = ' ';
        }
        if (isset($event["assigned_user"]['login_name'])) {
            $general["assigned_user"] = is_string($event["assigned_user"]['login_name']) ? $this->replaceStrC($event["assigned_user"]['login_name']) : ' ';
            if ($general["assigned_user"] == '<unknown>') {
                $general["assigned_user"] = 'unknown';
            }
        } else {
            $general["assigned_user"] = ' ';
        }
        if (isset($event["category"])) {
            $general["category"] = is_string($event["category"]) ? $event["category"] : ' ';
        } else {
            $general["category"] = ' ';
        }
        if (isset($event["title"])) {
            $general["title"] = is_string($event["title"]) ? $this->replaceStrC($event["title"]) : ' ';
        } else {
            $general["title"] = ' ';
        }
        if (isset($event["time_created"])) {
            $general["time_created"] = is_string($event["time_created"]) ? $event["time_created"] : ' ';
        } else {
            $general["time_created"] = ' ';
        }
        if (isset($event["time_created_label"])) {
            $general["time_created_label"] = is_string($event["time_created_label"]) ? $event["time_created_label"] : ' ';
        } else {
            $general["time_created_label"] = ' ';
        }
        if (isset($event["application"])) {
            $general["application"] = is_string($event["application"]) ? $event["application"] : ' ';
        } else {
            $general["application"] = ' ';
        }
        if (isset($event["description"])) {
            $general["description"] = is_string($event["description"]) ? $this->replaceStrC($event["description"]) : ' ';
            if (filter_var($general["description"], FILTER_VALIDATE_URL)) {
                $general["description_is_URL"] = $general["description"];
            }
        } else {
            $general["description"] = ' ';
        }
        //Custom attributes section 
        $custom_attribute = array();
        if (isset($event["custom_attribute_list"]["custom_attribute"])) {
            $ca_list_section = $event["custom_attribute_list"]["custom_attribute"];
            if (count($ca_list_section, COUNT_RECURSIVE) > 5) {
                foreach ($event["custom_attribute_list"]["custom_attribute"] as $att) {
                    $new_att = array();
                    if (isset($att["name"])) {
                        $new_att["name"] = is_string($att["name"]) ? $att["name"] : ' ';
                    } else {
                        $new_att["name"] = ' ';
                    }
                    if (isset($att["value"])) {
                        $new_att["value"] = is_string($att["value"]) ? $this->replaceStrC($att["value"]) : ' ';
                    } else {
                        $new_att["value"] = ' ';
                    }
                    array_push($custom_attribute, $new_att);
                }
            } else {
                $new_att = array();
                if (isset($ca_list_section["name"])) {
                    $new_att["name"] = is_string($ca_list_section["name"]) ? $ca_list_section["name"] : ' ';
                } else {
                    $new_att["name"] = ' ';
                }
                if (isset($ca_list_section["value"])) {
                    $new_att["value"] = is_string($ca_list_section["value"]) ? $this->replaceStrC($ca_list_section["value"]) : ' ';
                } else {
                    $new_att["value"] = ' ';
                }
                array_push($custom_attribute, $new_att);
            }
        }
        //Annotations section
        $att_annotation = array();
        if (isset($event["annotation_list"]["annotation"])) {
            if (count($event["annotation_list"]["annotation"], COUNT_RECURSIVE) > 13) {
                foreach ($event["annotation_list"]["annotation"] as $att_a) {
                    $annotation_list = array();
                    if (isset($att_a["author"])) {
                        $annotation_list["author"] = is_string($att_a["author"]) ? $this->replaceStrC($att_a["author"]) : ' ';
                    } else {
                        $annotation_list["author"] = ' ';
                    }
                    if (isset($att_a["time_created"])) {
                        $annotation_list["time_created"] = is_string($att_a["time_created"]) ? $this->replaceStrC($att_a["time_created"]) : ' ';
                    } else {
                        $annotation_list["time_created"] = ' ';
                    }
                    if (isset($att_a["time_created_label"])) {
                        $annotation_list["time_created_label"] = is_string($att_a["time_created_label"]) ? $this->replaceStrC($att_a["time_created"]) : ' ';
                    } else {
                        $annotation_list["time_created_label"] = ' ';
                    }
                    if (isset($att_a["text"])) {
                        $annotation_list["text"] = is_string($att_a["text"]) ? $this->replaceStrC($att_a["text"]) : ' ';
                    } else {
                        $annotation_list["text"] = ' ';
                    }
                    if (isset($att_a["id"])) {
                        $annotation_list["id"] = is_string($att_a["id"]) ? $this->replaceStrC($att_a["id"]) : ' ';
                    } else {
                        $annotation_list["id"] = ' ';
                    }
                    array_push($att_annotation, $annotation_list);
                }
            } else {
                $annotation_list = array();
                if (isset($event["annotation_list"]["annotation"]["author"])) {
                    $annotation_list["author"] = is_string($event["annotation_list"]["annotation"]["author"]) ?
                        $this->replaceStrC($event["annotation_list"]["annotation"]["author"]) : ' ';
                } else {
                    $annotation_list["author"] = ' ';
                }
                if (isset($event["annotation_list"]["annotation"]["time_created"])) {
                    $annotation_list["time_created"] = is_string($event["annotation_list"]["annotation"]["time_created"]) ?
                        $this->replaceStrC($event["annotation_list"]["annotation"]["time_created"]) : ' ';
                } else {
                    $annotation_list["time_created"] = ' ';
                }
                if (isset($event["annotation_list"]["annotation"]["time_created_label"])) {
                    $annotation_list["time_created_label"] = is_string($event["annotation_list"]["annotation"]["time_created"]) ?
                        $this->replaceStrC($event["annotation_list"]["annotation"]["time_created_label"]) : ' ';
                } else {
                    $annotation_list["time_created_label"] = ' ';
                }
                if (isset($event["annotation_list"]["annotation"]["text"])) {
                    $annotation_list["text"] = is_string($event["annotation_list"]["annotation"]["text"]) ?
                        $this->replaceStrC($event["annotation_list"]["annotation"]["text"]) : ' ';
                } else {
                    $annotation_list["text"] = ' ';
                }
                if (isset($event["annotation_list"]["annotation"]["id"])) {
                    $annotation_list["id"] = is_string($event["annotation_list"]["annotation"]["id"]) ?
                        $this->replaceStrC($event["annotation_list"]["annotation"]["id"]) : ' ';
                } else {
                    $annotation_list["id"] = ' ';
                }
                array_push($att_annotation, $annotation_list);
            }
        }
        //IM Details
        $im_details = array();
        if (isset($event["custom_attribute_list"]["custom_attribute"])) {
            $ca_list = $event["custom_attribute_list"]["custom_attribute"];
            if (count($ca_list, COUNT_RECURSIVE) > 5) {
                foreach ($ca_list as $att_im) {
                    if ($att_im["name"] == 'MS') {
                        if (preg_match('/[IM]{2}[0-9]{7}/', $att_im["value"],  $match)) {
                            $im_details["details"] = $this->getIMDetail($match[0]);
                        }
                    }
                }
            } else {
                if ($ca_list["name"] == 'MS') {
                    if (preg_match('/[IM]{2}[0-9]{7}/', $ca_list["value"],  $match)) {
                        $im_details["details"] = $this->getIMDetail($match[0]);
                    }
                }
            }
        }
        //History
        $history_lines = $this->getEventHistory($general['id']);
        //OMI BSM
        $user_bsm = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();
        return view('events.event_detail')->with([
            'title' => 'Detalle Evento',
            'breadcrumb' => [
                [
                    'title' => 'Detalle Evento'
                ]
            ],
            'general' => $general,
            'custom_attribute' => $custom_attribute,
            'att_annotation' => $att_annotation,
            'im_details' => $im_details,
            'history_lines' => $history_lines,
            'user_bsm' => $user_bsm
        ]);
    }

    public function getEventHistory($id)
    {
        $history = [];

        try {
            $client = new Client();

            $request = $client->request('GET', "http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/{$id}/history_line_list", [
                'auth' => ['OPC.Zenoss', '+0ruge5.X3m137-']
            ]);

            $xml = simplexml_load_string($request->getBody()->getContents());
            $json  = json_encode($xml);
            $list = json_decode($json, true);
            $list = $list['history_line'] ?? [];

            foreach ($list as $key => $element) {
                if (gettype($key) === 'string') {
                    $element = $list;
                }

                foreach ($element['changed_properties'] as $property) {
                    $p = [
                        'property'      => $property['property_name'],
                        'modified_by'   => $element['modified_by'],
                        'time_changed'  => $element['time_changed']
                    ];

                    switch ($property['property_name']) {
                        case 'assigned_group':
                            $p['current_value'] = $property['current_group_name'];
                            $p['previous_value'] = $property['previous_group_name'];
                            break;
                        case 'assigned_user':
                            $p['current_value'] = $property['current_user_name'];
                            $p['previous_value'] = $property['previous_user_name'];
                            break;
                        case 'custom_attribute':
                            $p['property'] = $property['key'];
                        default:
                            $p['current_value'] = !empty($property['current_value']) ? $property['current_value'] : '';
                            $p['previous_value'] = !empty($property['previous_value']) ? $property['previous_value'] : '';
                            break;
                    }

                    $history[] = $p;
                }

                if (gettype($key) === 'string') {
                    break;
                }
            }
        } catch (ConnectException $connectException) {
            Log::info('ConnectException Message: ' . $connectException->getMessage());

            $history['ERROR'] = $connectException->getMessage();
        } catch (RequestException $requestException) {
            Log::info('RequestException Message: ' . $requestException->getMessage());

            $history['ERROR'] = $requestException->getMessage();
        } catch (ClientException $clientException) {
            Log::info('ClientException Mess$clientExceptionage: ' . $clientException->getMessage());

            $history['ERROR'] = $clientException->getMessage();
        } finally {
            return $history;
        }
    }

    /**
     * Get IM Detail.
     *
     * @param  String  $im
     * @return Array $im_detail_array
     */
    public function getIMDetail($im)
    {
        $client = new Client([
            'verify' => false
        ]);
        $res = $client->request('POST', 'https://172.20.45.172/itoc/servicemanager/consulta', [
            'auth' => ['service_manager', 'ServiceManager_2021*'],
            'form_params' => [
                'IM-SD' => $im,
                'tipo' => 'IM'
            ]
        ]);
        $im_detail = $res->getBody();
        $im_detail = $im_detail->getContents();
        $im_detail = html_entity_decode($im_detail);
        $im_detail = json_decode($im_detail, true);
        $im_detail_array = array();
        if (isset($im_detail["Detalle"]) && isset($im_detail["Detalle"]["ConsultaIMResult"]["model"]["instance"])) {
            $im_detail = $im_detail['Detalle'];
            $detail = $im_detail["ConsultaIMResult"]["model"]["instance"];
            if (isset($detail["IncidentID"])) {
                $im_detail_array["IncidentID"] = is_string($detail["IncidentID"]) ? $detail["IncidentID"] : ' ';
            } else {
                $im_detail_array["IncidentID"] = ' ';
            }
            if (isset($detail["AssignmentGroup"])) {
                $im_detail_array["AssignmentGroup"] = is_string($detail["AssignmentGroup"]) ? $detail["AssignmentGroup"] : ' ';
            } else {
                $im_detail_array["AssignmentGroup"] = ' ';
            }
            if (isset($detail["AffectedCI"])) {
                $im_detail_array["AffectedCI"] = is_string($detail["AffectedCI"]) ? $detail["AffectedCI"] : ' ';
            } else {
                $im_detail_array["AffectedCI"] = ' ';
            }
            if (isset($detail["Assignee"])) {
                $im_detail_array["Assignee"] = is_string($detail["Assignee"]) ? $detail["Assignee"] : ' ';
            } else {
                $im_detail_array["Assignee"] = ' ';
            }
            if (isset($detail["Contact"])) {
                $im_detail_array["Contact"] = is_string($detail["Contact"]) ? $detail["Contact"] : ' ';
            } else {
                $im_detail_array["Contact"] = ' ';
            }
            if (isset($detail["AlertStatus"])) {
                $im_detail_array["AlertStatus"] = is_string($detail["AlertStatus"]) ? $detail["AlertStatus"] : ' ';
            } else {
                $im_detail_array["AlertStatus"] = ' ';
            }
            if (isset($detail["Status"])) {
                $im_detail_array["Status"] = is_string($detail["Status"]) ? $detail["Status"] : ' ';
            } else {
                $im_detail_array["Status"] = ' ';
            }
            if (isset($detail["JournalUpdates"])) {
                $im_detail_array["JournalUpdates"] = implode("\r\n", $detail["JournalUpdates"]);
            } else {
                $im_detail_array["JournalUpdates"] = ' ';
            }
        } else {
            $im_detail_array["IncidentID"] = 'No se encontró información del IM';
            $im_detail_array["AssignmentGroup"] = 'No se encontró información del IM';
            $im_detail_array["AffectedCI"] = 'No se encontró información del IM';
            $im_detail_array["Assignee"] = 'No se encontró información del IM';
            $im_detail_array["Contact"] = 'No se encontró información del IM';
            $im_detail_array["AlertStatus"] = 'No se encontró información del IM';
            $im_detail_array["Status"] = 'No se encontró información del IM';
            $im_detail_array["JournalUpdates"] = 'No se encontró información del IM';
        }
        return  $im_detail_array;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param String  $idEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, String $idEvent)
    {
        Log::info('-----------------------------METHOD: update (event)------------------------');
        Log::info('Datos de entrada: ' . print_r($request->all(), true));
        Log::info('Id del evento: ' . $idEvent);
        $xml = new \DomDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xml_enviar = $xml->createElement('event');
        $xml_enviar->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
        if ($request->input('state') != null) {
            Log::info('***********Set state: ' . $request->input('state'));
            $xml_enviar->appendChild($xml->createElement('state', $request->input('state')));
        }
        if ($request->input('severity') != null) {
            Log::info('***********Set severity: ' . $request->input('severity'));
            $xml_enviar->appendChild($xml->createElement('severity', $request->input('severity')));
        }
        if ($request->input('priority') != null) {
            Log::info('***********Set priority: ' . $request->input('priority'));
            $xml_enviar->appendChild($xml->createElement('priority', $request->input('priority')));
        }
        if ($request->input('assigned_user') != null) {
            if ($request->input('assigned_user') == 'unknown' || $request->input('state_initial') != $request->input('state')) {
                $assigned_user_ = $request->input('assigned_user_logged');
            } else {
                $assigned_user_ = $request->input('assigned_user') != $request->input('assigned_user_logged') ? $request->input('assigned_user') : $request->input('assigned_user_logged');
            }
            Log::info('***********Set assigned_user_updated: ' . $assigned_user_);
            $xml_assigned_group = $xml->createElement('assigned_group');
            $xml_assigned_group->appendChild($xml->createElement('id', '-1'));
            $xml_enviar->appendChild($xml_assigned_group);
            $assigned_user_details = $this->userDetails($assigned_user_);
            $xml_assigned_user = $xml->createElement('assigned_user');
            $xml_assigned_user->appendChild($xml->createElement('id', $assigned_user_details["id"]));
            $xml_assigned_user->appendChild($xml->createElement('login_name', $assigned_user_details["login_name"]));
            $xml_assigned_user->appendChild($xml->createElement('user_name', $assigned_user_details["user_name"]));
            $xml_enviar->appendChild($xml_assigned_user);
        }
        $xml->appendChild($xml_enviar);
        $xml_body = $xml->saveXML();
        Log::info($xml_body);
        try {
            $client = new Client();
            $res = $client->request('PUT', "http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/{$idEvent}/?alt=json", [
                'auth' => ['OPC.Zenoss', '+0ruge5.X3m137-'],
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8'
                ],
                'body'   => $xml_body
            ]);
            $event_updated = $res->getBody();
            $event_updated = $event_updated->getContents();
            $event_updated = html_entity_decode($event_updated);
            $event_updated = json_decode($event_updated, true);
            Log::info('Resultado Exitoso: ');
            Log::info(json_encode($event_updated));
        } catch (ConnectException $e) {
            Log::info('ConnectException Message: ' . $e->getMessage());
            return redirect()->back()->with('serviceErrorConnection', $e->getMessage());
        } catch (RequestException $er) {
            Log::info('RequestException Message: ' . $er->getMessage());
            return redirect()->back()->with('serviceErrorData', $er->getMessage());
        } catch (ClientException $err) {
            Log::info('ClientException Message: ' . $err->getMessage());
            return redirect()->back()->with('serviceErrorData', $err->getMessage());
        }
        return redirect()->route('events.show', $idEvent);
    }

    public function singleUserAssign(String $assigned_user, String $password, String $idEvent)
    {
        Log::info('-----------------------------METHOD: singleUserAssign------------------------');
        Log::info('***********Id Evento: ' . $idEvent);
        Log::info('***********assigned_user: ' . $assigned_user);
        Log::info('***********pass: ' . $password);
        $xml = new \DomDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xml_enviar = $xml->createElement('event');
        $xml_enviar->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
        $xml_assigned_group = $xml->createElement('assigned_group');
        $xml_assigned_group->appendChild($xml->createElement('id', '-1'));
        $xml_enviar->appendChild($xml_assigned_group);
        $assigned_user_details = $this->userDetails($assigned_user);
        $xml_assigned_user = $xml->createElement('assigned_user');
        $xml_assigned_user->appendChild($xml->createElement('id', $assigned_user_details["id"]));
        $xml_assigned_user->appendChild($xml->createElement('login_name', $assigned_user_details["login_name"]));
        $xml_assigned_user->appendChild($xml->createElement('user_name', $assigned_user_details["user_name"]));
        $xml_enviar->appendChild($xml_assigned_user);
        $xml->appendChild($xml_enviar);
        $xml_body = $xml->saveXML();
        Log::info($xml_body);
        try {
            $client = new Client();
            $res = $client->request('PUT', "http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/{$idEvent}/?alt=json", [
                'auth' => [$assigned_user, $password],
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8'
                ],
                'body'   => $xml_body
            ]);
            $event_updated = $res->getBody();
            $event_updated = $event_updated->getContents();
            $event_updated = html_entity_decode($event_updated);
            $event_updated = json_decode($event_updated, true);
            Log::info('Resultado Exitoso de asignacion de usuario: ');
            Log::info(json_encode($event_updated));
        } catch (ConnectException $e) {
            Log::info('ConnectException Message: ' . $e->getMessage());
            return  $e->getMessage();
        } catch (RequestException $er) {
            Log::info('RequestException Message: ' . $er->getMessage());
            return  $er->getMessage();
        } catch (ClientException $err) {
            Log::info('ClientException Message: ' . $err->getMessage());
            return  $err->getMessage();
        }
        return 'Successful user assignment';
    }

    /**
     * Massive Assign to Me.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function massiveAssign(Request $request)
    {
        Log::info('----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------');
        Log::info('-----------------------------METHOD: massiveAssign------------------------');
        $idsAssignArray = explode("/", $request->input('idsAssign'));
        $user = $request->input('User_OMI_Assign');
        $password = $request->input('Password_OMI_Assign');
        $assigned_user = $request->input('assigned_user');
        $num = 0;
        Log::info('***********user omi: ' . $user);
        Log::info('***********password omi : ' . $password);
        Log::info('***********assigned_user : ' . $assigned_user);
        Log::info('***********ids eventos: ' . $request->input('idsAssign'));
        foreach ($idsAssignArray as $idEvent) {
            $idEvent = rtrim($idEvent);
            $num++;
            if (isset($idEvent) && strlen($idEvent) == 36) {
                Log::info('***********');
                Log::info('***********Número de evento: ' . $num);
                Log::info('***********Id Evento: ' . $idEvent);
                $xml = new \DomDocument('1.0', 'UTF-8');
                $xml->formatOutput = true;
                $xml_enviar = $xml->createElement('event');
                $xml_enviar->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
                $xml_assigned_group = $xml->createElement('assigned_group');
                $xml_assigned_group->appendChild($xml->createElement('id', '-1'));
                $xml_enviar->appendChild($xml_assigned_group);
                $assigned_user_details = $this->userDetails($assigned_user);
                $xml_assigned_user = $xml->createElement('assigned_user');
                $xml_assigned_user->appendChild($xml->createElement('id', $assigned_user_details["id"]));
                $xml_assigned_user->appendChild($xml->createElement('login_name', $assigned_user_details["login_name"]));
                $xml_assigned_user->appendChild($xml->createElement('user_name', $assigned_user_details["user_name"]));
                $xml_enviar->appendChild($xml_assigned_user);
                $xml->appendChild($xml_enviar);
                $xml_body = $xml->saveXML();
                Log::info($xml_body);
                try {
                    $client = new Client();
                    $res = $client->request('PUT', "http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/{$idEvent}/?alt=json", [
                        'auth' => [$user, $password],
                        'headers' => [
                            'Content-Type' => 'text/xml; charset=UTF8'
                        ],
                        'body'   => $xml_body
                    ]);
                    $event_updated = $res->getBody();
                    $event_updated = $event_updated->getContents();
                    $event_updated = html_entity_decode($event_updated);
                    $event_updated = json_decode($event_updated, true);
                    //$event_updated = $event_updated['event'];
                    Log::info('Resultado Exitoso: ');
                    Log::info(json_encode($event_updated));
                } catch (ConnectException $e) {
                    Log::info('ConnectException Message: ' . $e->getMessage());
                    return redirect()->back()->with('serviceErrorConnection', 'Error de conexión, intentar nuevamente.');
                } catch (RequestException $er) {
                    Log::info('RequestException Message: ' . $er->getMessage());
                    return redirect()->back()->with('serviceErrorData', 'Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.');
                } catch (ClientException $err) {
                    Log::info('ClientException Message: ' . $err->getMessage());
                    return redirect()->back()->with('serviceErrorData', 'Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.');
                }
            }
        }
        return redirect()->back();
    }

    /**
     * Gets details from user selected.
     *
     * @param String  $assigned_user
     * @return Array  $assigned_user_details
     */
    public function userDetails($assigned_user)
    {
        $assigned_user_details = array();
        if ($assigned_user == 'angela.galvan') {
            $assigned_user_details["id"] = '172';
            $assigned_user_details["login_name"] = 'angela.galvan';
            $assigned_user_details["user_name"] = 'Angela Galvan';
        } else if ($assigned_user == 'juan.cruz') {
            $assigned_user_details["id"] = '148';
            $assigned_user_details["login_name"] = 'juan.cruz';
            $assigned_user_details["user_name"] = 'Juan Cruz';
        } else if ($assigned_user == 'lorena.cruz') {
            $assigned_user_details["id"] = '71';
            $assigned_user_details["login_name"] = 'lorena.cruz';
            $assigned_user_details["user_name"] = 'Lorena Cruz';
        } else if ($assigned_user == 'mario.gutierrez') {
            $assigned_user_details["id"] = '207';
            $assigned_user_details["login_name"] = 'mario.gutierrez';
            $assigned_user_details["user_name"] = 'Mario Gutierrez';
        } else if ($assigned_user == 'alan.torres') {
            $assigned_user_details["id"] = '189';
            $assigned_user_details["login_name"] = 'alan.torres';
            $assigned_user_details["user_name"] = 'Alan Torres';
        } else if ($assigned_user == 'OPC.Zenoss') {
            $assigned_user_details["id"] = '130';
            $assigned_user_details["login_name"] = 'OPC.Zenoss';
            $assigned_user_details["user_name"] = 'OPC Zenoss';
        } else if ($assigned_user == 'arkadi.padilla') {
            $assigned_user_details["id"] = '199';
            $assigned_user_details["login_name"] = 'arkadi.padilla';
            $assigned_user_details["user_name"] = 'Arkadi Padilla';
        } else if ($assigned_user == 'artemio.urbina') {
            $assigned_user_details["id"] = '203';
            $assigned_user_details["login_name"] = 'artemio.urbina';
            $assigned_user_details["user_name"] = 'artemio.urbina';
        } else if ($assigned_user == 'enrique.quezada') {
            $assigned_user_details["id"] = '206';
            $assigned_user_details["login_name"] = 'enrique.quezada';
            $assigned_user_details["user_name"] = 'Enrique Quezada';
        } else if ($assigned_user == 'operaciones.itoc') {
            $assigned_user_details["id"] = '105';
            $assigned_user_details["login_name"] = 'operaciones.itoc';
            $assigned_user_details["user_name"] = 'Operaciones ITOC';
        } else if ($assigned_user == 'juan.arriaga') {
            $assigned_user_details["id"] = '152';
            $assigned_user_details["login_name"] = 'juan.arriaga';
            $assigned_user_details["user_name"] = 'Juan Carlos Arriaga';
        } else if ($assigned_user == 'erik.cerda') {
            $assigned_user_details["id"] = '200';
            $assigned_user_details["login_name"] = 'erik.cerda';
            $assigned_user_details["user_name"] = 'Erik Cerda';
        } else if ($assigned_user == 'andres.sanchez') {
            $assigned_user_details["id"] = '57';
            $assigned_user_details["login_name"] = 'andres.sanchez';
            $assigned_user_details["user_name"] = 'Andres Sanchez';
        } else if ($assigned_user == 'capacitacion.itoc3') {
            $assigned_user_details["id"] = '150';
            $assigned_user_details["login_name"] = 'capacitacion.itoc3';
            $assigned_user_details["user_name"] = 'Capacitación ITOC 3';
        } else if ($assigned_user == 'capacitacion.itoc2') {
            $assigned_user_details["id"] = '149';
            $assigned_user_details["login_name"] = 'capacitacion.itoc2';
            $assigned_user_details["user_name"] = 'Capacitación ITOC 2';
        } else if ($assigned_user == 'capacitacion.itoc') {
            $assigned_user_details["id"] = '147';
            $assigned_user_details["login_name"] = 'capacitacion.itoc';
            $assigned_user_details["user_name"] = 'Capacitación ITOC';
        } else if ($assigned_user == 'osvaldo.martinez') {
            $assigned_user_details["id"] = '204';
            $assigned_user_details["login_name"] = 'osvaldo.martinez';
            $assigned_user_details["user_name"] = 'osvaldo.martinez';
        } else if ($assigned_user == 'jose.medina') {
            $assigned_user_details["id"] = '42';
            $assigned_user_details["login_name"] = 'jose.medina';
            $assigned_user_details["user_name"] = 'jose.medina';
        } else if ($assigned_user == 'cesar.gutierrez') {
            $assigned_user_details["id"] = '197';
            $assigned_user_details["login_name"] = 'cesar.gutierrez';
            $assigned_user_details["user_name"] = 'cesar.gutierrez';
        } else if ($assigned_user == 'pablo.gutierrez') {
            $assigned_user_details["id"] = '168';
            $assigned_user_details["login_name"] = 'pablo.gutierrez';
            $assigned_user_details["user_name"] = 'Pablo Gutierrez';
        } else if ($assigned_user == 'luis.manzo') {
            $assigned_user_details["id"] = '140';
            $assigned_user_details["login_name"] = 'luis.manzo';
            $assigned_user_details["user_name"] = 'Luis Manzo';
        } else if ($assigned_user == 'israel.gonzalez') {
            $assigned_user_details["id"] = '135';
            $assigned_user_details["login_name"] = 'israel.gonzalez';
            $assigned_user_details["user_name"] = 'Israel González';
        } else if ($assigned_user == 'Ignacio.rodriguez') {
            $assigned_user_details["id"] = '202';
            $assigned_user_details["login_name"] = 'Ignacio.rodriguez';
            $assigned_user_details["user_name"] = 'Ignacio Rodríguez';
        } else if ($assigned_user == 'david.martinez') {
            $assigned_user_details["id"] = '193';
            $assigned_user_details["login_name"] = 'david.martinez';
            $assigned_user_details["user_name"] = 'David Martinez';
        } else if ($assigned_user == 'cgonzalez') {
            $assigned_user_details["id"] = '27';
            $assigned_user_details["login_name"] = 'cgonzalez';
            $assigned_user_details["user_name"] = 'Christian Gonzalez';
        } else if ($assigned_user == 'angelica.hernandez') {
            $assigned_user_details["id"] = '198';
            $assigned_user_details["login_name"] = 'angelica.hernandez';
            $assigned_user_details["user_name"] = 'Angelica Hernandez';
        } else if ($assigned_user == 'andrea.valencia') {
            $assigned_user_details["id"] = '164';
            $assigned_user_details["login_name"] = 'andrea.valencia';
            $assigned_user_details["user_name"] = 'Andrea Valencia';
        } else if ($assigned_user == 'alexis.torres') {
            $assigned_user_details["id"] = '189';
            $assigned_user_details["login_name"] = 'alexis.torres';
            $assigned_user_details["user_name"] = 'Alexis Torres';
        } else if ($assigned_user == 'darwin.diaz') {
            $assigned_user_details["id"] = '154';
            $assigned_user_details["login_name"] = 'darwin.diaz';
            $assigned_user_details["user_name"] = 'Darwin Díaz';
        } else if ($assigned_user == 'maria.meneses') {
            $assigned_user_details["id"] = '153';
            $assigned_user_details["login_name"] = 'maria.meneses';
            $assigned_user_details["user_name"] = 'Maria Meneses';
        } else if ($assigned_user == 'unknown') {
            $assigned_user_details["id"] = '-1';
            $assigned_user_details["login_name"] = '<unknown>';
            $assigned_user_details["user_name"] = '<unknown>';
        } else if ($assigned_user == 'hector.rodriguez') {
            $assigned_user_details["id"] = '155';
            $assigned_user_details["login_name"] = 'hector.rodriguez';
            $assigned_user_details["user_name"] = 'hector.rodriguez';
        } else if ($assigned_user == 'juan.palma') {
            $assigned_user_details["id"] = '151';
            $assigned_user_details["login_name"] = 'juan.palma';
            $assigned_user_details["user_name"] = 'Juan Palma';
        } else if ($assigned_user == 'Brayan Sanchez') {
            $assigned_user_details["id"] = '212';
            $assigned_user_details["login_name"] = 'brayan.sanchez';
            $assigned_user_details["user_name"] = 'brayan.sanchez';
        } else if ($assigned_user == 'Ramiro Estrada') {
            $assigned_user_details["id"] = '171';
            $assigned_user_details["login_name"] = 'ramiro.estrada';
            $assigned_user_details["user_name"] = 'Ramiro Estrada';
        } else if ($assigned_user == 'Juan Benitez') {
            $assigned_user_details["id"] = '209';
            $assigned_user_details["login_name"] = 'juan.benitez';
            $assigned_user_details["user_name"] = 'Juan Benitez';
        } else {
            $assigned_user_details["id"] = '-1';
            $assigned_user_details["login_name"] = '<unknown>';
            $assigned_user_details["user_name"] = '<unknown>';
        }
        return $assigned_user_details;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param String  $idEvent
     * @return \Illuminate\Http\Response
     */
    public function edit($idEvent)
    {
        return view('events.event_createIM')->with([
            'title' => 'Crear IM',
            'breadcrumb' => [
                [
                    'title' => 'Crear IM'
                ]
            ],
            'idEvent' => $idEvent
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $param = array();
        $param["IM"] = $request->all();
        $param["tipo"] = "IM";
        $param["Aplicacion"] = "OMI";
        $param["EventoId"] = $request->input('idEvent');
        $client = new Client([
            'verify' => false
        ]);
        $res = $client->request('POST', 'https://172.20.45.172/itoc/servicemanager/integracion/correlacionador', [
            'auth' => ['service_manager', 'ServiceManager_2021*'],
            'form_params' => $param
        ]);
        $im_created = $res->getBody();
        $im_created = $im_created->getContents();
        if (is_string($im_created) && preg_match('/[IM]{2}[0-9]{7}/', $im_created,  $match)) {
            $im_created = $match[0];
            $request->merge([
                'name' => 'MS',
                'value' => $im_created,
                'User_OMI_CA' => 'OPC.Zenoss',
                'Password_OMI_CA' => '+0ruge5.X3m137-',
                'from_createIM' => true
            ]);
            return $this->manageCustomAttributes($request, $request->input('idEvent'), 'POST');
        } else {
            $im_created = html_entity_decode($im_created);
            $im_created = json_decode($im_created, true);
            return view('events.event_createIM')->with([
                'title' => 'Crear IM',
                'breadcrumb' => [
                    [
                        'title' => 'Crear IM'
                    ]
                ],
                'idEvent' => $request->input('idEvent'),
                'is_error' => json_encode($im_created, true)
            ]);
        }
    }

    /**
     * Masive Custom Attribute.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function masiveCustomAttributes(Request $request)
    {
        Log::info('-----------------------------METHOD: masiveCustomAttributes------------------------');
        $idsArray = explode("/", $request->input('ids'));
        $name = $request->input('name-CAM');
        $value = $request->input('value-CAM');
        $user = $request->input('User_OMI_CAM');
        $password = $request->input('Password_OMI_CAM');
        $url_att = '';
        $num = 0;
        Log::info('***********user omi: ' . $user);
        Log::info('***********password omi: ' . $password);
        Log::info('***********Custom Attribute: ' . $name);
        Log::info('***********Value: ' . $value);
        Log::info('***********ids eventos' . $request->input('ids'));
        foreach ($idsArray as $idEvent) {
            $idEvent = rtrim($idEvent);
            $name = rtrim($name);
            $value = rtrim($value);
            $num++;
            if (isset($idEvent) && strlen($idEvent) == 36) {
                Log::info('***********Número de evento: ' . $num);
                Log::info('***********Id Evento: ' . $idEvent);
                $xml = new \DomDocument('1.0', 'UTF-8');
                $xml->formatOutput = true;
                $xml_enviar = $xml->createElement('custom_attribute');
                $xml_enviar->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
                $xml_enviar->setAttribute('self', 'http://bsmtelmexit.triara.mexico/opr-web/rest/9.10/event_list/' . $idEvent .
                    '/custom_attribute_list/' . $name);
                $xml_enviar->setAttribute('type', 'urn:x-hp:2009:software:data_model:opr:type:event:custom_attribute');
                $xml_enviar->setAttribute('version', '1.0');
                $xml_enviar->appendChild($xml->createElement('name', $name));
                $xml_enviar->appendChild($xml->createElement('value', $value));
                $xml->appendChild($xml_enviar);
                $xml_body = $xml->saveXML();
                Log::info($xml_body);
                try {
                    $client = new Client();
                    $res = $client->request('POST', 'http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/' . $idEvent .
                        '/custom_attribute_list/' . $url_att . '?alt=json', [
                        'auth' => [$user, $password],
                        'headers' => [
                            'Content-Type' => 'text/xml; charset=UTF8'
                        ],
                        'body'   => $xml_body
                    ]);
                    $ca_created = $res->getBody();
                    $ca_created = $ca_created->getContents();
                    $ca_created = html_entity_decode($ca_created);
                    $ca_created = json_decode($ca_created, true);
                    Log::info('Resultado Exitoso: ');
                    Log::info(json_encode($ca_created));
                } catch (ConnectException $e) {
                    Log::info('ConnectException Message: ' . $e->getMessage());
                    return redirect()->back()->with('serviceErrorConnection', 'Error de conexión, intentar nuevamente.');
                } catch (RequestException $er) {
                    Log::info('RequestException Message: ' . $er->getMessage());
                    return redirect()->back()->with('serviceErrorData', 'Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.');
                } catch (ClientException $err) {
                    Log::info('ClientException Message: ' . $err->getMessage());
                    return redirect()->back()->with('serviceErrorData', 'Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.');
                }
            }
        }
        return  redirect()->back();
    }

    /**
     * Manage Custom Attribute.
     *
     * @param \Illuminate\Http\Request $request
     * @param  String  $idEvent
     * @param  String  $method
     * @return \Illuminate\Http\Response
     */
    public function manageCustomAttributes(Request $request, String $idEvent, String $method)
    {
        Log::info('-----------------------------METHOD: manageCustomAttributes------------------------');
        if ($method == 'PUT') {
            $name = $request->input('name-edit');
            $value = $request->input('value-edit');
            $url_att = $name;
            $user = $request->input('User_OMI_Edit_CA');
            $password = $request->input('Password_OMI_Edit_CA');
        } else if ($method == 'DELETE') {
            $name = $request->input('name-delete');
            $value = $request->input('value-delete');
            $url_att = $name;
            $user = $request->input('User_OMI_Delete_CA');
            $password = $request->input('Password_OMI_Delete_CA');
        } else {
            $name = $request->input('name');
            $value = $request->input('value');
            $url_att = '';
            $user = $request->input('User_OMI_CA');
            $password = $request->input('Password_OMI_CA');
        }
        Log::info('***********Method: ' . $method);
        Log::info('***********name: ' . $name);
        Log::info('***********Value : ' . $value);
        Log::info('***********att : ' . $url_att);
        Log::info('***********user omi: ' . $user);
        Log::info('***********password omi: ' . $password);
        Log::info('***********Id Evento: ' . $idEvent);
        $from_createIM = $request->input('from_createIM') ? true : false;
        $xml = new \DomDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xml_enviar = $xml->createElement('custom_attribute');
        $xml_enviar->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
        $xml_enviar->setAttribute('self', 'http://bsmtelmexit.triara.mexico/opr-web/rest/9.10/event_list/' . $idEvent .
            '/custom_attribute_list/' . $name);
        $xml_enviar->setAttribute('type', 'urn:x-hp:2009:software:data_model:opr:type:event:custom_attribute');
        $xml_enviar->setAttribute('version', '1.0');
        $xml_enviar->appendChild($xml->createElement('name', $name));
        $xml_enviar->appendChild($xml->createElement('value', $value));
        $xml->appendChild($xml_enviar);
        $xml_body = $xml->saveXML();
        Log::info($xml_body);
        try {
            $client = new Client();
            $res = $client->request($method, 'http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/' . $idEvent .
                '/custom_attribute_list/' . $url_att . '?alt=json', [
                'auth' => [$user, $password],
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8'
                ],
                'body'   => $xml_body
            ]);
            $ca_created = $res->getBody();
            $ca_created = $ca_created->getContents();
            $ca_created = html_entity_decode($ca_created);
            $ca_created = json_decode($ca_created, true);
            Log::info('Resultado Exitoso: ');
            Log::info(json_encode($ca_created));
        } catch (ConnectException $e) {
            Log::info('Error Message: ' . $e->getMessage());
            if ($from_createIM) {
                return view('events.event_createIM')->with([
                    'title' => 'Crear IM',
                    'breadcrumb' => [
                        [
                            'title' => 'Crear IM'
                        ]
                    ],
                    'idEvent' => $idEvent,
                    'is_error' => 'Error de conexión, intentar nuevamente.'
                ]);
            } else {
                return redirect()->back()->with('serviceErrorConnection', $e->getMessage());
            }
        } catch (RequestException $er) {
            Log::info('Error Message: ' . $er->getMessage());
            if ($from_createIM) {
                return view('events.event_createIM')->with([
                    'title' => 'Crear IM',
                    'breadcrumb' => [
                        [
                            'title' => 'Crear IM'
                        ]
                    ],
                    'idEvent' => $idEvent,
                    'is_error' => 'Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.'
                ]);
            } else {
                return redirect()->back()->with('serviceErrorData', $er->getMessage());
            }
        } catch (ClientException $err) {
            Log::info('Error Message: ' . $err->getMessage());
            if ($from_createIM) {
                return view('events.event_createIM')->with([
                    'title' => 'Crear IM',
                    'breadcrumb' => [
                        [
                            'title' => 'Crear IM'
                        ]
                    ],
                    'idEvent' => $idEvent,
                    'is_error' => 'Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.'
                ]);
            } else {
                return redirect()->back()->with('serviceErrorData', $err->getMessage());
            }
        }
        return redirect()->route('events.show', $idEvent);
    }

    /**
     * Masive Annotation
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function masiveAnnotation(Request $request)
    {
        Log::info('-----------------------------METHOD: masiveAnnotation------------------------');
        $idsAMArray = explode("/", $request->input('idsAM'));
        $name = $request->input('User_OMI_AM');
        $password = $request->input('Password_OMI_AM');
        $value = $request->input('text_AM');
        $num = 0;
        Log::info('***********user omi: ' . $name);
        Log::info('***********password omi : ' . $password);
        Log::info('***********value : ' . $value);
        Log::info('***********ids eventos' . $request->input('idsAM'));
        foreach ($idsAMArray as $idEvent) {
            $idEvent = rtrim($idEvent);
            $num++;
            if (isset($idEvent) && strlen($idEvent) == 36) {
                Log::info('***********Número de evento: ' . $num);
                Log::info('***********Id Evento: ' . $idEvent);
                $name = rtrim($name);
                $value = rtrim($value);
                $xml = new \DomDocument('1.0', 'UTF-8');
                $xml->formatOutput = true;
                $xml_enviar = $xml->createElement('annotation');
                $xml_enviar->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
                $xml_enviar->appendChild($xml->createElement('text', $value));
                $xml_enviar->appendChild($xml->createElement('author', $name));
                $xml->appendChild($xml_enviar);
                $xml_body = $xml->saveXML();
                Log::info($xml_body);
                try {
                    $client = new Client();
                    $res = $client->request('POST', "http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/{$idEvent}/annotation_list?alt=json", [
                        'auth' => [$name, $password],
                        'headers' => [
                            'Content-Type' => 'text/xml; charset=UTF8'
                        ],
                        'body'   => $xml_body
                    ]);
                    $ca_created = $res->getBody();
                    $ca_created = $ca_created->getContents();
                    $ca_created = html_entity_decode($ca_created);
                    $ca_created = json_decode($ca_created, true);
                    Log::info('Resultado Exitoso: ');
                    Log::info(json_encode($ca_created));
                } catch (ConnectException $e) {
                    Log::info('ConnectException Message: ' . $e->getMessage());
                    return redirect()->back()->with('serviceErrorConnection', 'Error de conexión, intentar nuevamente.');
                } catch (RequestException $er) {
                    Log::info('RequestException Message: ' . $er->getMessage());
                    return redirect()->back()->with('serviceErrorConnection', 'Error de conexión, Inténtelo más tarde.');
                } catch (ClientException $err) {
                    Log::info('ClientException Message: ' . $err->getMessage());
                    return redirect()->back()->with('serviceErrorData', 'Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.');
                }
            }
        }
        return  redirect()->back();
    }

    /**
     * Manage Annotation.
     *
     * @param \Illuminate\Http\Request $request
     * @param  String  $idEvent
     * @param  String  $method
     * @return \Illuminate\Http\Response
     */
    public function createAnnotation(Request $request, String $idEvent, String $method)
    {
        Log::info('-----------------------------METHOD: createAnnotation------------------------');
        if ($method == 'PUT') {
            $name = $request->input('User_OMI_Edit');
            $password = $request->input('Password_OMI_Edit');
            $id_annotation = $request->input('id_annotation_edit');
            $value = $request->input('text-edit');
        } else if ($method == 'DELETE') {
            $name = $request->input('User_OMI_Delete');
            $password = $request->input('Password_OMI_Delete');
            $id_annotation = $request->input('id_annotation_delte');
            $value = $request->input('text-delete');
        } else {
            $name = $request->input('User_OMI');
            $password = $request->input('Password_OMI');
            $value = $request->input('text');
            $id_annotation = '';
        }
        Log::info('***********Method: ' . $method);
        Log::info('***********user omi: ' . $name);
        Log::info('***********password omi : ' . $password);
        Log::info('***********value : ' . $value);
        Log::info('***********id_annotation omi: ' . $id_annotation);
        Log::info('***********Id Evento: ' . $idEvent);
        $xml = new \DomDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xml_enviar = $xml->createElement('annotation');
        $xml_enviar->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
        $xml_enviar->appendChild($xml->createElement('text', $value));
        $xml_enviar->appendChild($xml->createElement('author', $name));
        $xml->appendChild($xml_enviar);
        $xml_body = $xml->saveXML();
        Log::info($xml_body);
        try {
            $client = new Client();
            $res = $client->request($method, "http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/{$idEvent}/annotation_list/{$id_annotation}?alt=json", [
                'auth' => [$name, $password],
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8'
                ],
                'body'   => $xml_body
            ]);
            $ca_created = $res->getBody();
            $ca_created = $ca_created->getContents();
            $ca_created = html_entity_decode($ca_created);
            $ca_created = json_decode($ca_created, true);
            //$ca_created = $ca_created['annotation'];
        } catch (ConnectException $e) {
            Log::info('ConnectException Message: ' . $e->getMessage());
            return redirect()->back()->with('serviceErrorConnection', 'Error de conexión, intentar nuevamente.');
        } catch (RequestException $er) {
            Log::info('RequestException Message: ' . $er->getMessage());
            return redirect()->back()->with('serviceErrorData', 'Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.');
        } catch (ClientException $err) {
            Log::info('ClientException Message: ' . $err->getMessage());
            return redirect()->back()->with('serviceErrorData', 'Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.');
        }
        return redirect()->route('events.show', $idEvent);
    }

    /**
     * Massive Lifecycle State.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function massiveState(Request $request)
    {
        Log::info('----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------');
        Log::info('-----------------------------METHOD: massiveState------------------------');
        $idsAssignArray = explode("/", $request->input('idState'));
        $user = $request->input('User_OMI_State');
        $password = $request->input('Password_OMI_Status');
        $state_massive = $request->input('state_massive');
        $num = 0;
        Log::info('***********user omi: ' . $user);
        Log::info('***********password omi: ' . $password);
        Log::info('***********state_massive: ' . $state_massive);
        Log::info('***********ids eventos: ' . $request->input('idState'));
        foreach ($idsAssignArray as $idEvent) {
            $idEvent = rtrim($idEvent);
            $num++;
            if (isset($idEvent) && strlen($idEvent) == 36) {
                Log::info('*********** Entra al foreach massiveState ***********');
                Log::info('***********Número de evento: ' . $num);
                Log::info('***********Id Evento: ' . $idEvent);
                $xml = new \DomDocument('1.0', 'UTF-8');
                $xml->formatOutput = true;
                $xml_enviar = $xml->createElement('event');
                $xml_enviar->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
                $xml_enviar->appendChild($xml->createElement('state', $state_massive));
                $xml->appendChild($xml_enviar);
                $xml_body = $xml->saveXML();
                Log::info($xml_body);
                try {
                    $client = new Client();
                    $res = $client->request('PUT', "http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/{$idEvent}/?alt=json", [
                        'auth' => [$user, $password],
                        'headers' => [
                            'Content-Type' => 'text/xml; charset=UTF8'
                        ],
                        'body'   => $xml_body
                    ]);
                    $event_updated = $res->getBody();
                    $event_updated = $event_updated->getContents();
                    $event_updated = html_entity_decode($event_updated);
                    $event_updated = json_decode($event_updated, true);
                    Log::info('Resultado de cambio de Status exitoso: ');
                    Log::info(json_encode($event_updated));
                    //Aqui se implementa el método para asignarle el usuario 
                    $singleUserAssign = $this->singleUserAssign($user, $password, $idEvent);
                    Log::info('Resultado de asignación de usuario: ' . $singleUserAssign);
                } catch (ConnectException $e) {
                    Log::info('ConnectException Message: ' . $e->getMessage());
                    return redirect()->back()->with('serviceErrorConnection', 'Error de conexión, intentar nuevamente.');
                } catch (RequestException $er) {
                    Log::info('RequestException Message: ' . $er->getMessage());
                    return redirect()->back()->with('serviceErrorData', 'Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.');
                } catch (ClientException $err) {
                    Log::info('ClientException Message: ' . $err->getMessage());
                    return redirect()->back()->with('serviceErrorData', 'Error en las credenciales o en los datos ingresados, favor verificar e intentar nuevamente.');
                }
            }
        }
        return redirect()->back();
    }

    public function createCorrelation(Request $request)
    {
        $request->validate([
            'cause'     => 'bail | required | string',
            'events'    => 'bail | required | array | min:2'
        ]);

        $cause = $request->input('cause');
        $symptoms = $request->input('events');

        if (($key = array_search($cause, $symptoms)) !== false) {
            unset($symptoms[$key]);
        }

        $count = 0;

        $user = UsersBsm::where('entity_id', Auth::user()->id)->get()->first();

        foreach ($symptoms as $symptom) {
            $document = new \DomDocument('1.0', 'UTF-8');
            $document->formatOutput = true;
            $xml = $document->createElement('symptom');
            $xml->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
            $xml->appendChild($document->createElement('target_id', $symptom));
            $document->appendChild($xml);
            $xml = $document->saveXML();

            try {
                $client = new Client();

                $post = $client->post("http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/$cause/symptom_list?alt=json", [
                    'auth'      => [$user->us_bsm ?? '', $user->pass_bsm ?? ''],
                    'headers'   => [
                        'Content-Type' => 'text/xml; charset=UTF8'
                    ],
                    'body'      => $xml
                ]);

                $count++;
            } catch (ConnectException $co) {
                Log::error('ConnectException Message: ' . $co->getMessage());

                return redirect()->back()->with('serviceErrorConnection', 'Error de conexión, favor de verificar conexión e intentar nuevamente.');
            } catch (RequestException $re) {
                Log::error('RequestException Message: ' . $re->getResponse()->getBody()->getContents());

                return redirect()->back()->with('serviceErrorData', 'Error de credenciales / Información ingresada, favor de verificar e intentar nuevamente.');
            } catch (ClientException $ce) {
                Log::error('ClientException Message: ' . $ce->getResponse()->getBody()->getContents());

                return redirect()->back()->with('serviceErrorData', 'Error de credenciales / Información ingresada, favor de verificar e intentar nuevamente.');
            }
        }

        return redirect()->back()->withSuccess("$count eventos han sido relacionados al evento $cause.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Events $events
     * @return \Illuminate\Http\Response
     */
    public function destroy(Events $events)
    {
        //
    }

    /** 
     * Replacing accented characters
     * 
     * @param String  $str
     * @return String  $str_r
     */
    public function replaceStrC($str)
    {
        $search = explode(",", "á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,Ã¡,Ã©,Ã­,Ã³,Ãº,Ã±,ÃÃ¡,ÃÃ©,ÃÃ­,ÃÃ³,ÃÃº,ÃÃ±,Ã“,Ã ,Ã‰,Ã ,Ãš,â€œ,â€ ,Â¿,ü,Ã‘");
        $replace = explode(",", "á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,Ó,Á,É,Í,Ú,\",\",¿,&uuml;,Ñ");
        $str_r = str_replace($search, $replace, $str);
        return $str_r;
    }

    /** 
     * Get event age
     * 
     * @param String  $created
     * @param \DateTime  $api_date
     * @return String  Event Age
     */
    public function getDatetime($time_received, $api_date)
    {
        $event = new \DateTime($time_received);
        $interval = $event->diff($api_date);
        return ($interval->format('%a')) ? $interval->format('%ad ') . $interval->format('%hh') : $interval->format('%hh ') . $interval->format('%imin');
    }

    /** 
     * Subtract 5 hrs
     * @param String  $time
     * @return String  $sub_time
     */
    public function subtract5Hrs($time)
    {
        $sub_time = (new \DateTime($time))->sub(new \DateInterval('PT5H'))->format('Y-m-d\TH:i:s.vP');
        return $sub_time;
    }
}
