<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class ImageProfileController extends Controller
{
    public function test(Request $req)
    {
        $final_resp = array();
        $final_resp['msg'] = '';
        $final_resp['imagen_actualizada'] = false;
        $final_resp['url_imagen_actualizada'] = '';

        // fecha de hoy
        $fecha_de_hoy = Carbon::now();
        $mes_fecha_de_hoy = $fecha_de_hoy->month;
        $dia_fecha_de_hoy = $fecha_de_hoy->day;
        $anio_fecha_de_hoy = $fecha_de_hoy->year;
        $hora_fecha_de_hoy = $fecha_de_hoy->hour;
        $minuto_fecha_de_hoy = $fecha_de_hoy->minute;
        $segundo_fecha_de_hoy = $fecha_de_hoy->second;

        $fecha_de_hoy = sprintf(
            '%s_%s_%s_%s_%s_%s__',
            $anio_fecha_de_hoy,
            $mes_fecha_de_hoy,
            $dia_fecha_de_hoy,
            $hora_fecha_de_hoy,
            $minuto_fecha_de_hoy,
            $segundo_fecha_de_hoy
        );

        $data = $req->all();
        $imagen_base64 = $data['image'];
        $imagen_base64 = str_replace('data:image/png;base64,', '', $imagen_base64);
        $imagen_base64 = str_replace(' ', '+', $imagen_base64);
        $random_string = Str::random(10);
        $nombre_imagen_por_guardar = $fecha_de_hoy . $random_string . '.' . 'png';
        $imagen_decodificada_base64 = base64_decode($imagen_base64);

        // validar que la imagen que si exista, en caso de que sea asi, se elimina la imagen
        Storage::disk('profile_images')->put($nombre_imagen_por_guardar, $imagen_decodificada_base64);

        $imagen_perfil_cargada_correctamente = Storage::disk('profile_images')->exists($nombre_imagen_por_guardar);

        // se registra el nombre de la imagen en el usuario dentro de la BD
        $user = Auth::user();

        if ($user) {
            //se revisa si antes tenia una imagen de usuario asignada, de ser asi se elimina del storage
            if (!empty($user->path_image_profile)) {
                Storage::disk('profile_images')->delete($user->path_image_profile);
            }

            $user->path_image_profile = $nombre_imagen_por_guardar;
            $user->save();
        }

        if ($imagen_perfil_cargada_correctamente) {
            $final_resp['msg'] = 'Se cargo la imagen del perfil correctamente.';
            $final_resp['imagen_actualizada'] = true;
            $final_resp['url_imagen_actualizada'] = Storage::disk('profile_images')->url($nombre_imagen_por_guardar);
        } else {
            $final_resp['msg'] = 'Carga de imagen del perfil sin exito.';
            $final_resp['imagen_actualizada'] = false;
        }

        return response()->json($final_resp);
    }
}
