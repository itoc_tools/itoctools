<?php

namespace App\Http\Controllers;

use App\Filter;
use ErrorException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Psr7\Message;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;

class TesterController extends Controller
{
    public function __invoke(Request $request)
    {
        return view('nc.events.index')->with([
            'breadcrumb' => [
                ['name' => 'Inicio', 'url' => url('')],
                ['name' => 'Eventos']
            ]
        ]);
    }

    /*
        1. Eliminar notifiedPlainTemplate/notified de la tabla de eventos.
        2. Crear tabla de relación con id_event (string), filter_id (integer) y notified (string).
        3. Crear relación en los modelos.
        4. Modificar consulta para recuperar eventos que cumplan las condiciones del filtro y no se hayan notificado.
        5. Agregar consulta para comprobar que el evento no ha sido notificado a través del filtro.
        6. Conservar procedimiento para notificar.
        7. Crear relación al finalizar la aplicación del filtro.
        8. Actualizar los eventos que fueron notificados al terminar la iteración sobre los filtros.

        A. Para todos los eventos que ya existan en BD, establecer el campo notified en 'ok'.
        B. Revisar si se hace uso del campo notified o por validación de fechas.
    */

    /* public function __invoke(Request $request) {
        $filters = Filter::where('state', true)->get();

        foreach ($filters as $filter) {
            $events = DB::select("SELECT * FROM events AS t1 INNER JOIN temp_custom_attribute AS t2 ON t1.id_event = t2.id_event WHERE {$filter->sql} AND t1.created_at > '{$filter->created_at}'");

            foreach ($events as $event) {
                $relationship = DB::select("SELECT * FROM event_filter WHERE id_event LIKE '{$event->id_event}' AND filter_id = {$filter->id}"); // To Do: AND notified IS NULL

                if (empty($relationship)) {
                    Log::info("ID Evento: {$event->id_event} | Filtro: {$filter->id} | Inicia: " . date('d-m-Y H:i:s'));

                    $this->sendMail($event, $filter);

                    Log::info("ID Evento: {$event->id_event} | Filtro: {$filter->id} | Finaliza: " . date('d-m-Y H:i:s'));
                }
            }
        }
    } */

    public function sendMail($event, $filter)
    {
        try {
            $client = new Client();

            $params = [
                'subject'       => $this->replaceValues($event, $filter, $filter->subject, 'Subject'),
                'cuenta'        => $filter->cuenta,
                'tag'           => $filter->tag,
                'contactos'     => explode(',', $filter->to),
                'contactos_cc'  => empty($filter->cc) ? [] : explode(',', $filter->cc),
                'body'          => $this->createTemplate($event, $filter)
            ];

            Log::info("ID Evento: {$event->id_event} | Filtro: {$filter->id} | Send Mail | Params: " . json_encode($params));

            $request = $client->request('POST', 'http://172.20.45.172/notificacion/mail/avanzado', ['form_params' => $params]);

            Log::info("ID Evento: {$event->id_event} | Filtro: {$filter->id} | Send Mail | Response: {$request->getStatusCode()} - {$request->getBody()->getContents()}");

            $this->Notified($event, $filter, $request->getStatusCode());
        } catch (ConnectException $e) {
            Log::error("ID Evento: {$event->id_event} | Filtro: {$filter->id} | Send Mail | ConnectException: {$e->getMessage()}");
        } catch (RequestException $re) {
            Log::error("ID Evento: {$event->id_event} | Filtro: {$filter->id} | Send Mail | RequestException: {$re->getMessage()}");
        } catch (ClientException $ce) {
            Log::error("ID Evento: {$event->id_event} | Filtro: {$filter->id} | Send Mail | ClientException: " . Message::toString($ce->getResponse()));
        }
    }

    public function replaceValues($event, $filter, $original, $param)
    {
        $attributes = array();

        preg_match_all('/#(\w+)/', $original, $attributes, PREG_PATTERN_ORDER);

        $modified = $original;

        foreach ($attributes[1] as $attribute) {
            try {
                $modified = str_replace("#$attribute", utf8_decode($event->{$attribute}), $modified);
            } catch (ErrorException $e) {
                Log::error("ID Evento: {$event->id_event} | Filtro: {$filter->id} | Create $param | ErrorException: {$e->getMessage()}");
            }
        }

        return $modified;
    }

    public function createTemplate($event, $filter)
    {
        $fields = json_decode($filter['template'], true);

        $style = '';
        $img = '';

        if ($filter->check_template != "true") {
            $style = '<style>table { width: 100%; } table tbody tr { color: #000000; font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; text-align: left; } td.header { font-weight: bold; text-align: right; width: 20%; }</style>';

            $img = "<div><img src='http://200.57.138.134/ITOC/images/bannerItoc.jpg' width='100%'></div>";
        }

        $html = "<html><head><meta charset='utf-8' content-type='text/html'>$style</head><body><div>$img<table><tbody>";

        $body = '';

        foreach ($fields as $key => $field) {
            $color = 'rgb(255, 255, 255)';

            if ($filter->check_template != "true" && ($key % 2 == 0)) {
                $color = 'rgb(240, 248, 255)';
            }

            $body .= "<tr style='background-color: $color;'><td class='header'>{$field['name']}:</td><td>{$field['value']}</td></tr>";
        }

        $body = $this->replaceValues($event, $filter, $body, 'Template');

        $html .= "$body</tbody></table></div></body></html>";

        return $html;
    }

    public function Notified($event, $filter, $code)
    {
        DB::insert('INSERT INTO event_filter (id_event, filter_id, notified) VALUES (?, ?, ?)', [$event->id_event, $filter->id, ($code == 200 ? 'ok' : NULL)]);
    }
}