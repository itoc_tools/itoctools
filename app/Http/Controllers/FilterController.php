<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Filter;
use App\Helpers\LogActivity;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filters = Filter::all();


        return view('admin.filters.index')->with([
            'title' => 'Filtros',
            'breadcrumb' => [
                [
                    'title' => 'Filtros'
                ]
            ],
            'filters' => $filters
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contacts = Contact::all();

        return view('admin.filters.wizard')->with([
            'title' => 'Wizard',
            'breadcrumb' => [
                [
                    'title' => 'Wizard Eventos'
                ]
            ],
            'contacts' => $contacts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Filter::create($request->all());

        LogActivity::addToLog('Nuevo filtro ' . $request->name);
        return response()->json(array('msg' => "Filtro guardado con éxito"), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Filter $filter
     * @return \Illuminate\Http\Response
     */
    public function show(Filter $filter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Filter $filter
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $filters = Filter::find($id);

        $contacts = Contact::all();

        $checked = $filters->check_template == "true" ? "checked" : "";

        return view('admin.filters.edit')->with([
            'title' => 'Editar Filtro',
            'breadcrumb' => [
                [
                    'title' => 'Editar Filtro'
                ]
            ],
            'contacts' => $contacts,
            'filters' => $filters,
            'checked' => $checked,
            'template' => json_decode($filters->template, true),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Filter $filter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $filter = Filter::findOrFail($id);
        $filters = $request->all();
        $filter->fill($filters)->save();
        LogActivity::addToLog('Edición de filtro ' . $request->name);
        return response()->json(array('msg' => "Filtro guardado con éxito"), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Filter $filter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Filter $filter, $id)
    {
        $filter = Filter::find($id);
        $filter->delete();
        LogActivity::addToLog('Filtro eliminado ' . $filter->name);
        return redirect(route('filter.index'));
    }

    public function replicateFilter($id)
    {
        $filter = Filter::find($id);

        $newFilter = $filter->replicate();
        $newFilter->name = $newFilter->name . " (Copy " . $id . ")";
        $newFilter->save();

        LogActivity::addToLog('Filtro duplicado' . $newFilter->name);

        return redirect(route('filter.index'));
    }

    public function filterSwitch(Request $request)
    {
        $filter = Filter::find($request->id_filter);
        $filter->state = filter_var($request->state, FILTER_VALIDATE_BOOLEAN);
        $filter->save();
    }
}
