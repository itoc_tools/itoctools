<?php

namespace App\Http\Controllers;

use App\Tree;
use App\DeviceTree;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeviceTreeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Process ajax request.
     * Obtiene un árbol de dispositivos.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTree()
    {
        $id = Tree::getIdByClassName(DeviceTree::class);
        $tree = DeviceTree::getById($id);

        return json_encode($tree);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parent = DeviceTree::find($request->parent);

        $node = [
            'name' => $request->text,
            'type' => $request->type
        ];

        return json_encode(DeviceTree::create($node,$parent));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->text;

        if (DeviceTree::find($id)->update(['name' => $name])) {
            return 'SUCCESS';
        }
        return 'FAIL';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $node = DeviceTree::find($id);
        if ($node->delete()) {
            return 'SUCCESS';
        }
        return 'FAIL';
    }
}
