<?php

namespace App\Http\Controllers;

use App\Attribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use MongoDB\Driver\Session;
use Symfony\Component\Console\Input\Input;

class AttributeController extends Controller
{
    public function attribute()
    {
        return Attribute::all();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attributes = Attribute::all();

        return view('admin.attributes.index')->with([
            'title' => 'Atributos',
            'breadcrumb' => [
                [
                    'title' => 'Atributos'
                ]
            ],
            'attributes' => $attributes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.attributes.create')->with([
            'title' => 'Nuevo atributo',
            'breadcrumb' => [
                [
                    'title' => 'Nuevo atributo'
                ]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attribute = new Attribute();

        $attribute->label = $request->input('label');
        $attribute->type = "string";
        $attribute->operators = json_encode(["equal", "not_equal", "contains", "not_contains"]);
        $attribute->category = "custom_attribute";

        $attribute->save();

        return redirect()->route('attribute.index')->with('success', 'Atributo guardado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Attribute $attribute
     * @return \Illuminate\Http\Response
     */
    public function show(Attribute $attribute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Attribute $attribute
     * @return \Illuminate\Http\Response
     */
    public function edit(Attribute $attribute, $id)
    {
        $attribute = Attribute::find($id);

        return view('admin.attributes.edit')->with([
            'title' => 'Editar atributo',
            'breadcrumb' => [
                [
                    'title' => 'Editar atributo'
                ]
            ],
            'attribute' => $attribute
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Attribute $attribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attribute = Attribute::find($id);

        $attribute->label = $request->label;
        $attribute->type = "string";
        $attribute->operators = json_encode(["equal", "not_equal", "contains", "not_contains"]);

        $attribute->save();

        return redirect()->route('attribute.index')->with('success', 'Atributo actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Attribute $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute $attribute)
    {
        //
    }
}
