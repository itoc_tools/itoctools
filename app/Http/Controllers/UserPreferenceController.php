<?php

namespace App\Http\Controllers;

use Auth;
use App\SystemModule;
use App\UserPreference;
use Illuminate\Http\Request;

class UserPreferenceController extends Controller 
{
    public function getEventsTablePreferences() {
        $user = Auth::user();

        $module = SystemModule::where('name', 'Eventos')->first();

        $relationship = $user->preferences()->where('system_module_id', $module->id);

        $options = [];

        if ($relationship->exists()) {
            $relationship = $relationship->first();

            $preferences = json_decode($relationship->preferences, true);

            $options = $preferences['DataTables'];
        }

        return response()->json($options, 200);
    }

    /**
     * Store events table's columns order in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function saveEventsTableState(Request $request) {
        $name = $request->input('name');
        $state = $request->input('state');

        $user = Auth::user();
        $module = SystemModule::where('name', 'Eventos')->first();

        $relationship = $user->preferences()->where('system_module_id', $module->id);

        if ($relationship->exists()) {
            $userPreference = $relationship->first();

            $preferences = json_decode($userPreference->preferences, true);

            if (isset($preferences['DataTables'])) {
                foreach ($preferences['DataTables'] as $key => $preference) {
                    $preferences['DataTables'][$key] = ['state' => $preference['state']];
                }
            }

            if (strtolower($name) !== 'reset') {
                $preferences['DataTables'][$name] = [
                    'state'     => $state,
                    'current'   => true
                ];
            }

            $userPreference->preferences = json_encode($preferences);
            $userPreference->save();
        } else if (strtolower($name) !== 'reset') {
            $preferences['DataTables'][$name] = [
                'state'     => $state, 
                'current'   => true
            ];

            $userPreference = new UserPreference();
            $userPreference->user()->associate($user);
            $userPreference->systemModule()->associate($module);
            $userPreference->preferences = json_encode($preferences);
            $userPreference->save();
        }

        return redirect()->back()->withSuccess('La configuración de columnas ha sido aplicada.');
    }
}
