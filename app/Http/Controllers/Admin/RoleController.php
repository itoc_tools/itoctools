<?php

namespace App\Http\Controllers\Admin;

use Silber\Bouncer\Database\Ability;
use Silber\Bouncer\Database\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        //$this->validaRoleUser('users_manage');
        $roles = Role::all();
        return datatables()->of($roles)->make();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->validaRoleUser('users_manage');
        return view('admin.roles.role')->with([
            'title' => 'Roles',
            'breadcrumb' => [
                [
                    'title' => 'Roles'
                ]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$this->validaRoleUser('users_manage');
        $title = 'Roles';
        $breadcrumb = [
            [
                'title' => 'Roles',
                'url' => route('roles.index'),
            ],
            [
                'title' => 'Nuevo',
            ],
        ];

        $abilities = Ability::all();
        return view('admin.roles.create')->with(compact('abilities', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->validaRoleUser('users_manage');
        $role = Role::create($request->all());
        $role->allow($request->input('abilities'));
        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ability  $ability
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //$this->validaRoleUser('users_manage');
        $title = 'Detalle';
        $breadcrumb = [
            [
                'title' => 'Detalle',
                'url' => route('roles.index'),
            ],
            [
                'title' => 'Detalle',
            ],
        ];

        return view('admin.roles.show')->with(compact('role', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ability  $ability
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //$this->validaRoleUser('users_manage');
        $title = 'Roles';
        $breadcrumb = [
            [
                'title' => 'Roles',
                'url' => route('roles.index'),
            ],
            [
                'title' => 'Editar',
            ],
        ];

        $abilities = Ability::all();
        return view('admin.roles.edit')->with(compact('role', 'abilities', 'title', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ability  $ability
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        //$this->validaRoleUser('users_manage');
        // Validar datos
        $messages = [];
        $rules = [
            'name' => ['required', 'min:3', ($role->name != $request->name ? 'unique:roles' : '')]
        ];

        $this->validate($request, $rules, $messages);

        $role->update($request->all());
        foreach ($role->getAbilities() as $ability) {
            $role->disallow($ability->name);
        }
        $role->allow($request->input('abilities'));

        $request->session()->flash(
            'notification',
            [
                'type' => 'success',
                'header' => '¡Bien hecho!',
                'message' => 'El rol se actualizó correctamente',
            ]
        );

        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ability  $ability
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //$this->validaRoleUser('users_manage');
        $role->delete();
        return redirect()->route('admin.roles.index');
    }

    /**
     * Delete all selected Role at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        //$this->validaRoleUser('users_manage');
        if ($request->input('ids')) {
            $entries = Role::whereIn('id', $request->input('ids'))->get();
            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    /**
     * Valida si el usuario cuenta con la habilidad necesaria.
     *
     * @param String $ability
     */
    public function validaRoleUser($ability)
    {
        if (!Gate::allows($ability)) {
            return abort(401);
        }
    }
}
