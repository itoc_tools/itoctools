<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\UsersBsm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Silber\Bouncer\Database\Ability;
use Silber\Bouncer\Database\Role;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $users = User::all();
        return datatables()->of($users)->make();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.user')->with([
            'title' => 'Usuarios',
            'breadcrumb' => [
                [
                    'title' => 'Usuarios'
                ]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $title = 'Usuario';
        $breadcrumb = [
            [
                'title' => 'Usuario',
                'url' => route('users.index'),
            ],
            [
                'title' => 'Nuevo',
            ],
        ];

        $abilities = Ability::all();
        $roles = Role::all();
        return view('admin.users.create')->with(compact('roles', 'abilities', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validar datos
        $messages = [];
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed']
        ];

        $this->validate($request, $rules, $messages);

        $user = User::create($request->all());
        // --- Asignar un rol al usuario
        foreach ($request->input('roles') as $role) {
            $user->assign($role);
        }
        // --- Aplicar habilidad a un usuario
        $abilities = $request->input('abilities');
        if ($abilities) {
            foreach ($abilities as $abilitie) {
                $user->allow($abilitie);
            }
        }
        UsersBsm::create([
            'entity_id' => $user->id,
            'us_bsm' => $request->input('us_bsm'),
            'pass_bsm' => $request->input('pass_bsm')
        ]);
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user_bsm = UsersBsm::where('entity_id', $user->id)->get()->first();
        $title = 'Detalle';
        $breadcrumb = [
            [
                'title' => 'Detalle',
                'url' => route('users.index'),
            ],
            [
                'title' => 'Detalle',
            ],
        ];
        return view('admin.users.show')->with(compact('user', 'title', 'breadcrumb', 'user_bsm'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user_bsm = UsersBsm::where('entity_id', $user->id)->get()->first();
        $title = 'Usuarios';
        $breadcrumb = [
            [
                'title' => 'Usuarios',
                'url' => route('users.index'),
            ],
            [
                'title' => 'Editar',
            ],
        ];

        $roles = Role::all();
        $abilities = Ability::all();
        return view('admin.users.edit')->with(compact('user', 'roles', 'abilities', 'title', 'breadcrumb', 'user_bsm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // Validar datos
        $messages = [];
        $rules = [
            'email' => ['required', ($user->email != $request->email ? 'unique:users' : '')]
        ];

        $this->validate($request, $rules, $messages);

        $user->update($request->all());
        // --- Roles
        foreach ($user->roles as $role) {
            $user->retract($role);
        }
        $user->assign($request->input('roles'));
        // --- Habilidades
        foreach ($user->getAbilities() as $ability) {
            $user->disallow($ability->name);
        }
        $user->allow($request->input('abilities'));
        // BSM 
        $user_bsm = UsersBsm::where('entity_id', $user->id)->get()->first();
        if (isset($user_bsm)) {
            $user_bsm->update([
                'entity_id' => $user->id,
                'us_bsm' => $request->input('us_bsm'),
                'pass_bsm' => $request->input('pass_bsm')
            ]);
        } else {
            UsersBsm::create([
                'entity_id' => $user->id,
                'us_bsm' => $request->input('us_bsm'),
                'pass_bsm' => $request->input('pass_bsm')
            ]);
        }

        $request->session()->flash(
            'notification',
            [
                'type' => 'success',
                'header' => '¡Bien hecho!',
                'message' => 'El usuario se actualizó correctamente',
            ]
        );

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('admin.users.index');
    }

    /**
     * Delete all selected Role at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = Role::whereIn('id', $request->input('ids'))->get();
            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
