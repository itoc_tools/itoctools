<?php

namespace App\Http\Controllers;

use App\Integration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class IntegrationController extends Controller 
{
    private $validation_rules = [
        'name'              => 'bail | required | string', 
        'description'       => 'bail | required | string', 
        'method'            => 'bail | required | string', 
        'endpoint'          => 'bail | required | string | url', 
        'auth_type'         => 'bail | required | string', 
        'auth_params'       => 'bail | required | array', 
        'headers'           => 'bail | nullable | array', 
        'body_type'         => 'bail | required | string', 
        'body_params_type'  => 'bail | required_if:body_type,===,raw | string', 
        'body_params'       => 'bail | required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $integrations = Integration::all();

        return view('admin.integrations.index')->with([
            'title' => 'Integraciones',
            'breadcrumb' => [[
                'title' => 'Integraciones'
            ]],
            'integrations' => $integrations
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() { }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate($this->validation_rules);

        $integration = new Integration($request->all());
        $integration->body_params_type = ($integration->body_type === 'params') ? null : $integration->body_params_type;
        $integration->save();

        return redirect()->route('integrations.index')->with('success', "Se añadió la integración {$integration->name}");
    }

    /**
     * Display the specified resource.
     *
     * @param \App\integration $integration
     * @return \Illuminate\Http\Response
     */
    public function show(Integration $integration) {
        return response()->json($integration);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\integration $integration
     * @return \Illuminate\Http\Response
     */
    public function edit(Integration $integration) { }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\integration $integration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Integration $integration) {
        $request->validate($this->validation_rules);

        $integration->fill($request->all());
        $integration->body_params_type = ($integration->body_type === 'params') ? null : $integration->body_params_type;
        $integration->save();

        return redirect()->route('integrations.index')->with('success', "Se actualizó la integración {$integration->name}");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\integration $integration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Integration $integration) {
        $integration->delete();

        return redirect()->route('integrations.index')->with('success', "Se eliminó la integración {$integration->name}");
    }

    public function connect(Request $request, Integration $integration) {
        $options = [];
        $options['auth'] = array_values(json_decode($integration->auth_params, true));
        $options['headers'] = json_decode($integration->headers, true);

        $params = $request->except('_token');

        if ($integration->body_type === 'params') {
            $params = json_encode($params);
        } else {
            $params = $params['body'];
        }

        switch ($integration->method) {
            case 'GET':
                $options['query'] = $params;
            break;
            case 'POST':
                $options['body'] = $params;
            break;
        }

        $response = Http::send($integration->method, $integration->endpoint, $options);

        return response()->json([$response->body()], $response->status());
    }
}