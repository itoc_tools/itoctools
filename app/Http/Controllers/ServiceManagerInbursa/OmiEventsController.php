<?php

namespace App\Http\Controllers\ServiceManagerInbursa;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;

class OmiEventsController extends Controller
{
    public function addCustomAttributeEvent($name, $value, $idEvent)
    {
        // $urlEndpoint = 'http://172.20.45.177/itoctools/public/addcustomattribute';
        /* -------Its comment to avoid issue for not resolve Host-------
         $urlEndpoint = 'https://itoc-plus.triara.mexico/addcustomattribute';
        $client = new Client();

        $resp = $client->request('POST', $urlEndpoint, [
            'form_params' => [
                'name' => $name,
                'value' => $value,
                'idEvent' => $idEvent,
            ]
        ]);
        return json_decode($resp->getBody()); */
        Log::info('................ From  ServiceManagerInbursaEventsCron: addcustomattribute');
        Log::info('................ ID:  ' . $idEvent);
        Log::info('................ name:  ' . $name);
        Log::info('................ value:  ' . $value);
        $xml = new \DomDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xml_enviar = $xml->createElement('custom_attribute');
        $xml_enviar->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
        $xml_enviar->setAttribute('self', 'http://bsmtelmexit.triara.mexico/opr-web/rest/9.10/event_list/' . $idEvent .
            '/custom_attribute_list/' . $name);
        $xml_enviar->setAttribute('type', 'urn:x-hp:2009:software:data_model:opr:type:event:custom_attribute');
        $xml_enviar->setAttribute('version', '1.0');
        $xml_enviar->appendChild($xml->createElement('name', $name));
        $xml_enviar->appendChild($xml->createElement('value', $value));
        $xml->appendChild($xml_enviar);
        $xml_body = $xml->saveXML();
        Log::info($xml_body);
        $ca_created = [];
        try {
            $client = new Client();
            $res = $client->request('POST', 'http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/' . $idEvent .
                '/custom_attribute_list/?alt=json', [
                'auth' => ['OPC.Zenoss', '+0ruge5.X3m137-'],
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8'
                ],
                'body'   => $xml_body
            ]);
            $ca_created = $res->getBody();
            $ca_created = $ca_created->getContents();
            $ca_created = html_entity_decode($ca_created);
            $ca_created = json_decode($ca_created, true);
        } catch (ConnectException $e) {
            Log::info('Error Message: ' . $e->getMessage());
            $ca_created = array(
                "status" => "error",
                "Error Message" => $e->getMessage()
            );
        } catch (RequestException $er) {
            Log::info('Error Message: ' . $er->getMessage());
            $ca_created = array(
                "status" => "error",
                "Error Message" => $er->getMessage()
            );
        } catch (ClientException $err) {
            Log::info('Error Message: ' . $err->getMessage());
            $ca_created = array(
                "status" => "error",
                "Error Message" => $err->getMessage()
            );
        }
        return $ca_created;
    }

    public function addAnnotationEvent($value, $idEvent)
    {
        // $urlEndpoint = 'http://172.20.45.177/itoctools/public/addannotation';
        /* -------Its comment to avoid issue for not resolve Host-------
        $urlEndpoint = 'https://itoc-plus.triara.mexico/addannotation';
        $client = new Client();
        $body['text'] = $text;
        $body['idEvent'] = $idEvent;
        $resp = $client->request('POST', $urlEndpoint, [
            'form_params' => [
                'text' => $text,
                'idEvent' => $idEvent,
            ]
        ]);
        return json_decode($resp->getBody()); */
        Log::info('................ From ServiceManagerInbursaEventsCron: addannotation');
        Log::info('................ ID:  ' . $idEvent);
        Log::info('................ text:  ' . $value);
        $xml = new \DomDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xml_enviar = $xml->createElement('annotation');
        $xml_enviar->setAttribute('xmlns', 'http://www.hp.com/2009/software/opr/data_model');
        $xml_enviar->appendChild($xml->createElement('text', $value));
        $xml->appendChild($xml_enviar);
        $xml_body = $xml->saveXML();
        Log::info($xml_body);
        $ca_created = [];
        try {
            $client = new Client();
            $res = $client->request('POST', "http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/{$idEvent}/annotation_list?alt=json", [
                'auth' => ['OPC.Zenoss', '+0ruge5.X3m137-'],
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8'
                ],
                'body'   => $xml_body
            ]);
            $ca_created = $res->getBody();
            $ca_created = $ca_created->getContents();
            $ca_created = html_entity_decode($ca_created);
            $ca_created = json_decode($ca_created, true);
            $ca_created = $ca_created['annotation'];
        } catch (ConnectException $e) {
            Log::info('Error Message: ' . $e->getMessage());
            $ca_created = array(
                "status" => "error",
                "Error Message" => $e->getMessage()
            );
        } catch (RequestException $er) {
            Log::info('Error Message: ' . $er->getMessage());
            $ca_created = array(
                "status" => "error",
                "Error Message" => $er->getMessage()
            );
        } catch (ClientException $err) {
            Log::info('Error Message: ' . $err->getMessage());
            $ca_created = array(
                "status" => "error",
                "Error Message" => $err->getMessage()
            );
        }
        return $ca_created;
    }
}
