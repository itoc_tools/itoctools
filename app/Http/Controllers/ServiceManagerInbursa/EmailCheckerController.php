<?php

namespace App\Http\Controllers\ServiceManagerInbursa;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EmailCheckerController extends Controller
{
    public function getListEmailEvents(){
        $urlEndpoint = 'http://172.20.45.129:8080/process/email/inbursa';
        $client = new Client();
        
        $obtainedEmails = $client->request('GET', $urlEndpoint, [
            'auth' =>['user','12345']
        ]);

        $obtainedEmails = json_decode($obtainedEmails->getBody());
        return $obtainedEmails;
    }
}
