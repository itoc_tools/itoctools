<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::all();

        return view('admin.contacts.index', compact('contacts'))->with([
                'title' => 'Contactos',
                'breadcrumb' => [
                    [
                        'title' => 'Contactos'
                    ]
                ],
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.contacts.create')->with([
            'title' => 'Nuevo Contacto',
            'breadcrumb' => [
                [
                    'title' => 'Nuevo Contacto'
                ]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:contacts,email',
            'telephone' => 'required|unique:contacts,telephone'
        ];

        $customMessages = [
            'required' => 'El :attribute field is required.',
            'unique' => 'El :attribute ya esta registrado'
        ];

        $this->validate($request, $rules, $customMessages);

        Contact::create($request->all());

        return redirect()->route('contact.index')->with('success', 'Contacto guardado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contact::find($id);
        return view('admin.contacts.show')->with([
            'title' => 'Detalle Contacto',
            'breadcrumb' => [
                [
                    'title' => 'Detalle Contacto'
                ]
            ],
            'contact' => $contact
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact, $id)
    {
        $contact = Contact::find($id);

        return view('admin.contacts.edit')->with([
            'title' => 'Editar Contacto',
            'breadcrumb' => [
                [
                    'title' => 'Editar Contacto'
                ]
            ],
            'contact' => $contact
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }
}
