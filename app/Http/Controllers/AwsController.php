<?php

namespace App\Http\Controllers;

use App\Repositories\Aws;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AwsController extends Controller {

	/**
	 * M�todos
	 */
	private $base_url = 'https://172.20.45.129:8080/';
	public static $base_url_static = 'https://172.20.45.129:8080/';
	private $guzzleHttp_client = null;
	private static $availableMethods = [
		'GET',
		'HEAD',
		'POST',
		'PUT',
		'DELETE',
		'CONNECT',
		'OPTIONS',
		'TRACE'
	];

	/**
	 * Funciones
	 */

	/**
	 * Construct.
	 */
	public function __construct() {
		$this->guzzleHttp_client = new Client(['http_errors' => false, 'verify' => false]);
	}

	/**
	 * Display a listing of the resource.
	 */
	public function index() {

		try {

			$defaultView = 'admin.aws.index';
			$returnParams = [
				'title' => 'AWS',
				'breadcrumb' => [
					[
						'title' => 'AWS'
					]
				],
			];

			// $instanceTypeTree = [];
			// $instancesType = $this->listServiceTypes();
			// for($i = 0; $i < count($instancesType); $i++) {
			// 	$instanceTypeTree[$instancesType[$i]['Nombre']] = $this->listInstancesByType($instancesType[$i]['Nombre']);
			// }

			// Se adjunta a los par�metros de la vista el listado de usuarios con la key users
			$returnParams['p_users'] = $this->listUsers();
			$returnParams['p_clients'] = $this->listClients();
			$returnParams['p_instances'] = ['ec2' => [], 'rds' => []]; //$instanceTypeTree;
			$returnParams['p_alerts'] = $this->listAlerts();

			return view($defaultView)->with($returnParams);

		}
		catch (ConnectException $e) {

			return $e->getMessage();

		}
		finally {

			// Silencio

		}

	}

	public function showAccount($id) {

		try {

			$defaultView = 'admin.aws.accounts';
			$accountsList = [];

			$returnParams = [
				'title' => 'AWS',
				'breadcrumb' => [
					[
						'title' => 'AWS'
					]
				],
			];

			if(is_string($id) && preg_match('/[0-9]+/', $id) == 0) {
				
				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'Formato de id es incorrecto';

				return view($defaultView)->with($returnParams);

			}

			if( !isset($id) || (is_int($id) && $id < 1) ) {

				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'No se encontr� Id';

				return view($defaultView)->with($returnParams);

			}

			$accountsList = $this->getAccountsUserAWSId($id)['data'];
			$clientsList = $this->listClients();
			$newAccountList = [];

			if(array_key_exists('data', $clientsList)) {
				for($i = 0; $i < count($clientsList['data']); $i++) {
					for($j = 0; $j < count($accountsList); $j++) {
						if($clientsList['data'][$i]['Accountid'] === $accountsList[$j]['Id']) {
							$accountsList[$j]['Client'] = $clientsList['data'][$i]['Nombrecliente'];
							array_push($newAccountList, $accountsList[$j]);
							break;
						}
					}
				}
			}

			if(count($newAccountList) === 0) {
				for($i = 0; $i < count($accountsList); $i++) {
					$accountsList[$i]['Client'] = '';
				}
				$newAccountList = $accountsList;
			}

			$returnParams['instances'] = $newAccountList;
			$returnParams['userId'] = $id;

			return view($defaultView)->with($returnParams);

		}
		catch(ConnectException $e) {

			return $e->getMessage();

		}
		finally {

			// Silencio aqu� tambi�n

		}

	}

	public function showServices($accountId, $clientId) {

		try {

			$defaultView = 'admin.aws.services';
			$clientName = null;
			$servicesTypeList = [];
			$httpParams = [
				'json' => [
					'Credential' => [
						'Name' => ''
					]
				]
			];
			$returnParams = [
				'title' => 'AWS',
				'breadcrumb' => [
					[
						'title' => 'AWS'
					]
				],
			];

			// Account id
			if(!is_int($accountId) && !is_long($accountId) && !is_string($accountId) ) {

				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'No se encontr� id de cliente';

				return view($defaultView)->with($returnParams);

			}

			if(is_string($accountId) && preg_match('/[0-9]+/', $accountId) == 0) {

				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'Formato de id de cliente no se encontr�';

				return view($defaultView)->with($returnParams);

			}


			// Client id
			if(!is_int($clientId) && !is_long($clientId) && !is_string($clientId) ) {

				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'No se encontr� id de cliente';

				return view($defaultView)->with($returnParams);

			}

			if(is_string($clientId) && preg_match('/[0-9]+/', $clientId) == 0) {

				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'Formato de id de cliente no se encontr�';

				return view($defaultView)->with($returnParams);

			}

			$clientName = $this->getClientNameById($accountId, $clientId);

			if( !isset($clientName) ) {

				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'Id de cliente no se encontr� en nuestra base de datos';

				return view($defaultView)->with($returnParams);

			}

			// Obtiene el listado completo de tipos de servicios
			$response = $this->guzzleHttp_client->post($this->base_url . 'aws/dataBase/getTypeServices', $httpParams);
			$contentResponse = $response->getBody()->getContents();
			$contentResponse = json_decode($contentResponse, true);

			if(array_key_exists('User', $contentResponse) && count($contentResponse['User']) > 0) {
				$servicesTypeList = $contentResponse['User'];
			}

			if(count($servicesTypeList) == 0) {

				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'No se encontr� ning�n tipo de servicio de AWS registrado en base de datos.';

				return view($defaultView)->with($returnParams);

			}

			$clientServicesList = [];
			for($i = 0; $i < count($servicesTypeList); $i++) {
				if(array_key_exists('Nombre', $servicesTypeList[$i])) {
					$serviceName = $servicesTypeList[$i]['Nombre'];

					$httpParamsInstances = [
						'json' => [
							'Credential' => [
								'TypeSession' => 2,
								'Accountid' => $accountId,
								'Id' => $clientId,
							],
							'TypeInstance' => $serviceName,
							'Instances' => []
						]
					];

					$response = $this->guzzleHttp_client->post($this->base_url . 'aws/agent/getInstances', $httpParamsInstances);
					$contentResponse = $response->getBody()->getContents();
					$contentResponse = json_decode($contentResponse, true);

					if(array_key_exists('message', $contentResponse)) {

						$defaultView = 'admin.aws.error';
						$returnParams['errorMessage'] = 'Error al tratar de obtener las instancias del cliente. C�digo de error de AWS: ' . $contentResponse['message'];

						return view($defaultView)->with($returnParams);
					}

					$tmpInstanceStatistics = [];
					$stoppedInstances = 0;
					$initializedInstances = 0;
					if(array_key_exists('Instances', $contentResponse)) {

						if( is_array($contentResponse['Instances']) && count($contentResponse['Instances']) > 0) {

							$Instances = $contentResponse['Instances'];

							for($j = 0; $j < count($Instances); $j++) {

								if(array_key_exists('InstanceStatus', $Instances[$j])) {
									if($Instances[$j]['InstanceStatus'] === 'stopped') {
										$stoppedInstances++;
									}
									else {
										$initializedInstances++;
									}
								}

							}

						}
						
					}

					$clientServicesList[$serviceName] = [
						'STOPPED' => $stoppedInstances,
						'INITIALIZED' => $initializedInstances,
						'TOTAL' => $initializedInstances + $stoppedInstances,
					];
				}
			}

			$returnParams['services'] = $clientServicesList;
			$returnParams['clientName'] = $clientName;
			$returnParams['clientId'] = $clientId;
			$returnParams['accountId'] = $accountId;

			return view($defaultView)->with($returnParams);

		}
		catch(ConnectException $e) {

			return $e->getMessage();

		}
		finally {

			// No hay nada que finalizar

		}

		
	}

	public function showInstances($type, $accountId, $clientId) {

		try {

			$defaultView = 'admin.aws.instances';
			$instancesList = [];
			$httpParams = [
				'json' => [
					'Credential' => [
						'TypeSession' => 2,
						'Accountid' => $accountId,
						'Id' => $clientId,
					],
					'TypeInstance' => $type,
					'Instances' => []
				]
			];
			$returnParams = [
				'title' => 'AWS',
				'breadcrumb' => [
					[
						'title' => 'AWS'
					]
				],
			];

			// Account id
			if(!is_int($accountId) && !is_long($accountId) && !is_string($accountId) ) {

				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'No se encontr� id de cliente';

				return view($defaultView)->with($returnParams);

			}

			if(is_string($accountId) && preg_match('/[0-9]+/', $accountId) == 0) {

				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'Formato de id de cliente no se encontr�';

				return view($defaultView)->with($returnParams);

			}

			// Client id
			if(!is_int($clientId) && !is_long($clientId) && !is_string($clientId) ) {

				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'No se encontr� id de cliente';

				return view($defaultView)->with($returnParams);

			}

			if(is_string($clientId) && preg_match('/[0-9]+/', $clientId) == 0) {

				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'Formato de id de cliente no se encontr�';

				return view($defaultView)->with($returnParams);

			}

			$response = $this->guzzleHttp_client->post($this->base_url . 'aws/agent/getInstances', $httpParams);
			$contentResponse = $response->getBody()->getContents();
			$contentResponse = json_decode($contentResponse, true);

			if(array_key_exists('message', $contentResponse)) {

				$defaultView = 'admin.aws.error';
				$returnParams['errorMessage'] = 'Error al tratar de obtener las instancias del cliente. C�digo de error de AWS: ' . $contentResponse['message'];

				return view($defaultView)->with($returnParams);
			}

			if(array_key_exists('Instances', $contentResponse) && count($contentResponse['Instances']) > 0) {
				$instancesList = $contentResponse['Instances'];
			}

			for($i = 0; $i < count($instancesList); $i++) {
				$tagsList = $instancesList[$i]['Tag'];

				$instanceName = null;
				for($j = 0; $j < count($tagsList); $j++) {
					if(array_key_exists('Key', $tagsList[$j]) && array_key_exists('Value', $tagsList[$j])) {
						if($tagsList[$j]['Key'] === 'Name') {
							$instanceName = $tagsList[$j]['Value'];
						}
						elseif($tagsList[$j]['Key'] === 'aws:servicecatalog:productArn') {
							if(isset($instanceName)) {
								break;
							}
							else {
								$instanceName = $tagsList[$j]['Value'];
							}
						}
					}
				}
				$instancesList[$i]['NameInstance'] = $instanceName;

			}

			$returnParams['instanceType'] = $type;
			$returnParams['accountId'] = $accountId;
			$returnParams['clientId'] = $clientId;
			$returnParams['instancesList'] = $instancesList;
	
			return view($defaultView)->with($returnParams);

		}
		catch(ConnectException $e) {

			return $e->getMessage();

		}
		finally {

			// Nope

		}

	}

	public static function rawRequest($params, $postParams, $method) {
		$httpGetUsersURI = implode('/', $params);
		$httpGetUsersURL = self::$base_url_static . $httpGetUsersURI;
		$method = strtoupper($method);
		if(self::isAvailableMethod($method)) {
			$httpGetUserRequestType = $method;
		}
		else {
			$httpGetUserRequestType = 'POST';
		}

		$httpParams =[
			'json' => json_decode($postParams, true)
		];

		$contentResponse = null;

		$guzzleHttp_client = new Client(['http_errors' => false, 'verify' => false]);
		$response = $guzzleHttp_client->request($httpGetUserRequestType, $httpGetUsersURL, $httpParams);
		if($response->getStatusCode() === 200) {

			$contentResponse = $response->getBody()->getContents();
			$contentResponse = json_decode($contentResponse, true);
		
		}

		return $contentResponse;
	}

	/**
	 * Funciones privadas
	 */

	private function listUsers() {
		$httpGetUsersURI = 'aws/dataBase/getUsers';
		$httpGetUsersURL = $this->base_url . $httpGetUsersURI;
		$httpGetUserRequestType = 'POST';
		$httpParams = [
			'json' => [
				'Credential' => [
					'Accountid' => ''
				]
			]
		];
		$usersList = [
			'statusCode' => 'ok',
			'statusMessage' => 'Ning�n mensaje',
			'data' => []
		];

		// Se obtiene el listado de los usuarios
		$response = $this->guzzleHttp_client->request($httpGetUserRequestType, $httpGetUsersURL, $httpParams);

		// Se valida el c�digo de respuesta
		if($response->getStatusCode() === 200) {

			$contentResponse = $response->getBody()->getContents();
			$contentResponse = json_decode($contentResponse, true);

			// Valida que se tenga exista llave User
			if( array_key_exists('User', $contentResponse) ) {

				$usersList['statusCode'] = 'ok';
				$usersList['statusMessage'] = 'Se encuentran ' . count($contentResponse['User']) . ' usuarios';
				$usersList['data'] = $contentResponse['User'];

			}

			// En caso de que no tenga campo User
			else {

				$usersList['statusCode'] = 'fail';
				$usersList['statusMessage'] = 'Error al consultar (' . $httpGetUsersURI . '): No se encontr� campo User.';
				Log::warning('No se encontr� campo User. Consulta ' . $httpGetUserRequestType . ' ' . $httpGetUsersURL . ' ' . json_encode($httpParams));

			}

		}

		// En caso de que el c�digo sea distinto a 200
		else {

			$usersList['statusCode'] = 'fail';
			$usersList['statusMessage'] = 'Error al consultar (' . $httpGetUsersURI . '): C�digo de error HTTP ' . $response->getStatusCode();
			Log::warning('Error ' . $response->getStatusCode() . ' Consulta ' . $httpGetUserRequestType . ' ' . $httpGetUsersURL . ' ' . json_encode($httpParams));

		}

		return $usersList;
	}

	private function listClients() {
		$httpGetCustomersURI = 'aws/dataBase/getCustomers';
		$httpGetCustomersURL = $this->base_url . $httpGetCustomersURI;
		$httpGetCustomerRequestType = 'POST';
		$httpParams = [
			'json' => [
				'Credential' => [
					'Accountid' => ''
				]
			]
		];
		$customersList = [
			'statusCode' => 'ok',
			'statusMessage' => 'Ning�n mensaje',
			'data' => []
		];

		// Se obtiene lista de clientes
		$response = $this->guzzleHttp_client->request($httpGetCustomerRequestType, $httpGetCustomersURL, $httpParams);

		// Se valida el c�digo de respuesta
		if($response->getStatusCode() === 200) {

			$contentResponse = $response->getBody()->getContents();
			$contentResponse = json_decode($contentResponse, true);

			// Valida que se tenga exista llave Customers
			if( array_key_exists('Customers', $contentResponse) ) {

				$customersList['statusCode'] = 'ok';
				$customersList['statusMessage'] = 'Se encontraron ' . count($contentResponse['Customers']) . ' clientes';
				$customersList['data'] = $contentResponse['Customers'];

			}

			// En caso de que no exista llave Customers
			else {

				$usersList['statusCode'] = 'fail';
				$usersList['statusMessage'] = 'Error al consultar (' . $httpGetCustomersURI . '): No se encontr� campo Customers.';
				Log::warning('No se encontr� campo Customers. Consulta ' . $httpGetCustomerRequestType . ' ' . $httpGetCustomersURL . ' ' . json_encode($httpParams));

			}

		}

		// En caso de que el c�digo sea distinto a 200
		else {

			$customersList['statusCode'] = 'fail';
			$customersList['statusMessage'] = 'Error al consultar (' . $httpGetCustomersURI . '): C�digo de error HTTP ' . $response->getStatusCode();
			Log::warning('Error ' . $response->getStatusCode() . ' Consulta ' . $httpGetCustomerRequestType . ' ' . $httpGetCustomersURL . ' ' . json_encode($httpParams));

		}

		return $customersList;
	}

	private function listServiceTypes() {
		$httpGetServiceTypesURI = 'aws/dataBase/getTypeServices';
		$httpGetServiceTypesURL = $this->base_url . $httpGetServiceTypesURI;
		$httpGetServiceTypesRequestType = 'POST';
		$httpParams = [
			'json' => [
				'Credential' => [
					'Name' => ''
				]
			]
		];
		$serviceTypesList = [
			'statusCode' => 'ok',
			'statusMessage' => 'Ning�n mensaje',
			'data' => []
		];

		// Se obtiene lista de clientes
		$response = $this->guzzleHttp_client->request($httpGetServiceTypesRequestType, $httpGetServiceTypesURL, $httpParams);

		// Se valida el c�digo de respuesta
		if($response->getStatusCode() === 200) {

			$contentResponse = $response->getBody()->getContents();
			$contentResponse = json_decode($contentResponse, true);

			// Valida que se tenga usuarios
			if(array_key_exists('User', $contentResponse)) {

				$usersList['statusCode'] = 'ok';
				$usersList['statusMessage'] = 'Se encuentran ' . count($contentResponse['User']) . ' usuarios';
				$serviceTypesList['data'] = $contentResponse['User'];

			}
			else {

				$usersList['statusCode'] = 'fail';
				$usersList['statusMessage'] = 'Error al consultar (' . $httpGetServiceTypesURI . '): No se encontr� campo User.';
				Log::warning('No se encontr� campo User. Consulta ' . $httpGetServiceTypesRequestType . ' ' . $httpGetServiceTypesURL . ' ' . json_encode($httpParams));

			}

		}
		else {

			$customersList['statusCode'] = 'fail';
			$customersList['statusMessage'] = 'Error al consultar (' . $httpGetServiceTypesURI . '): C�digo de error HTTP ' . $response->getStatusCode();
			Log::warning('Error ' . $response->getStatusCode() . ' Consulta ' . $httpGetServiceTypesRequestType . ' ' . $httpGetServiceTypesURL . ' ' . json_encode($httpParams));

		}

		return $serviceTypesList;
	}

	private function listInstances() {
		$httpGetInstancesURI = 'aws/agent/getInstances';
		$httpGetInstancesURL = $this->base_url . $httpGetInstancesURI;
		$httpGetInstancesRequestType = 'POST';
		$instancesList = [
			'statusCode' => 'ok',
			'statusMessage' => 'Ning�n mensaje',
			'data' => []
		];

		$serviceTypesList = $this->listServiceTypes();
		$usersList = $this->listUsers();

		$c_instanceList = [];
		$c_statusMessage = [];

		if(array_key_exists('data', $serviceTypesList) && is_array($serviceTypesList['data'])) {

			if(array_key_exists('data', $usersList) && is_array($usersList['data'])) {

				for($i = 0; $i < count($serviceTypesList['data']); $i++) {

					if(array_key_exists('Nombre', $serviceTypesList['data'][$i])) {

						for($j = 0; $j < count($usersList['data']); $j++) {

							if(array_key_exists('Idaws', $usersList['data'][$j])) {

								$serviceTypeNombre = $serviceTypesList['data'][$i]['Nombre'];
								$AWSid = $usersList['data'][$j]['Idaws'];
								$accountsList = $this->getAccountsUserAWSId($AWSid);

								if(array_key_exists('data', $accountsList) && is_array($accountsList['data'])) {

									for($k = 0; $k < count($accountsList['data']); $k++) {

										if(array_key_exists('Id', $accountsList['data'][$k])) {

											$accountId = $accountsList['data'][$k]['Id'];

											$httpParams = [
												'json' => [
													'Credential' => [
														'TypeSession' => 2,
														'Accountid' => $AWSid,
														'Id' =>$accountId
													],
													'TypeInstance' => $serviceTypeNombre,
													'Instances' => []
												]
											];

											$response = $this->guzzleHttp_client->post($httpGetInstancesRequestType, $httpGetInstancesURL, $httpParams);

											// Se valida el c�digo de respuesta
											if($response->getStatusCode() === 200) {

												$contentResponse = $response->getBody()->getContents();
												$contentResponse = json_decode($contentResponse, true);

												if(array_key_exists('Instances', $contentResponse)) {

													$c_instanceList = array_merge($contentResponse['Instances'], $c_instanceList);

												}
												else{

													// -- No se encontr� campo Instance
													array_push($c_statusMessage, 'No se encontr� campo Instances. Consulta ' . $httpGetInstancesRequestType . ' ' . $httpGetInstancesURL . ' ' . json_encode($httpParams));

												}

											}
											else {

												// -- Error 500
												array_push($c_statusMessage, 'Error al consultar (' . $httpGetInstancesURI . '): C�digo de error HTTP ' . $response->getStatusCode());

											}

										}
										else {

											// -- No se encontr� campo Id
											array_push($c_statusMessage, 'No se encontr� campo Id. Al listar las cuentas');

										}

									}

								}
								else {

									// -- No se encontr� campo data
									array_push($c_statusMessage, 'No se encontr� campo data. Al listar las cuentas');

								}

							}
							else {

								// -- No se encontr� campo Idaws
								array_push($c_statusMessage, 'No se encontr� campo Idaws. Al listar usuarios');

							}

						}

					}
					else {

						// -- No se encontr� campo nombre
						array_push($c_statusMessage, 'No se encontr� campo Nombre. Al listar los tipos de servicios');

					}

				}

				$instancesList['data'] = $c_instanceList;
				if(count($c_statusMessage) > 0) {
					$instancesList['statusCode'] = 'fail';
					$instancesList['statusMessage'] = implode(' ', $c_statusMessage);
					Log::warning(implode(' ', $c_statusMessage));
				}

			}
			else {

				$instancesList['statusCode'] = 'fail';
				$instancesList['statusMessage'] = 'Error al obtener lista de Usuarios: No se encontr� campo data.';

			}

		}
		else {

			$instancesList['statusCode'] = 'fail';
			$instancesList['statusMessage'] = 'Error al obtener lista de tipo de servicios: No se encontr� campo data.';

		}

		return $instancesList;
	}

	private function listEC2Instances() {
		return $this->listInstancesByType('ec2');
	}

	private function listRDSInstances() {
		return $this->listInstancesByType('rds');
	}

	private function listInstancesByType($type) {
		$httpGetInstancesURI = 'aws/agent/getInstances';
		$httpGetInstancesURL = $this->base_url . $httpGetInstancesURI;
		$httpGetInstancesRequestType = 'POST';
		$instancesList = [];

		$usersList = $this->listUsers();

		for($j = 0; $j < count($usersList); $j++) {
			$AWSid = $usersList[$j]['Idaws'];

			$accountsList = $this->getAccountsUserAWSId($AWSid)['data'];

			for($k = 0; $k < count($accountsList); $k++) {
				$accountId = $accountsList[$k]['Id'];

				$httpParams = [
					'json' => [
						'Credential' => [
							'TypeSession' => 2,
							'Accountid' => $AWSid,
							'Id' =>$accountId
						],
						'TypeInstance' => $type,
						'Instances' => []
					]
				];

				$response = $this->guzzleHttp_client->request($httpGetInstancesRequestType, $httpGetInstancesURL, $httpParams);

				// Se valida el c�digo de respuesta
				if($response->getStatusCode() === 200) {

					$contentResponse = $response->getBody()->getContents();
					$contentResponse = json_decode($contentResponse, true);

					if(array_key_exists('Instances', $contentResponse) && count($contentResponse['Instances']) > 0) {
						$instancesList = array_merge($contentResponse['Instances'], $instancesList);
					}

				}
				else {

					Log::warning('Error ' . $response->getStatusCode() . ' Consulta ' . $httpGetInstancesRequestType . ' ' . $httpGetInstancesURL . ' ' . json_encode($httpParams));

				}
			}
		}

		return $instancesList;
	}

	private function listAlerts() {
		$httpGetDescribeAlarmsURI = 'aws/agent/getDescribeAlarms';;
		$httpGetDescribeAlarmsURL = $this->base_url . $httpGetDescribeAlarmsURI;
		$httpGetDescribeAlarmsType = 'POST';
		$usersList = $this->listUsers();
		$alertsList = [
			'statusCode' => 'ok',
			'statusMessage' => 'Ning�n mensaje',
			'data' => []
		];

		if( array_key_exists('statusCode', $usersList) && $usersList['statusCode'] === 'ok' ) {

			if( array_key_exists('data', $usersList) ) {

				$usersList = $usersList['data'];

				$c_alerts = [];
				for($j = 0; $j < count($usersList); $j++) {

					$AWSid = $usersList[$j]['Idaws'];
					$accountsList = $this->getAccountsUserAWSId($AWSid)['data'];
		
					for($k = 0; $k < count($accountsList); $k++) {

						$accountId = $accountsList[$k]['Id'];
		
						$httpParams = [
							'json' => [
								'Credential' => [
									'TypeSession' => 2,
									'Accountid' => $AWSid,
									'Id' =>$accountId
								],
								'TypeInstance' => 'cloudwatch',
								'AlarmNames' => []
							]
						];
		
						$response = $this->guzzleHttp_client->request($httpGetDescribeAlarmsType, $httpGetDescribeAlarmsURL, $httpParams);
		
						if($response->getStatusCode() === 200) {
		
							$contentResponse = $response->getBody()->getContents();
							$contentResponse = json_decode($contentResponse, true);
		
							if(array_key_exists('MetricAlarms', $contentResponse) && count($contentResponse['MetricAlarms']) > 0) {
								$c_alerts = array_merge($contentResponse['MetricAlarms'], $c_alerts);
							}
		
						}
						else {

							$alertsList['statusCode'] = 'fail';
							$alertsList['statusMessage'] = 'Error al consultar (' . $httpGetDescribeAlarmsURI . '): C�digo de error HTTP ' . $response->getStatusCode();
							Log::warning('Error ' . $response->getStatusCode() . ' Consulta ' . $httpGetDescribeAlarmsType . ' ' . $httpGetDescribeAlarmsURL . ' ' . json_encode($httpParams));
		
						}
		
					}
		
				}

				if(count($c_alerts) > 0) {
					$alertsList['statusCode'] = 'ok';
					$alertsList['statusMessage'] = 'Se encuentra ' . count($c_alerts) . ' alertas';
					$alertsList['data'] = $c_alerts;
				}

			}
			else {

				$alertsList['statusCode'] = 'fail';
				$alertsList['statusMessage'] = 'Error al obtener usuarios: No se pudo obtener lista de usuarios';

			}

		}
		else {

			$alertsList['statusCode'] = 'fail';
			$alertsList['statusMessage'] = 'Error al obtener usuarios: No se pudo obtener lista de usuarios';

		}

		return $alertsList;
	}

	private function getAccountsUserAWSId($AWSid) {
		$httpGetAccountURI = 'aws/agent/getAccount';
		$httpGetAccountURL = $this->base_url . $httpGetAccountURI;
		$httpGetAccountRequestType = 'POST';
		$accountsList = [
			'statusCode' => 'ok',
			'statusMessage' => 'Ning�n mensaje',
			'data' => []
		];

		if(strlen($AWSid) > 0) {

			$httpParams = [
				'json' => [
					'Credential' => [
						'TypeSession' => 1,
						'Accountid' => $AWSid
					],
					'TypeInstance' => 'organizations'
				]
			];

			$response = $this->guzzleHttp_client->request($httpGetAccountRequestType, $httpGetAccountURL, $httpParams);

			// Se valida el c�digo de respuesta
			if($response->getStatusCode() === 200) {

				$contentResponse = $response->getBody()->getContents();
				$contentResponse = json_decode($contentResponse, true);

				if(array_key_exists('Accounts', $contentResponse)) {

					$accountsList['statusCode'] = 'ok';
					$accountsList['statusMessage'] = 'Se encuentra ' . count($contentResponse['Accounts']) . ' cuentas';
					$accountsList['data'] = $contentResponse['Accounts'];

				}
				else {

					$accountsList['statusCode'] = 'fail';
					$accountsList['statusMessage'] = 'Error al consultar (' . $httpGetAccountURI . '): No se encontr� campo Accounts';

					Log::warning('Error al consultar (' . $httpGetAccountURI . '): No se encontr� campo Accounts');
				}

			}
			else {

				$accountsList['statusCode'] = 'fail';
				$accountsList['statusMessage'] = 'Error al consultar (' . $httpGetAccountURI . '): C�digo de error HTTP ' . $response->getStatusCode();

				Log::warning('Error ' . $response->getStatusCode() . ' Consulta ' . $httpGetAccountRequestType . ' ' . $httpGetAccountURL . ' ' . json_encode($httpParams));

			}

		}
		else {

			$accountsList['statusCode'] = 'fail';
			$accountsList['statusMessage'] = 'Id de cuenta no es valida ' . $AWSid;

			Log::warning('Id de cuenta no es valida ' . $AWSid);

		}

		return $accountsList;
	}

	private function getClientNameById(string $accountId, string $clientId) {
		$httpGetAccountURL = $this->base_url . 'aws/agent/getAccount';
		$httpGetAccountRequestType = 'POST';
		$clientName = null;
		$httpParams = [
			'json' => [
				'Credential' => [
					'TypeSession' => 1,
					'Accountid' => $accountId,
				],
				'TypeInstance' => 'organizations'
			]
		];

		$response = $this->guzzleHttp_client->request($httpGetAccountRequestType, $httpGetAccountURL, $httpParams);
		
		// Se valida el c�digo de respuesta
		if($response->getStatusCode() === 200) {

			$contentResponse = $response->getBody()->getContents();
			$contentResponse = json_decode($contentResponse, true);

			if( array_key_exists('Accounts', $contentResponse) && count($contentResponse['Accounts']) > 0 ) {

				$accounts = $contentResponse['Accounts'];

				for($i = 0; $i < count($accounts); $i++) {

					if( (string)$accounts[$i]['Id'] == (string)$clientId ) {
						$clientName = $accounts[$i]['Name'];
					}

				}
			}

		}

		// En caso de que el c�digo sea distinto a 200
		else {

			Log::warning('Error ' . $response->getStatusCode() . ' Consulta ' . $httpGetAccountRequestType . ' ' . $httpGetAccountURL . ' ' . json_encode($httpParams));

		}

		return $clientName;

	}

	private static function isAvailableMethod(string $method) {
		for($i = 0; $i < count(self::$availableMethods); $i++) {
			if(strtoupper(self::$availableMethods[$i]) === strtoupper($method)) {
				return true;
			}
		}
		return false;
	}
}
