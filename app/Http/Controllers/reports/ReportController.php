<?php

namespace App\Http\Controllers\reports;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;

class ReportController extends Controller
{
    public function homeReports()
    {
        $jasper_report_name = "SQLNagiosReport.jasper";
        $jasper_report_name_without_file_ext = pathinfo($jasper_report_name, PATHINFO_FILENAME);
        $jasper_directory = "report_created";

        $path_jar_reporter = Storage::disk('public')->path("jasper_reporter/jasperReporter.jar");

        $driver = "mssql";
        $ip_localhost = "";
        $port = "";
        $database_name = "";
        $username = "";
        $password = "";
        $jasper_file_path = Storage::disk('jasper_folder')->path($jasper_report_name);
        $destination_path_reports = Storage::disk('jasper_folder')->path($jasper_directory);

        $process = new Process([
            'java', '-jar', $path_jar_reporter, $driver, $ip_localhost, $port, $database_name,
            $username, $password, $jasper_file_path, $destination_path_reports
        ]);

        $process->run();

        $output = "msgError: " . $process->getErrorOutput() . "\n";
        $output .= "msg: " . $process->getOutput() . "\n";
        $output .= "exitCode: " . $process->getExitCode();

        if ($process->getExitCode() > 0) {
            return $output;
        }

        $reporte_html = Storage::disk('jasper_folder')->get($jasper_directory . "/" .
            $jasper_report_name_without_file_ext . ".html");

        // report paths
        $url_path_report_pdf = Storage::disk('jasper_folder')->url($jasper_directory . "/" .
            $jasper_report_name_without_file_ext . ".pdf");

        $url_path_report_html = Storage::disk('jasper_folder')->url($jasper_directory . "/" .
            $jasper_report_name_without_file_ext . ".html");

        $url_path_report_xls = Storage::disk('jasper_folder')->url($jasper_directory . "/" .
            $jasper_report_name_without_file_ext . ".xls");

        return view('reports/home')->with([
            'title' => 'Reportes',
            'breadcrumb' => [
                [
                    'title' => 'Reportes',
                    'url' => '',
                ],
                [
                    'title' => Auth::user()->name,
                ],
            ],
            'reporte_html' => $reporte_html,
            'url_path_report_pdf' => $url_path_report_pdf,
            'url_path_report_html' => $url_path_report_html,
            'url_path_report_xls' => $url_path_report_xls,
        ]);
    }
}
