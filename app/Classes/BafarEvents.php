<?php

namespace App\Classes;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

class BafarEvents
{

    //funcion que permite pbtener una lista de correo del buzon del cliente Bafar por medio
    //de una api REST
    function getEmailListFromEndpointRest()
    {
        $urlEndpoint = 'https://172.20.45.129:8080/process/email/bafar';
        $client = new Client(['verify' => false]);
        $obtainedEmails = [];

        try {
            $response = $client->request('GET', $urlEndpoint, [
                'auth' => ['user', '12345']
            ]);
            Log::info('Realizando peticion rest al endpoint para obtencion de correos del cliente Bafar...');
            $obtainedEmails = json_decode($response->getBody(), true);
            Log::info('lo que se obtuvo: ' . var_dump(json_encode($obtainedEmails)));
        } catch (ClientException $e) {
            Log::error('Sucedio un error al realizar la peticion de correo del cliente Bafar: ' .
                $e->getMessage());
        }

        return $obtainedEmails;
    }

    //funcion la cual se encarga de generar un template json con los datos obtenidos de un correo del buzon del
    //cliente Bafar. Estos formatos/campos son necesarios para que lo lea el endpoint de la integracion
    //php del cliente Bafar
    function generateJsonBodyForObmEvent($email)
    {
        $idServidor = 92;
        $jsonEmailEvent = [];

        //mapeado de campos obtenidos de cada correo del buzon del cliente Bafar
        $jsonEmailEvent['emailBody'] = $email['Body'];
        $jsonEmailEvent['dateEmail'] = $email['Date'];
        $jsonEmailEvent['fromEmail'] = $email['From'];
        $jsonEmailEvent['subjectEmail'] = $email['Subject'];
        $jsonEmailEvent['toEmail'] = $email['To'];
        $jsonEmailEvent['id_Servidor'] = $idServidor;
        $jsonEmailEvent['subject'] = 'Ms August';

        $gravedad = 'Critical';
        $prioridad = 'Highest';
        $categoria = 'Status/Storage';
        $subcategoria = 'Almacenamiento';
        $titulo = 'ITOC - A / Storage / Bafar / stnimble01 / subject';
        $descripcion = $jsonEmailEvent['emailBody'];
        $aplicacion = 'Nimble';
        $cliente = 'Bafar';
        $solicitud = 'Favor de generar un IM a Almacenamiento y fuera del horario ' .
            'de oficina comunicarse con el personal de guardia.';
        $plataforma = 'Externa';
        $hostname = 'stnimble01';
        $responsable = 'Torre Storage';
        $proveedor = 'HP';
        $correo = 'notificacionesbwtriara@bafar.com.mx';
        $instrucciones = 'Verificar que se haya recibido el correo de la cuenta ' .
            'notificacionesbwtriara@bafar.com.mx y generar el IM a personal de ' .
            'Almacenamiento ( Storage )';

        $jsonEmailEvent['subject'] = $titulo;
        $jsonEmailEvent['Descripcion'] = $descripcion;
        $jsonEmailEvent['Gravedad'] = $gravedad;
        $jsonEmailEvent['Prioridad'] = $prioridad;
        $jsonEmailEvent['Categoria'] = $categoria;
        $jsonEmailEvent['Subcategoria'] = $subcategoria;
        $jsonEmailEvent['Aplicacion'] = $aplicacion;
        $jsonEmailEvent['Cliente'] = $cliente;
        $jsonEmailEvent['Plataforma'] = $plataforma;
        $jsonEmailEvent['Hostname'] = $hostname;
        $jsonEmailEvent['Solicitud'] = $solicitud;
        $jsonEmailEvent['Responsable'] = $responsable;
        $jsonEmailEvent['Instrucciones'] = $instrucciones;
        $jsonEmailEvent['Fecha_Creacion'] = $jsonEmailEvent['dateEmail'];
        $jsonEmailEvent['Correo'] = $correo;
        $jsonEmailEvent['Proveedor'] = $proveedor;

        Log::info('Cuerpo del correo:' . $jsonEmailEvent['emailBody']);
        Log::info('Fecha del correo:' . $jsonEmailEvent['dateEmail']);
        Log::info('Remitente del correo:' . $jsonEmailEvent['fromEmail']);
        Log::info('Titulo del correo:' . $jsonEmailEvent['subjectEmail']);
        Log::info('Destinatario del correo:' . $jsonEmailEvent['toEmail']);
        Log::info('Id Servidor para el mapeo del JSON:' . $jsonEmailEvent['id_Servidor']);

        return $jsonEmailEvent;
    }

    function sendListEmailJsonBafarIntegration($emailList)
    {
        $urlEndpoint = 'https://172.20.45.172:8088/evento/integracion/bafar';
        $client = new Client();
        $obtainedEmails = [];

        try {
            $response = $client->request('POST', $urlEndpoint, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
                'body' => json_encode($emailList),
                'verify' => false
            ]);
            Log::info('Realizando peticion rest al endpoint de la integracion PHP para envio de eventos al BSM-OMI...');
            $obtainedEmails = json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            Log::error('Sucedio un error al realizar la peticion de correo del cliente Bafar: ' .
                $e->getMessage());
        }

        return $obtainedEmails;
    }

    //funcion que permite el enviar eventos al omi por medio de una lista de correos obtenidos
    //anteriormente a la consulta de la API REST
    function generateEventsFromEmailList($emailList)
    {
        $eventListBafar = [];

        if (count($emailList) > 0) {
            Log::info('Se localizaron en total ' . count($emailList) .
                ' correos, a continuacion se procede a generar los eventos en OMI/OBM...');

            foreach ($emailList as $email) {

                $jsonEmail = $this->generateJsonBodyForObmEvent($email);
                array_push($eventListBafar, $jsonEmail);
            }
        } else {
            Log::info('La lista de correos del cliente Bafar se encuentra vacia, ' .
                'no es necesario procesar correo alguno');
        }

        //se envia la lista de eventos email en formato json a la integracion de Bafar
        Log::info('Enviando la lista de eventos a la integracion php via REST...');
        return $eventListBafar;
    }
}
