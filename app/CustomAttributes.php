<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomAttributes extends Model
{
    use HasFactory;
    protected $table = 'temp_custom_attribute';
    protected $guarded = [];
}
