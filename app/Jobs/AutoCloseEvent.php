<?php

namespace App\Jobs;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Exception\BadResponseException;

class AutoCloseEvent implements ShouldQueue 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $events = $this->getSMTP2GOEvents();

        foreach ($events as $event) {
            $result = $this->closeEventInOMI($event);

            Log::info('RESULTADO CIERRE EVENTO OMI SMTP2GO = ' . print_r($result, true));

            if ($result['response']) {
                $this->closeSMTP2GOEvent($event);
            }
        }
        
        // --> Daniel
        $events = $this->getEventBridgeEvents();
        foreach ($events as $event) {
            $result = $this->closeEventInOMI($event);

            Log::info('RESULTADO CIERRE EVENTO OMI EVENTBRIDGE = ' . print_r($result, true));

            if ($result['response']) {
                $this->closeEventBridgeEvent($event);
            }
        }
    }

    private function getEventBridgeEvents() {
        $eventBridgeEvents = [];

        $events = DB::select("SELECT b.event_id omi_id, a.event_id, b.created_at FROM track_event_bridge_aws a LEFT JOIN track_events b ON a.id COLLATE utf8mb4_unicode_ci = b.id WHERE a.cleared = 0");

        foreach ($events as $event) {
            $createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $event->created_at, 'America/Mexico_City');

            $current = Carbon::now()->timezone('America/Mexico_City');

            $diff = $current->diffInHours($createdAt);

            if ($diff >= 1) {
                $eventBridgeEvents[] = $event;
            }
        }

        return $eventBridgeEvents;
    }

    private function closeEventBridgeEvent($event) {
        DB::update('UPDATE track_event_bridge_aws SET cleared = ? WHERE event_id = ?', [1, $event->event_id]);

        DB::update('UPDATE track_events SET closed = ?, closed_at = ? WHERE id = ?', [1, date('Y-m-d H:i:s'), $event->omi_id]);
    }

    private function getSMTP2GOEvents() {
        $smtp2goEvents = [];

        $events = DB::select("SELECT id AS omi_id, t1.event_id, created_at FROM track_smtp2go_events AS t1 INNER JOIN track_events AS t2 ON t1.event_id = t2.event_id WHERE t1.closed = 0");

        foreach ($events as $event) {
            $createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $event->created_at, 'America/Mexico_City');

            $current = Carbon::now()->timezone('America/Mexico_City');

            $diff = $current->diffInHours($createdAt);

            Log::info("EVENTO SMTP2GO | CreatedAt: {$createdAt->toDateTimeString()} | Current: {$current->toDateTimeString()} | Diff: $diff");

            if ($diff >= 1) {
                $smtp2goEvents[] = $event;
            }
        }

        return $smtp2goEvents;
    }

    private function closeSMTP2GOEvent($event) {
        DB::update('UPDATE track_smtp2go_events SET closed = ? WHERE event_id = ?', [1, $event->event_id]);

        DB::update('UPDATE track_events SET closed = ?, closed_at = ? WHERE id = ?', [1, date('Y-m-d H:i:s'), $event->omi_id]);
    }

    private function closeEventInOMI($event) {
        $omi = trim($event->omi_id);

        if (isset($omi) && (strlen($omi) == 36)) {
            try {
                $client = new Client();

                $response = $client->request('PUT', "http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list/$omi", [
                    'auth' => ['OPC.Zenoss', '+0ruge5.X3m137-'],
                    'headers' => ['Content-Type' => 'application/xml'],
                    'body' => "<event xmlns='http://www.hp.com/2009/software/opr/data_model'><id>$omi</id><state>closed</state></event>"
                ]);

                if ($response->getStatusCode() == 200) {
                    $output = ['response' => true, 'details' => $response->getBody()->getContents()];
                } else {
                    $output = ['response' => false, 'details' => "Código de Error: {$response->getStatusCode()}"];
                }
            } catch (BadResponseException $exception) {
                $response = $exception->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();

                $output = ['response' => false, 'details' => $responseBodyAsString];
            }
        } else {
            $output = ['response' => false, 'details' => "ID is not valid, it was not possible to close this event in OMI. ID: $omi"];
        }

        return $output;
    }

}