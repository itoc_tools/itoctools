<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Exception\BadResponseException;

class RepeatNotification implements ShouldQueue 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $notifications = $this->getFailedNotifications();

        foreach ($notifications as $notification) {
            $this->closeFailedNotification($notification);

            $result = $this->sendRequest2API($notification);

            Log::info('RESULTADO REINTENTO NOTIFICACION FALLIDA = ' . print_r($result, true));
        }
    }

    private function getFailedNotifications() {
        $notifications = DB::select("SELECT * FROM itoc_failed_notifications WHERE notificado = 0");

        return $notifications;
    }

    private function closeFailedNotification($notification) {
        DB::update('UPDATE itoc_failed_notifications SET notificado = ? WHERE regla = ? AND evento = ? AND aplicacion = ?', [1, $notification->regla, $notification->evento, $notification->aplicacion]);
    }

    private function sendRequest2API($notification) {
        $data = ['IDRegla' => $notification->regla, 'evid_OMI_regenerado' => trim($notification->evento), 'Aplicacion' => $notification->aplicacion];

        try {
            $client = new Client();

            $response = $client->request('POST', 'http://172.20.45.172/itoc/omi/enriquece/evento', [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => json_encode($data)
            ]);

            if ($response->getStatusCode() == 200) {
                $output = ['response' => true, 'details' => $response->getBody()->getContents()];
            } else {
                $output = ['response' => false, 'details' => "Código de Error: {$response->getStatusCode()}"];
            }
        } catch (BadResponseException $exception) {
            $response = $exception->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            $output = ['response' => false, 'details' => $responseBodyAsString];
        }

        return $output;
    }
}