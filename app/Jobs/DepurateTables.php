<?php

namespace App\Jobs;

use App\FailedJobs;
use App\Jobs;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class DepurateTables implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $failedJobs_table = FailedJobs::count();
        $jobs_table = Jobs::count();

        //Get datetime witch carbon and remove events less than the date
        $endDate = Carbon::now()->subDay(10);
        $endDate_custom = Carbon::now()->subDay(1);
        DB::table('events')
            ->where('created_at', '<', $endDate)
            ->delete();

        DB::table('temp_custom_attribute')
            ->where('created_at', '<', $endDate_custom)
            ->delete();

        if ($failedJobs_table > 250) {
            $numToDeleteF = $failedJobs_table - 250;
            FailedJobs::orderBy('failed_at', 'asc')
                ->limit($numToDeleteF)
                ->delete();
        }
        if ($jobs_table > 250) {
            $numToDeleteJ = $jobs_table - 250;
            Jobs::orderBy('created_at', 'asc')
                ->limit($numToDeleteJ)
                ->delete();
        }
    }
}
