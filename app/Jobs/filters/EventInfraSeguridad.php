<?php

namespace App\Jobs\filters;

use App\EventsFilters;
use ErrorException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class EventInfraSeguridad implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sql_response = $this->sql_request();
        if ($sql_response["id"] == 1) {
            EventsFilters::where('filter_name', 'EventInfraSeguridad')->delete();
            $events = $sql_response['response'];
            foreach ($events as $value) {
                $this->createEvents($value);
            }
        } else {
            Log::info('EventInfraSeguridad Filter fail: ' . json_encode($sql_response));
        }
    }

    /** 
     * Execute SQL Request
     * 
     * @return Array $response
     */
    private function sql_request()
    {
        $response = array();
        $rows = array();
        $serverName = '172.20.45.141\SQL2K16,1443';
        $connectionOptions = array(
            "database" => "itoc_Admon_Herramientas",
            "uid" => "alberto.garcia",
            "pwd" => "X:~sVG.5g7t3pC0zC7q0"
        );

        $sql = "
        SELECT top (500) 
        t1.ID as id_event
        ,t1.APPLICATION as application
        ,us.USER_NAME as user_name
        ,t1.SUBCATEGORY as sub_category
		,t1.TYPE as type
		,us.USER_NAME as assigned_user
        ,grp.GRP_GROUP_NAME as assigned_group
        ,t1.CATEGORY as category
        ,t1.PRIORITY as priority 
        ,t1.SEVERITY as severity
        ,t1.STATE as state 
        ,t1.TIME_CHANGED as  time_changed
        ,t1.TIME_STATE_CHANGED
        ,t1.TIME_CREATED as time_created
        ,t1.TIME_RECEIVED as time_received
        ,t1.TITLE as title
        ,[OBJECT] as object
        ,t1.DESCRIPTION as description
		,t1.ORIGINAL_DATA as original_data
        ,t1.RELATED_CI_HINT as RELATED_CI_HINT
        ,STUFF((SELECT ' ___ ' + concat(t0.ID, ' ::: ', t0.VERSION, ' ::: ', t0.USER_ID, ' ::: ', t0.TIME_CHANGED, ' ::: ', t0.EVENT_REF)
             FROM [172.20.40.89].[BSMOMI].[dbo].HISTORY_LINE t0 with (nolock)
             WHERE t1.ID = t0.EVENT_REF
             FOR XML PATH('')), 1, 5, '') [symptom_list]
        ,STUFF((SELECT ' ___ ' + concat(t2.idx, ' ::: ', t2.elt)
             FROM [172.20.40.89].[BSMOMI].[dbo].[EVENT_CUSTOM_ATTRIBUTES] t2 with (nolock)
             WHERE t1.ID = t2.EVENT_ID
             FOR XML PATH('')), 1, 5, '') [custom_attribute_list]
        ,STUFF((SELECT ' ___ ' + concat(t3.TEXT, ' ::: ', t3.AUTHOR, ' ::: ', t3.TIME_CREATED)
             FROM [172.20.40.89].[BSMOMI].[dbo].[EVENT_ANNOTATIONS] t3 with (nolock)
             WHERE t1.ID = t3.EVENT_ID
             FOR XML PATH('')), 1, 5, '') [annotation_list]
        FROM [172.20.40.89].[BSMOMI].[dbo].[ALL_EVENTS] t1 with (nolock) 
        LEFT JOIN [172.20.40.89].[BSMMGNT].[dbo].[USERS] us with (nolock) on t1.ASSIGNED_USER =us.USER_ID
        LEFT JOIN [172.20.40.89].[BSMMGNT].[dbo].[GROUPS_AUTH] grp with(nolock) on t1.assigned_group =grp.GRP_GROUP_ID

        WHERE t1.STATE !='CLOSED' AND (UPPER(t1.TITLE) LIKE UPPER('%EBI ITOC%')
            OR UPPER(t1.TITLE) LIKE UPPER('%EBI TRIARA Monterrey%')
            OR UPPER(t1.TITLE) LIKE UPPER('%EBI TRIARA Queretaro%')
            OR UPPER(t1.TITLE) LIKE UPPER('%EBISER-MTY%')
            OR UPPER(t1.TITLE) LIKE UPPER('%EBISERVER-MTY%')
            OR UPPER(t1.TITLE) LIKE UPPER('%FORESEER%')
            OR UPPER(t1.TITLE) LIKE UPPER('%Infraestructura EBI%')
            OR UPPER(t1.TITLE) LIKE UPPER('%Infraestructura\/Infraestructura Climatizacion y Energia%')
            OR UPPER(t1.TITLE) LIKE UPPER('%Intellution%')
            OR UPPER(t1.TITLE) LIKE UPPER('%TQEBI%'))
        
        ORDER BY t1.TIME_RECEIVED DESC
        ";

        try {
            $conn = sqlsrv_connect($serverName, $connectionOptions);
            if ($conn === false) {
                if (($errors = sqlsrv_errors()) != null) {
                    $list_errors = json_encode($errors);
                    $response = array("id" => 0, "response" => "Error en la conexion a la Base de Datos" . $list_errors);
                }
                $response = array("id" => 0, "response" => "Error en la conexion a la Base de Datos");
            } else {
                $stmt = sqlsrv_query($conn, $sql);
                if ($stmt) {
                    while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
                        array_push($rows, $row);
                    }
                    $response = array("id" => 1, "response" => $rows);
                } else {
                    $response = array("id" => 0, "response" => "Error en SQL: sqlsrv_query fail");
                }
            }
        } catch (ErrorException $e) {
            $response = array("id" => 0, "response" => "SQL Serve ErrorException" . $e);
        }
        return $response;
    }

    /**
     * Execute the create.
     *
     * @return void
     */
    public function createEvents($value)
    {
        if (isset($value['id_event'])) {
            EventsFilters::create(
                [
                    'id_event' => $value['id_event'] ?? null,
                    'filter_name' => 'EventInfraSeguridad' ?? null,
                    //'total_size' => $total_size ?? null,
                    'annotation_list' => isset($value['annotation_list']) ? json_encode($value['annotation_list']) : null,
                    'application' => isset($value['application']) ? $value['application'] : null,
                    'assigned_group' => isset($value['assigned_group']) ? json_encode($this->replaceStrC($value['assigned_group'])) : null,
                    'assigned_user' => isset($value['assigned_user']) ? json_encode($this->replaceStrC($value['assigned_user'])) : null,
                    'category' => isset($value['category']) ? $value['category'] : null,
                    'ci_resolution_info' => isset($value['ci_resolution_info']) ? json_encode($this->replaceStrC($value['ci_resolution_info'])) : null,
                    'control_transferred' => isset($value['control_transferred']) ? $value['control_transferred'] : null,
                    'custom_attribute_list' => isset($value['custom_attribute_list']) ? json_encode($value['custom_attribute_list']) : json_encode([]),
                    'description' => isset($value['description']) ? $value['description'] : null,
                    'drilldown_url' => isset($value['drilldown_url']) ? $value['drilldown_url'] : null,
                    'duplicate_count' => isset($value['duplicate_count']) ? $value['duplicate_count'] : null,
                    'history_line_list_ref' => isset($value['history_line_list_ref']) ? json_encode($this->replaceStrC($value['history_line_list_ref'])) : null,
                    'instruction_available' => isset($value['instruction_available']) ? $value['instruction_available'] : null,
                    'log_only' => $value['log_only'] ?? null,
                    'originating_server' => isset($value['originating_server']) ? json_encode($value['originating_server']) : null,
                    'priority' => isset($value['priority']) ? $value['priority'] : null,
                    'received_as_notify' => isset($value['received_as_notify']) ? $value['received_as_notify'] : null,
                    'received_on_ci_downtime' => isset($value['received_on_ci_downtime']) ? $value['received_on_ci_downtime'] : null,
                    'relationships_included' => isset($value['relationships_included']) ? $value['relationships_included'] : null,
                    'self' => isset($value['self']) ? $value['self'] : null,
                    'sequence_number' => isset($value['sequence_number']) ? $value['sequence_number'] : null,
                    'severity' => isset($value['severity']) ? $value['severity'] : null,
                    'skip_duplicate_suppression' => isset($value['skip_duplicate_suppression']) ? $value['skip_duplicate_suppression'] : null,
                    'state' => isset($value['state']) ? $value['state'] : null,
                    'sub_category' => isset($value['sub_category']) ? $value['sub_category'] : null,
                    'symptom_list' => isset($value['symptom_list']) ? json_encode($value['symptom_list']) : null,
                    'time_changed' => isset($value['time_changed']) ? $value['time_changed'] : null,
                    'time_changed_label' => isset($value['time_changed_label']) ? $value['time_changed_label'] : null,
                    'time_created' => isset($value['time_created']) ? $value['time_created'] : null,
                    'time_created_label' => isset($value['time_created_label']) ? $value['time_created_label'] : null,
                    'time_received' => isset($value['time_received']) ? $value['time_received'] : null,
                    'time_received_label' => isset($value['time_received_label']) ? $value['time_received_label'] : null,
                    'title' => isset($value['title']) ? json_encode($this->replaceStrC($value['title'])) : null,
                    'type' => isset($value['type']) ? json_encode($this->replaceStrC($value['type'])) : null,
                    'version' => isset($value['version']) ? json_encode($this->replaceStrC($value['version'])) : null,
                    'user_name' => isset($value['user_name']) ? $this->replaceStrC($value['user_name']) : null,
                    'object' => isset($value['object']) ? $value['object'] : '',
                    'time_created_dt' => isset($value['time_created']) ? $value['time_created'] : null,
                    'time_received_dt' => isset($value['time_received']) ? $value['time_received'] : null,
                    'time_changed_dt' => isset($value['time_changed']) ? $value['time_changed'] : null,
                    'original_data' => isset($value['original_data']) ? $value['original_data'] : null,
                    'RELATED_CI_HINT' => isset($value['RELATED_CI_HINT']) ? $value['RELATED_CI_HINT'] : null
                ]
            );
        }
    }

    /** 
     * Replacing accented characters
     * 
     * @param String  $str
     * @return String  $str_r
     */
    public function replaceStrC($str)
    {
        $search = explode(",", "á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,Ã¡,Ã©,Ã­,Ã³,Ãº,Ã±,ÃÃ¡,ÃÃ©,ÃÃ­,ÃÃ³,ÃÃº,ÃÃ±,Ã“,Ã ,Ã‰,Ã ,Ãš,â€œ,â€ ,Â¿,ü,Ã‘");
        $replace = explode(",", "á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,Ó,Á,É,Í,Ú,\",\",¿,&uuml;,Ñ");
        $str_r = str_replace($search, $replace, $str);
        return $str_r;
    }
}
