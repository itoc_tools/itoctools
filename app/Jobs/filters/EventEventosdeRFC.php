<?php

namespace App\Jobs\filters;

use App\EventsFilters;
use ErrorException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class EventEventosdeRFC implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    //Handle para traer los eventos de la Base de Datos del Itco Plus
    public function handle()
    {
        $events_query = DB::select('SELECT * FROM events WHERE
            UPPER(custom_attribute_list) LIKE UPPER("%MS ::: RFC%")
            OR UPPER(custom_attribute_list) LIKE UPPER("%MS :::  RFC")
            ORDER BY time_received DESC');

        EventsFilters::where('filter_name', 'EventEventosdeRFC')->delete();
        foreach ($events_query as $value) {
            $value = (array)$value;
            $this->createEvents($value);
        }
    }


    /**
     * Execute the create.
     *
     * @return void
     */
    public function createEvents($value)
    {
        if (isset($value['id_event'])) {
            EventsFilters::create(
                [
                    'id_event' => $value['id_event'] ?? null,
                    'filter_name' => 'EventEventosdeRFC' ?? null,
                    //'total_size' => $total_size ?? null,
                    'annotation_list' => isset($value['annotation_list']) ? json_encode($value['annotation_list']) : null,
                    'application' => isset($value['application']) ? $value['application'] : null,
                    'assigned_group' => isset($value['assigned_group']) ? json_encode($this->replaceStrC($value['assigned_group'])) : null,
                    'assigned_user' => isset($value['assigned_user']) ? json_encode($this->replaceStrC($value['assigned_user'])) : null,
                    'category' => isset($value['category']) ? $value['category'] : null,
                    'ci_resolution_info' => isset($value['ci_resolution_info']) ? json_encode($this->replaceStrC($value['ci_resolution_info'])) : null,
                    'control_transferred' => isset($value['control_transferred']) ? $value['control_transferred'] : null,
                    'custom_attribute_list' => isset($value['custom_attribute_list']) ? json_encode($value['custom_attribute_list']) : json_encode([]),
                    'description' => isset($value['description']) ? $value['description'] : null,
                    'drilldown_url' => isset($value['drilldown_url']) ? $value['drilldown_url'] : null,
                    'duplicate_count' => isset($value['duplicate_count']) ? $value['duplicate_count'] : null,
                    'history_line_list_ref' => isset($value['history_line_list_ref']) ? json_encode($this->replaceStrC($value['history_line_list_ref'])) : null,
                    'instruction_available' => isset($value['instruction_available']) ? $value['instruction_available'] : null,
                    'log_only' => $value['log_only'] ?? null,
                    'originating_server' => isset($value['originating_server']) ? json_encode($value['originating_server']) : null,
                    'priority' => isset($value['priority']) ? $value['priority'] : null,
                    'received_as_notify' => isset($value['received_as_notify']) ? $value['received_as_notify'] : null,
                    'received_on_ci_downtime' => isset($value['received_on_ci_downtime']) ? $value['received_on_ci_downtime'] : null,
                    'relationships_included' => isset($value['relationships_included']) ? $value['relationships_included'] : null,
                    'self' => isset($value['self']) ? $value['self'] : null,
                    'sequence_number' => isset($value['sequence_number']) ? $value['sequence_number'] : null,
                    'severity' => isset($value['severity']) ? $value['severity'] : null,
                    'skip_duplicate_suppression' => isset($value['skip_duplicate_suppression']) ? $value['skip_duplicate_suppression'] : null,
                    'state' => isset($value['state']) ? $value['state'] : null,
                    'sub_category' => isset($value['sub_category']) ? $value['sub_category'] : null,
                    'symptom_list' => isset($value['symptom_list']) ? json_encode($value['symptom_list']) : null,
                    'time_changed' => isset($value['time_changed']) ? $value['time_changed'] : null,
                    'time_changed_label' => isset($value['time_changed_label']) ? $value['time_changed_label'] : null,
                    'time_created' => isset($value['time_created']) ? $value['time_created'] : null,
                    'time_created_label' => isset($value['time_created_label']) ? $value['time_created_label'] : null,
                    'time_received' => isset($value['time_received']) ? $value['time_received'] : null,
                    'time_received_label' => isset($value['time_received_label']) ? $value['time_received_label'] : null,
                    'title' => isset($value['title']) ? json_encode($this->replaceStrC($value['title'])) : null,
                    'type' => isset($value['type']) ? json_encode($this->replaceStrC($value['type'])) : null,
                    'version' => isset($value['version']) ? json_encode($this->replaceStrC($value['version'])) : null,
                    'user_name' => isset($value['user_name']) ? $this->replaceStrC($value['user_name']) : null,
                    'object' => isset($value['object']) ? $value['object'] : '',
                    'time_created_dt' => isset($value['time_created']) ? $value['time_created'] : null,
                    'time_received_dt' => isset($value['time_received']) ? $value['time_received'] : null,
                    'time_changed_dt' => isset($value['time_changed']) ? $value['time_changed'] : null,
                    'original_data' => isset($value['original_data']) ? $value['original_data'] : null,
                    'RELATED_CI_HINT' => isset($value['RELATED_CI_HINT']) ? $value['RELATED_CI_HINT'] : null
                ]
            );
        }
    }

    /** 
     * Replacing accented characters
     * 
     * @param String  $str
     * @return String  $str_r
     */
    public function replaceStrC($str)
    {
        $search = explode(",", "á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,Ã¡,Ã©,Ã­,Ã³,Ãº,Ã±,ÃÃ¡,ÃÃ©,ÃÃ­,ÃÃ³,ÃÃº,ÃÃ±,Ã“,Ã ,Ã‰,Ã ,Ãš,â€œ,â€ ,Â¿,ü,Ã‘");
        $replace = explode(",", "á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,Ó,Á,É,Í,Ú,\",\",¿,&uuml;,Ñ");
        $str_r = str_replace($search, $replace, $str);
        return $str_r;
    }
}
