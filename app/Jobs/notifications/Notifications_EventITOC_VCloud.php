<?php

namespace App\Jobs\notifications;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;


class Notifications_EventITOC_VCloud implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $ensure_mail = true;
        $sub_time = $this->subtract12Hrs();
        while ($ensure_mail) {
            try {
                $client = new Client();
                $size = '1000';
                $res = $client->request('POST', "http://bsmtelmexit.triara.mexico/opr-console/rest/9.10/event_list?alt=json&page_size={$size}", [
                    'auth' => ['OPC.Zenoss', '+0ruge5.X3m137-'],
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ],
                    'body'   => 'query=time_created LT ' . $sub_time . ' AND severity IN ("minor", "major", "warning", "critical")' .
                        'AND state IN ("open", "in_progress")  AND (application LIKE "$25Zabbix AMX$25" OR application LIKE "$25vRealize Operations$25")'
                ]);
                $events = $res->getBody()->getContents();
                $events = json_decode($events, true);
                $total_size = isset($events['event_list']['total_size']) ? $events['event_list']['total_size'] : 'No viene el parámetro';
                Log::info('total_size_Event_ITOC_VCloud: ' .  $total_size);
                if (isset($events['event_list']['event'])) {
                    $events = $events['event_list']['event'];
                    $ensure_mail = false;
                    $this->sendMail($events);
                }
            } catch (ConnectException $e1) {
                $ensure_mail = true;
                Log::info('Error ConnectException Message (Notifications_EventITOC_VCloud): ' . $e1->getMessage());
            } catch (RequestException $e2) {
                $ensure_mail = true;
                Log::info('Error RequestException Message: (Notifications_EventITOC_VCloud)' . $e2->getMessage());
            } catch (ClientException $e3) {
                $ensure_mail = true;
                Log::info('Error ClientException Message: (Notifications_EventITOC_VCloud)' . $e3->getMessage());
            }
        }
    }

    /**
     * @param $events
     * @param $filter
     * @return void
     * @throws GuzzleException
     * API envio de correos
     */
    public function sendMail($events)
    {
        $body = $this->createTemplate($events);
        try {
            $client = new Client();
            $res = $client->request('POST', 'http://172.20.45.172/notificacion/mail/avanzado', ['form_params' => [
                'subject' => 'Check list diario: Eventos con event_age > 12 hrs de ITOC_VCloud',
                'cuenta' => 'notificacion.itoc@triara.com',
                'tag' => 'Notificaciones ITOC_VCloud',
                'contactos' => explode(',', 'DIAGNOSTICOS.ITOC@triara.com, julio.garcia@triara.com, cesar.gutierrez@triara.com'),
                /* 'contactos' => explode(',', 'notificaciones_eventos@yopmail.com'), */
                'contactos_cc' =>  explode(',', 'alberto.garcia@triara.com'),
                'body' =>  $body
            ]]);
            Log::info('(Notifications_EventITOC_VCloud): ' . $res->getBody()->getContents());
        } catch (ConnectException $e) {
            Log::info('Error ConnectException Message: (Notifications_EventITOC_VCloud)' . $e->getMessage());
        } catch (RequestException $er) {
            Log::info('Error RequestException Message: (Notifications_EventITOC_VCloud)' . $er->getMessage());
        } catch (ClientException $err) {
            Log::info('Error ClientException Message: (Notifications_EventITOC_VCloud)' . $err->getResponse());
        }
    }

    /**
     * @param $events
     * @return string
     * Se genera la plantilla html
     */
    public function createTemplate($events)
    {
        $template_part1 = <<<EOT
        <html>
            <head>
                <meta Content-type="text/html" charset="utf-8">
            </head>
            <body>
                <div>
                    <div>
                        <img style="float: left; width: 538px; height: 119px;" src="http://200.57.138.134/ITOC/images/bannerItoc.jpg" />
                    </div>
                    <table width="100%">
                        <thead>
                            <tr>
                                <th style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: center; width: 20%;">ID</td>
                                <th style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: center; width: 16%;">Application</td>
                                <th style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: center; width: 12%;">State</td>
                                <th style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: center; width: 12%;">Event Age</td>
                                <th style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: center; width: 18%;">Time Received</td>
                                <th style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: center; width: 22%;">Title</td>
                            </tr>
                        </thead>
                    <tbody>
        EOT;
        $count = 0;
        $html_lleno_parte2 = '';
        if (count($events, COUNT_RECURSIVE) < 300) {
            $html_lleno_parte2 = $this->html_lleno_parte2($events, $html_lleno_parte2, $count);
        } else {
            foreach ($events as $value) {
                $count++;
                $html_lleno_parte2 = $this->html_lleno_parte2($value, $html_lleno_parte2, $count);
            }
        }
        $template_part3 = <<<EOT
                        </tbody>
                    </table>
                </div>
            </body>
        </html>
        EOT;
        return $template_part1 . $html_lleno_parte2 . $template_part3;
    }

    /**
     * @param $event
     * @return string
     * Se genera la plantilla html_lleno_parte2
     */
    public function html_lleno_parte2($event, $html_lleno_parte2, $count)
    {
        $id_event = isset($event['id']) ? $event['id'] : 'id';
        $application = isset($event['application']) ? $event['application'] : 'application';
        $state = isset($event['state']) ? $event['state'] : 'state';
        $time_received_label = isset($event['time_received_label']) ? $event['time_received_label'] : 'time_received_label';
        $title = isset($event['title']) ? $event['title'] : 'title';
        $time_created =  $event['time_created'];
        $api_date =  $this->apiDate();
        $event_age = $this->getDatetime($time_created, $api_date);
        if (($count % 2) == 0) {
            $odd = <<<EOT
                        <tr style="background-color: #00FFFF">
                            <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000080; text-align: left; width: 20%;">$id_event</td>
                            <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 16%;">$application</td>
                            <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 12%;">$state</td>
                            <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 12%;">$event_age</td>
                            <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 18%;">$time_received_label</td>
                            <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 22%;">$title</td>
                        </tr>
                        EOT;
            $html_lleno_parte2 = $html_lleno_parte2 . $odd;
        } else {
            $even = <<<EOT
                        <tr style="background-color: #FFFFFF">
                            <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 20%;">$id_event</td>
                            <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 16%;">$application</td>
                            <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 12%;">$state</td>
                            <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 12%;">$event_age</td>
                            <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 18%;">$time_received_label</td>
                            <td style="font-family: calibri, helvetica, arial, sans-serif; font-size: 1em; color: #000000; text-align: left; width: 22%;">$title</td>
                        </tr>
                        EOT;
            $html_lleno_parte2 = $html_lleno_parte2 . $even;
        }
        return  $html_lleno_parte2;
    }

    /** 
     * Subtract 12 hrs
     * 
     * @return String  $sub_time
     */
    public function subtract12Hrs()
    {
        $sub_time = Carbon::now(new \DateTimeZone('America/Mexico_City'))->sub(new \DateInterval('PT12H'))->format('Y-m-d\TH:i:s.vP');
        return $sub_time;
    }

    /** 
     * Get time
     * 
     * @return DateTime $res
     */
    public function apiDate()
    {
        $res = new \DateTime(Carbon::now(new \DateTimeZone('America/Mexico_City'))->format('Y-m-d\TH:i:s.vP'));
        return $res;
    }

    public function getDatetime($created, $api_data)
    {
        $event = new \DateTime($created);
        $interval = $event->diff($api_data);
        return ($interval->format('%a')) ? $interval->format('%ad ') . $interval->format('%hh') : $interval->format('%hh ') . $interval->format('%imin');
    }
}
