<?php

namespace App\Jobs\notifications;

use App\CustomAttributes;
use App\Events;
use App\Attribute;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CustomAttributeEvents implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Segenera la estructura para obtener los custom attributes
        $this->structureCustomAttribute();
    }

    /**
     * @return CustomAttributes[]|\Illuminate\Database\Eloquent\Collection
     * Se obtinen los eventos para obtener los custom attributes.
     * Se crea una nueva tabla para guardar los custom attributes de todos los eventos.
     */
    public function structureCustomAttribute()
    {
        $event = Events::whereNull('notification_process')->get();
        if (!empty($event->toArray())) {
            $attributes = array();
            foreach ($event as $val) {
                $customs = array();
                $rowsCA = array();
                $str = $val->custom_attribute_list;
                $custom_attribute = (explode(" ___ ", $str));
                foreach ($custom_attribute as $custom_att) {
                    $rowsCA = (explode(" ::: ", $custom_att));
                    array_push($customs, $rowsCA);
                }
                $data = $customs;
                $attributes[] = $this->getCustomAttributeList($val, $data);
                if (isset($val->id_event)) {
                    $id_obtained = $val->id_event;
                    Events::where('id_event', $id_obtained)->update(['notification_process' => 'processed']);
                }
            }
            //Obtener los atributos con categoria Custom_Attributes de la tabla attributes 
            $customAttribute_attribute = Attribute::where('category', '=', "custom_attribute")->pluck('label');
            //Obtener el titulo de las columnas de la tabla temp_custom_attribute
            $columns_temp_custom_attribute = Schema::getColumnListing('temp_custom_attribute');
            //Validar si hay cambios en los en los atributos custom_attributes para actualizar la tabla temp_custom_attribute
            $difference_result = array_udiff($customAttribute_attribute->toArray(), $columns_temp_custom_attribute, 'strcasecmp');
            if (empty($difference_result)) {
                //Inserta o actualiza los custom attributes
                foreach ($attributes as $attribute) {
                    CustomAttributes::updateOrCreate(
                        ['id_event' => $attribute['id_event'] ?? null],
                        $attribute
                    );
                }
            } else {
                //Crea columnas en la tabla con sus nuevos campos
                Schema::table('temp_custom_attribute', function (Blueprint $table) {
                    $customAttribute_attribute = Attribute::where('category', '=', "custom_attribute")->pluck('label');
                    $columns_temp_custom_attribute = Schema::getColumnListing('temp_custom_attribute');
                    $difference_result = array_udiff($customAttribute_attribute->toArray(), $columns_temp_custom_attribute, 'strcasecmp');
                    foreach ($difference_result as  $tittle) {
                        $table->text($tittle)->nullable();
                    }
                });
                foreach ($attributes as $attribute) {
                    CustomAttributes::updateOrCreate(
                        ['id_event' => $attribute['id_event'] ?? null],
                        $attribute
                    );
                }
            }
        } else {
        }
    }

    /**
     * @return array
     * Se obtienen los custom attributes y se retornan en un arreglo.
     */
    public function getCustomAttributeList($event, $data)
    {
        $attributes = array();
        $custom = DB::table('attributes')->where('category', 'custom_attribute')->get();
        $custom = json_decode($custom, true);
        if (is_array($data) || is_object($data)) {
            // If yes, then foreach() will iterate over it.
            foreach ($data as $val) {
                foreach ($custom as $item) {
                    if (isset($val[0])) {
                        if ($val[0] == $item['label']) {
                            $attributes["id_event"] = $event->id_event;
                            $attributes[$val[0]] = $val[1];
                        }
                    }
                }
            }
        }
        return $attributes;
    }
}
