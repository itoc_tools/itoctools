<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FillAWSOrganizations implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	private $base_url = 'https://172.20.45.129:8080/';
	private $guzzleHttp_client = null;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct() { }

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle() {

		$this->guzzleHttp_client = new Client(['http_errors' => false, 'verify' => false]);

		$organizationsList = $this->getOrganizations();

		Log::info('EVENTO AWS Autofill | Se contabilizan ' . count($organizationsList) . ' organizaciones asociadas a clientes');

		if(is_array($organizationsList) && count($organizationsList) > 0) {

			DB::table('aws_organizations')->truncate();

			for($i = 0; $i < count($organizationsList); $i++) {
				DB::table('aws_organizations')->insert($organizationsList[$i]);
			}

		}

	}

	private function getOrganizations() {

		$users = $this->listUsers()['data'];

		Log::info('EVENTO AWS Autofill | Se contabilizan ' . count($users) . ' usuarios');
		//Log::info('EVENTO AWS Autofill | ' . json_encode($users));

		$organizations_list = [];
		for($i = 0; $i < count($users); $i++) {
			$c_organization = [];

			$user_idAWS = $users[$i]['Idaws'];
			$user_name = $users[$i]['Usuario'];

			$c_organization['user'] = $user_name;
			$c_organization['idaws_user'] = $user_idAWS;

			$organizationsList = $this->getAccountsUserAWSId($user_idAWS)['data'];
			Log::info('EVENTO AWS Autofill | Se contabilizan ' . count($organizationsList) . ' organizaciones en total para el usuario ' . $user_name);
			Log::info('EVENTO AWS Autofill | ' . json_encode($organizationsList));

			for($j = 0; $j < count($organizationsList); $j++) {

				$organization_id = $organizationsList[$j]['Id'];
				$organization_name = $organizationsList[$j]['Name'];
				$organization_email = $organizationsList[$j]['Email'];
				$organization_status = $organizationsList[$j]['Status'];

				$c_organization['idaws_organization'] = $organization_id;
				$c_organization['name'] = $organization_name;
				$c_organization['email'] = $organization_email;
				$c_organization['status'] = $organization_status;

				$customer = $this->findCustomersByIdAWS($organization_id)['data'];
				if(is_array($customer) && count($customer) > 0) {
					$client_name = $customer['Nombrecliente'];
					$client_accountname = $customer['Nombrecuenta'];
					$client_landingzone = $customer['Landingzone'];

					$c_organization['clientname'] = $client_name;
					$c_organization['accountname'] = $client_accountname;
					$c_organization['landingzone'] = $client_landingzone;

					array_push($organizations_list, $c_organization);
				}

			}
		}

		return $organizations_list;
	}

	private function listUsers() {
		$httpGetUsersURI = 'aws/dataBase/getUsers';
		$httpGetUsersURL = $this->base_url . $httpGetUsersURI;
		$httpGetUserRequestType = 'POST';
		$httpParams = [
			'json' => [
				'Credential' => [
					'Accountid' => ''
				]
			]
		];
		$usersList = [
			'statusCode' => 'ok',
			'statusMessage' => 'Ningún mensaje',
			'data' => []
		];

		// Se obtiene el listado de los usuarios
		$response = $this->guzzleHttp_client->request($httpGetUserRequestType, $httpGetUsersURL, $httpParams);

		// Se valida el código de respuesta
		if($response->getStatusCode() === 200) {

			$contentResponse = $response->getBody()->getContents();
			$contentResponse = json_decode($contentResponse, true);

			// Valida que se tenga exista llave User
			if( array_key_exists('User', $contentResponse) ) {

				$usersList['statusCode'] = 'ok';
				$usersList['statusMessage'] = 'Se encuentran ' . count($contentResponse['User']) . ' usuarios';
				$usersList['data'] = $contentResponse['User'];

			}

			// En caso de que no tenga campo User
			else {

				$usersList['statusCode'] = 'fail';
				$usersList['statusMessage'] = 'Error al consultar (' . $httpGetUsersURI . '): No se encontró campo User.';
				Log::warning('No se encontró campo User. Consulta ' . $httpGetUserRequestType . ' ' . $httpGetUsersURL . ' ' . json_encode($httpParams));

			}

		}

		// En caso de que el código sea distinto a 200
		else {

			$usersList['statusCode'] = 'fail';
			$usersList['statusMessage'] = 'Error al consultar (' . $httpGetUsersURI . '): Código de error HTTP ' . $response->getStatusCode();
			Log::warning('Error ' . $response->getStatusCode() . ' Consulta ' . $httpGetUserRequestType . ' ' . $httpGetUsersURL . ' ' . json_encode($httpParams));

		}

		return $usersList;
	}

	private function getAccountsUserAWSId($AWSid) {
		$httpGetAccountURI = 'aws/agent/getAccount';
		$httpGetAccountURL = $this->base_url . $httpGetAccountURI;
		$httpGetAccountRequestType = 'POST';
		$accountsList = [
			'statusCode' => 'ok',
			'statusMessage' => 'Ningún mensaje',
			'data' => []
		];

		if(strlen($AWSid) > 0) {

			$httpParams = [
				'json' => [
					'Credential' => [
						'TypeSession' => 1,
						'Accountid' => $AWSid
					],
					'TypeInstance' => 'organizations'
				]
			];

			$response = $this->guzzleHttp_client->request($httpGetAccountRequestType, $httpGetAccountURL, $httpParams);

			// Se valida el código de respuesta
			if($response->getStatusCode() === 200) {

				$contentResponse = $response->getBody()->getContents();
				$contentResponse = json_decode($contentResponse, true);

				if(array_key_exists('Accounts', $contentResponse)) {

					$accountsList['statusCode'] = 'ok';
					$accountsList['statusMessage'] = 'Se encuentra ' . count($contentResponse['Accounts']) . ' cuentas';
					$accountsList['data'] = $contentResponse['Accounts'];

				}
				else {

					$accountsList['statusCode'] = 'fail';
					$accountsList['statusMessage'] = 'Error al consultar (' . $httpGetAccountURI . '): No se encontró campo Accounts';

					Log::warning('Error al consultar (' . $httpGetAccountURI . '): No se encontró campo Accounts');
				}

			}
			else {

				$accountsList['statusCode'] = 'fail';
				$accountsList['statusMessage'] = 'Error al consultar (' . $httpGetAccountURI . '): Código de error HTTP ' . $response->getStatusCode();

				Log::warning('Error ' . $response->getStatusCode() . ' Consulta ' . $httpGetAccountRequestType . ' ' . $httpGetAccountURL . ' ' . json_encode($httpParams));

			}

		}
		else {

			$accountsList['statusCode'] = 'fail';
			$accountsList['statusMessage'] = 'Id de cuenta no es valida ' . $AWSid;

			Log::warning('Id de cuenta no es valida ' . $AWSid);

		}

		return $accountsList;
	}

	private function findCustomersByIdAWS($AWSid) {
		$httpGetCustomersURI = 'aws/dataBase/getCustomers';
		$httpGetCustomersURL = $this->base_url . $httpGetCustomersURI;
		$httpGetCustomerRequestType = 'POST';
		$httpParams = [
			'json' => [
				'Credential' => [
					'Accountid' => $AWSid
				]
			]
		];
		$customersList = [
			'statusCode' => 'ok',
			'statusMessage' => 'Ningún mensaje',
			'data' => []
		];

		// Se obtiene lista de clientes
		$response = $this->guzzleHttp_client->request($httpGetCustomerRequestType, $httpGetCustomersURL, $httpParams);

		// Se valida el código de respuesta
		if($response->getStatusCode() === 200) {

			$contentResponse = $response->getBody()->getContents();
			$contentResponse = json_decode($contentResponse, true);

			// Valida que se tenga exista llave Customers
			if( array_key_exists('Customers', $contentResponse) ) {

				$customersList['statusCode'] = 'ok';
				$customersList['statusMessage'] = 'Se encontraron ' . count($contentResponse['Customers']) . ' clientes';
				if(is_array($contentResponse['Customers']) && count($contentResponse['Customers']) > 0) {
					$customersList['data'] = $contentResponse['Customers'][0];
				}

			}

			// En caso de que no exista llave Customers
			else {

				$usersList['statusCode'] = 'fail';
				$usersList['statusMessage'] = 'Error al consultar (' . $httpGetCustomersURI . '): No se encontró campo Customers.';
				Log::warning('No se encontró campo Customers. Consulta ' . $httpGetCustomerRequestType . ' ' . $httpGetCustomersURL . ' ' . json_encode($httpParams));

			}

		}

		// En caso de que el código sea distinto a 200
		else {

			$customersList['statusCode'] = 'fail';
			$customersList['statusMessage'] = 'Error al consultar (' . $httpGetCustomersURI . '): Código de error HTTP ' . $response->getStatusCode();
			Log::warning('Error ' . $response->getStatusCode() . ' Consulta ' . $httpGetCustomerRequestType . ' ' . $httpGetCustomersURL . ' ' . json_encode($httpParams));

		}

		return $customersList;
	}

}
