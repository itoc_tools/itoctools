<?php

namespace App\Repositories;

class Events extends GuzzleHttpRequest
{
    public function all()
    {
        return $this->get('event_list?alt=json&page_size=250');
    }

    public function find($id)
    {
        return $this->get("event_list/{$id}?alt=json");
    }
}
