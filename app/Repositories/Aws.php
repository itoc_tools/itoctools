<?php

namespace App\Repositories;

class Aws extends GuzzleHttpRequest
{
    public function all()
    {
        return $this->get('instances');
    }
}
