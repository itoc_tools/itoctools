<?php

namespace App\Repositories;

use GuzzleHttp\Client;

class GuzzleHttpRequest
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function get($url)
    {
        $response = $this->client->request('GET', $url, [
            'auth' => ['OPC.Zenoss', '+0ruge5.X3m137-']
        ]);

        return $response->getBody()->getContents();
    }
}
