<?php

namespace App\Console;

use App\Filter;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\ServiceManagerInbursaEventsCron;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        /* ServiceManagerInbursaEventsCron::class, */];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        // $schedule->command('inbursaServiceManager:cron')->everyFiveMinutes();

        /*  $schedule->command('inbursaServiceManager:cron')->everyFiveMinutes();
        $schedule->command('bafarEmailEvents:cron')->everyFiveMinutes(); */
        //$schedule->command('notification:eventitocvloud')->timezone('America/Mexico_City')->dailyAt('9:00');
        //$schedule->command('notification:eventitocvloud')->timezone('America/Mexico_City')->dailyAt('9:30');
        //$schedule->command('aws:organizations')->timezone('America/Mexico_City')->twiceDaily(0, 12);

        // Activate inbursa alerts
        /*
        $schedule->call(function () {
            $filter = Filter::find(39);
            $filter->state = 1;
            $filter->save();
            Log::info('Inbursa Alerts Activated');
        })->weekdays()->at('9:00');
        */

        // Disable inbursa alerts
        /* $schedule->call(function () {
            $filter = Filter::find(39);
            $filter->state = 0;
            $filter->save();
            Log::info('Inbursa Alerts Disabled');
        })->weekdays()->at('21:00'); */
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

    /**
     * @param \Spatie\ShortSchedule\ShortSchedule $shortSchedule
     */
    protected function shortSchedule(\Spatie\ShortSchedule\ShortSchedule $shortSchedule)
    {
        // this artisan command will run every second
        $shortSchedule->command('main:geteventsmain')->everySeconds(15);
        $shortSchedule->command('delete:events')->everySeconds(10);
        $shortSchedule->command('depurate:tables')->everySeconds(200);
        $shortSchedule->command('get:eventsFiltersExceptionIMSD')->everySeconds(20);
        $shortSchedule->command('get:eventsFilters')->everySeconds(25);
        $shortSchedule->command('get:eventsFiltersIngHerramientas')->everySeconds(50);
        $shortSchedule->command('attribute:events')->everySeconds(70);
        /* 
        $shortSchedule->command('notification:events')->everySeconds(35);*/
        // Jobs de Alexis
        /* $shortSchedule->command('autoclose:events')->everySeconds(60);
        $shortSchedule->command('repeat:notifications')->everySeconds(60); */
    }
}
