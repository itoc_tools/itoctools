<?php

namespace App\Console\Commands;

use App\Jobs\AutoCloseEvent;
use Illuminate\Console\Command;

class AutoCloseEvents extends Command 
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autoclose:events';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close events automatically';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {
        $autoclose = new AutoCloseEvent();

        dispatch($autoclose)->onQueue('autoclose');
    }
}