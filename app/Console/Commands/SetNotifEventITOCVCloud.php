<?php

namespace App\Console\Commands;

use App\Jobs\notifications\Notifications_EventITOC_VCloud;
use Illuminate\Console\Command;

class SetNotifEventITOCVCloud extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:eventitocvloud';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $notifications_EventITOC_VCloud = new Notifications_EventITOC_VCloud();
        dispatch($notifications_EventITOC_VCloud)->onQueue('eventitocvloud');
    }
}
