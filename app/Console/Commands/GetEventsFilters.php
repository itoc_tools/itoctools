<?php

namespace App\Console\Commands;

use App\Jobs\filters\EventTelmexSlim;
use App\Jobs\filters\EventAbiertosExcepcion;
use App\Jobs\filters\EventInfraHardware;
use App\Jobs\filters\EventInfraRed;
use App\Jobs\filters\EventInfraSeguridad;
use App\Jobs\filters\EventZenossQro;
use App\Jobs\filters\EventZenossMtyItoc;
use App\Jobs\filters\EventZenossSRE;
use App\Jobs\filters\EventZenossGBMV;
use App\Jobs\filters\EventAlertsCriticasChat;
use App\Jobs\filters\EventITOC_VCloud;
use App\Jobs\filters\EventExchangeCriticas;
use App\Jobs\filters\EventPlataformasClientes;
use App\Jobs\filters\EventPingReinicios;
use App\Jobs\filters\EventExcepcionSinAtender;
use App\Jobs\filters\EventSitescope;
use App\Jobs\filters\EventNagiosURLsZabbix;
use App\Jobs\filters\EventAWSNewRelic;
use App\Jobs\filters\EventEventosdeRFC;
use App\Jobs\filters\EventZenossCloudVIP;
use App\Jobs\filters\EventEventosResueltos;
use App\Jobs\filters\EventClientesSAP;
use App\Jobs\filters\EventAlertasAvaya;
use App\Jobs\filters\EventIntegracionesITOCTOOLs;
use App\Jobs\filters\EventAlertasNA;
use App\Jobs\filters\EventEventosDummyPrueba;
use App\Jobs\filters\EventPoleos;
use Illuminate\Console\Command;

class GetEventsFilters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:eventsFilters';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se obtienen todos los filtros del api OMI';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dispatch(new EventTelmexSlim())->onQueue('telmexSlim');
        dispatch(new EventAbiertosExcepcion())->onQueue('abiertosExcepcion');
        dispatch(new EventInfraHardware())->onQueue('infraHardware');
        dispatch(new EventInfraRed())->onQueue('infraRed');
        dispatch(new EventInfraSeguridad())->onQueue('infraSeguridad');
        dispatch(new EventZenossQro())->onQueue('zenossQro');
        dispatch(new EventZenossMtyItoc())->onQueue('zenossMtyItoc');
        dispatch(new EventZenossSRE())->onQueue('zenossSRE');
        dispatch(new EventZenossGBMV())->onQueue('zenossGBMV');
        dispatch(new EventAlertsCriticasChat())->onQueue('alertsCriticasChat');
        dispatch(new EventITOC_VCloud())->onQueue('ITOC_VCloud');
        dispatch(new EventExchangeCriticas())->onQueue('exchangeCriticas');

        dispatch(new EventPlataformasClientes())->onQueue('plataformasClientes');
        dispatch(new EventPingReinicios())->onQueue('pingReinicios');
        dispatch(new EventExcepcionSinAtender())->onQueue('excepcionSinAtender');
        dispatch(new EventSitescope())->onQueue('sitescope');
        dispatch(new EventNagiosURLsZabbix())->onQueue('nagiosURLsZabbix');
        dispatch(new EventAWSNewRelic())->onQueue('aWSNewRelic');
        dispatch(new EventEventosdeRFC())->onQueue('eventosdeRFC');
        dispatch(new EventZenossCloudVIP())->onQueue('zenossCloudVIP');
        dispatch(new EventEventosResueltos())->onQueue('eventosResueltos');
        dispatch(new EventClientesSAP())->onQueue('clientesSAP');
        dispatch(new EventAlertasAvaya())->onQueue('alertasAvaya');
        dispatch(new EventIntegracionesITOCTOOLs())->onQueue('integracionesITOCTOOLs');
        dispatch(new EventAlertasNA())->onQueue('alertasNA');
        dispatch(new EventEventosDummyPrueba())->onQueue('eventosDummyPrueba');
        dispatch(new EventPoleos())->onQueue('poleos');
    }
}
