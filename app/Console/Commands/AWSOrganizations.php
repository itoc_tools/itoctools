<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Jobs\FillAWSOrganizations;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class AWSOrganizations extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'aws:organizations';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Obtiene la información de las organizaciones asociadas a los usuarios de AWS y llena la tabla aws_organizations con esta información.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle() {
		Log::info('EVENTO AWS Autofill | Inicia comando');
		$job = new FillAWSOrganizations();

		dispatch($job)->onQueue('AWSOrganizations');
	}
}
