<?php

namespace App\Console\Commands;

use App\Jobs\notifications\CustomAttributeEvents;
use Illuminate\Console\Command;

class SetCustomAttributeEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attribute:events';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $customAttributes = new CustomAttributeEvents();
        dispatch($customAttributes)->onQueue('attribute');
    }
}
