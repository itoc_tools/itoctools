<?php

namespace App\Console\Commands;

use App\Jobs\filters\EventIngHerramientas;
use Illuminate\Console\Command;

class GetEventsFiltersIngHerramientas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:eventsFiltersIngHerramientas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se obtienen todos los filtros del api OMI IngHerramientas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dispatch(new EventIngHerramientas())->onQueue('ingHerramientas');
    }
}
