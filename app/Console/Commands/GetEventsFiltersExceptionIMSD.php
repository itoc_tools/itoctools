<?php

namespace App\Console\Commands;

use App\Jobs\filters\EventExcepcionSinIMoSD;
use Illuminate\Console\Command;

class GetEventsFiltersExceptionIMSD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:eventsFiltersExceptionIMSD';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se obtienen todos los filtros del api OMI exception SD IM';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dispatch(new EventExcepcionSinIMoSD())->onQueue('excepcionSinIMoSD');
    }
}
