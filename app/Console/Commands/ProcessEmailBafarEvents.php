<?php

namespace App\Console\Commands;

use App\Classes\BafarEvents;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ProcessEmailBafarEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bafarEmailEvents:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando el cual permite consultar los correos del cliente bafar y generar eventos en BSM OMI';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    // funcion que permite realizar una peticion a un rest service, la cual obtiene una lista de correos en formato json
    // del cliente Bafar
    public function handle()
    {
        $bafarEvents = new BafarEvents();
        $emailList = $bafarEvents->getEmailListFromEndpointRest();

        if(count($emailList) > 0){
            Log::info('Se procede a ejecutar el cron con los '.count($emailList).' correos localizados...');
            $emailList = $bafarEvents->generateEventsFromEmailList($emailList);
            $emailList = $bafarEvents->sendListEmailJsonBafarIntegration($emailList);
        }else{
            Log::info('No se localizaron correos a procesar, se omite ejecucion del cron...');
        }
    }
}
