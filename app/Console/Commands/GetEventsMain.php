<?php

namespace App\Console\Commands;

use App\Jobs\byDB\GetsEvents;
use Illuminate\Console\Command;

class GetEventsMain extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'main:geteventsmain';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $getsEvents = new GetsEvents();
        dispatch($getsEvents)->onQueue('geteventsmain');
    }
}
