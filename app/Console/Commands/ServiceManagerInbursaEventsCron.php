<?php

namespace App\Console\Commands;

use App\Http\Controllers\ServiceManagerInbursa\EmailCheckerController;
use App\Http\Controllers\ServiceManagerInbursa\OmiEventsController;
use App\TrackEvent;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ServiceManagerInbursaEventsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbursaServiceManager:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron el cual verifica el estatus de los eventos en el Service Manager de ' .
        'Inbursa y lo actualiza dentro de la herramienta OMI Itoc';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $emailController = new EmailCheckerController();
        $omiController = new OmiEventsController();
        $emailEventsList = [];

        try{
            $emailEventsList = $emailController->getListEmailEvents();
            $emailEventsList = json_decode(json_encode($emailEventsList), true);
        }catch(ServerException $exception){
            Log::error('Sucedio un error al tratar de obtener la lista de eventos en el service '.
            'manager Inbursa por medio de la Api Rest: '.$exception->getMessage());
        }

        if (count($emailEventsList) > 0) {

            Log::info('Se inicializa la verificacion de eventos Service Manager Inbursa');
            Log::info('Numero de eventos/tickets detectados dentro del SM: ' . count($emailEventsList));

            foreach ($emailEventsList as $email) {

                $generatedTicket = $email['Body']['Ticket'];
                $statusTicket = $email['Body']['Estado'];
                $solutionTicket = $email['Body']['Resolución'];
                $omiEventId = $email['Body']['Evid_omi'];
                $dateEvent = $email['Body']['Fecha'];

                Log::info('Datos recopilados del ticket');
                Log::info('Ticket: ' . $generatedTicket);
                Log::info('Estatus del ticket: ' . $statusTicket);
                Log::info('Resolucion del ticket: ' . $solutionTicket);
                Log::info('Id evento OMI: ' . $omiEventId);
                Log::info('Fecha del ticket: ' . $dateEvent);

                try {
                    // se agrega atributo MS
                    $resp = $omiController->addCustomAttributeEvent('MS', $generatedTicket, $omiEventId);

                    // se agrega atributo Estatus MS
                    $resp = $omiController->addCustomAttributeEvent('Estatus SM', $statusTicket, $omiEventId);

                    // se agrega atributo Solucion SM
                    $resp = $omiController->addCustomAttributeEvent('Solución SM', $solutionTicket, $omiEventId);

                    // se agrega anotacion de momento del registro de fecha y hora
                    $resp = $omiController->addAnnotationEvent($dateEvent, $omiEventId);

                    // se realizan operacion en la BD
                    $trackedEvent = new TrackEvent();
                    $dbStatusTicketIsClosed = false;
                    $resultDbAction = false;

                    // se verifica si el ticket esta en estatus cerrado o abierto
                    if (str_contains(strtolower($statusTicket), 'cerrado')) {
                        $dbStatusTicketIsClosed = true;
                    }

                    // si estatus del ticket es abierto, se actualizan las columnas id, updated_at.
                    // En caso de que sea cerrado, se actualizan las columnas closed, closed_at
                    if (!$dbStatusTicketIsClosed) {
                        $resultDbAction = $trackedEvent->updateTrackEvent([
                            'id' => $generatedTicket,
                            'event_id' => $omiEventId,
                            'updated_at' => $dateEvent
                        ]);

                        Log::info(sprintf(
                            'Actualizacion de ticket abierto en BD: %s',
                            ($resultDbAction ? 'exitosa' : 'fallida')
                        ));
                    } else {
                        $resultDbAction = $trackedEvent->updateTrackEvent([
                            'event_id' => $omiEventId,
                            'closed' => $dbStatusTicketIsClosed,
                            'closed_at' => $dateEvent
                        ]);

                        Log::info(sprintf(
                            'Actualizacion de ticket cerrado en BD: %s',
                            ($resultDbAction ? 'exitosa' : 'fallida')
                        ));
                    }
                } catch (ServerException $e) {
                    Log::error(sprintf('Sucedio un error al intentar actualizar el evento del OMI con el id' .
                        ' %s. Message Error: %s', $omiEventId, $e->getMessage()));
                }
            }
        }
    }
}
