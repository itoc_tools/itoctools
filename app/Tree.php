<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Tree extends Model
{
    protected $guarded= [];
    /**
     * Obtiene el propio modelo treeable.
     */
    public function treeable()
    {
        return $this->morphTo();
    }

    /**
     * Obtiene el identifcador de un árbol de acuerdo a su clase.
     * @param  App\DeviceTree $class
     * @return int $id
     */
    public static function getIdByClassName($class)
    {
        $params = [
            ['user_id', Auth::user()->id],
            ['treeable_type', $class]
        ];

        $id = self::where($params)->first()->treeable_id;

        return $id;
    }
}

