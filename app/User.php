<?php

namespace App;

use Hash;
use Illuminate\Notifications\Notifiable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 */
class User extends Authenticatable 
{
    use Notifiable;
    use HasRolesAndAbilities;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Obtiene los documentos propiedad de un usuario.
    */
    public function documents() 
    {
        return $this->hasMany(Document::class);
    }

    /**
     * Hash password
     * @param $input
    */
    public function setPasswordAttribute($input) 
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    /**
     * TODO. Documentar.
    */
    public function role() 
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    /**
     * Obtiene las preferencias por m�dulo del sistema.
    */
    public function preferences() 
    {
        return $this->hasMany(UserPreference::class);
    }
}
