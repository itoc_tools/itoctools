<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    use HasFactory;

    protected $table = 'events';
    protected $guarded = [];

    protected $fillable =
    [
        'id_event',
        'annotation_list',
        'application',
        'assigned_group',
        'assigned_user',
        'category',
        'ci_resolution_info',
        'control_transferred',
        'custom_attribute_list',
        'description',
        'drilldown_url',
        'duplicate_count',
        'history_line_list_ref',
        'instruction_available',
        'log_only',
        'originating_server',
        'priority',
        'received_as_notify',
        'received_on_ci_downtime',
        'relationships_included',
        'self',
        'sequence_number',
        'severity',
        'skip_duplicate_suppression',
        'state',
        'sub_category',
        'symptom_list',
        'time_changed',
        'time_changed_label',
        'time_created',
        'time_created_label',
        'time_received',
        'time_received_label',
        'title',
        'type',
        'version',
        'notified',
        'user_name',
        'object',
        'notification_process',
        'time_created_dt',
        'time_received_dt',
        'time_changed_dt',
        'original_data',
        'RELATED_CI_HINT'
    ];
}
