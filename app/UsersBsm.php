<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersBsm extends Model
{
    use HasFactory;

    protected $table = 'users_bsm';
    protected $guarded = [];

    protected $fillable = [
        'entity_id', 'us_bsm', 'pass_bsm'
    ];
}
