<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

// Co

class Integration extends Model 
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'method', 'endpoint', 'auth_type', 'auth_params', 'headers', 'body_type', 'body_params_type', 'body_params'];

    public function setAuthParamsAttribute($value) {
        $this->attributes['auth_params'] = json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    public function setHeadersAttribute($value) {
        $headers = array();

        foreach ($value as $array) {
            $pair = array_values($array);

            $headers[$pair[0]] = $pair[1];
        }

        $this->attributes['headers'] = json_encode($headers, JSON_UNESCAPED_UNICODE);
    }

    public function setBodyParamsAttribute($value) {
        if ($this->attributes['body_type'] === 'params') {
            $value = json_encode(array_merge($value), JSON_UNESCAPED_UNICODE);
        } else {
            $value = preg_replace('/\r|\n/', '', $value);
        }

        $this->attributes['body_params'] = $value;
    }
}