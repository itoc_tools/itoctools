<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventsFilters extends Model
{
    use HasFactory;

    protected $table = 'events_filters';
    protected $guarded = [];
}
