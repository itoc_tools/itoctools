<?php

use Illuminate\Support\Facades\Storage;

if (!function_exists('verify_image_profile')) {

    function verify_image_profile()
    {
        $nombre_imagen = '';
        $usuario = auth()->user();
        $url_public_imagen = url('/theme/assets/images/users/default_user_profile.png');

        if (!empty($usuario)) {
            $nombre_imagen = $usuario->path_image_profile;
        }

        // se verifica que exista la imagen, de lo contrario se devuelve la imagen por default
        if (Storage::disk('profile_images')->exists($nombre_imagen)) {
            $url_public_imagen = Storage::disk('profile_images')->url($nombre_imagen);
        }

        return $url_public_imagen;
    }
}
