<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    use HasFactory;

    protected $fillable =
        [
            'name',
            'description',
            'sql',
            'template',
            'cuenta',
            'tag',
            'to',
            'cc',
            'subject',
            'check_template',
            'state'
        ];
}
