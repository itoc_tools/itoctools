<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class TrackEvent extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $keyType = 'string';
    protected $table = 'track_events';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'id', 'event_id', 'monitoring_tool_id', 'monitoring_tool_closed',
        'closed', 'monitoring_tool_closed_at', 'closed_at', 'created_at', 'updated_at'
    ];


    public function saveTrackedEvent(array $data)
    {
        $result = false;

        // realiza el insert a la DB, en caso de que alguno de los datos no este en el formato correcto
        // se lanza un QueryException, se atrapa y se establece el resultado como fallido/false
        try {
            $this->event_id = $data['id_event'];
            
            if(isset($data['monitoring_tool_id'])){
                $this->monitoring_tool_id = $data['monitoring_tool_id'];
            }else{
                $this->monitoring_tool_id = 0;
            }

            $this->created_at = $data['created_at'];

            $result = $this->save($data);
        } catch (QueryException $exception) {
            Log::error('Sucedio un error al insertar el evento a la BD: ' .
                $exception->getMessage());

            $result = false;
        }

        return $result;
    }

    public function updateTrackEvent(array $data)
    {
        $result = false;

        // realiza el update a la DB, en caso de que alguno de los datos no este en el formato correcto
        // se lanza un QueryException, se atrapa y se establece el resultado como fallido/false
        try {
            $result = $this::where('event_id', '=', $data['event_id'])->update($data);
        } catch (QueryException $exception) {
            Log::error('Sucedio un error al actualizar el evento a la BD: ' .
                $exception->getMessage());

            $result = false;
        }

        return $result;
    }
}
