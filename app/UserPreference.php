<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User;

class UserPreference extends Model 
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'preferences'
    ];

    /**
     * Obtiene el módulo del sistema al que corresponde la preferencia.
    */
    public function systemModule() 
    {
        return $this->belongsTo(SystemModule::class);
    }

    /**
     * Obtiene el usuario al que pertenece la preferencia.
    */
    public function user() 
    {
        return $this->belongsTo(User::class);
    }
}